use super::common::{Device, SsdpData};
use super::*;
use std::net::{IpAddr, Ipv4Addr};

const fn build_config() -> Config {
    Config {
        multicast_ttl: 2,
        multicast_loop: false,
        bind_addr: IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
        response_ttl: Duration::from_secs(60),
        debug_notify: false,
        max_message_size: 1024,
        stop_retry_delay: Duration::from_secs(1),
    }
}

fn build_ssdp_data() -> SsdpData {
    SsdpData {
        device: Device {
            uuid: "uuid:5a53429f-318c-4d6c-a83a-acdfb2a670ed".to_owned(),
            name: "urn:schemas-upnp-org:device:MediaServer:1".to_owned(),
            services: vec![
                "urn:schemas-upnp-org:service:ContentDirectory:1".to_owned(),
                "urn:schemas-upnp-org:service:ConnectionManager:1".to_owned(),
            ],
        },
        location: "http://localhost:8080/root.xml".to_owned(),
        user_agent: "Podcast UPNP".to_owned(),
    }
}

fn build_message_processor() -> MessageProcessor {
    let config = build_config();
    let ssdp_data = build_ssdp_data();
    MessageProcessor { config, ssdp_data }
}

fn parse_example_request_header(src: &str) -> Header {
    let fixed_src = src.replace('\n', "\r\n");
    let mut cursour = Cursor::new(fixed_src);

    Header::read(&mut cursour).unwrap()
}

#[tokio::test]
async fn m_search_request_root_device() {
    let message_processor = build_message_processor();

    let request_header_bytes = include_str!("examples/m_search_root_device_valid.http");
    let request_header = parse_example_request_header(request_header_bytes);

    let result = message_processor
        .m_search_request(&request_header)
        .await
        .unwrap();

    assert_eq!(result.len(), 1);
}

#[tokio::test]
async fn m_search_request_media_server() {
    let message_processor = build_message_processor();

    let request_header_bytes = include_str!("examples/m_search_media_server_valid.http");

    let request_header = parse_example_request_header(request_header_bytes);

    let result = message_processor
        .m_search_request(&request_header)
        .await
        .unwrap();

    assert_eq!(result.len(), 1);
}

#[tokio::test]
async fn m_search_ssdp_all() {
    let message_processor = build_message_processor();

    let request_header_bytes = include_str!("examples/m_search_ssdp_all_valid.http");

    let request_header = parse_example_request_header(request_header_bytes);

    let result = message_processor
        .m_search_request(&request_header)
        .await
        .unwrap();

    assert_eq!(result.len(), 4);
}

#[tokio::test]
async fn m_search_uuid() {
    let message_processor = build_message_processor();

    let request_header_bytes = include_str!("examples/m_search_uuid_valid.http");

    let request_header = parse_example_request_header(request_header_bytes);

    let result = message_processor
        .m_search_request(&request_header)
        .await
        .unwrap();

    assert_eq!(result.len(), 1);
}

#[tokio::test]
async fn m_search_request_media_render_unexpected() {
    let message_processor = build_message_processor();

    let request_header_bytes = include_str!("examples/m_search_media_render_valid.http");

    let request_header = parse_example_request_header(request_header_bytes);

    println!("Request header {:?}", request_header);

    let result = message_processor
        .m_search_request(&request_header)
        .await
        .unwrap();

    assert_eq!(result.len(), 0);
}

use std::io::Cursor;
use std::net::{IpAddr, SocketAddr};
use std::sync::Arc;
use std::time::Duration;

use http_tiny::Header;
use http_tiny::HeaderFields;
use http_tiny::HeaderStartLine;
use thiserror::Error as ThisError;
use tokio::net::UdpSocket;

use super::common;
use super::errors;
use super::DataBuilder as SsdpDataBuilder;

#[cfg(test)]
mod tests;

#[derive(Debug, Clone)]
pub struct Config {
    /// SSDP listener bind address (IP).
    /// Currently only IPv4 supported.
    pub bind_addr: IpAddr,

    /// SSDP listener response TTL.
    /// Currently only IPv4 supported.
    pub response_ttl: Duration,

    /// SSDP listener UDP multicast TTL.
    pub multicast_ttl: u32,

    /// SSDP listener UDP multicast loop.
    pub multicast_loop: bool,

    /// Print received SSDP notify messages
    pub debug_notify: bool,

    /// SSDP listener max message size
    pub max_message_size: usize,

    /// SSDP listener stop retry delay
    pub stop_retry_delay: Duration,
}

#[derive(Debug, ThisError)]
pub enum Error {
    #[error("Bind SSDP listener error: {0}")]
    Bind(#[from] std::io::Error),

    #[error("SSDP error: {0}")]
    Ssdp(common::Error),
}

#[derive(Clone)]
pub struct Starter {
    config: Config,
    stop_notify: Arc<tokio::sync::Notify>,
    stopped_notify: Arc<tokio::sync::Notify>,
    ssdp_data_builder: Arc<dyn SsdpDataBuilder>,
}

impl Starter {
    pub fn new(config: Config, ssdp_data_builder: Arc<dyn SsdpDataBuilder>) -> Self {
        let stop_notify = Arc::new(tokio::sync::Notify::new());
        let stopped_notify = Arc::new(tokio::sync::Notify::new());

        Self {
            config,
            stop_notify,
            stopped_notify,
            ssdp_data_builder,
        }
    }

    pub async fn start(&self) -> Result<(), Error> {
        let ssdp_data = self.ssdp_data_builder.build().await.map_err(Error::Ssdp)?;
        let udp_socket = self.build_udp_socket().await?;

        let listener = Listener {
            ssdp_data,
            config: self.config.clone(),
            stop_notify: self.stop_notify.clone(),
        };

        let stopped_notify = self.stopped_notify.clone();
        tokio::spawn(async move {
            if let Err(err) = listener.handle_loop(udp_socket).await {
                error!("Handle loop error: {}", err);
            };

            stopped_notify.notify_one();
            info!("Listener task stopped");
        });

        Ok(())
    }

    async fn build_udp_socket(&self) -> Result<UdpSocket, Error> {
        use socket2::{Domain, Protocol, SockAddr, Socket, Type};

        let domain = match self.config.bind_addr {
            IpAddr::V4(_) => Domain::IPV4,
            IpAddr::V6(_) => Domain::IPV6,
        };

        let bind_addr = SockAddr::from(SocketAddr::new(self.config.bind_addr, common::SSDP_PORT));

        let socket = Socket::new(domain, Type::DGRAM, Some(Protocol::UDP))?;

        socket.set_reuse_address(true)?;
        socket.set_nonblocking(true)?;

        socket.set_multicast_loop_v4(self.config.multicast_loop)?;
        socket.set_multicast_ttl_v4(self.config.multicast_ttl)?;

        socket.bind(&bind_addr)?;

        let udp_socket = UdpSocket::from_std(socket.into())?;

        info!(
            "SSDP listener started on {addr} (TTL: {ttl}, loop: {loop})",
            addr = udp_socket.local_addr()?,
            ttl = udp_socket.multicast_ttl_v4()?,
            loop = udp_socket.multicast_loop_v4()?,
        );

        Ok(udp_socket)
    }

    pub async fn stop(&self) {
        debug!("Notify listener stop");
        self.stop_notify.notify_one();

        debug!("Wait listener stopped");
        self.stopped_notify.notified().await;

        debug!("Listener stopped");
    }
}

#[derive(Clone)]
pub struct Listener {
    config: Config,
    ssdp_data: common::SsdpData,
    stop_notify: Arc<tokio::sync::Notify>,
}

impl Listener {
    async fn handle_loop(&self, udp_socket: UdpSocket) -> common::Result<()> {
        self.join_multicast(&udp_socket)?;

        let udp_socket = Arc::new(udp_socket);
        info!("Start SSDP listener loop");
        loop {
            tokio::select! {
                result = self.handle_message(udp_socket.clone()) => {
                    if let Err(error) = result {
                        error!("Message error {}", error);
                    }
                },
                _ = self.stop_notify.notified() => break,
            }
        }
        info!("Stop SSDP listener loop");

        let udp_socket = self.wait_udp_socket(udp_socket).await;

        self.leave_multicast(&udp_socket)?;

        Ok(())
    }

    fn join_multicast(&self, udp_socket: &UdpSocket) -> common::Result<()> {
        match self.config.bind_addr {
            IpAddr::V4(local_addr) => {
                info!(
                    "Join multicast {} on {}",
                    common::SSDP_MULTICAST_V4_ADDR,
                    local_addr,
                );
                udp_socket.join_multicast_v4(common::SSDP_MULTICAST_V4_ADDR, local_addr)?;
            }

            IpAddr::V6(_local_addr) => unimplemented!(),
        };

        Ok(())
    }

    fn leave_multicast(&self, udp_socket: &UdpSocket) -> common::Result<()> {
        match self.config.bind_addr {
            IpAddr::V4(local_addr) => {
                info!(
                    "Leave multicast {} on {}",
                    common::SSDP_MULTICAST_V4_ADDR,
                    local_addr
                );
                udp_socket.leave_multicast_v4(common::SSDP_MULTICAST_V4_ADDR, local_addr)?;
            }

            IpAddr::V6(_local_addr) => unimplemented!(),
        };

        Ok(())
    }

    async fn wait_udp_socket(&self, udp_socket: Arc<UdpSocket>) -> UdpSocket {
        let mut udp_socket = udp_socket;

        loop {
            match Arc::try_unwrap(udp_socket) {
                Ok(udp_socket) => {
                    return udp_socket;
                }
                Err(udp_send_value) => {
                    udp_socket = udp_send_value;
                    warn!("Send half unwrap error");
                    tokio::time::sleep(self.config.stop_retry_delay).await;
                }
            }
        }
    }

    async fn handle_message(&self, udp_socket: Arc<UdpSocket>) -> common::Result<()> {
        let mut buf = vec![0; self.config.max_message_size];
        let (len, remote_addr) = udp_socket.recv_from(&mut buf).await?;
        debug!("Receive {} bytes from {}", len, &remote_addr);

        let message_processor = MessageProcessor {
            config: self.config.clone(),
            ssdp_data: self.ssdp_data.clone(),
        };

        let udp_socket = udp_socket.clone();

        tokio::spawn(async move {
            if let Err(err) = message_processor
                .process_message(buf, remote_addr, udp_socket)
                .await
            {
                error!("Process error: {}", err);
            }
        });

        Ok(())
    }
}

struct MessageProcessor {
    config: Config,
    ssdp_data: common::SsdpData,
}

impl MessageProcessor {
    async fn process_message(
        &self,
        buf: Vec<u8>,
        remote_addr: std::net::SocketAddr,
        udp_socket: Arc<UdpSocket>,
    ) -> common::Result<()> {
        let mut cursor = Cursor::new(&buf[..]);
        let request_header = Header::read(&mut cursor)?;

        Self::print_trace(&buf[..])?;

        let responses = self
            .route_message(
                request_header.start_line().request_method().as_ref(),
                &request_header,
            )
            .await?;

        for (response, delay) in responses {
            let udp_send = udp_socket.clone();
            tokio::spawn(async move {
                tokio::time::sleep(delay).await;

                if let Err(err) = Self::replay_to(udp_send, &remote_addr, response).await {
                    error!("Replay error: {}", err);
                }
            });
        }

        Ok(())
    }

    async fn replay_to(
        udp_socket: Arc<UdpSocket>,
        remote_addr: &std::net::SocketAddr,
        response_header: Header,
    ) -> common::Result<()> {
        let mut buf = Vec::new();
        let mut cursor = std::io::Cursor::new(&mut buf);
        response_header.write_all(&mut cursor)?;

        if log_enabled!(log::Level::Trace) {
            trace!(
                "Send message: {}\n to {}",
                String::from_utf8(buf.clone())?,
                remote_addr,
            );
        }

        udp_socket.send_to(buf.as_slice(), remote_addr).await?;

        Ok(())
    }

    fn print_trace(request_header_bytes: &[u8]) -> common::Result<()> {
        trace!(
            "Receive {}",
            String::from_utf8(request_header_bytes.to_vec())?
        );

        Ok(())
    }

    const M_SEARCH: &'static [u8] = b"M-SEARCH";
    const NOTIFY: &'static [u8] = b"NOTIFY";

    async fn route_message(
        &self,
        method: &[u8],
        header: &Header,
    ) -> common::Result<Vec<(Header, Duration)>> {
        match method {
            Self::NOTIFY => self.debug_notify(header).await,
            Self::M_SEARCH => self.m_search_request(header).await,
            method => {
                warn!("Unknown method {}", String::from_utf8(method.to_vec())?);
                Ok(Vec::new())
            }
        }
    }

    async fn debug_notify(
        &self,
        request_header: &Header,
    ) -> common::Result<Vec<(Header, Duration)>> {
        if !self.config.debug_notify {
            return Ok(Vec::new());
        }

        let notification_type_header = b"NT";
        let notification_type = request_header
            .fields()
            .get(notification_type_header)
            .map(Vec::from)
            .map(String::from_utf8);

        let notification_sub_type_header = b"NTS";
        let notification_sub_type = request_header
            .fields()
            .get(notification_sub_type_header)
            .map(Vec::from)
            .map(String::from_utf8);

        let server_header = b"SERVER";
        let server = request_header
            .fields()
            .get(server_header)
            .map(Vec::from)
            .map(String::from_utf8);

        debug!(
            "Receive NOTIFY with NT {:?} NTS {:?} from SERVER {:?}",
            notification_type, notification_sub_type, server,
        );

        Ok(Vec::new())
    }

    async fn m_search_request(
        &self,
        request_header: &Header,
    ) -> common::Result<Vec<(Header, Duration)>> {
        let search_target_header = b"ST";
        let search_target = request_header
            .fields()
            .get(search_target_header)
            .map(Vec::from)
            .map(String::from_utf8)
            .ok_or_else(|| errors::RequiredHeaderMissed::new("ST"))??;

        let max_time_header = b"MX";
        let max_time = request_header
            .fields()
            .get(&max_time_header[..])
            .map(Vec::from)
            .map(String::from_utf8)
            .ok_or_else(|| errors::RequiredHeaderMissed::new("MX"))??
            .parse::<f64>()?;

        // TODO: remote IP
        info!(
            "Process M-SEARCH message with ST {} MX {}",
            &search_target, max_time
        );

        let targets = self.route_search_target(&search_target);

        let response_headers = targets
            .iter()
            .map(|target| {
                let delay = Duration::from_secs_f64(rand::random::<f64>() * max_time);
                // TODO: remote IP
                info!("Notify {} with delay {}", target, delay.as_secs_f64());
                let response_header = self.build_response(target);
                response_header.map(|rh| (rh, delay))
            })
            .collect::<common::Result<Vec<(Header, Duration)>>>()?;

        Ok(response_headers)
    }

    fn route_search_target(&self, search_target: &str) -> Vec<String> {
        let parsed_search_target = search_target.split(':').collect::<Vec<&str>>();

        trace!("Parsed search target {:?}", parsed_search_target);

        match parsed_search_target.as_slice() {
            ["ssdp", "all"] => vec![
                self.ssdp_data.device.uuid.clone(),
                self.ssdp_data.device.name.clone(),
            ]
            .into_iter()
            .chain(self.ssdp_data.device.services.clone())
            .collect(),
            ["upnp", "rootdevice"] => vec![search_target.into()],
            ["uuid", _device_uuid] => {
                if self.ssdp_data.device.uuid.as_str() == search_target {
                    vec![self.ssdp_data.device.uuid.clone()]
                } else {
                    debug!("Search target {} (device) ignored!", search_target);
                    Vec::new()
                }
            }
            ["urn", "schemas-upnp-org", "device", _device_type, _version] => {
                if self.ssdp_data.device.name == search_target {
                    vec![self.ssdp_data.device.name.clone()]
                } else {
                    debug!("Search target {} (device) ignored!", search_target);
                    Vec::new()
                }
            }
            ["urn", "schemas-upnp-org", "service", _service_type, _version] => {
                if let Some(service_name) = self
                    .ssdp_data
                    .device
                    .services
                    .iter()
                    .find(|service| *service == search_target)
                {
                    vec![service_name.clone()]
                } else {
                    debug!("Search target {} (service) ignored!", search_target);
                    Vec::new()
                }
            }
            _ => {
                warn!("Search target {} ignored!", search_target);
                Vec::new()
            }
        }
    }

    fn build_response(&self, search_target: &str) -> common::Result<Header> {
        let usn = if self.ssdp_data.device.uuid.as_str() == search_target {
            self.ssdp_data.device.uuid.clone()
        } else {
            format!("{}::{}", self.ssdp_data.device.uuid, search_target)
        };

        let date = chrono::Utc::now();

        let header_line = HeaderStartLine::new_response(200, "OK".as_bytes());
        let mut header_fields = HeaderFields::new();

        header_fields.set(b"LOCATION", self.ssdp_data.location.as_bytes());
        header_fields.set(b"ST", search_target.as_bytes());
        header_fields.set(b"SERVER", self.ssdp_data.user_agent.as_bytes());
        header_fields.set(
            b"CACHE-CONTROL",
            format!("max-age={}", self.config.response_ttl.as_secs()).as_bytes(),
        );
        header_fields.set(b"EXT", "".as_bytes());
        header_fields.set(b"USN", usn.as_bytes());
        header_fields.set(b"Content-Length", "0".as_bytes());
        header_fields.set(b"DATE", date.to_rfc2822().as_bytes());

        let response_hedaer = Header::new(header_line, header_fields);

        Ok(response_hedaer)
    }
}

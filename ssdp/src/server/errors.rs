use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Debug)]
pub struct RequiredHeaderMissed {
    header_name: String,
}

impl RequiredHeaderMissed {
    pub(super) fn new<H>(header_name: H) -> Self
    where
        H: Into<String>,
    {
        Self {
            header_name: header_name.into(),
        }
    }
}

impl Display for RequiredHeaderMissed {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "Required field {} missed", self.header_name)
    }
}

impl Error for RequiredHeaderMissed {}

pub struct HeaderError(http_tiny::error::Error);

impl std::fmt::Display for HeaderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

impl std::fmt::Debug for HeaderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}

impl From<http_tiny::error::Error> for HeaderError {
    fn from(error: http_tiny::error::Error) -> Self {
        Self(error)
    }
}

impl Error for HeaderError {}

use std::error::Error as StdError;
use std::net::Ipv4Addr;
use std::result::Result as StdResult;

pub(super) const SSDP_PORT: u16 = 1900;
pub(super) const SSDP_MULTICAST_V4_ADDR: Ipv4Addr = Ipv4Addr::new(239, 255, 255, 250);

// TODO: Check https://github.com/rust-lang/rust/issues/27709
// pub const SSDP_MULTICAST_V6_LINK_LOCAL_ADDR: Ipv6Addr =
//     Ipv6Addr::new(0xFF, 0x02, 0, 0, 0, 0, 0, 0x0C);
// pub const SSDP_MULTICAST_V6_SITE_LOCAL_ADDR: Ipv6Addr =
//     Ipv6Addr::new(0xFF, 0x05, 0, 0, 0, 0, 0, 0x0C);
// pub const IPV6_ANY_INTERFACE: u32 = 0;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Header error: {0}")]
    Header(super::errors::HeaderError),

    #[error("Header missed: {0}")]
    HeaderMissed(#[from] super::errors::RequiredHeaderMissed),

    #[error("UTF-8 error: {0}")]
    Utf8(#[from] std::string::FromUtf8Error),

    #[error("Num parse error: {0}")]
    Num(#[from] std::num::ParseFloatError),

    #[error("IO error: {0}")]
    Io(#[from] std::io::Error),

    #[error("Other error: {0}")]
    Other(#[from] Box<dyn StdError>),
}

impl From<http_tiny::error::Error> for Error {
    fn from(error: http_tiny::error::Error) -> Self {
        Self::Header(super::errors::HeaderError::from(error))
    }
}

pub type Result<T> = StdResult<T, Error>;

#[derive(Clone)]
pub struct Device {
    pub uuid: String,
    pub name: String,
    pub services: Vec<String>,
}

#[derive(Clone)]
pub struct SsdpData {
    pub location: String,
    pub user_agent: String,
    pub device: Device,
}

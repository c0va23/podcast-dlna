use std::net::{IpAddr, SocketAddr};
use std::sync::Arc;
use std::time::Duration;

use http_tiny::Header;
use thiserror::Error as ThisError;
use tokio::net::UdpSocket;

use super::common;
use super::DataBuilder as SsdpDataBuilder;

#[derive(Debug, Clone)]
pub struct Config {
    /// SSDP notification message TTL.
    pub ttl: Duration,

    /// Used to prevent the expiration of MTSP notifications.
    /// Notifications will be sent at intervals equal to TTL - TTL_OVERLAY.
    pub ttl_overlay: Duration,

    /// SSDP notifier binding address.
    pub bind_addr: IpAddr,

    /// SSDP notifier binding port.
    /// 0 - random port.
    pub bind_port: u16,

    /// SSDP listener UDP multicast TTL.
    pub multicast_ttl: u32,

    /// SSDP listener UDP multicast loop.
    pub multicast_loop: bool,
}

#[derive(Debug, ThisError)]
pub enum Error {
    #[error("TTL ({ttl:?}) should be greater that ttl_overlay ({ttl_overlay:?})")]
    InvalidNotificationInterval {
        ttl: Duration,
        ttl_overlay: Duration,
    },

    #[error("Bind SSDP notifier error: {0}")]
    Bind(#[from] std::io::Error),

    #[error("SSDP data error: {0}")]
    SsdpData(#[from] common::Error),
}

pub struct Starter {
    config: Config,
    stop_notify: Arc<tokio::sync::Notify>,
    stopped_notify: Arc<tokio::sync::Notify>,
    ssdp_data_builder: Arc<dyn SsdpDataBuilder>,
}

impl Starter {
    pub fn new(config: Config, ssdp_data_builder: Arc<dyn SsdpDataBuilder>) -> Self {
        let stop_notify = Arc::new(tokio::sync::Notify::new());
        let stopped_notify = Arc::new(tokio::sync::Notify::new());

        Self {
            config,
            stop_notify,
            stopped_notify,
            ssdp_data_builder,
        }
    }

    pub async fn start(&self) -> Result<(), Error> {
        let notification_interval = self.notification_interval()?;
        let ssdp_data = self
            .ssdp_data_builder
            .build()
            .await
            .map_err(Error::SsdpData)?;

        let udp_socket = self.bind_udp_socket().await?;

        let stop_notify = self.stop_notify.clone();
        let stopped_notify = self.stopped_notify.clone();

        let config = self.config.clone();

        tokio::spawn(async move {
            Notifier {
                config,
                ssdp_data,
                stop_notify,
                udp_socket,
                notification_interval,
            }
            .notify_loop()
            .await;

            stopped_notify.notify_one();
            info!("Notifier task stopped");
        });

        Ok(())
    }

    fn notification_interval(&self) -> Result<Duration, Error> {
        self.config.ttl.checked_sub(self.config.ttl_overlay).ok_or(
            Error::InvalidNotificationInterval {
                ttl: self.config.ttl,
                ttl_overlay: self.config.ttl_overlay,
            },
        )
    }

    async fn bind_udp_socket(&self) -> Result<UdpSocket, Error> {
        let bind_addr = SocketAddr::new(self.config.bind_addr, self.config.bind_port);
        let udp_socket = UdpSocket::bind(bind_addr).await?;
        // TODO: add IPv6 support
        udp_socket.set_multicast_ttl_v4(self.config.multicast_ttl)?;
        udp_socket.set_multicast_loop_v4(self.config.multicast_loop)?;

        info!(
            "Start notifier on {addr} (TTL: {ttl}, loop: {loop})",
            addr = udp_socket.local_addr()?,
            ttl = udp_socket.multicast_ttl_v4()?,
            loop = udp_socket.multicast_loop_v4()?,
        );
        Ok(udp_socket)
    }

    pub async fn stop(&self) {
        self.stop_notify.notify_one();
        self.stopped_notify.notified().await;

        debug!("Notifier stopped");
    }
}

struct Notifier {
    config: Config,
    ssdp_data: common::SsdpData,
    stop_notify: Arc<tokio::sync::Notify>,
    udp_socket: tokio::net::UdpSocket,
    notification_interval: Duration,
}

impl Notifier {
    async fn notify_loop(&mut self) {
        let mut next_notify = tokio::time::Instant::now() + self.notification_interval;

        info!("Start notifier loop");

        loop {
            if let Err(err) = self.notify_all("ssdp:alive").await {
                error!("Notify all {} error {}", "ssdp:alive", err);
            }

            tokio::select! {
                _ = tokio::time::sleep_until(next_notify) => {
                    next_notify += self.notification_interval;
                }
                _ = self.stop_notify.notified() => break
            };
        }

        info!("Stop notifier loop");

        if let Err(err) = self.notify_all("ssdp:byebye").await {
            error!("Notify all {} error {}", "ssdp:byebye", err);
        }
    }

    fn multicast_addr(&self) -> std::net::SocketAddr {
        match self.config.bind_addr {
            IpAddr::V4(_) => std::net::SocketAddr::V4(std::net::SocketAddrV4::new(
                common::SSDP_MULTICAST_V4_ADDR,
                common::SSDP_PORT,
            )),
            IpAddr::V6(_) => {
                // TODO: implement V6 version
                unimplemented!()
            }
        }
    }

    async fn notify_all(&mut self, notification_sub_type: &str) -> common::Result<()> {
        let ssdp_data = self.ssdp_data.clone();
        let base_notifications = vec![("upnp:rootdevice", true), (&ssdp_data.device.uuid, false)];

        for (notification_type, usn_suffix) in base_notifications {
            self.notify(notification_type, notification_sub_type, usn_suffix)
                .await?;
        }

        self.notify(ssdp_data.device.name.as_str(), notification_sub_type, true)
            .await?;

        for service in ssdp_data.device.services {
            self.notify(&service, notification_sub_type, true).await?;
        }

        Ok(())
    }

    async fn notify(
        &mut self,
        notification_type: &str,
        notification_sub_type: &str,
        usn_suffix: bool,
    ) -> common::Result<()> {
        let date = chrono::Utc::now();

        let target_addr = self.multicast_addr();

        debug!(
            "Notify {} {} {}",
            target_addr, notification_sub_type, notification_type
        );
        let notify_request =
            self.build_notify_request(notification_type, notification_sub_type, usn_suffix, &date)?;

        let payload = Self::build_payload(&notify_request)?;

        self.udp_socket
            .send_to(payload.as_slice(), target_addr)
            .await?;

        Ok(())
    }

    fn build_payload(notify_request: &http_tiny::Header) -> common::Result<Vec<u8>> {
        let mut buf = Vec::new();
        let mut cursor = std::io::Cursor::new(&mut buf);
        notify_request.write_all(&mut cursor)?;
        trace!("Payload: {}", String::from_utf8(buf.clone()).unwrap());
        Ok(buf)
    }

    fn build_notify_request(
        &self,
        notification_type: &str,
        notification_sub_type: &str,
        usn_suffix: bool,
        date: &chrono::DateTime<chrono::Utc>,
    ) -> Result<http_tiny::Header, http_tiny::error::Error> {
        let usn = if usn_suffix {
            format!("{}::{}", self.ssdp_data.device.uuid, notification_type)
        } else {
            self.ssdp_data.device.uuid.clone()
        };

        let uri = b"*";
        let method = b"NOTIFY";
        let start_line = http_tiny::HeaderStartLine::new_request(method.to_vec(), uri.to_vec());

        let mut fields = http_tiny::HeaderFields::new();
        fields.set(b"LOCATION", self.ssdp_data.location.as_bytes());
        fields.set(
            b"HOST",
            format!("{}:{}", common::SSDP_MULTICAST_V4_ADDR, common::SSDP_PORT).as_bytes(),
        );
        fields.set(b"NT", notification_type);
        fields.set(b"NTS", notification_sub_type);
        fields.set(b"SERVER", self.ssdp_data.user_agent.as_bytes());
        fields.set(
            b"CACHE-CONTROL",
            format!("max-age={}", self.config.ttl.as_secs()),
        );
        fields.set(b"EXT", "");
        fields.set(b"USN", usn);
        fields.set(b"Content-Length", "0");
        fields.set(b"DATE", date.to_rfc2822());

        Ok(Header::new(start_line, fields))
    }
}

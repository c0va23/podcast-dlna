pub mod common;
mod errors;
pub mod listener;
pub mod notifier;

#[async_trait::async_trait]
pub trait DataBuilder: Sync + Send {
    async fn build(&self) -> common::Result<common::SsdpData>;
}

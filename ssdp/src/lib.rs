#[macro_use]
extern crate log;

use server::common;
use server::listener;
use server::notifier;

mod server;

pub type Listener = listener::Starter;
pub type ListenerConfig = listener::Config;
pub type Notifier = notifier::Starter;
pub type NotifierConfig = notifier::Config;
pub type Data = common::SsdpData;
pub type Device = common::Device;
pub type Error = common::Error;
pub type Result<T> = common::Result<T>;

pub use server::DataBuilder;

ARG BASE_IMAGE

FROM ${BASE_IMAGE}

RUN mkdir /app
WORKDIR /app

ARG LLD_PACKAGE
RUN apt update && apt install -y curl make gcc git jq clang "${LLD_PACKAGE}"

ADD scripts ./scripts
RUN ./scripts/deb-variant.sh \
    && ./scripts/fix-lld-link.sh "${LLD_PACKAGE}"

ARG RUST_VERSION
RUN ./scripts/rust-install.sh "${RUST_VERSION}"

ENV PATH=${PATH}:/root/.cargo/bin
RUN cd /root/.cargo && /app/scripts/cargo-deb-install.sh /root/.cargo/bin/cargo-deb

ENV RUSTFLAGS="-C linker=clang -C link-arg=-fuse-ld=lld"

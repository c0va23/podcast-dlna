ARG RUST_VERSION
ARG BASE_IMAGE

FROM rust:${RUST_VERSION}-${BASE_IMAGE}

ENV RUST_VERSION="${RUST_VERSION}"

RUN mkdir /app

WORKDIR /app

ADD Makefile ./
ADD makefiles ./makefiles
ADD scripts ./scripts

RUN make check-rust-version
RUN rustup component add rustfmt clippy

RUN apt update && apt install -y lld clang jq

ENV RUSTFLAGS="-C linker=clang -C link-arg=-fuse-ld=lld"

use std::iter::{Copied, Enumerate};
use std::ops::Range;
use std::ops::RangeFrom;
use std::slice::Iter;

use nom::Needed;

#[derive(Clone, Debug)]
pub(crate) struct View<'a> {
    data: &'a [u8],
    range: Range<usize>,
}

impl<'a> View<'a> {
    pub(crate) fn new(data: &'a [u8]) -> Self {
        let range = 0..data.len();
        Self { data, range }
    }

    pub(crate) fn data(&self) -> &'a [u8] {
        &self.data[self.range.clone()]
    }

    pub(crate) fn data_range(&self) -> Range<usize> {
        self.range.clone()
    }

    pub(crate) fn to_vec(&self) -> Vec<u8> {
        self.data().to_vec()
    }

    pub(crate) fn into_vec(self) -> Vec<u8> {
        self.data().to_vec()
    }

    pub(crate) fn iter(&self) -> std::slice::Iter<u8> {
        self.data().iter()
    }
}

impl<'a, const N: usize> From<&'a [u8; N]> for View<'a> {
    fn from(data: &'a [u8; N]) -> Self {
        Self::new(data)
    }
}

impl<'a> From<&'a [u8]> for View<'a> {
    fn from(data: &'a [u8]) -> Self {
        Self::new(data)
    }
}

impl<'a> PartialEq for View<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.data() == other.data()
    }
}

impl<'a> nom::InputLength for View<'a> {
    fn input_len(&self) -> usize {
        self.range.len()
    }
}

impl<'a> nom::InputLength for &View<'a> {
    fn input_len(&self) -> usize {
        (*self).input_len()
    }
}

impl<'a> nom::FindSubstring<&[u8]> for View<'a> {
    fn find_substring(&self, substr: &[u8]) -> Option<usize> {
        self.data().find_substring(substr)
    }
}

impl<'a> nom::Slice<Range<usize>> for View<'a> {
    fn slice(&self, range: Range<usize>) -> Self {
        let range_start = self.range.start + range.start;
        let range_end = range_start + range.len();
        let new_range = range_start..range_end;

        Self {
            data: self.data,
            range: new_range,
        }
    }
}

impl<'a> nom::Slice<RangeFrom<usize>> for View<'a> {
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        let range_start = self.range.start + range.start;
        let new_range = range_start..self.range.end;

        Self {
            data: self.data,
            range: new_range,
        }
    }
}

impl<'a> bitvec::view::BitView for View<'a> {
    type Store = u8;

    fn view_bits<O>(&self) -> &bitvec::slice::BitSlice<O, Self::Store>
    where
        O: bitvec::order::BitOrder,
    {
        self.data().view_bits()
    }

    fn view_bits_mut<O>(&mut self) -> &mut bitvec::slice::BitSlice<O, Self::Store>
    where
        O: bitvec::order::BitOrder,
    {
        unreachable!()
    }
}

impl<'a> nom::InputIter for View<'a> {
    type Item = u8;

    type Iter = Enumerate<Self::IterElem>;

    type IterElem = Copied<Iter<'a, Self::Item>>;

    fn iter_indices(&self) -> Self::Iter {
        self.iter_elements().enumerate()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.data().iter().copied()
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.data().iter().position(|b| predicate(*b))
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        if self.range.len() >= count {
            Ok(count)
        } else {
            Err(Needed::new(count - self.range.len()))
        }
    }
}

impl<'a> nom::InputTake for View<'a> {
    fn take(&self, count: usize) -> Self {
        let range_end = self.range.start + count;
        let new_range = self.range.start..range_end;
        Self {
            data: self.data,
            range: new_range,
        }
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        let middle = self.range.start + count;
        (
            Self {
                data: self.data,
                range: middle..self.range.end,
            },
            Self {
                data: self.data,
                range: self.range.start..middle,
            },
        )
    }
}

impl<'a> nom::UnspecializedInput for View<'a> {}

#[cfg(test)]
mod tests {
    use super::View;

    #[test]
    fn view_new() {
        let data = &[0, 1, 2, 3, 4];

        let view = View::new(data);

        assert_eq!(data, view.data);
        assert_eq!(0, view.range.start);
        assert_eq!(5, view.range.end);
    }

    #[test]
    fn view_len() {
        use nom::InputLength;

        let data = &[0, 1, 2, 3, 4];

        let view = View::new(data);

        assert_eq!(5, view.input_len());
    }

    #[test]
    fn view_slice_range() {
        use nom::Slice;

        let data = &[0, 1, 2, 3, 4];

        let view = View::new(data);

        let sub_view = view.slice(0..3);
        assert_eq!(data[0..3], sub_view.data[sub_view.range]);

        let sub_view = view.slice(3..5);
        assert_eq!(data[3..5], sub_view.data[sub_view.range]);

        let sub_view = view.slice(0..4).slice(1..4);
        assert_eq!(data[1..4], sub_view.data[sub_view.range]);
    }

    #[test]
    fn view_slice_range_from() {
        use nom::Slice;

        let data = &[0, 1, 2, 3, 4];

        let view = View::new(data);

        let sub_view = view.slice(0..);
        assert_eq!(data[0..], sub_view.data[sub_view.range]);

        let sub_view = view.slice(3..);
        assert_eq!(data[3..], sub_view.data[sub_view.range]);

        let sub_view = view.slice(2..).slice(1..);
        assert_eq!(data[3..], sub_view.data[sub_view.range]);
    }

    #[test]
    fn view_input_take() {
        use nom::InputTake;

        let data = b"ASCII\0";

        let view = View::new(data);

        let expected_view = View { data, range: 0..4 };

        assert_eq!(expected_view, view.take(4), "take");
        assert_eq!(b"ASCI", view.take(4).data(), "take");

        let expected_tail_view = View { data, range: 4..6 };

        let expected_split = (expected_tail_view, expected_view);

        assert_eq!(expected_split, view.take_split(4), "take_split");
    }

    #[test]
    fn view_find_sub_string() {
        use nom::FindSubstring;

        let data = b"ASCII\0";

        let view = View::new(data);

        assert_eq!(Some(5), view.find_substring(b"\0"), "null");
        assert_eq!(Some(2), view.find_substring(b"C"), "C");
        assert_eq!(None, view.find_substring(b"X"), "not found");
    }

    #[test]
    fn view_take_unit() {
        const NULL: [u8; 1] = [0];

        use nom::bytes::complete::take_until;
        use nom::Slice;

        use crate::mp3::NomVerboseError;

        let data = b"ASCII\0";

        let view = View::new(data);

        let null = &NULL[..];

        let (input, chars) = take_until::<_, _, NomVerboseError>(null)(view.clone()).unwrap();

        assert_eq!(view.slice(0..5), chars, "match");
        assert_eq!(view.slice(5..), input, "tail");
    }

    #[test]
    fn view_input_iter_slice_index() {
        use nom::{InputIter, Needed};

        let data = b"012345";

        let view = View::new(data);

        assert_eq!(Ok(0), view.slice_index(0), "0");
        assert_eq!(Ok(3), view.slice_index(3), "3");
        assert_eq!(Ok(6), view.slice_index(6), "6");
        assert_eq!(Err(Needed::new(1)), view.slice_index(7), "err");
    }
}

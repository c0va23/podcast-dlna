#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![deny(clippy::print_stdout)]
#![deny(clippy::dbg_macro)]

pub mod mp3;

mod bytes;

pub use mp3::{
    File as Mp3File, NomVerboseError as ParseVerboseError, ParseDurationError, ParseError,
};

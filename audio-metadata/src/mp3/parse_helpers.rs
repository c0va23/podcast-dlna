use bitvec::view::BitView;
use nom::InputLength;
use nom_bitvec::BSlice;

use crate::mp3::{
    errors::{BitsError, CombinedError},
    BitsInput, BitsResult, BytesInput, BytesResult,
};

macro_rules! define_bit_slices {
    {
        $(
            $name:ident => [$($value:literal),*]
        )*
    } => {

        lazy_static::lazy_static! {
            $(
                static ref $name: nom_bitvec::BSlice<'static, bitvec::prelude::Msb0, u8> = {
                    use bitvec::prelude::{Msb0, bits};

                    nom_bitvec::BSlice(bits![static Msb0, u8; $($value,)*])
                };
            )*
        }
    };
    {
        $(
            $name:ident => [$value:literal; $count:expr]
        )*
    } => {
        lazy_static::lazy_static! {
            $(
                static ref $name: nom_bitvec::BSlice<'static, bitvec::prelude::Msb0, u8> =
                    nom_bitvec::BSlice(bits![static bitvec::prelude::Msb0, u8; $value; $count]);
            )*
        }
    };
}

pub(crate) use define_bit_slices;

define_bit_slices! {
    TRUE_VALUE => [1]
    FALSE_VALUE => [0]
}

pub(crate) fn parse_bool<'a, E>(input: BitsInput<'a>) -> nom::IResult<BitsInput, bool, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    alt((
        value(true, tag(*TRUE_VALUE)),
        value(false, tag(*FALSE_VALUE)),
    ))(input)
}

const BYTE_SIZE: usize = u8::BITS as usize;

fn bit_slice_aligned(bits_input: BitsInput) -> bool {
    bits_input.0.len() % BYTE_SIZE == 0
}

pub(crate) fn with_bits<'a, T, E>(
    mut bits_parser: impl FnMut(BitsInput<'a>) -> BitsResult<'a, T, E>,
) -> impl FnMut(BytesInput<'a>) -> BytesResult<'a, T, E>
where
    E: CombinedError<'a>,
{
    use nom::{Err, Needed, Slice};

    move |input: BytesInput<'a>| {
        let bits_view = input.data().view_bits();
        let bits_input = BSlice(bits_view);
        let (bits_input, value) = bits_parser(bits_input).map_err(|err| match err {
            Err::Incomplete(Needed::Size(bits_size)) => {
                Err::Incomplete(Needed::new(bits_size.get() / BYTE_SIZE))
            }
            Err::Incomplete(Needed::Unknown) => Err::Incomplete(Needed::Unknown),
            Err::Error(err) => Err::Error(err),
            Err::Failure(err) => Err::Failure(err),
        })?;

        let rest_len = bits_input.0.len() / BYTE_SIZE;
        let consumed_byte_len = input.input_len() - rest_len;

        if !bit_slice_aligned(bits_input) {
            return Err(nom::Err::Failure(E::from_error_kind(
                input.slice(0..consumed_byte_len),
                nom::error::ErrorKind::Satisfy,
            )));
        }

        Ok((input.slice(consumed_byte_len..), value))
    }
}

#[cfg(test)]
mod tests {
    use bitvec::view::BitView;
    use nom::error::ErrorKind;
    use nom_bitvec::BSlice;

    use super::super::errors::{NomError, NomErrorValue, NomInput, NomVerboseError};
    use crate::bytes::View;

    #[test]
    fn with_bits() {
        use super::with_bits;
        use nom::bytes::complete::take;

        let src = vec![0_u8; 3];
        let input = View::new(&src[..]);

        assert_eq!(
            Ok::<_, nom::Err<NomVerboseError>>((View::new(&src[3..3]), BSlice(input.view_bits()))),
            with_bits(take(24_usize))(input.clone()),
        );

        assert_eq!(
            Err(nom::Err::Failure(NomVerboseError {
                errors: vec![NomError {
                    value: NomErrorValue::Parse(ErrorKind::Satisfy),
                    input: NomInput::Bytes((&src[0..3]).to_vec()),
                }],
            })),
            with_bits(take(23_usize))(input.clone()),
        );

        assert_eq!(
            Err(nom::Err::Failure(NomVerboseError {
                errors: vec![NomError {
                    value: NomErrorValue::Parse(ErrorKind::Satisfy),
                    input: NomInput::Bytes((&src[0..2]).to_vec()),
                }],
            })),
            with_bits(take(15_usize))(input.clone()),
        );
    }
}

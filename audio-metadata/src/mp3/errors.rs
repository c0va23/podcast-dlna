use bitvec::order::Msb0;
use bitvec::prelude::BitVec;
use nom::error::{ContextError, ErrorKind, ParseError};
use thiserror::Error as ThisError;

use super::{BitsInput, BytesInput};

#[derive(Debug, ThisError, PartialEq)]
pub struct NomVerboseError {
    pub(crate) errors: Vec<NomError>,
}

impl NomVerboseError {
    fn with_error(self, error: NomError) -> Self {
        Self {
            errors: self.errors.into_iter().chain([error]).collect(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub(crate) struct NomError {
    pub(crate) input: NomInput,
    pub(crate) value: NomErrorValue,
}

impl NomError {
    #[allow(clippy::needless_pass_by_value)]
    fn from_bytes(input: BytesInput, value: NomErrorValue) -> Self {
        Self {
            input: NomInput::Bytes(input.to_vec()),
            value,
        }
    }

    fn from_bits(input: BitsInput, value: NomErrorValue) -> Self {
        Self {
            input: NomInput::Bits(input.0.to_bitvec()),
            value,
        }
    }
}

impl std::fmt::Display for NomError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "(")?;
        writeln!(f, "\t{:?},", self.input)?;
        writeln!(f, "\t{:?},", self.value)?;
        writeln!(f, ")")
    }
}

#[derive(Debug, PartialEq)]
pub(crate) enum NomErrorValue {
    Parse(nom::error::ErrorKind),
    Context(&'static str),
}

#[derive(Debug, PartialEq)]
pub(crate) enum NomInput {
    Bytes(Vec<u8>),
    Bits(BitVec<Msb0, u8>),
}

impl<'a> ParseError<BytesInput<'a>> for NomVerboseError {
    fn from_error_kind(input: BytesInput, kind: ErrorKind) -> Self {
        Self {
            errors: vec![NomError::from_bytes(input, NomErrorValue::Parse(kind))],
        }
    }

    fn append(input: BytesInput, kind: ErrorKind, other: Self) -> Self {
        other.with_error(NomError::from_bytes(input, NomErrorValue::Parse(kind)))
    }
}

impl<'a> ContextError<BytesInput<'a>> for NomVerboseError {
    fn add_context(input: BytesInput, ctx: &'static str, other: Self) -> Self {
        other.with_error(NomError::from_bytes(input, NomErrorValue::Context(ctx)))
    }
}

impl<'a> ParseError<BitsInput<'a>> for NomVerboseError {
    fn from_error_kind(input: BitsInput, kind: ErrorKind) -> Self {
        Self {
            errors: vec![NomError::from_bits(input, NomErrorValue::Parse(kind))],
        }
    }

    fn append(input: BitsInput, kind: ErrorKind, other: Self) -> Self {
        other.with_error(NomError::from_bits(input, NomErrorValue::Parse(kind)))
    }
}

impl<'a> ContextError<BitsInput<'a>> for NomVerboseError {
    fn add_context(input: BitsInput, ctx: &'static str, other: Self) -> Self {
        other.with_error(NomError::from_bits(input, NomErrorValue::Context(ctx)))
    }
}

impl std::fmt::Display for NomVerboseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "[")?;
        for error in &self.errors {
            writeln!(f, "{}", error)?;
        }
        writeln!(f, "]")
    }
}

pub(crate) trait BytesError<'a>:
    ParseError<BytesInput<'a>> + ContextError<BytesInput<'a>>
{
}

pub(crate) trait BitsError<'a>:
    ParseError<BitsInput<'a>> + ContextError<BitsInput<'a>>
{
}

pub(crate) trait CombinedError<'a>: BytesError<'a> + BitsError<'a> {}

impl<'a> BytesError<'a> for NomVerboseError {}
impl<'a> BitsError<'a> for NomVerboseError {}
impl<'a> CombinedError<'a> for NomVerboseError {}

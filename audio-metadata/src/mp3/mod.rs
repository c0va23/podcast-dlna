/// MP3 file structure
use bitvec::order::Msb0;
use conv::prelude::*;
use nom::InputLength;
use nom_bitvec::BSlice;
use thiserror::Error as ThisError;

pub(crate) mod parse_helpers;

mod errors;
pub mod frame;
pub mod id3v2;
mod utils;

#[cfg(test)]
pub(crate) mod test_helpers;
#[cfg(test)]
mod tests;

use errors::CombinedError;
pub use errors::NomVerboseError;

use self::frame::{AudioFrame, BitrateMethod, Header, MetaData};
use self::id3v2::AttachedPicture;

use crate::bytes::View as BytesView;

pub(crate) type BitsInput<'a> = BSlice<'a, Msb0, u8>;
pub(crate) type BytesInput<'a> = BytesView<'a>;
pub(crate) type BitsResult<'a, T, E> = nom::IResult<BitsInput<'a>, T, E>;
pub(crate) type BytesResult<'a, T, E> = nom::IResult<BytesInput<'a>, T, E>;

#[derive(Debug)]
pub struct File<'a> {
    pub id3v2: Option<id3v2::Id3V2<'a>>,
    first_audio_frame: frame::AudioFrame<'a>,
    tail_audio_frames: Vec<frame::AudioFrame<'a>>,
}

#[derive(ThisError, Debug)]
pub enum ParseError {
    #[error("Parse MP3 file error: {0:?}")]
    Error(errors::NomVerboseError),

    #[error("Nom incomplete: {0}")]
    Incomplete(usize),
}

#[derive(ThisError, Debug)]
pub enum ParseDurationError {
    #[error("Number conv error: {0}")]
    NumConv(#[from] conv::PosOverflow<usize>),

    #[error("Unknown duration")]
    UnknownValue,

    // It allow build on 32bit arch
    #[error("Possible bug into `conv` crate: {0}")]
    NotReachable(#[from] conv::NoError),
}

pub enum BitRate {
    Constant {
        bits_per_second: usize,
    },
    Variable {
        average_bit_per_second: usize,
        source_frame_count: usize,
    },
}

impl BitRate {
    #[must_use]
    pub fn value(&self) -> usize {
        match self {
            Self::Constant { bits_per_second } => *bits_per_second,
            Self::Variable {
                average_bit_per_second,
                ..
            } => *average_bit_per_second,
        }
    }
}

// Public methods
impl<'a> File<'a> {
    /// Parse MP3 file structure (ID3V2, Xing/Info frame, Audio frames)
    ///
    /// # Errors
    /// Return nom errors
    pub fn parse(input: &'a [u8]) -> Result<Self, ParseError> {
        use nom::Needed;

        let input = super::bytes::View::new(input);

        let (_input, mp3_file) = Self::parse_nom_internal::<NomVerboseError>(input.clone())
            .map_err(|err| match err {
                nom::Err::Error(err) | nom::Err::Failure(err) => ParseError::Error(err),
                nom::Err::Incomplete(needed) => {
                    let min_len = match needed {
                        Needed::Size(size) => size.get() + input.input_len(),
                        Needed::Unknown => Header::MAX_FRAME_SIZE,
                    };
                    ParseError::Incomplete(min_len)
                }
            })?;

        Ok(mp3_file)
    }

    const SAMPLES_PER_FRAME: u64 = 1152;
    const MICROSECOND_PER_SECOND: u64 = 1_000_000;

    /// Parse MP3 duration by prefix and known file size
    /// Now work only with CBR files.
    ///
    /// # How it work?
    /// 1. Calculate audio frames size without all metadata (ID3V2 and Xing/Info frame).
    /// 2. Get bitrate from first audio frame.
    /// 3. Duration in seconds is audio frame data size divided by bitrate
    ///
    /// # Errors
    ///
    /// Return error [`ParseDurationError::PrefixTooShort`], if provided [`MP3File`] not
    /// contains information for detect duration.
    ///
    /// Can return [`ParseDurationError::NumConv`] for invalid MP3 files.
    pub fn duration_by_prefix(
        &self,
        file_size: usize,
    ) -> Result<std::time::Duration, ParseDurationError> {
        if self.is_constant_bit_rate() {
            self.constant_duration_by_prefix(file_size)
        } else {
            self.variable_duration()
        }
    }

    fn constant_duration_by_prefix(
        &self,
        file_size: usize,
    ) -> Result<std::time::Duration, ParseDurationError> {
        let bit_rate = self.first_audio_frame.header.bitrate.value();

        // TODO: handle id3v1 tag size
        let header_size = self.id3v2.as_ref().map_or(0, id3v2::Id3V2::total_size);
        let metadata_size = self.meta_data_frame_size();

        let audio_data_size = file_size - header_size - metadata_size;

        let duration_secs = f64::value_from(audio_data_size)? / f64::value_from(bit_rate / 8)?;

        Ok(std::time::Duration::from_secs_f64(duration_secs))
    }

    fn variable_duration(&self) -> Result<std::time::Duration, ParseDurationError> {
        if let Some(frames) = self.meta_data().and_then(|md| md.frames) {
            let sampling_rate = self.first_audio_frame.header.sampling_rate.value();
            let total_samples = Self::SAMPLES_PER_FRAME * u64::from(frames);
            let duration =
                (Self::MICROSECOND_PER_SECOND * total_samples) / u64::value_from(sampling_rate)?;

            Ok(std::time::Duration::from_micros(duration))
        } else {
            Err(ParseDurationError::UnknownValue)
        }
    }

    fn is_constant_bit_rate(&self) -> bool {
        self.meta_data()
            .map(|meta_data| meta_data.bitrate_method)
            .as_ref()
            .map_or(true, BitrateMethod::is_constant)
    }

    pub fn audio_frames(&self) -> impl std::iter::Iterator<Item = &AudioFrame> {
        let tail_slice = &self.tail_audio_frames[..];
        vec![&self.first_audio_frame]
            .into_iter()
            .chain(tail_slice.iter())
    }

    #[must_use]
    pub fn bit_rate(&self) -> BitRate {
        if self.is_constant_bit_rate() {
            BitRate::Constant {
                bits_per_second: self.first_audio_frame.header.bitrate.value(),
            }
        } else {
            let total_bit_rate = self
                .audio_frames()
                .map(|frame| frame.header.bitrate.value())
                .sum::<usize>();
            let source_frame_count = self.audio_frames().count();
            let average_bit_per_second = total_bit_rate / source_frame_count;
            BitRate::Variable {
                average_bit_per_second,
                source_frame_count,
            }
        }
    }

    pub fn attached_pictures(&self) -> Vec<AttachedPicture> {
        self.id3v2.as_ref().map_or_else(Vec::new, |tag| {
            tag.frames
                .iter()
                .filter_map(|frame| {
                    if id3v2::ids::ATTACHED_PICTURE.match_id(frame.id()) {
                        id3v2::frames::attached_picture::<NomVerboseError>(frame.view())
                            .map(|(_input, attached_picture)| attached_picture)
                            .ok()
                    } else {
                        None
                    }
                })
                .collect()
        })
    }
}

// Private functions
impl<'a> File<'a> {
    fn parse_nom_internal<E>(input: BytesInput<'a>) -> BytesResult<'a, Self, E>
    where
        E: CombinedError<'a>,
    {
        use nom::{combinator::opt, error::context, multi::many0};

        let (input, id3v2) =
            context("id3v2", opt(id3v2::id3v2))(input).map_err(Self::expand_id3v2_need_error)?;

        let (input, first_audio_frame) =
            context("first_audio_frame", frame::audio_frame(true))(input)?;
        let (input, audio_frames) =
            context("audio_frames", many0(frame::audio_frame(false)))(input)?;

        let mp3_file = File {
            id3v2,
            first_audio_frame,
            tail_audio_frames: audio_frames,
        };
        Ok((input, mp3_file))
    }

    fn expand_id3v2_need_error<E>(error: nom::Err<E>) -> nom::Err<E> {
        use nom::{Err::Incomplete, Needed};

        match error {
            // Reduce parse tries by forcing request first frame if id3v2 tag too big.
            Incomplete(Needed::Size(size)) => {
                Incomplete(Needed::new(size.get() + Header::MAX_FRAME_SIZE))
            }
            other => other,
        }
    }

    fn meta_data(&self) -> Option<MetaData> {
        use nom::combinator::opt;

        let audio_frame = &self.first_audio_frame;
        let input = audio_frame.payload.view();
        let header = &audio_frame.header;

        opt(frame::meta_data::<NomVerboseError>(header))(input)
            .map(|(_input, meta_data)| meta_data)
            .unwrap_or(None)
    }

    fn meta_data_frame_size(&self) -> usize {
        self.meta_data()
            .map_or(0, |_| self.first_audio_frame.header.total_frame_size())
    }
}

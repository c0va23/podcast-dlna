use std::ops::AddAssign;
use std::ops::ShlAssign;

use bitvec::prelude::*;
use nom::ToUsize;

use super::super::{
    errors::{BitsError, BytesError, CombinedError},
    parse_helpers::{define_bit_slices, parse_bool, with_bits},
    BitsResult, BytesResult, BytesView,
};

use super::super::{BitsInput, BytesInput};
use super::{
    AsciiString, AudioFrame, BitRate, BitrateMethod, Channels, Emphasis, Header, Layer, MetaData,
    MetaDataFlags, ModeExtension, MpegVersionId, SamplingRate, TagRevision,
};

define_bit_slices! {
    MPEG_VERSION_2_5 => [0, 0]
    MPEG_VERSION_RESERVED => [0, 1]
    MPEG_VERSION_2 => [1, 0]
    MPEG_VERSION_1 => [1, 1]
}

pub(crate) fn mpeg_version<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, MpegVersionId, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(MpegVersionId::Mpeg2_5, tag(*MPEG_VERSION_2_5)),
        value(MpegVersionId::Reserved, tag(*MPEG_VERSION_RESERVED)),
        value(MpegVersionId::Mpeg2, tag(*MPEG_VERSION_2)),
        value(MpegVersionId::Mpeg1, tag(*MPEG_VERSION_1)),
    ))(input)?;

    Ok((input, value))
}

define_bit_slices! {
    LAYER_NOT_DEFINED =>[0, 0]
    LAYER_III => [0, 1]
    LAYER_II =>[1, 0]
    LAYER_I =>[1, 1]
}

pub(crate) fn layer<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, Layer, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(Layer::NotDefined, tag(*LAYER_NOT_DEFINED)),
        value(Layer::Iii, tag(*LAYER_III)),
        value(Layer::Ii, tag(*LAYER_II)),
        value(Layer::I, tag(*LAYER_I)),
    ))(input)?;

    Ok((input, value))
}

define_bit_slices! {
    BIT_RATE_FREE => [0, 0, 0, 0]
    BIT_RATE_KBPS32 => [0, 0, 0, 1]
    BIT_RATE_KBPS40 => [0, 0, 1, 0]
    BIT_RATE_KBPS48 => [0, 0, 1, 1]
    BIT_RATE_KBPS56 => [0, 1, 0, 0]
    BIT_RATE_KBPS64 => [0, 1, 0, 1]
    BIT_RATE_KBPS80 => [0, 1, 1, 0]
    BIT_RATE_KBPS96 => [0, 1, 1, 1]
    BIT_RATE_KBPS112 => [1, 0, 0, 0]
    BIT_RATE_KBPS128 => [1, 0, 0, 1]
    BIT_RATE_KBPS160 => [1, 0, 1, 0]
    BIT_RATE_KBPS192 => [1, 0, 1, 1]
    BIT_RATE_KBPS224 => [1, 1, 0, 0]
    BIT_RATE_KBPS256 => [1, 1, 0, 1]
    BIT_RATE_KBPS320 => [1, 1, 1, 0]
    BIT_RATE_BAD => [1, 1, 1, 1]
}

pub(crate) fn bit_rate<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, BitRate, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(BitRate::Free, tag(*BIT_RATE_FREE)),
        value(BitRate::Kbps32, tag(*BIT_RATE_KBPS32)),
        value(BitRate::Kbps40, tag(*BIT_RATE_KBPS40)),
        value(BitRate::Kbps48, tag(*BIT_RATE_KBPS48)),
        value(BitRate::Kbps56, tag(*BIT_RATE_KBPS56)),
        value(BitRate::Kbps64, tag(*BIT_RATE_KBPS64)),
        value(BitRate::Kbps80, tag(*BIT_RATE_KBPS80)),
        value(BitRate::Kbps96, tag(*BIT_RATE_KBPS96)),
        value(BitRate::Kbps112, tag(*BIT_RATE_KBPS112)),
        value(BitRate::Kbps128, tag(*BIT_RATE_KBPS128)),
        value(BitRate::Kbps160, tag(*BIT_RATE_KBPS160)),
        value(BitRate::Kbps192, tag(*BIT_RATE_KBPS192)),
        value(BitRate::Kbps224, tag(*BIT_RATE_KBPS224)),
        value(BitRate::Kbps256, tag(*BIT_RATE_KBPS256)),
        value(BitRate::Kbps320, tag(*BIT_RATE_KBPS320)),
        value(BitRate::Bad, tag(*BIT_RATE_BAD)),
    ))(input)?;

    Ok((input, value))
}

define_bit_slices! {
    SAMPLING_RATE_HZ44100 => [0, 0]
    SAMPLING_RATE_HZ48000 => [0, 1]
    SAMPLING_RATE_HZ32000 => [1, 0]
    SAMPLING_RATE_RESERVED => [1, 1]
}

pub(crate) fn sampling_rate<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, SamplingRate, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(SamplingRate::Hz44100, tag(*SAMPLING_RATE_HZ44100)),
        value(SamplingRate::Hz48000, tag(*SAMPLING_RATE_HZ48000)),
        value(SamplingRate::Hz32000, tag(*SAMPLING_RATE_HZ32000)),
        value(SamplingRate::Reserved, tag(*SAMPLING_RATE_RESERVED)),
    ))(input)?;

    Ok((input, value))
}

define_bit_slices! {
    CHANNELS_STEREO => [0, 0]
    CHANNELS_JOINT_STEREO => [0, 1]
    CHANNELS_DUAL => [1, 0]
    CHANNELS_MONO => [1, 1]
}

pub(crate) fn channels<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, Channels, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(Channels::Stereo, tag(*CHANNELS_STEREO)),
        value(Channels::JointStereo, tag(*CHANNELS_JOINT_STEREO)),
        value(Channels::Dual, tag(*CHANNELS_DUAL)),
        value(Channels::Mono, tag(*CHANNELS_MONO)),
    ))(input)?;

    Ok((input, value))
}

define_bit_slices! {
    EMPHASIS_NONE => [0, 0]
    EMPHASIS_FIFTY_FIFTEEN => [0, 1]
    EMPHASIS_RESERVED => [1, 0]
    EMPHASIS_CCIT_J17 => [1, 1]
}

pub(crate) fn emphasis<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, Emphasis, E>
where
    E: BitsError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, value) = alt((
        value(Emphasis::None, tag(*EMPHASIS_NONE)),
        value(Emphasis::FiftyFifteen, tag(*EMPHASIS_FIFTY_FIFTEEN)),
        value(Emphasis::Reserved, tag(*EMPHASIS_RESERVED)),
        value(Emphasis::CcitJ17, tag(*EMPHASIS_CCIT_J17)),
    ))(input)?;

    Ok((input, value))
}

pub(crate) fn variadic_number<'a, N, E>(
    bit_len: usize,
) -> impl Fn(BitsInput<'a>) -> BitsResult<'a, N, E>
where
    N: Default + AddAssign + ShlAssign<usize> + From<u8> + Copy,
    E: BitsError<'a>,
{
    use nom::bytes::complete::take;

    move |input: BitsInput| {
        let mut value = N::default();
        let (input, bits) = take(bit_len)(input)?;
        let one_value = N::from(1_u8);

        for bit in bits.0 {
            value <<= 1_usize;
            if *bit {
                value += one_value;
            }
        }

        Ok((input, value))
    }
}

define_bit_slices! {
    FRAME_MAGIC => [1; 11]
}

fn mode_extension<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, ModeExtension, E>
where
    E: BitsError<'a>,
{
    use nom::sequence::tuple;

    let (input, (intensity_stereo, ms_stereo)) = tuple((parse_bool, parse_bool))(input)?;

    let mode_extension = ModeExtension {
        intensity_stereo,
        ms_stereo,
    };
    Ok((input, mode_extension))
}

pub(crate) fn header<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, Header, E>
where
    E: BitsError<'a>,
{
    use nom::{
        bytes::complete::tag,
        bytes::streaming::take,
        combinator::{map_parser, verify},
        error::context,
        sequence::tuple,
    };

    const HEADER_SIZE: usize = 32;

    let (
        input,
        (
            _magic,
            mpeg_version_id,
            layer,
            crc_protection,
            bit_rate,
            sampling_rate,
            padding,
            private,
            channels,
            mode_extension,
            copyright,
            original,
            emphasis,
        ),
    ) = map_parser(
        take(HEADER_SIZE),
        tuple((
            context("magic", tag(*FRAME_MAGIC)),
            context(
                "mpeg_version_id",
                verify(mpeg_version, MpegVersionId::verify),
            ),
            context("layer", verify(layer, Layer::verify)),
            context("crc_protection", parse_bool),
            context("bit_rate", bit_rate),
            context("sampling_rate", sampling_rate),
            context("padding", parse_bool),
            context("private", parse_bool),
            context("channels", channels),
            context("mode_extension", mode_extension),
            context("copyright", parse_bool),
            context("original", parse_bool),
            context("emphasis", emphasis),
        )),
    )(input)?;

    let header = Header {
        mpeg_version_id,
        layer,
        bitrate: bit_rate,
        sampling_rate,
        channels,
        copyright,
        crc_protection,
        mode_extension,
        original,
        padding,
        private,
        emphasis,
    };

    Ok((input, header))
}

use crate::mp3::utils::BytesData;

pub(crate) fn audio_frame<'a, E>(
    needed: bool,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, AudioFrame, E>
where
    E: CombinedError<'a>,
{
    use nom::{combinator::map, error::context};

    let take = move |count| {
        move |input: BytesInput<'a>| {
            if needed {
                nom::bytes::streaming::take(count)(input)
            } else {
                nom::bytes::complete::take(count)(input)
            }
        }
    };

    move |input: BytesInput<'a>| {
        let (input, header) = context("header", with_bits(header))(input)?;

        let (input, payload) =
            context("data", map(take(header.frame_size()), BytesData::new))(input)?;

        let audio_frame = AudioFrame { header, payload };

        Ok((input, audio_frame))
    }
}

define_bit_slices! {
    META_DATA_MAGIC => [0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE]
}

pub(crate) fn meta_data_flags<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, MetaDataFlags, E>
where
    E: BitsError<'a>,
{
    use nom::{bytes::complete::tag, error::context, sequence::tuple};

    let (input, (_padding, frames, bytes, toc, quality_indicator)) = tuple((
        context("padding", tag(*META_DATA_MAGIC)),
        context("frames", parse_bool),
        context("bytes", parse_bool),
        context("toc", parse_bool),
        context("quality_indicator", parse_bool),
    ))(input)?;

    let meta_data_flags = MetaDataFlags {
        frames,
        bytes,
        toc,
        quality_indicator,
    };

    Ok((input, meta_data_flags))
}

pub(crate) fn tag_revision<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, TagRevision, E>
where
    E: BitsError<'a>,
{
    let (input, revision) = variadic_number::<u8, E>(4)(input)?;

    let tag_revision = if revision == TagRevision::RESERVED_REVISION {
        TagRevision::Reserved(revision)
    } else {
        TagRevision::Revision(revision)
    };

    Ok((input, tag_revision))
}

pub(crate) fn bitrate_method<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, BitrateMethod, E>
where
    E: BitsError<'a>,
{
    let (input, bitrate_method_id) = variadic_number(4)(input)?;

    let bitrate_method = match bitrate_method_id {
        BitrateMethod::UNKNOWN => BitrateMethod::Unknown,
        BitrateMethod::CONSTANT_BITRATE => BitrateMethod::ConstantBitrate,
        BitrateMethod::AVERAGE_BITRATE => BitrateMethod::AverageBitrate,
        BitrateMethod::VARIABLE_BITRATE1 => BitrateMethod::VariableBitrate1,
        BitrateMethod::VARIABLE_BITRATE2 => BitrateMethod::VariableBitrate2,
        BitrateMethod::VARIABLE_BITRATE3 => BitrateMethod::VariableBitrate3,
        BitrateMethod::VARIABLE_BITRATE4 => BitrateMethod::VariableBitrate4,
        BitrateMethod::CONSTANT_BITRATE_2PASS => BitrateMethod::ConstantBitrate2Pass,
        BitrateMethod::AVERAGE_BITRATE_2PASS => BitrateMethod::AverageBitrate2Pass,
        BitrateMethod::RESERVED => BitrateMethod::Reserved,
        other => BitrateMethod::Other(other),
    };

    Ok((input, bitrate_method))
}

fn sized_ascii_string<'a, C, E>(
    size: C,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, AsciiString, E>
where
    C: ToUsize + Copy,
    E: BytesError<'a>,
{
    move |input: BytesInput<'a>| {
        use nom::bytes::streaming::take;

        let (input, chars) = take(size)(input)?;

        Ok((input, AsciiString::new(chars)))
    }
}

pub(crate) fn meta_data<'a, E>(
    header: &Header,
) -> impl FnMut(BytesInput<'a>) -> BytesResult<'a, MetaData, E>
where
    E: CombinedError<'a>,
{
    let header = header.clone();

    move |input: BytesInput<'a>| {
        use nom::{
            bytes::streaming::take,
            combinator::{cond, flat_map, map, map_parser, rest, success, verify},
            error::context,
            number::complete::be_u32,
            sequence::tuple,
        };

        let flagged_values = |flags: MetaDataFlags| {
            // TODO: replace into_vec with trait
            let toc = map(take(MetaData::TOC_SIZE), BytesView::into_vec);

            tuple((
                success(flags.clone()),
                context("frames", cond(flags.frames, be_u32)),
                context("bytes", cond(flags.bytes, be_u32)),
                context("toc", cond(flags.toc, toc)),
                context("quality_indicator", cond(flags.quality_indicator, be_u32)),
            ))
        };

        let meta_data_size = header.frame_size();
        let prefix_padding_size = MetaData::prefix_padding_size(&header);

        let (
            input,
            (
                _prefix_padding,
                magic,
                (flags, frames, bytes, toc, quality_indicator),
                extension_codec,
                (tag_revision, bitrate_method),
                _suffix_padding,
            ),
        ) = map_parser(
            take(meta_data_size),
            tuple((
                context(
                    "prefix_padding",
                    map(take(prefix_padding_size), BytesData::new),
                ),
                context(
                    "magic",
                    verify(
                        sized_ascii_string(MetaData::MAGIC_SIZE),
                        MetaData::check_magic,
                    ),
                ),
                flat_map(context("flags", with_bits(meta_data_flags)), flagged_values),
                context(
                    "extension_codec",
                    sized_ascii_string(MetaData::EXTENSION_CODEC_SIZE),
                ),
                with_bits(tuple((
                    context("tag_revision", tag_revision),
                    context("bitrate_method", bitrate_method),
                ))),
                context("suffix_padding", map(rest, BytesData::new)),
            )),
        )(input)?;

        let meta_data = MetaData {
            magic,
            flags,
            frames,
            bytes,
            toc,
            quality_indicator,
            extension_codec,
            tag_revision,
            bitrate_method,
        };

        Ok((input, meta_data))
    }
}

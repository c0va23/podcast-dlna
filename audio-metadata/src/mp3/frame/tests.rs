use bitvec::prelude::*;

use nom::error::{ErrorKind as NomErrorKind, VerboseErrorKind as NomVerboseErrorKind};

use super::super::test_helpers::{
    build_bitvec_from_bitslice_slice, check_parser_test_cases, ErrorRange, ErrorSrc, ParseTestCase,
};
use super::*;

use pretty_assertions::assert_eq;

#[test]
fn header_frame_size() {
    fn build_frame_header(bitrate: BitRate, sampling_rate: SamplingRate, padding: bool) -> Header {
        Header {
            mpeg_version_id: MpegVersionId::Mpeg1,
            layer: Layer::Iii,
            crc_protection: false,
            bitrate,
            sampling_rate,
            padding,
            channels: Channels::Stereo,
            mode_extension: ModeExtension::default(),
            original: false,
            private: false,
            copyright: false,
            emphasis: Emphasis::None,
        }
    }

    struct TestCase {
        frame_header: Header,
        expected_total_frame_size: usize,
        expected_frame_size: usize,
    }

    let test_cases = &[
        TestCase {
            frame_header: build_frame_header(BitRate::Kbps192, SamplingRate::Hz44100, true),
            expected_total_frame_size: 627,
            expected_frame_size: 623,
        },
        TestCase {
            frame_header: build_frame_header(BitRate::Kbps128, SamplingRate::Hz44100, false),
            expected_total_frame_size: 417,
            expected_frame_size: 413,
        },
        TestCase {
            frame_header: build_frame_header(BitRate::Kbps128, SamplingRate::Hz48000, false),
            expected_total_frame_size: 384,
            expected_frame_size: 380,
        },
        TestCase {
            frame_header: build_frame_header(BitRate::Kbps320, SamplingRate::Hz44100, false),
            expected_total_frame_size: 1044,
            expected_frame_size: 1040,
        },
        TestCase {
            frame_header: build_frame_header(BitRate::Kbps320, SamplingRate::Hz44100, true),
            expected_total_frame_size: 1045,
            expected_frame_size: 1041,
        },
    ];

    for test_case in test_cases {
        let total_frame_size = test_case.frame_header.total_frame_size();
        assert_eq!(test_case.expected_total_frame_size, total_frame_size);

        let frame_size = test_case.frame_header.frame_size();
        assert_eq!(test_case.expected_frame_size, frame_size);
    }
}

#[test]
fn mpeg_version_id() {
    use super::parsers::mpeg_version;

    let test_cases = [
        ParseTestCase {
            name: "MPEG 2.5",
            source_bits: bitvec![Msb0, u8; 0, 0],
            expected_result: Ok(MpegVersionId::Mpeg2_5),
        },
        ParseTestCase {
            name: "Reserved",
            source_bits: bitvec![Msb0, u8; 0, 1],
            expected_result: Ok(MpegVersionId::Reserved),
        },
        ParseTestCase {
            name: "MPEG2",
            source_bits: bitvec![Msb0, u8; 1, 0],
            expected_result: Ok(MpegVersionId::Mpeg2),
        },
        ParseTestCase {
            name: "MPEG1",
            source_bits: bitvec![Msb0, u8; 1, 1],
            expected_result: Ok(MpegVersionId::Mpeg1),
        },
        ParseTestCase {
            name: "truncated",
            source_bits: bitvec![Msb0, u8; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(mpeg_version, &test_cases[..]);
}

#[test]
fn layer() {
    use super::parsers::layer;

    let test_cases = &[
        ParseTestCase {
            name: "Not defined",
            source_bits: bitvec![Msb0, u8; 0, 0],
            expected_result: Ok(Layer::NotDefined),
        },
        ParseTestCase {
            name: "III",
            source_bits: bitvec![Msb0, u8; 0, 1],
            expected_result: Ok(Layer::Iii),
        },
        ParseTestCase {
            name: "II",
            source_bits: bitvec![Msb0, u8; 1, 0],
            expected_result: Ok(Layer::Ii),
        },
        ParseTestCase {
            name: "I",
            source_bits: bitvec![Msb0, u8; 1, 1],
            expected_result: Ok(Layer::I),
        },
        ParseTestCase {
            name: "truncated",
            source_bits: bitvec![Msb0, u8; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(layer, test_cases);
}

#[test]
#[allow(clippy::too_many_lines)]
fn bit_rate() {
    use super::parsers::bit_rate;

    let test_cases = &[
        ParseTestCase {
            name: "Free",
            source_bits: bitvec![Msb0, u8; 0, 0, 0, 0],
            expected_result: Ok(BitRate::Free),
        },
        ParseTestCase {
            name: "Kbps32",
            source_bits: bitvec![Msb0, u8; 0, 0, 0, 1],
            expected_result: Ok(BitRate::Kbps32),
        },
        ParseTestCase {
            name: "Kbps40",
            source_bits: bitvec![Msb0, u8; 0, 0, 1, 0],
            expected_result: Ok(BitRate::Kbps40),
        },
        ParseTestCase {
            name: "Kbps48",
            source_bits: bitvec![Msb0, u8; 0, 0, 1, 1],
            expected_result: Ok(BitRate::Kbps48),
        },
        ParseTestCase {
            name: "Kbps56",
            source_bits: bitvec![Msb0, u8; 0, 1, 0, 0],
            expected_result: Ok(BitRate::Kbps56),
        },
        ParseTestCase {
            name: "Kbps64",
            source_bits: bitvec![Msb0, u8; 0, 1, 0, 1],
            expected_result: Ok(BitRate::Kbps64),
        },
        ParseTestCase {
            name: "Kbps80",
            source_bits: bitvec![Msb0, u8; 0, 1, 1, 0],
            expected_result: Ok(BitRate::Kbps80),
        },
        ParseTestCase {
            name: "Kbps96",
            source_bits: bitvec![Msb0, u8; 0, 1, 1, 1],
            expected_result: Ok(BitRate::Kbps96),
        },
        ParseTestCase {
            name: "Kbps112",
            source_bits: bitvec![Msb0, u8; 1, 0, 0, 0],
            expected_result: Ok(BitRate::Kbps112),
        },
        ParseTestCase {
            name: "Kbps128",
            source_bits: bitvec![Msb0, u8; 1, 0, 0, 1],
            expected_result: Ok(BitRate::Kbps128),
        },
        ParseTestCase {
            name: "Kbps160",
            source_bits: bitvec![Msb0, u8; 1, 0, 1, 0],
            expected_result: Ok(BitRate::Kbps160),
        },
        ParseTestCase {
            name: "Kbps192",
            source_bits: bitvec![Msb0, u8; 1, 0, 1, 1],
            expected_result: Ok(BitRate::Kbps192),
        },
        ParseTestCase {
            name: "Kbps224",
            source_bits: bitvec![Msb0, u8; 1, 1, 0, 0],
            expected_result: Ok(BitRate::Kbps224),
        },
        ParseTestCase {
            name: "Kbps256",
            source_bits: bitvec![Msb0, u8; 1, 1, 0, 1],
            expected_result: Ok(BitRate::Kbps256),
        },
        ParseTestCase {
            name: "Kbps320",
            source_bits: bitvec![Msb0, u8; 1, 1, 1, 0],
            expected_result: Ok(BitRate::Kbps320),
        },
        ParseTestCase {
            name: "Bad",
            source_bits: bitvec![Msb0, u8; 1, 1, 1, 1],
            expected_result: Ok(BitRate::Bad),
        },
        ParseTestCase {
            name: "Truncated",
            source_bits: bitvec![Msb0, u8; 1; 3],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..3),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..3),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(bit_rate, test_cases);
}

#[test]
fn sampling_rate() {
    use super::parsers::sampling_rate;

    let test_cases = &[
        ParseTestCase {
            name: "44100",
            source_bits: bitvec![Msb0, u8; 0, 0],
            expected_result: Ok(SamplingRate::Hz44100),
        },
        ParseTestCase {
            name: "48000",
            source_bits: bitvec![Msb0, u8; 0, 1],
            expected_result: Ok(SamplingRate::Hz48000),
        },
        ParseTestCase {
            name: "32000",
            source_bits: bitvec![Msb0, u8; 1, 0],
            expected_result: Ok(SamplingRate::Hz32000),
        },
        ParseTestCase {
            name: "Reserved",
            source_bits: bitvec![Msb0, u8; 1, 1],
            expected_result: Ok(SamplingRate::Reserved),
        },
        ParseTestCase {
            name: "Truncated",
            source_bits: bitvec![Msb0, u8; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(sampling_rate, test_cases);
}

#[test]
fn channels() {
    use super::parsers::channels;

    let test_cases = &[
        ParseTestCase {
            name: "Stereo",
            source_bits: bitvec![Msb0, u8; 0, 0],
            expected_result: Ok(Channels::Stereo),
        },
        ParseTestCase {
            name: "Join Stereo",
            source_bits: bitvec![Msb0, u8; 0, 1],
            expected_result: Ok(Channels::JointStereo),
        },
        ParseTestCase {
            name: "Dual",
            source_bits: bitvec![Msb0, u8; 1, 0],
            expected_result: Ok(Channels::Dual),
        },
        ParseTestCase {
            name: "Mono",
            source_bits: bitvec![Msb0, u8; 1, 1],
            expected_result: Ok(Channels::Mono),
        },
        ParseTestCase {
            name: "Truncated",
            source_bits: bitvec![Msb0, u8; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(channels, test_cases);
}

#[test]
fn emphasis() {
    use super::parsers::emphasis;

    let test_cases = &[
        ParseTestCase {
            name: "None",
            source_bits: bitvec![Msb0, u8; 0, 0],
            expected_result: Ok(Emphasis::None),
        },
        ParseTestCase {
            name: "50/15 ms",
            source_bits: bitvec![Msb0, u8; 0, 1],
            expected_result: Ok(Emphasis::FiftyFifteen),
        },
        ParseTestCase {
            name: "Reserved",
            source_bits: bitvec![Msb0, u8; 1, 0],
            expected_result: Ok(Emphasis::Reserved),
        },
        ParseTestCase {
            name: "CCIT J.17",
            source_bits: bitvec![Msb0, u8; 1, 1],
            expected_result: Ok(Emphasis::CcitJ17),
        },
        ParseTestCase {
            name: "Truncated",
            source_bits: bitvec![Msb0, u8; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..1),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
            ])),
        },
    ];

    check_parser_test_cases(emphasis, test_cases);
}

#[test]
#[allow(clippy::too_many_lines)]
fn header() {
    use super::parsers::header;

    let test_cases = &[
        ParseTestCase {
            name: "Ok: Default",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG1
                0, 1, // layer: III
                0, // crc
                1, 1, 1, 0, // bit rate: 320
                0, 1, // sampling rate: 48000
                0, // padding
                0, // private
                0, 0, // channels: stereo
                0, 0, // extension
                0, // copyright
                0, // original
                0, 0, // emphasis: none
            ],
            expected_result: Ok(Header {
                mpeg_version_id: MpegVersionId::Mpeg1,
                layer: Layer::Iii,
                crc_protection: false,
                bitrate: BitRate::Kbps320,
                sampling_rate: SamplingRate::Hz48000,
                padding: false,
                private: false,
                channels: Channels::Stereo,
                mode_extension: ModeExtension::default(),
                copyright: false,
                original: false,
                emphasis: Emphasis::None,
            }),
        },
        ParseTestCase {
            name: "Ok: Mode extension 2",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG1
                0, 1, // layer: III
                0, // crc
                1, 1, 1, 0, // bit rate: 320
                0, 1, // sampling rate: 48000
                0, // padding
                0, // private
                0, 0, // channels: stereo
                1, 0, // extension
                0, // copyright
                0, // original
                0, 0, // emphasis: none
            ],
            expected_result: Ok(Header {
                mpeg_version_id: MpegVersionId::Mpeg1,
                layer: Layer::Iii,
                crc_protection: false,
                bitrate: BitRate::Kbps320,
                sampling_rate: SamplingRate::Hz48000,
                padding: false,
                private: false,
                channels: Channels::Stereo,
                mode_extension: ModeExtension {
                    intensity_stereo: true,
                    ms_stereo: false,
                },
                copyright: false,
                original: false,
                emphasis: Emphasis::None,
            }),
        },
        ParseTestCase {
            name: "Ok: High bits",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG1
                0, 1, // layer: III
                1, // crc
                1, 1, 1, 1, // bit rate: 320
                1, 1, // sampling rate: 48000
                1, // padding
                1, // private
                1, 1, // channels: stereo
                1, 1, // extension
                1, // copyright
                1, // original
                1, 1, // emphasis: none
            ],
            expected_result: Ok(Header {
                mpeg_version_id: MpegVersionId::Mpeg1,
                layer: Layer::Iii,
                crc_protection: true,
                bitrate: BitRate::Bad,
                sampling_rate: SamplingRate::Reserved,
                padding: true,
                private: true,
                channels: Channels::Mono,
                mode_extension: ModeExtension {
                    intensity_stereo: true,
                    ms_stereo: true,
                },
                copyright: true,
                original: true,
                emphasis: Emphasis::CcitJ17,
            }),
        },
        ParseTestCase {
            name: "Ok: Low bits",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG1
                0, 1, // layer: III
                0, // crc
                0, 0, 0, 0, // bit rate: 320
                0, 0, // sampling rate: 48000
                0, // padding
                0, // private
                0, 0, // channels: stereo
                0, 0, // extension
                0, // copyright
                0, // original
                0, 0, // emphasis: none
            ],
            expected_result: Ok(Header {
                mpeg_version_id: MpegVersionId::Mpeg1,
                layer: Layer::Iii,
                crc_protection: false,
                bitrate: BitRate::Free,
                sampling_rate: SamplingRate::Hz44100,
                padding: false,
                private: false,
                channels: Channels::Stereo,
                mode_extension: ModeExtension::default(),
                copyright: false,
                original: false,
                emphasis: Emphasis::None,
            }),
        },
        ParseTestCase {
            name: "Err: partial magic",
            source_bits: bitvec![
                Msb0, u8;
                1, // partial magic
            ],
            expected_result: Err(ErrorSrc::Incomplete(31)),
        },
        ParseTestCase {
            name: "Err: partial mpeg version",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, // mpeg version id: partial
            ],
            expected_result: Err(ErrorSrc::Incomplete(20)),
        },
        ParseTestCase {
            name: "Err: invalid magic value",
            source_bits: bitvec![
                Msb0, u8;
                0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // invalid magic
                // tail
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..32),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..32),
                    NomVerboseErrorKind::Context("magic"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: invalid mpeg version",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 0, // mpeg version id: MPEG2 (unexpected)
                // tail
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0,
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(11..32),
                    NomVerboseErrorKind::Nom(NomErrorKind::Verify),
                ),
                (
                    ErrorRange::Bits(11..32),
                    NomVerboseErrorKind::Context("mpeg_version_id"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: partial layer",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG2
                1, // layer: partial
            ],
            expected_result: Err(ErrorSrc::Incomplete(18)),
        },
        ParseTestCase {
            name: "Err: invalid layer",
            source_bits: bitvec![
                Msb0, u8;
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                1, 1, // mpeg version id: MPEG2
                1, 1, // layer: I (unexpected)
                // Tail
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0,
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(13..32),
                    NomVerboseErrorKind::Nom(NomErrorKind::Verify),
                ),
                (
                    ErrorRange::Bits(13..32),
                    NomVerboseErrorKind::Context("layer"),
                ),
            ])),
        },
    ];

    check_parser_test_cases(header, test_cases);
}

#[test]
#[allow(clippy::too_many_lines)]
fn audio_frame() {
    use super::parsers::audio_frame;

    let test_cases = &[
        {
            let frame_data = &[0_u8; 92];
            ParseTestCase {
                name: "Ok: 32/48000",
                source_bits: build_bitvec_from_bitslice_slice(&[
                    bits![
                        Msb0, u8;
                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                        1, 1, // mpeg version id: MPEG1
                        0, 1, // layer: III
                        0, // crc
                        0, 0, 0, 1, // bit rate: 32
                        0, 1, // sampling rate: 48000
                        0, // padding
                        0, // private
                        0, 0, // channels: stereo
                        0, 0, // mode extension
                        0, // copyright
                        0, // original
                        0, 0, // emphasis: none
                    ],
                    frame_data.view_bits(),
                ]),
                expected_result: Ok(AudioFrame {
                    header: Header {
                        mpeg_version_id: MpegVersionId::Mpeg1,
                        layer: Layer::Iii,
                        crc_protection: false,
                        bitrate: BitRate::Kbps32,
                        sampling_rate: SamplingRate::Hz48000,
                        padding: false,
                        private: false,
                        channels: Channels::Stereo,
                        mode_extension: ModeExtension::default(),
                        copyright: false,
                        original: false,
                        emphasis: Emphasis::None,
                    },
                    payload: BytesData::new(frame_data),
                }),
            }
        },
        {
            let frame_data = &[1_u8; 1436];
            ParseTestCase {
                name: "Ok: 320/32000",
                source_bits: build_bitvec_from_bitslice_slice(&[
                    bits![
                        Msb0, u8;
                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                        1, 1, // mpeg version id: MPEG1
                        0, 1, // layer: III
                        0, // crc
                        1, 1, 1, 0, // bit rate: 320
                        1, 0, // sampling rate: 32000
                        0, // padding
                        0, // private
                        0, 0, // channels: stereo
                        0, 0, // mode extension
                        0, // copyright
                        0, // original
                        0, 0, // emphasis: none
                    ],
                    frame_data.view_bits(),
                ]),
                expected_result: Ok(AudioFrame {
                    header: Header {
                        mpeg_version_id: MpegVersionId::Mpeg1,
                        layer: Layer::Iii,
                        crc_protection: false,
                        bitrate: BitRate::Kbps320,
                        sampling_rate: SamplingRate::Hz32000,
                        padding: false,
                        private: false,
                        channels: Channels::Stereo,
                        mode_extension: ModeExtension::default(),
                        copyright: false,
                        original: false,
                        emphasis: Emphasis::None,
                    },
                    payload: BytesData::new(frame_data),
                }),
            }
        },
        {
            let frame_data = vec![1_u8; 0];
            ParseTestCase {
                name: "Err: 320/32000 partial",
                source_bits: build_bitvec_from_bitslice_slice(&[
                    bits![
                        Msb0, u8;
                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // magic
                        1, 1, // mpeg version id: MPEG1
                        0, 1, // layer: III
                        0, // crc
                        1, 1, 1, 0, // bit rate: 320
                        1, 0, // sampling rate: 32000
                        0, // padding
                        0, // private
                        0, 0, // channels: stereo
                        0, 0, // mode extension
                        0, // copyright
                        0, // original
                        0, 0, // emphasis: none
                    ],
                    frame_data.view_bits(),
                ]),
                expected_result: Err(ErrorSrc::Error(vec![
                    (
                        ErrorRange::Bytes(4..4),
                        NomVerboseErrorKind::Nom(NomErrorKind::Eof),
                    ),
                    (
                        ErrorRange::Bytes(4..4),
                        NomVerboseErrorKind::Context("data"),
                    ),
                ])),
            }
        },
    ];

    check_parser_test_cases(audio_frame(false), test_cases);
}

#[test]
#[allow(clippy::too_many_lines)]
fn meta_data_flags() {
    use super::parsers::meta_data_flags;

    let test_cases = &[
        ParseTestCase {
            name: "Ok: low bits",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                0, // frames
                0, // bytes
                0, // toc
                0, // quality indicator
            ],
            expected_result: Ok(MetaDataFlags {
                frames: false,
                bytes: false,
                toc: false,
                quality_indicator: false,
            }),
        },
        ParseTestCase {
            name: "Ok: frames",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                1, // frames
                0, // bytes
                0, // toc
                0, // quality indicator
            ],
            expected_result: Ok(MetaDataFlags {
                frames: true,
                bytes: false,
                toc: false,
                quality_indicator: false,
            }),
        },
        ParseTestCase {
            name: "Ok: bytes",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                0, // frames
                1, // bytes
                0, // toc
                0, // quality indicator
            ],
            expected_result: Ok(MetaDataFlags {
                frames: false,
                bytes: true,
                toc: false,
                quality_indicator: false,
            }),
        },
        ParseTestCase {
            name: "Ok: toc",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                0, // frames
                0, // bytes
                1, // toc
                0, // quality indicator
            ],
            expected_result: Ok(MetaDataFlags {
                frames: false,
                bytes: false,
                toc: true,
                quality_indicator: false,
            }),
        },
        ParseTestCase {
            name: "Ok: toc",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                0, // frames
                0, // bytes
                0, // toc
                1, // quality indicator
            ],
            expected_result: Ok(MetaDataFlags {
                frames: false,
                bytes: false,
                toc: false,
                quality_indicator: true,
            }),
        },
        ParseTestCase {
            name: "Err: incomplete padding bits",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..16),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..16),
                    NomVerboseErrorKind::Context("padding"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: not clean padding",
            source_bits: bitvec![Msb0, u8; 1; 24], // not clean padding
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..24),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..24),
                    NomVerboseErrorKind::Context("padding"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: not clean padding high",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 1, // not clean padding hight
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..28),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..28),
                    NomVerboseErrorKind::Context("padding"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: missed quality indicator",
            source_bits: bitvec![
                Msb0, u8;
                0, 0, 0, 0, 0, 0, 0, 0, // padding 0
                0, 0, 0, 0, 0, 0, 0, 0, // padding 1
                0, 0, 0, 0, 0, 0, 0, 0, // padding 2
                0, 0, 0, 0, // padding high
                0, // frames
                0, // bytes
                0, // toc
            ],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(31..31),
                    NomVerboseErrorKind::Nom(NomErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(31..31),
                    NomVerboseErrorKind::Nom(NomErrorKind::Alt),
                ),
                (
                    ErrorRange::Bits(31..31),
                    NomVerboseErrorKind::Context("quality_indicator"),
                ),
            ])),
        },
    ];

    check_parser_test_cases(meta_data_flags, test_cases);
}

#[test]
fn tag_version() {
    use super::parsers::tag_revision;

    let test_cases = &[
        ParseTestCase {
            name: "low revision",
            source_bits: bitvec![Msb0, u8; 0, 0, 0, 0],
            expected_result: Ok(TagRevision::Revision(0)),
        },
        ParseTestCase {
            name: "high revision",
            source_bits: bitvec![Msb0, u8; 1, 1, 1, 0],
            expected_result: Ok(TagRevision::Revision(14)),
        },
        ParseTestCase {
            name: "high reserved",
            source_bits: bitvec![Msb0, u8; 1, 1, 1, 1],
            expected_result: Ok(TagRevision::Reserved(15)),
        },
        ParseTestCase {
            name: "Err: partial",
            source_bits: bitvec![Msb0, u8; 1, 1, 1],
            expected_result: Err(ErrorSrc::Error(vec![(
                ErrorRange::Bits(0..3),
                NomVerboseErrorKind::Nom(NomErrorKind::Eof),
            )])),
        },
    ];

    check_parser_test_cases(tag_revision, test_cases);
}

#[test]
#[allow(clippy::too_many_lines)]
fn meta_data() {
    use super::parsers::meta_data;

    let header = Header {
        mpeg_version_id: MpegVersionId::Mpeg1,
        layer: Layer::Iii,
        crc_protection: false,
        bitrate: BitRate::Kbps128,
        sampling_rate: SamplingRate::Hz32000,
        padding: false,
        private: false,
        channels: Channels::Stereo,
        mode_extension: ModeExtension::default(),
        copyright: false,
        original: false,
        emphasis: Emphasis::None,
    };

    let test_cases = &[
        ParseTestCase {
            name: "Ok: all flags low [Xing]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Xing".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, // frames
                    0, // bytes
                    0, // toc
                    0, // quality indicator
                ],
                // MetaData data
                b"123456789".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 0, 0, // tag revision: 0
                    0, 0, 0, 1, // bitrate method: constant
                ],
                vec![0_u8; 522].view_bits(), // suffix padding

            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Xing".into()),
                flags: MetaDataFlags {
                    frames: false,
                    bytes: false,
                    toc: false,
                    quality_indicator: false,
                },
                frames: None,
                bytes: None,
                toc: None,
                tag_revision: TagRevision::Revision(0),
                quality_indicator: None,
                extension_codec: AsciiString::new(b"123456789".into()),
                bitrate_method: BitrateMethod::ConstantBitrate,
            }),
        },
        ParseTestCase {
            name: "Ok: all flags high [Info]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    1, // frames
                    1, // bytes
                    1, // toc
                    1, // quality indicator
                ],
                // MetaData data
                vec![
                    0, 0, 0, 0xFF, // frames:
                    0, 0, 0xFF, 0, // bytes
                ].view_bits(),
                vec![7_u8; MetaData::TOC_SIZE].view_bits(), // TOC
                vec![0, 0xFF, 0, 0].view_bits(), // quality indicator
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 410].view_bits(), // suffix padding
            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Info".into()),
                flags: MetaDataFlags {
                    frames: true,
                    bytes: true,
                    toc: true,
                    quality_indicator: true,
                },
                frames: Some(0xFF),
                bytes: Some(0xFF00),
                toc: Some(vec![7; MetaData::TOC_SIZE]),
                quality_indicator: Some(0xFF_00_00),
                tag_revision: TagRevision::Revision(2),
                extension_codec: AsciiString::new(b"987654321".into()),
                bitrate_method: BitrateMethod::AverageBitrate,
            }),
        },
        ParseTestCase {
            name: "Ok: frames flags high [Info]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    1, // frames
                    0, // bytes
                    0, // toc
                    0, // quality indicator
                ],
                // MetaData data
                vec![0xFF, 0, 0, 0].view_bits(), // frames:
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 518].view_bits(), // suffix padding

            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Info".into()),
                flags: MetaDataFlags {
                    frames: true,
                    bytes: false,
                    toc: false,
                    quality_indicator: false,
                },
                frames: Some(0xFF_00_00_00),
                bytes: None,
                toc: None,
                quality_indicator: None,
                tag_revision: TagRevision::Revision(2),
                extension_codec: AsciiString::new(b"987654321".into()),
                bitrate_method: BitrateMethod::AverageBitrate,
            }),
        },
        ParseTestCase {
            name: "Ok: frames flags high [Info]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, // frames
                    1, // bytes
                    0, // toc
                    0, // quality indicator
                ],
                // MetaData data
                vec![0, 0, 0x02, 0].view_bits(), // bytes:
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 518].view_bits(), // suffix padding
            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Info".into()),
                flags: MetaDataFlags {
                    frames: false,
                    bytes: true,
                    toc: false,
                    quality_indicator: false,
                },
                frames: None,
                bytes: Some(0x00_00_02_00),
                toc: None,
                quality_indicator: None,
                tag_revision: TagRevision::Revision(2),
                extension_codec: AsciiString::new(b"987654321".into()),
                bitrate_method: BitrateMethod::AverageBitrate,
            }),
        },
        ParseTestCase {
            name: "Ok: quality indicator flags high [Info]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, // frames
                    0, // bytes
                    0, // toc
                    1, // quality indicator
                ],
                // MetaData data
                vec![0, 0x67, 0, 0].view_bits(), // quality indicator:
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 518].view_bits(), // suffix padding
            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Info".into()),
                flags: MetaDataFlags {
                    frames: false,
                    bytes: false,
                    toc: false,
                    quality_indicator: true,
                },
                frames: None,
                bytes: None,
                toc: None,
                quality_indicator: Some(0x67_00_00),
                tag_revision: TagRevision::Revision(2),
                extension_codec: AsciiString::new(b"987654321".into()),
                bitrate_method: BitrateMethod::AverageBitrate,
            }),
        },
        ParseTestCase {
            name: "Ok: toc flags high [Info]",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, // frames
                    0, // bytes
                    1, // toc
                    0, // quality indicator
                ],
                // MetaData data
                vec![123_u8; MetaData::TOC_SIZE].view_bits(), // TOC
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 422].view_bits(), // suffix padding
            ]),
            expected_result: Ok(MetaData {
                magic: AsciiString::new(b"Info".into()),
                flags: MetaDataFlags {
                    frames: false,
                    bytes: false,
                    toc: true,
                    quality_indicator: false,
                },
                frames: None,
                bytes: None,
                toc: Some(vec![123; MetaData::TOC_SIZE]),
                quality_indicator: None,
                tag_revision: TagRevision::Revision(2),
                extension_codec: AsciiString::new(b"987654321".into()),
                bitrate_method: BitrateMethod::AverageBitrate,
            }),
        },
        ParseTestCase {
            name: "Err: incomplete prefix padding",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE - 1].view_bits(), // prefix padding
            ]),
            expected_result: Err(ErrorSrc::Incomplete(541)),
        },
        ParseTestCase {
            name: "Err: invalid magic",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![1_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"TEXT".view_bits(), // magic: invalid
                vec![0; 536].view_bits(), // tail
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(32..572),
                    NomVerboseErrorKind::Nom(NomErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(32..572),
                    NomVerboseErrorKind::Context("magic"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: truncated flags",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags: truncated
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, 0, 0, 0, // flags
                ],
            ]),
            expected_result: Err(ErrorSrc::Incomplete(532)),
        },
        ParseTestCase {
            name: "Err: truncated suffix",
            #[rustfmt::skip]
            source_bits: build_bitvec_from_bitslice_slice(&[
                // MetaData prefix
                vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE].view_bits(), // prefix padding
                b"Info".view_bits(), // magic
                // MetaDataFlags
                bits![Msb0, u8; 0; MetaDataFlags::PADDING_MAGIC_BITS_SIZE], // padding
                bits![
                    Msb0, u8;
                    0, // frames
                    0, // bytes
                    0, // toc
                    1, // quality indicator
                ],
                // MetaData data
                vec![0, 0x67, 0, 0].view_bits(), // quality indicator:
                b"987654321".view_bits(), // extension codec
                bits![
                    Msb0, u8;
                    0, 0, 1, 0, // tag revision: 0
                    0, 0, 1, 0, // bitrate method: average
                ],
                vec![0_u8; 421].view_bits(), // suffix padding
            ]),
            expected_result: Err(ErrorSrc::Incomplete(97)),
        },
    ];

    check_parser_test_cases(meta_data(&header), test_cases);
}

#[test]
fn bitrate_method() {
    use super::parsers::bitrate_method;

    let test_cases = &[
        ParseTestCase {
            name: "err: partial",
            source_bits: bitvec![Msb0, u8; 1, 1, 1],
            expected_result: Err(ErrorSrc::Error(vec![(
                ErrorRange::Bits(0..3),
                NomVerboseErrorKind::Nom(NomErrorKind::Eof),
            )])),
        },
        ParseTestCase {
            name: "unknown",
            source_bits: bitvec![Msb0, u8; 0, 0, 0, 0],
            expected_result: Ok(BitrateMethod::Unknown),
        },
        ParseTestCase {
            name: "constant",
            source_bits: bitvec![Msb0, u8; 0, 0, 0, 1],
            expected_result: Ok(BitrateMethod::ConstantBitrate),
        },
        ParseTestCase {
            name: "average",
            source_bits: bitvec![Msb0, u8; 0, 0, 1, 0],
            expected_result: Ok(BitrateMethod::AverageBitrate),
        },
        ParseTestCase {
            name: "variable 1",
            source_bits: bitvec![Msb0, u8; 0, 0, 1, 1],
            expected_result: Ok(BitrateMethod::VariableBitrate1),
        },
        ParseTestCase {
            name: "variable 2",
            source_bits: bitvec![Msb0, u8; 0, 1, 0, 0],
            expected_result: Ok(BitrateMethod::VariableBitrate2),
        },
        ParseTestCase {
            name: "variable 3",
            source_bits: bitvec![Msb0, u8; 0, 1, 0, 1],
            expected_result: Ok(BitrateMethod::VariableBitrate3),
        },
        ParseTestCase {
            name: "variable 4",
            source_bits: bitvec![Msb0, u8; 0, 1, 1, 0],
            expected_result: Ok(BitrateMethod::VariableBitrate4),
        },
        ParseTestCase {
            name: "constant 2 pass",
            source_bits: bitvec![Msb0, u8; 1, 0, 0, 0],
            expected_result: Ok(BitrateMethod::ConstantBitrate2Pass),
        },
        ParseTestCase {
            name: "average 2 pass",
            source_bits: bitvec![Msb0, u8; 1, 0, 0, 1],
            expected_result: Ok(BitrateMethod::AverageBitrate2Pass),
        },
        ParseTestCase {
            name: "reserved",
            source_bits: bitvec![Msb0, u8; 1, 1, 1, 1],
            expected_result: Ok(BitrateMethod::Reserved),
        },
        ParseTestCase {
            name: "other(7)",
            source_bits: bitvec![Msb0, u8; 0, 1, 1, 1],
            expected_result: Ok(BitrateMethod::Other(7)),
        },
        ParseTestCase {
            name: "other(10)",
            source_bits: bitvec![Msb0, u8; 1, 0, 1, 0],
            expected_result: Ok(BitrateMethod::Other(10)),
        },
    ];

    check_parser_test_cases(bitrate_method, test_cases);
}

#[test]
fn bitrate_value() {
    let test_cases = vec![
        (BitRate::Kbps32, 32_000),
        (BitRate::Kbps40, 40_000),
        (BitRate::Kbps48, 48_000),
        (BitRate::Kbps56, 56_000),
        (BitRate::Kbps64, 64_000),
        (BitRate::Kbps80, 80_000),
        (BitRate::Kbps96, 96_000),
        (BitRate::Kbps112, 112_000),
        (BitRate::Kbps128, 128_000),
        (BitRate::Kbps160, 160_000),
        (BitRate::Kbps192, 192_000),
        (BitRate::Kbps224, 224_000),
        (BitRate::Kbps256, 256_000),
        (BitRate::Kbps320, 320_000),
        (BitRate::Bad, 0),
        (BitRate::Free, 0),
    ];

    for (bit_rate, expected_value) in test_cases {
        assert_eq!(
            expected_value,
            bit_rate.value(),
            "Case {value}",
            value = expected_value,
        );
    }
}

#[test]
fn sampling_rate_value() {
    let test_cases = [
        (SamplingRate::Hz44100, 44_100),
        (SamplingRate::Hz48000, 48_000),
        (SamplingRate::Hz32000, 32_000),
        (SamplingRate::Reserved, 0),
    ];

    for (sampling_rate, expected_value) in test_cases {
        assert_eq!(
            expected_value,
            sampling_rate.value(),
            "Case {value}",
            value = expected_value,
        );
    }
}

#[test]
fn meta_data_prefix_padding_size() {
    fn build_header(mpeg_version_id: MpegVersionId, channels: Channels) -> Header {
        Header {
            bitrate: BitRate::Free,
            channels,
            mpeg_version_id,
            copyright: false,
            original: false,
            crc_protection: false,
            emphasis: Emphasis::None,
            layer: Layer::Iii,
            mode_extension: ModeExtension::default(),
            padding: false,
            private: false,
            sampling_rate: SamplingRate::Hz32000,
        }
    }

    let not_mono_channel = Channels::Stereo;
    let test_cases = [
        (
            "MPEG1/NOT MONO",
            build_header(MpegVersionId::Mpeg1, not_mono_channel),
            36 - MetaData::HEADER_SIZE,
        ),
        (
            "MPEG1/MONO",
            build_header(MpegVersionId::Mpeg1, Channels::Mono),
            21 - MetaData::HEADER_SIZE,
        ),
        (
            "MPEG2/NOT MONO",
            build_header(MpegVersionId::Mpeg2, not_mono_channel),
            21 - MetaData::HEADER_SIZE,
        ),
        (
            "MPEG2/MONO",
            build_header(MpegVersionId::Mpeg2, Channels::Mono),
            13 - MetaData::HEADER_SIZE,
        ),
    ];

    for (name, header, expected_size) in test_cases {
        assert_eq!(
            expected_size,
            MetaData::prefix_padding_size(&header),
            "Case {name}",
            name = name,
        );
    }
}

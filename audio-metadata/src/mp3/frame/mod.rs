#![allow(clippy::struct_excessive_bools)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::default_trait_access)]

/// MP3 frame structure
/// See: [`mp3Frame`](https://id3.org/mp3Frame)
/// See: [`MP3inside`](http://www.multiweb.cz/twoinches/MP3inside.htm)
/// See: [`MP3 tag info`](http://gabriel.mp3-tech.org/mp3infotag.html)
/// Implemented ONLY MPEG1 Layer III.
use super::utils::BytesData;
use super::BytesInput;

mod parsers;

pub(crate) use parsers::audio_frame;
pub(crate) use parsers::meta_data;

#[cfg(test)]
mod tests;

#[derive(Debug, Clone, PartialEq, Copy)]
pub enum MpegVersionId {
    // not official
    Mpeg2_5,
    Reserved,
    Mpeg2,
    Mpeg1,
}

impl MpegVersionId {
    #[allow(clippy::trivially_copy_pass_by_ref)]
    fn verify(&self) -> bool {
        *self == Self::Mpeg1
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Layer {
    NotDefined,
    Iii,
    Ii,
    I,
}

impl Layer {
    fn verify(&self) -> bool {
        *self == Layer::Iii
    }
}

// BitRate for MPEG1 Layer III
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BitRate {
    Free,
    Kbps32,
    Kbps40,
    Kbps48,
    Kbps56,
    Kbps64,
    Kbps80,
    Kbps96,
    Kbps112,
    Kbps128,
    Kbps160,
    Kbps192,
    Kbps224,
    Kbps256,
    Kbps320,
    Bad,
}

impl BitRate {
    // Return bitrate value (in bits into second)
    #[must_use]
    pub const fn value(self) -> usize {
        match self {
            Self::Kbps32 => 32_000,
            Self::Kbps40 => 40_000,
            Self::Kbps48 => 48_000,
            Self::Kbps56 => 56_000,
            Self::Kbps64 => 64_000,
            Self::Kbps80 => 80_000,
            Self::Kbps96 => 96_000,
            Self::Kbps112 => 112_000,
            Self::Kbps128 => 128_000,
            Self::Kbps160 => 160_000,
            Self::Kbps192 => 192_000,
            Self::Kbps224 => 224_000,
            Self::Kbps256 => 256_000,
            Self::Kbps320 => 320_000,
            // TODO: Return result
            Self::Bad | Self::Free => 0,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SamplingRate {
    Hz44100,
    Hz48000,
    Hz32000,
    Reserved,
}

impl SamplingRate {
    pub(crate) const fn value(self) -> usize {
        match self {
            Self::Hz44100 => 44_100,
            Self::Hz48000 => 48_000,
            Self::Hz32000 => 32_000,
            Self::Reserved => 0,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Channels {
    Stereo,
    JointStereo,
    Dual,
    Mono,
}

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct ModeExtension {
    pub intensity_stereo: bool,
    pub ms_stereo: bool,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Emphasis {
    None,
    // 50/15ms,
    FiftyFifteen,
    Reserved,
    // CCIT J.17
    CcitJ17,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Header {
    pub mpeg_version_id: MpegVersionId,
    pub layer: Layer,
    pub crc_protection: bool,
    pub bitrate: BitRate,
    pub sampling_rate: SamplingRate,
    pub padding: bool,
    pub private: bool,
    pub channels: Channels,
    pub mode_extension: ModeExtension,
    pub copyright: bool,
    pub original: bool,
    pub emphasis: Emphasis,
}

impl Header {
    const HEADER_SIZE: usize = 4;
    const COEF: usize = 144;

    pub(crate) fn frame_size(&self) -> usize {
        self.total_frame_size() - Self::HEADER_SIZE
    }

    const fn padding_size(padding: bool) -> usize {
        if padding {
            1
        } else {
            0
        }
    }

    const fn calculate_frame_size(
        bitrate: BitRate,
        sampling_rate: SamplingRate,
        padding: bool,
    ) -> usize {
        Self::COEF * bitrate.value() / sampling_rate.value() + Self::padding_size(padding)
    }

    pub(crate) fn total_frame_size(&self) -> usize {
        Self::calculate_frame_size(self.bitrate, self.sampling_rate, self.padding)
    }

    pub(crate) const MAX_FRAME_SIZE: usize =
        Self::calculate_frame_size(BitRate::Kbps320, SamplingRate::Hz32000, true);
}

#[derive(Debug, PartialEq, Clone)]
pub struct AudioFrame<'a> {
    pub header: Header,
    pub payload: BytesData<'a>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct MetaDataFlags {
    pub frames: bool,
    pub bytes: bool,
    pub toc: bool,
    pub quality_indicator: bool,
}

impl MetaDataFlags {
    const PADDING_MAGIC_BITS_SIZE: usize = 28;
}

#[derive(PartialEq, Clone)]
pub struct AsciiString(Vec<u8>);

impl AsciiString {
    #[allow(clippy::needless_pass_by_value)]
    #[must_use]
    pub(crate) fn new(chars: BytesInput) -> Self {
        Self(chars.to_vec())
    }
}

impl std::fmt::Debug for AsciiString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", String::from_utf8_lossy(&self.0[..]))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TagRevision {
    Revision(u8),
    Reserved(u8),
}

impl TagRevision {
    const RESERVED_REVISION: u8 = 0b1111;
}

#[derive(Debug, PartialEq, Clone)]
pub enum BitrateMethod {
    Unknown,
    ConstantBitrate,
    AverageBitrate,
    VariableBitrate1,
    VariableBitrate2,
    VariableBitrate3,
    VariableBitrate4,
    ConstantBitrate2Pass,
    AverageBitrate2Pass,
    Reserved,
    Other(u8),
}

impl BitrateMethod {
    const UNKNOWN: u8 = 0;
    const CONSTANT_BITRATE: u8 = 1;
    const AVERAGE_BITRATE: u8 = 2;
    const VARIABLE_BITRATE1: u8 = 3;
    const VARIABLE_BITRATE2: u8 = 4;
    const VARIABLE_BITRATE3: u8 = 5;
    const VARIABLE_BITRATE4: u8 = 6;
    const CONSTANT_BITRATE_2PASS: u8 = 8;
    const AVERAGE_BITRATE_2PASS: u8 = 9;
    const RESERVED: u8 = 15;
}

impl BitrateMethod {
    pub(crate) fn is_constant(&self) -> bool {
        std::matches!(self, Self::ConstantBitrate | Self::ConstantBitrate2Pass)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct MetaData {
    pub magic: AsciiString,
    pub flags: MetaDataFlags,
    pub frames: Option<u32>,
    pub bytes: Option<u32>,
    pub toc: Option<Vec<u8>>,
    pub quality_indicator: Option<u32>,
    pub extension_codec: AsciiString,
    pub tag_revision: TagRevision,
    pub bitrate_method: BitrateMethod,
}

impl MetaData {
    const XING: &'static [u8] = b"Xing";
    const INFO: &'static [u8] = b"Info";

    const MAGIC_SIZE: usize = 4;
    pub(crate) const TOC_SIZE: usize = 100;

    pub(crate) const EXTENSION_CODEC_SIZE: usize = 9;

    const HEADER_SIZE: usize = 4;
    pub(crate) const MPEG1_NOT_MONO_PADDING_SIZE: usize = 36 - Self::HEADER_SIZE;
    const MPEG1_MONO_PADDING_SIZE: usize = 21 - Self::HEADER_SIZE;
    const MPEG2_NOT_MONO_PADDING_SIZE: usize = 21 - Self::HEADER_SIZE;
    const MPEG2_MONO_PADDING_SIZE: usize = 13 - Self::HEADER_SIZE;

    fn check_magic(magic: &AsciiString) -> bool {
        magic.0 == Self::XING || magic.0 == Self::INFO
    }

    fn prefix_padding_size(header: &Header) -> usize {
        match (header.mpeg_version_id, header.channels) {
            (MpegVersionId::Mpeg1, Channels::Mono) => Self::MPEG1_MONO_PADDING_SIZE,
            (MpegVersionId::Mpeg2, Channels::Mono) => Self::MPEG2_MONO_PADDING_SIZE,
            (MpegVersionId::Mpeg1, _) => Self::MPEG1_NOT_MONO_PADDING_SIZE,
            (MpegVersionId::Mpeg2, _) => Self::MPEG2_NOT_MONO_PADDING_SIZE,
            _ => unreachable!(),
        }
    }
}

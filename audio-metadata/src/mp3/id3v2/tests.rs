use super::super::utils::BytesData;
use super::*;
use crate::mp3::test_helpers::{
    bitvec_from_byte_slices, bitvec_from_bytes, check_parser_test_cases, ErrorRange, ErrorSrc,
    ParseTestCase,
};
use bitvec::prelude::*;

use nom::error::{ErrorKind, VerboseErrorKind};

use pretty_assertions::assert_eq;

#[test]
fn frame_id_value_match_id() {
    #![allow(clippy::similar_names)]

    let pic = FrameId3 { chars: *b"PIC" };
    let apic = FrameId4 { chars: *b"APIC" };
    let com = FrameId3 { chars: *b"COM" };

    assert!(super::ids::ATTACHED_PICTURE.match_id(super::FrameId::Three(pic)));
    assert!(super::ids::ATTACHED_PICTURE.match_id(super::FrameId::Four(apic)));
    assert!(!super::ids::ATTACHED_PICTURE.match_id(super::FrameId::Three(com)));
}

#[test]
#[allow(clippy::too_many_lines)]
fn synchsafe_integer() {
    use super::parsers::synchsafe_integer;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: zero",
            source_bits: bitvec_from_bytes(&[0b0, 0b0, 0b0, 0b0]),
            expected_result: Ok(SynchsafeInteger(0)),
        },
        ParseTestCase {
            name: "Ok: one bit",
            source_bits: bitvec_from_bytes(&[0b0, 0b0, 0b0, 0b1]),
            expected_result: Ok(SynchsafeInteger(1)),
        },
        ParseTestCase {
            name: "Ok: full one byte",
            source_bits: bitvec_from_bytes(&[0b0, 0b0, 0b0, 0b0111_1111]),
            expected_result: Ok(SynchsafeInteger(127)),
        },
        ParseTestCase {
            name: "Ok: next byte",
            source_bits: bitvec_from_bytes(&[0b0, 0b0, 0b1, 0b0111_1111]),
            expected_result: Ok(SynchsafeInteger(255)),
        },
        ParseTestCase {
            name: "OK: first bit in all bytes",
            source_bits: bitvec_from_bytes(&[0b1, 0b1, 0b1, 0b1]),
            expected_result: Ok(SynchsafeInteger(2_113_665)),
        },
        ParseTestCase {
            name: "Ok: last bit in all bytes",
            source_bits: bitvec_from_bytes(&[0b0100_0000, 0b0100_0000, 0b0100_0000, 0b0100_0000]),
            expected_result: Ok(SynchsafeInteger(135_274_560)),
        },
        ParseTestCase {
            name: "Ok: all possible bits",
            source_bits: bitvec_from_bytes(&[0b0111_1111, 0b0111_1111, 0b0111_1111, 0b0111_1111]),
            expected_result: Ok(SynchsafeInteger(268_435_455)),
        },
        ParseTestCase {
            name: "Err: not clean high bit in first byte",
            source_bits: bitvec_from_bytes(&[0b0, 0b0, 0b0, 0b1111_1111]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(3..4),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: not clean high bit in third byte",
            source_bits: bitvec_from_bytes(&[0b0, 0b1111_1111, 0b0, 0b0]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(1..4),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: bad size",
            source_bits: bitvec_from_bytes(&[0b0, 0b0]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(2..2),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (
                    ErrorRange::Bytes(0..2),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
            ])),
        },
    ];

    check_parser_test_cases(synchsafe_integer, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn header_flags() {
    use super::parsers::header_flags;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: zero",
            source_bits: bitvec_from_bytes(&[0b0000_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: false,
                extended_header: false,
                experimental: false,
                footer: false,
            }),
        },
        ParseTestCase {
            name: "Ok: unsynchronization",
            source_bits: bitvec_from_bytes(&[0b1000_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: true,
                extended_header: false,
                experimental: false,
                footer: false,
            }),
        },
        ParseTestCase {
            name: "Ok: extended_header",
            source_bits: bitvec_from_bytes(&[0b0100_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: false,
                extended_header: true,
                experimental: false,
                footer: false,
            }),
        },
        ParseTestCase {
            name: "Ok: experimental",
            source_bits: bitvec_from_bytes(&[0b0010_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: false,
                extended_header: false,
                experimental: true,
                footer: false,
            }),
        },
        ParseTestCase {
            name: "Ok: footer",
            source_bits: bitvec_from_bytes(&[0b0001_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: false,
                extended_header: false,
                experimental: false,
                footer: true,
            }),
        },
        ParseTestCase {
            name: "Ok: all",
            source_bits: bitvec_from_bytes(&[0b1111_0000]),
            expected_result: Ok(HeaderFlags {
                unsynchronization: true,
                extended_header: true,
                experimental: true,
                footer: true,
            }),
        },
        ParseTestCase {
            name: "Err: Empty input",
            source_bits: bitvec![Msb0, u8; 0; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bits(0..0),
                    VerboseErrorKind::Nom(ErrorKind::Tag),
                ),
                (
                    ErrorRange::Bits(0..0),
                    VerboseErrorKind::Nom(ErrorKind::Alt),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: not zero reserved",
            source_bits: bitvec_from_bytes(&[0b0000_1111]),
            expected_result: Err(ErrorSrc::Error(vec![(
                ErrorRange::Bits(4..8),
                VerboseErrorKind::Nom(ErrorKind::Tag),
            )])),
        },
    ];
    check_parser_test_cases(header_flags, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn header() {
    use super::parsers::header;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: Simple",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                3, 0, // Version
                0, // Flags
                0, 0, 0, 0, // Size
            ]),
            expected_result: Ok(Header {
                version_major: 3,
                version_minor: 0,
                flags: HeaderFlags {
                    unsynchronization: false,
                    extended_header: false,
                    experimental: false,
                    footer: false,
                },
                size: SynchsafeInteger(0),
            }),
        },
        ParseTestCase {
            name: "Ok: Filled",
            #[rustfmt::skip]
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                4, 0, // Version
                0b1111_0000, // Flags
                0, 0, 1, 127, // Size
            ]),
            expected_result: Ok(Header {
                version_major: 4,
                version_minor: 0,
                flags: HeaderFlags {
                    unsynchronization: true,
                    extended_header: true,
                    experimental: true,
                    footer: true,
                },
                size: SynchsafeInteger(255),
            }),
        },
        ParseTestCase {
            name: "Err: Missing magic value",
            source_bits: bitvec_from_bytes(&[0]),
            expected_result: Err(ErrorSrc::Incomplete(9)),
        },
        ParseTestCase {
            name: "Err: Invalid magic value",
            source_bits: bitvec_from_bytes(&[b'X'; 10]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..10),
                    VerboseErrorKind::Nom(ErrorKind::Tag),
                ),
                (ErrorRange::Bytes(0..10), VerboseErrorKind::Context("magic")),
            ])),
        },
        ParseTestCase {
            name: "Err: Missing major version",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
            ]),
            expected_result: Err(ErrorSrc::Incomplete(7)),
        },
        ParseTestCase {
            name: "Err: Missing minor version",
            #[rustfmt::skip]
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                3, // Partial version
            ]),
            expected_result: Err(ErrorSrc::Incomplete(6)),
        },
        ParseTestCase {
            name: "Err: Missing flags",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                3, 0, // Version
            ]),
            expected_result: Err(ErrorSrc::Incomplete(5)),
        },
        ParseTestCase {
            name: "Err: Missing size",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                3, 0, // Version
                0, // Flags
            ]),
            expected_result: Err(ErrorSrc::Incomplete(4)),
        },
        ParseTestCase {
            name: "Err: not full size",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', b'3', // Tag
                3, 0, // Version
                0, // Flags
                0, 0, 0, // Partial size
            ]),
            expected_result: Err(ErrorSrc::Incomplete(1)),
        },
    ];
    check_parser_test_cases(header, &test_cases[..]);
}

#[test]
fn extended_header() {
    use super::parsers::extended_header;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: empty",
            source_bits: bitvec_from_bytes(&[
                0, 0, 0, 4, // size
            ]),
            expected_result: Ok(ExtendedHeader {
                size: SynchsafeInteger(4),
                data: BytesData::new(&[]),
            }),
        },
        ParseTestCase {
            name: "Ok: small",
            source_bits: bitvec_from_bytes(&[
                0, 0, 0, 8, // size
                0, 1, 2, 3, // data
            ]),
            expected_result: Ok(ExtendedHeader {
                size: SynchsafeInteger(8),
                data: BytesData::new(&[0, 1, 2, 3]),
            }),
        },
        ParseTestCase {
            name: "Err: partial size",
            source_bits: bitvec_from_bytes(&[
                0, 0, // partial size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(0..0),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (
                    ErrorRange::Bytes(0..2),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
                (ErrorRange::Bytes(0..2), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: too small size",
            source_bits: bitvec_from_bytes(&[
                0, 0, 0, 3, // small size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(0..4), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial data",
            source_bits: bitvec_from_bytes(&[
                0, 0, 0, 8, // size
                1, 2, // partial data
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(4..6),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(4..6), VerboseErrorKind::Context("data")),
            ])),
        },
    ];
    check_parser_test_cases(extended_header, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn frame_v2() {
    use super::parsers::frame_v2;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: simple",
            source_bits: bitvec_from_bytes(&[
                b'T', b'X', b'T', // id
                0, 0, 3, // size
                0, 1, 2, // data
            ]),
            expected_result: Ok(FrameV2 {
                id: FrameId3::new(*b"TXT"),
                size: 3,
                data: BytesData::new(&[0, 1, 2]),
            }),
        },
        ParseTestCase {
            name: "Ok: min",
            source_bits: bitvec_from_bytes(&[
                b'T', b'X', b'T', // id
                0, 0, 1, // size
                0, // data
            ]),
            expected_result: Ok(FrameV2 {
                id: FrameId3::new(*b"TXT"),
                size: 1,
                data: BytesData::new(&[0]),
            }),
        },
        ParseTestCase {
            name: "Err: empty",
            source_bits: bitvec_from_bytes(&[
                b'P', b'I', b'C', // id
                0, 0, 0, // size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(3..6),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(3..6), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: incomplete id",
            source_bits: bitvec_from_bytes(&[
                b'I', b'D', // id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..2),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(0..2), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: invalid id",
            source_bits: bitvec_from_bytes(&[
                b'=', b'=', b'=', // id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..3),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(0..3), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: miss size",
            source_bits: bitvec_from_bytes(&[
                b'T', b'X', b'T', // id
            ]),
            // TODO: possible should be NomError
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(3..3),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(3..3), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: miss data",
            source_bits: bitvec_from_bytes(&[
                b'T', b'X', b'T', // id
                0, 0, 4, // size
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(3..3),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(3..3), VerboseErrorKind::Context("data")),
            ])),
        },
    ];
    check_parser_test_cases(frame_v2, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn frame_v3() {
    use super::parsers::frame_v3;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: min",
            source_bits: bitvec_from_bytes(&[
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 1, // size
                0, 0, // flags,
                0, // data
            ]),
            expected_result: Ok(FrameV3 {
                id: FrameId4::new(*b"TEXT"),
                flags: FrameFlags::clean(),
                size: 1,
                data: BytesData::new(&[0]),
            }),
        },
        ParseTestCase {
            name: "Ok: some data",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 4, // size
                1, 0, // flags,
                1, 2, 3, 4, // data
            ]),
            expected_result: Ok(FrameV3 {
                id: FrameId4::new(*b"APIC"),
                flags: FrameFlags { flags: 256 },
                size: 4,
                data: BytesData::new(&[1, 2, 3, 4]),
            }),
        },
        ParseTestCase {
            name: "Err: partial id",
            source_bits: bitvec_from_bytes(&[
                b'P', b'I', b'C', // partial id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..3),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(0..3), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: invalid id",
            source_bits: bitvec_from_bytes(&[
                b'-', b'-', b'-', b'-', // invalid id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(0..4), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial size",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, // partial size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(4..7),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(4..7), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial flags",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 1, // size
                0, // partial flags
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(8..9),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(8..9), VerboseErrorKind::Context("flags")),
            ])),
        },
        ParseTestCase {
            name: "Err: empty",
            source_bits: bitvec_from_bytes(&[
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 0, // size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(4..8),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(4..8), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial data",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 1, // size
                0, 0, // flags
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(10..10),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(10..10), VerboseErrorKind::Context("data")),
            ])),
        },
    ];

    check_parser_test_cases(frame_v3, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn frame_v4() {
    use super::parsers::frame_v4;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: min",
            source_bits: bitvec_from_bytes(&[
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 1, // size
                0, 0, // flags,
                0, // data
            ]),
            expected_result: Ok(FrameV4 {
                id: FrameId4::new(*b"TEXT"),
                flags: FrameFlags::clean(),
                size: SynchsafeInteger(1),
                data: BytesData::new(&[0]),
            }),
        },
        ParseTestCase {
            name: "Ok: data",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 4, // size
                1, 0, // flags,
                1, 2, 3, 4, // data
            ]),
            expected_result: Ok(FrameV4 {
                id: FrameId4::new(*b"APIC"),
                flags: FrameFlags { flags: 256 },
                size: SynchsafeInteger(4),
                data: BytesData::new(&[1, 2, 3, 4]),
            }),
        },
        ParseTestCase {
            name: "Err: partial id",
            source_bits: bitvec_from_bytes(&[
                b'P', b'I', b'C', // partial id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..3),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(0..3), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: invalid id",
            source_bits: bitvec_from_bytes(&[
                b'-', b'-', b'-', b'-', // invalid id
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(0..4), VerboseErrorKind::Context("id")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial size",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, // partial size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(7..7),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (
                    ErrorRange::Bytes(4..7),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
                (ErrorRange::Bytes(4..7), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial flags",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 1, // size
                0, // partial flags
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(8..9),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(8..9), VerboseErrorKind::Context("flags")),
            ])),
        },
        ParseTestCase {
            name: "Err: empty",
            source_bits: bitvec_from_bytes(&[
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 0, // size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(4..8),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(4..8), VerboseErrorKind::Context("size")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial data",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 1, // size
                0, 0, // flags
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(10..10),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(10..10), VerboseErrorKind::Context("data")),
            ])),
        },
        ParseTestCase {
            name: "Err: partial data",
            source_bits: bitvec_from_bytes(&[
                b'A', b'P', b'I', b'C', // id
                0, 0, 0, 2, // size
                0, 0, // flags
                0,
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(10..11),
                    VerboseErrorKind::Nom(ErrorKind::Eof),
                ),
                (ErrorRange::Bytes(10..11), VerboseErrorKind::Context("data")),
            ])),
        },
    ];

    check_parser_test_cases(frame_v4, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
fn footer() {
    use super::parsers::footer;

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: Simple",
            source_bits: bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                3, 0, // Version
                0, // Flags
                0, 0, 0, 0, // Size
            ]),
            expected_result: Ok(Footer {
                version_major: 3,
                version_minor: 0,
                flags: HeaderFlags {
                    unsynchronization: false,
                    extended_header: false,
                    experimental: false,
                    footer: false,
                },
                size: SynchsafeInteger(0),
            }),
        },
        ParseTestCase {
            name: "Ok: Filled",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                4, 0, // Version
                0b1111_0000, // Flags
                0, 0, 1, 127, // Size
            ]),
            expected_result: Ok(Footer {
                version_major: 4,
                version_minor: 0,
                flags: HeaderFlags {
                    unsynchronization: true,
                    extended_header: true,
                    experimental: true,
                    footer: true,
                },
                size: SynchsafeInteger(255),
            }),
        },
        ParseTestCase {
            name: "Err: invalid magic",
            #[rustfmt::skip]
            source_bits: bitvec_from_bytes(&[0; 10]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..10),
                    VerboseErrorKind::Nom(ErrorKind::Tag),
                ),
                (ErrorRange::Bytes(0..10), VerboseErrorKind::Context("magic")),
            ])),
        },
        ParseTestCase {
            name: "Err: Missing magic value",
            source_bits: bitvec_from_bytes(&[0]),
            expected_result: Err(ErrorSrc::Incomplete(9)),
        },
        ParseTestCase {
            name: "Err: Missing major version",
            source_bits: bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
            ]),
            expected_result: Err(ErrorSrc::Incomplete(7)),
        },
        ParseTestCase {
            name: "Err: Missing minor version",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                3, // Partial version
            ]),
            expected_result: Err(ErrorSrc::Incomplete(6)),
        },
        ParseTestCase {
            name: "Err: Missing flags",
            source_bits: bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                3, 0, // Version
            ]),
            expected_result: Err(ErrorSrc::Incomplete(5)),
        },
        ParseTestCase {
            name: "Err: Missing size",
            source_bits: bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                3, 0, // Version
                0, // Flags
            ]),
            expected_result: Err(ErrorSrc::Incomplete(4)),
        },
        ParseTestCase {
            name: "Err: not full size",
            source_bits: bitvec_from_bytes(&[
                b'3', b'D', b'I', // Tag
                3, 0, // Version
                0, // Flags
                0, 0, 0, // Partial size
            ]),
            expected_result: Err(ErrorSrc::Incomplete(1)),
        },
    ];

    check_parser_test_cases(footer, &test_cases[..]);
}

#[test]
#[allow(clippy::too_many_lines)]
#[allow(clippy::redundant_clone)]
fn id3v2() {
    use super::parsers::id3v2;

    let long_frame_data = vec![
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 1
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 2
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 3
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 4
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 5
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 6
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 7
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 8
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 9
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 10
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 11
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', // 12
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', 0, // 13
    ];

    let test_cases = vec![
        ParseTestCase {
            name: "Ok: V2 with one frame",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // magic
                2, 0, // version
                0, // flags
                0, 0, 0, 12, // header size
                // Frame
                b'T', b'X', b'T', // id
                0, 0, 6, // size,
                b'H', b'e', b'l', b'l', b'o', 0,
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 2,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(12),
                },
                extended_header: None,
                frames: vec![Frame::V2(FrameV2 {
                    id: FrameId3::new(*b"TXT"),
                    size: 6_u32,
                    data: BytesData::new(b"Hello\0"),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V3 with one frame",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                3, 0, // version
                0, // flags
                0, 0, 0, 16, // header size
                // Frame
                b'T', b'E', b'X', b'T', // FrameID
                0, 0, 0, 6, // Frame size,
                0, 0, // Flags,
                b'H', b'e', b'l', b'l', b'o', 0,
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 3,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(16),
                },
                extended_header: None,
                frames: vec![Frame::V3(FrameV3 {
                    id: FrameId4::new(*b"TEXT"),
                    size: 6,
                    flags: FrameFlags::clean(),
                    data: BytesData::new(b"Hello\0"),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V3 with one frame and padding",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                3, 0, // version
                0, // flags
                0, 0, 0, 20, // header size
                // Frame
                b'T', b'E', b'X', b'T', // FrameID
                0, 0, 0, 6, // Frame size,
                0, 0, // Flags,
                b'H', b'e', b'l', b'l', b'o', 0, // frame payload
                0, 0, 0, 0, // padding
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 3,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(20),
                },
                extended_header: None,
                frames: vec![Frame::V3(FrameV3 {
                    id: FrameId4::new(*b"TEXT"),
                    size: 6,
                    flags: FrameFlags::clean(),
                    data: BytesData::new(b"Hello\0"),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V4 with one TEXT frame",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0, // flags
                0, 0, 0, 16, // header size
                // Frame
                b'T', b'E', b'X', b'T', // FrameID
                0, 0, 0, 6, // Frame size,
                0, 0, // Flags,
                b'H', b'e', b'l', b'l', b'o', 0,
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(16),
                },
                extended_header: None,
                frames: vec![Frame::V4(FrameV4 {
                    id: FrameId4::new(*b"TEXT"),
                    size: SynchsafeInteger(6),
                    flags: FrameFlags::clean(),
                    data: BytesData::new(b"Hello\0"),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V4 with one TEXT frame and footer",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // magic
                4, 0,  // version
                16, // flags: footer
                0, 0, 0, 16, // size
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 6, // size,
                0, 0, // flags,
                b'H', b'e', b'l', b'l', b'o', 0, // data
                // Footer
                b'3', b'D', b'I', // magic
                4, 0,  // version
                16, // flags: footer
                0, 0, 0, 16, // header size
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags {
                        footer: true,
                        ..HeaderFlags::default()
                    },
                    size: SynchsafeInteger(16),
                },
                extended_header: None,
                frames: vec![Frame::V4(FrameV4 {
                    id: FrameId4::new(*b"TEXT"),
                    size: SynchsafeInteger(6),
                    flags: FrameFlags::clean(),
                    data: BytesData::new(b"Hello\0"),
                })],
                footer: Some(Footer {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags {
                        footer: true,
                        ..HeaderFlags::default()
                    },
                    size: SynchsafeInteger(16),
                }),
            }),
        },
        ParseTestCase {
            name: "Ok: V3 with one long TEXT frame",
            source_bits: bitvec_from_byte_slices([
                &[
                    // Header
                    b'I', b'D', b'3', // Magic
                    3, 0, // version
                    0, // flags
                    0, 0, 1, 11, // header size
                    // Frame
                    b'T', b'E', b'X', b'T', // id
                    0, 0, 0, 129, // size,
                    0, 0, // flags,
                ],
                &long_frame_data[..],
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 3,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(139),
                },
                extended_header: None,
                frames: vec![Frame::V3(FrameV3 {
                    id: FrameId4::new(*b"TEXT"),
                    size: 129,
                    flags: FrameFlags::clean(),
                    // TODO: share data block after rewrite to nom
                    data: BytesData::new(&long_frame_data[..]),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V4 with one long TEXT frame",
            source_bits: bitvec_from_byte_slices([
                &[
                    // Header
                    b'I', b'D', b'3', // Magic
                    4, 0, // version
                    0, // flags
                    0, 0, 1, 11, // size
                    // Frame
                    b'T', b'E', b'X', b'T', // id
                    0, 0, 1, 1, // size,
                    0, 0, // flags,
                ],
                &long_frame_data[..],
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags::default(),
                    size: SynchsafeInteger(139),
                },
                extended_header: None,
                frames: vec![Frame::V4(FrameV4 {
                    id: FrameId4::new(*b"TEXT"),
                    size: SynchsafeInteger(129),
                    flags: FrameFlags::clean(),
                    data: BytesData::new(&long_frame_data[..]),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V4 with extended header",
            #[rustfmt::skip]
            source_bits:  bitvec_from_byte_slices([
                &[
                    // Header
                    b'I', b'D', b'3', // Magic
                    4, 0, // version
                    0b0100_0000, // flags: extended header
                    0, 0, 1, 16, // size
                    // Extended header
                    0, 0, 1, 5, // size,
                ],
                &long_frame_data[..], // extended header data
                &[
                    // Frame
                    b'T', b'E', b'X', b'T', // id
                    0, 0, 0, 1, // size,
                    0, 0, // flags,
                    0, // data
                ],
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags {
                        extended_header: true,
                        ..HeaderFlags::default()
                    },
                    size: SynchsafeInteger(144),
                },
                extended_header: Some(ExtendedHeader {
                    size: SynchsafeInteger(133),
                    data: BytesData::new(&long_frame_data[..]),
                }),
                frames: vec![Frame::V4(FrameV4 {
                    id: FrameId4::new(*b"TEXT"),
                    size: SynchsafeInteger(1),
                    flags: FrameFlags::clean(),
                    data: BytesData::new(&[0]),
                })],
                footer: None,
            }),
        },
        ParseTestCase {
            name: "Ok: V4 with TEXT and COMM frames, padding and footer",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // magic
                4, 0,  // version
                0b0101_0000, // flags: footer, ext header
                0, 0, 0, 40, // size
                // Extended header
                0, 0, 0, 6, // size
                0, 0, // flags
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 6, // size,
                0, 0, // flags,
                b'H', b'e', b'l', b'l', b'o', 0, // data
                // Frame
                b'C', b'O', b'M', b'M', // id
                0, 0, 0, 3, // size,
                0, 0, // flags,
                b'1', b'2', b'3', // data
                // padding
                0, 0, 0, 0, 0,
                // Footer
                b'3', b'D', b'I', // magic
                4, 0,  // version
                0b0101_0000, // flags: footer, ext header
                0, 0, 0, 40, // header size
            ]),
            expected_result: Ok(Id3V2 {
                header: Header {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags {
                        footer: true,
                        extended_header: true,
                        ..HeaderFlags::default()
                    },
                    size: SynchsafeInteger(40),
                },
                extended_header: Some(ExtendedHeader {
                    size: SynchsafeInteger(6),
                    data: BytesData::new(&[0; 2]),
                }),
                frames: vec![
                    Frame::V4(FrameV4 {
                        id: FrameId4::new(*b"TEXT"),
                        size: SynchsafeInteger(6),
                        flags: FrameFlags::clean(),
                        data: BytesData::new(b"Hello\0"),
                    }),
                    Frame::V4(FrameV4 {
                        id: FrameId4::new(*b"COMM"),
                        size: SynchsafeInteger(3),
                        flags: FrameFlags::clean(),
                        data: BytesData::new(b"123"),
                    }),
                ],
                footer: Some(Footer {
                    version_major: 4,
                    version_minor: 0,
                    flags: HeaderFlags {
                        footer: true,
                        extended_header: true,
                        ..HeaderFlags::default()
                    },
                    size: SynchsafeInteger(40),
                }),
            }),
        },
        ParseTestCase {
            name: "Err: with partial magic",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', // magic: partial
            ]),
            expected_result: Err(ErrorSrc::Incomplete(8)),
        },
        ParseTestCase {
            name: "Err: V4 without any frame",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0b0000_0000, // flags: none
                0, 0, 0, 4, // size
                // padding
                0, 0, 0, 0, // data: ignored
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(10..14), VerboseErrorKind::Context("id")),
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Context("frame_v4"),
                ),
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Nom(ErrorKind::Many1),
                ),
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Context("frames"),
                ),
                (ErrorRange::Bytes(10..14), VerboseErrorKind::Context("body")),
            ])),
        },
        ParseTestCase {
            name: "Err: Unexpected version with frame",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                5, 0, // version
                0b0000_0000, // flags: none
                0, 0, 0, 4, // size
                // Unknown frame
                0, 0, 0, 0, // size,
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Nom(ErrorKind::Fail),
                ),
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Context("unknown"),
                ),
                (
                    ErrorRange::Bytes(10..14),
                    VerboseErrorKind::Context("frames"),
                ),
                (ErrorRange::Bytes(10..14), VerboseErrorKind::Context("body")),
            ])),
        },
        ParseTestCase {
            name: "Err: V4 with invalid frame size",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0, // flags
                0, 0, 0, 8, // size
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 129, // size: invalid
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(17..18),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(14..18),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
                (ErrorRange::Bytes(14..18), VerboseErrorKind::Context("size")),
                (
                    ErrorRange::Bytes(10..18),
                    VerboseErrorKind::Context("frame_v4"),
                ),
                (
                    ErrorRange::Bytes(10..18),
                    VerboseErrorKind::Context("frames"),
                ),
                (ErrorRange::Bytes(10..18), VerboseErrorKind::Context("body")),
            ])),
        },
        ParseTestCase {
            name: "Err: V3 with invalid header size",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // magic
                3, 0, // version
                0, // flags
                0, 0, 0, 139, // header size
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(9..10),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(6..10),
                    VerboseErrorKind::Nom(ErrorKind::Count),
                ),
                (ErrorRange::Bytes(6..10), VerboseErrorKind::Context("size")),
                (
                    ErrorRange::Bytes(0..10),
                    VerboseErrorKind::Context("header"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: V4 with not clean padding",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0, // flags
                0, 0, 0, 15, // size
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 1, // size,
                0, 0, // flags,
                0, // data
                // padding
                1, 2, 3, 4, // not clean
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(21..25),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (
                    ErrorRange::Bytes(21..25),
                    VerboseErrorKind::Context("padding"),
                ),
                (ErrorRange::Bytes(10..25), VerboseErrorKind::Context("body")),
            ])),
        },
        ParseTestCase {
            name: "Err: V4 with empty frame",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0, // flags
                0, 0, 0, 10, // size
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 0, // size: invalid,
                0, 0, // flags,
            ]),
            expected_result: Err(ErrorSrc::Failure(vec![
                (
                    ErrorRange::Bytes(14..20),
                    VerboseErrorKind::Nom(ErrorKind::Verify),
                ),
                (ErrorRange::Bytes(14..20), VerboseErrorKind::Context("size")),
                (
                    ErrorRange::Bytes(10..20),
                    VerboseErrorKind::Context("frame_v4"),
                ),
                (
                    ErrorRange::Bytes(10..20),
                    VerboseErrorKind::Context("frames"),
                ),
                (ErrorRange::Bytes(10..20), VerboseErrorKind::Context("body")),
            ])),
        },
        ParseTestCase {
            name: "Err: V4 to low size",
            source_bits: bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // Magic
                4, 0, // version
                0, // flags
                0, 0, 0, 20, // size: overflow
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 1, // size: invalid,
                0, 0, // flags,
                1, // data
            ]),
            expected_result: Err(ErrorSrc::Incomplete(9)),
        },
        ParseTestCase {
            name: "Err: V4 footer flag without footer",
            #[rustfmt::skip]
            source_bits:  bitvec_from_bytes(&[
                // Header
                b'I', b'D', b'3', // magic
                4, 0,  // version
                0b0001_0000, // flags: footer
                0, 0, 0, 16, // size
                // Frame
                b'T', b'E', b'X', b'T', // id
                0, 0, 0, 6, // size,
                0, 0, // flags,
                b'H', b'e', b'l', b'l', b'o', 0, // data
            ]),
            expected_result: Err(ErrorSrc::Incomplete(10)),
        },
    ];

    check_parser_test_cases(id3v2, &test_cases[..]);
}

#[test]
fn frame_id_is_valid_chars() {
    struct TestCase {
        chars: [u8; 4],
        want_valid: bool,
    }

    let test_cases = &[
        TestCase {
            chars: *b"TEXT",
            want_valid: true,
        },
        TestCase {
            chars: *b"AAAA",
            want_valid: true,
        },
        TestCase {
            chars: *b"ZZZZ",
            want_valid: true,
        },
        TestCase {
            chars: *b"0000",
            want_valid: true,
        },
        TestCase {
            chars: *b"9999",
            want_valid: true,
        },
        TestCase {
            chars: *b"abcd",
            want_valid: false,
        },
        TestCase {
            chars: *b"    ",
            want_valid: false,
        },
    ];

    for test_case in test_cases {
        let is_valid = FrameId::validate_chars(&test_case.chars[..]);
        assert_eq!(test_case.want_valid, is_valid);
    }
}

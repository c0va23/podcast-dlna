#![allow(clippy::struct_excessive_bools)]
#![allow(clippy::default_trait_access)]

/// ID3V2 header structure
/// See: [`ID3 tag version 2.4.0 - Main Structure`](https://id3.org/id3v2.4.0-structure)
use conv::prelude::*;

use super::utils::BytesData;
use super::BytesInput;

pub(crate) mod frames;
mod parsers;

#[cfg(test)]
mod tests;

pub(crate) use parsers::id3v2;

pub use frames::*;

#[derive(Debug, PartialEq, Clone)]
pub struct Id3V2<'a> {
    pub header: Header,
    pub extended_header: Option<ExtendedHeader<'a>>,
    pub frames: Vec<Frame<'a>>,
    pub footer: Option<Footer>,
}

impl<'a> Id3V2<'a> {
    pub(crate) const HEADER_SIZE: usize = 10;

    pub(crate) fn total_size(&self) -> usize {
        let footer_size = if self.header.flags.footer {
            Self::HEADER_SIZE
        } else {
            0
        };

        self.header.size.0 as usize + Self::HEADER_SIZE + footer_size
    }

    // Return frames slice
    #[must_use]
    pub fn frames(&self) -> &[Frame] {
        &self.frames[..]
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Header {
    pub version_major: u8,
    pub version_minor: u8,
    pub flags: HeaderFlags,
    pub size: SynchsafeInteger,
}

impl Header {
    fn total_size(&self) -> usize {
        self.size.value()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Footer {
    pub version_major: u8,
    pub version_minor: u8,
    pub flags: HeaderFlags,
    pub size: SynchsafeInteger,
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct HeaderFlags {
    pub unsynchronization: bool,
    pub extended_header: bool,
    pub experimental: bool,
    pub footer: bool,
}

#[derive(Debug, PartialEq, Clone)]
pub struct ExtendedHeader<'a> {
    pub size: SynchsafeInteger,
    pub data: BytesData<'a>,
}

impl<'a> ExtendedHeader<'a> {
    const MIN_SIZE: usize = SynchsafeInteger::BYTES_SIZE;

    fn validate_size(size: &SynchsafeInteger) -> bool {
        size.value() >= ExtendedHeader::MIN_SIZE
    }

    fn data_size(size: &SynchsafeInteger) -> usize {
        size.value() - SynchsafeInteger::BYTES_SIZE
    }
}

#[derive(PartialEq, Clone, Copy)]
pub struct FrameId3 {
    pub chars: [u8; Self::BYTES_SIZE],
}

impl FrameId3 {
    const BYTES_SIZE: usize = 3;

    const fn new(chars: [u8; Self::BYTES_SIZE]) -> Self {
        Self { chars }
    }

    // Pass by ref for verify parser
    #[allow(clippy::trivially_copy_pass_by_ref)]
    fn is_valid(&self) -> bool {
        FrameId::validate_chars(&self.chars[..])
    }
}

impl std::fmt::Debug for FrameId3 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{id} {vec:?}", id = self.to_string(), vec = self.chars)
    }
}

impl std::fmt::Display for FrameId3 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let id = String::from_utf8_lossy(&self.chars[..]);
        write!(f, "{}", id)
    }
}

#[derive(PartialEq, Clone, Copy)]
pub struct FrameId4 {
    pub chars: [u8; Self::BYTES_SIZE],
}

impl FrameId4 {
    const BYTES_SIZE: usize = 4;

    const fn new(chars: [u8; Self::BYTES_SIZE]) -> Self {
        Self { chars }
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    fn is_valid(&self) -> bool {
        FrameId::validate_chars(&self.chars[..])
    }
}

impl std::fmt::Debug for FrameId4 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{id} {vec:?}", id = self.to_string(), vec = self.chars)
    }
}

impl std::fmt::Display for FrameId4 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let id = String::from_utf8_lossy(&self.chars[..]);
        write!(f, "{}", id)
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum FrameId {
    Three(FrameId3),
    Four(FrameId4),
}

impl FrameId {
    const VALID_CHARS: std::ops::RangeInclusive<u8> = b'A'..=b'Z';
    const VALID_DIGIT: std::ops::RangeInclusive<u8> = b'0'..=b'9';

    fn is_valid_char(c: u8) -> bool {
        Self::VALID_CHARS.contains(&c) || Self::VALID_DIGIT.contains(&c)
    }

    fn validate_chars(chars: &[u8]) -> bool {
        chars.iter().all(|c| Self::is_valid_char(*c))
    }
}

impl std::fmt::Display for FrameId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Three(id) => id.fmt(f),
            Self::Four(id) => id.fmt(f),
        }
    }
}

pub struct FrameIdValue {
    three: FrameId3,
    four: FrameId4,
}

impl FrameIdValue {
    const fn new(three: [u8; 3], four: [u8; 4]) -> Self {
        Self {
            three: FrameId3::new(three),
            four: FrameId4::new(four),
        }
    }

    /// Match value with id any frame versions
    #[must_use]
    pub fn match_id(&self, id: FrameId) -> bool {
        match id {
            FrameId::Three(three) => self.three == three,
            FrameId::Four(four) => self.four == four,
        }
    }
}

pub mod ids {
    use super::FrameIdValue;

    pub const ATTACHED_PICTURE: FrameIdValue = FrameIdValue::new(*b"PIC", *b"APIC");
}

#[derive(Debug, PartialEq, Clone)]
pub enum Frame<'a> {
    V2(FrameV2<'a>),
    V3(FrameV3<'a>),
    V4(FrameV4<'a>),
}

impl<'a> Frame<'a> {
    /// Return frame ID
    #[must_use]
    pub fn id(&self) -> FrameId {
        match self {
            Self::V2(FrameV2 { id, .. }) => FrameId::Three(*id),
            Self::V3(FrameV3 { id, .. }) | Self::V4(FrameV4 { id, .. }) => FrameId::Four(*id),
        }
    }

    /// Return frame size exclude header size
    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn size(&self) -> usize {
        use conv::prelude::*;

        match self {
            Self::V2(FrameV2 { size, .. }) | Self::V3(FrameV3 { size, .. }) => *size,
            Self::V4(FrameV4 { size, .. }) => size.0,
        }
        .value_into()
        .unwrap() // This unwrap is safe
    }

    /// Return frame data
    #[must_use]
    pub fn data(&self) -> &[u8] {
        match self {
            Frame::V2(frame) => frame.data.data(),
            Frame::V3(frame) => frame.data.data(),
            Frame::V4(frame) => frame.data.data(),
        }
    }

    pub(crate) fn view(&self) -> BytesInput {
        match self {
            Frame::V2(frame) => frame.data.view(),
            Frame::V3(frame) => frame.data.view(),
            Frame::V4(frame) => frame.data.view(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct FrameV4<'a> {
    pub id: FrameId4,
    pub size: SynchsafeInteger,
    pub flags: FrameFlags,
    pub data: BytesData<'a>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FrameV3<'a> {
    pub id: FrameId4,
    pub size: u32,
    pub flags: FrameFlags,
    pub data: BytesData<'a>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FrameV2<'a> {
    pub id: FrameId3,
    pub size: u32,
    pub data: BytesData<'a>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FrameFlags {
    pub flags: u16,
}

impl FrameFlags {
    #[must_use]
    const fn new(flags: u16) -> Self {
        Self { flags }
    }

    /// Create clean flags
    #[must_use]
    #[allow(dead_code)]
    const fn clean() -> Self {
        Self::new(0)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SynchsafeInteger(pub(crate) u32);

impl SynchsafeInteger {
    #[allow(clippy::missing_panics_doc)]
    pub(crate) fn value(&self) -> usize {
        // Unwrap is safe
        usize::value_from(self.0).unwrap()
    }
}

impl SynchsafeInteger {
    const BYTES_SIZE: usize = 4;
    const BYTE_MASK: u8 = 0b0111_1111;
}

impl nom::ToUsize for SynchsafeInteger {
    fn to_usize(&self) -> usize {
        self.value()
    }
}

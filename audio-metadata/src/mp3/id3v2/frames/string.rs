use lazy_static::lazy_static;
use nom::error::{ErrorKind, FromExternalError, ParseError};

use super::super::super::errors::{BytesError, NomVerboseError};
use super::super::super::{BytesInput, BytesResult};
use super::TextEncoding;

lazy_static! {
    static ref TEXT_ENCODING_ISO_8859_1: &'static [u8] = &[TextEncoding::ISO_8859_1_VALUE];
    static ref TEXT_ENCODING_UTF_16: &'static [u8] = &[TextEncoding::UTF_16_VALUE];
    static ref TEXT_ENCODING_UTF_16_BE: &'static [u8] = &[TextEncoding::UTF_16_BE_VALUE];
    static ref TEXT_ENCODING_UTF_8: &'static [u8] = &[TextEncoding::UTF_8_VALUE];
}

pub(crate) fn text_encoding<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, TextEncoding, E>
where
    E: BytesError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::value};

    let (input, text_encoding) = alt((
        value(TextEncoding::Iso8859_1, tag(*TEXT_ENCODING_ISO_8859_1)),
        value(TextEncoding::Utf16, tag(*TEXT_ENCODING_UTF_16)),
        value(TextEncoding::Utf16Be, tag(*TEXT_ENCODING_UTF_16_BE)),
        value(TextEncoding::Utf8, tag(*TEXT_ENCODING_UTF_8)),
    ))(input)?;

    Ok((input, text_encoding))
}

pub(crate) enum StringError {
    Utf16(std::string::FromUtf16Error),
    Utf8(std::string::FromUtf8Error),
    Ascii,
}

impl<'a> FromExternalError<BytesInput<'a>, StringError> for NomVerboseError {
    fn from_external_error(input: BytesInput, kind: ErrorKind, _e: StringError) -> Self {
        NomVerboseError::from_error_kind(input, kind)
    }
}

pub(crate) trait FromStringError<'a>:
    FromExternalError<BytesInput<'a>, StringError>
{
}

impl<'a> FromStringError<'a> for NomVerboseError {}

#[allow(clippy::needless_pass_by_value)]
fn string_from_utf_8(chars: BytesInput) -> Result<String, StringError> {
    String::from_utf8(chars.to_vec()).map_err(StringError::Utf8)
}

fn u8_chars<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, BytesInput, E>
where
    E: BytesError<'a>,
{
    use nom::{
        bytes::complete::{tag, take_until},
        sequence::tuple,
    };

    let null = &TextEncoding::NULL_ONE_BYTE[..];
    let (input, (chars, _null)) = tuple((
        take_until(null),
        tag(null), // Skip null
    ))(input)?;

    Ok((input, chars))
}

fn string_from_iso8859_1(chars: BytesInput) -> Result<String, StringError> {
    // TODO: map char bigger 127 from ISO-8859-1 to UTF-8
    if chars.iter().all(u8::is_ascii) {
        string_from_utf_8(chars)
    } else {
        Err(StringError::Ascii)
    }
}

fn string_iso_8859_1<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::combinator::map_res;

    let (input, string) = map_res(u8_chars, string_from_iso8859_1)(input)?;

    Ok((input, string))
}

fn string_utf_8<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::combinator::map_res;

    let (input, string) = map_res(u8_chars, string_from_utf_8)(input)?;

    Ok((input, string))
}

fn string_from_utf_16(chars: &[u16]) -> Result<String, StringError> {
    String::from_utf16(chars).map_err(StringError::Utf16)
}

fn u16_chars<'a, E>(
    u16_parser: impl Fn(BytesInput<'a>) -> BytesResult<'a, u16, E>,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, Vec<u16>, E>
where
    E: BytesError<'a>,
{
    use nom::{bytes::complete::tag, multi::many_till};
    let null = &TextEncoding::NULL_TWO_BYTES[..];

    move |input| {
        let (input, (chars, _null)) = many_till(&u16_parser, tag(null))(input)?;

        Ok((input, chars))
    }
}

fn string_utf_16_be<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::{combinator::map_res, number::complete::be_u16};

    let (input, string) =
        map_res(u16_chars(be_u16), |chars| string_from_utf_16(&chars[..]))(input)?;

    Ok((input, string))
}

fn string_utf_16_le<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::{combinator::map_res, number::complete::le_u16};

    let (input, string) =
        map_res(u16_chars(le_u16), |chars| string_from_utf_16(&chars[..]))(input)?;

    Ok((input, string))
}

fn string_utf_16<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::{branch::alt, bytes::complete::tag, combinator::cut, sequence::tuple};

    let (input, (_bom, string)) = alt((
        tuple((tag(TextEncoding::BOM_LE), cut(string_utf_16_le))),
        tuple((tag(TextEncoding::BOM_BE), cut(string_utf_16_be))),
    ))(input)?;

    Ok((input, string))
}

pub(crate) fn null_terminated_string<'a, E>(
    text_encoding: TextEncoding,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, String, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    move |input| {
        let (input, string) = match text_encoding {
            TextEncoding::Iso8859_1 => string_iso_8859_1(input)?,
            TextEncoding::Utf8 => string_utf_8(input)?,
            TextEncoding::Utf16 => string_utf_16(input)?,
            TextEncoding::Utf16Be => string_utf_16_be(input)?,
        };

        Ok((input, string))
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::non_ascii_literal)]

    use nom::error::{ErrorKind, VerboseErrorKind};

    use super::super::TextEncoding;
    use crate::mp3::test_helpers::{
        bitvec_from_bytes, check_parser_test_cases, ErrorRange, ErrorSrc, ParseTestCase,
    };

    #[test]
    fn null_terminated_string_iso_8859_1() {
        let test_cases = vec![
            ParseTestCase {
                name: "Ok: ascii string",
                source_bits: bitvec_from_bytes(b"ASCII\0"),
                expected_result: Ok("ASCII".to_owned()),
            },
            ParseTestCase {
                name: "Ok: mime",
                source_bits: bitvec_from_bytes(b"image/png\0"),
                expected_result: Ok("image/png".to_owned()),
            },
            ParseTestCase {
                name: "Err: Not ASCII",
                source_bits: bitvec_from_bytes(&[0b1000_0000, 0]),
                expected_result: Err(ErrorSrc::Error(vec![(
                    ErrorRange::Bytes(0..2),
                    VerboseErrorKind::Nom(ErrorKind::MapRes),
                )])),
            },
        ];

        check_parser_test_cases(
            super::null_terminated_string(TextEncoding::Iso8859_1),
            &test_cases[..],
        );
    }

    #[test]
    fn null_terminated_string_utf8() {
        let test_cases = vec![
            ParseTestCase {
                name: "Ok: cyrillic",
                source_bits: bitvec_from_bytes("Кириллица\0".as_bytes()),
                expected_result: Ok("Кириллица".to_owned()),
            },
            ParseTestCase {
                name: "Ok: ascii",
                source_bits: bitvec_from_bytes(b"image/png\0"),
                expected_result: Ok("image/png".to_owned()),
            },
            ParseTestCase {
                name: "Err: invalid utf-8",
                source_bits: bitvec_from_bytes(&[0b1111_1111, 0]),
                expected_result: Err(ErrorSrc::Error(vec![(
                    ErrorRange::Bytes(0..2),
                    VerboseErrorKind::Nom(ErrorKind::MapRes),
                )])),
            },
        ];

        check_parser_test_cases(
            super::null_terminated_string(TextEncoding::Utf8),
            &test_cases[..],
        );
    }

    #[test]
    #[allow(clippy::too_many_lines)]
    fn null_terminated_string_utf16() {
        let test_cases = vec![
            ParseTestCase {
                name: "Ok: cyrillic little endian",
                source_bits: bitvec_from_bytes(&[
                    0xff, 0xfe, // BOM LE
                    0x10, 0x04, // А
                    0x11, 0x04, // Б
                    0x12, 0x04, // В
                    0x13, 0x04, // Г
                    0x14, 0x04, // Д
                    0x20, 0x00, // space
                    0x4b, 0x04, // ы
                    0x4c, 0x04, // ь
                    0x4d, 0x04, // э
                    0x4e, 0x04, // ю
                    0x4f, 0x04, // я
                    0x00, 0x00, // null
                ]),
                expected_result: Ok("АБВГД ыьэюя".to_owned()),
            },
            ParseTestCase {
                name: "Ok: cyrillic big endian",
                source_bits: bitvec_from_bytes(&[
                    0xfe, 0xff, // BOM BE
                    0x04, 0x10, // А
                    0x04, 0x11, // Б
                    0x04, 0x12, // В
                    0x04, 0x13, // Г
                    0x04, 0x14, // Д
                    0x00, 0x20, // space
                    0x04, 0x4b, // ы
                    0x04, 0x4c, // ь
                    0x04, 0x4d, // э
                    0x04, 0x4e, // ю
                    0x04, 0x4f, // я
                    0x00, 0x00, // null
                ]),
                expected_result: Ok("АБВГД ыьэюя".to_owned()),
            },
            ParseTestCase {
                name: "Ok: ascii little endian",
                source_bits: bitvec_from_bytes(&[
                    0xff, 0xfe, // BOM LE
                    b'i', 0, // i
                    b'm', 0, // m
                    b'a', 0, // a
                    b'g', 0, // g
                    b'e', 0, // e
                    b'/', 0, // /
                    b'p', 0, // p
                    b'n', 0, // n
                    b'g', 0, // g
                    0, 0, // null
                ]),
                expected_result: Ok("image/png".to_owned()),
            },
            ParseTestCase {
                name: "Err: without bom",
                source_bits: bitvec_from_bytes(&[
                    b'i', 0, // i
                    0x00, 0x00, // null
                ]),
                expected_result: Err(ErrorSrc::Error(vec![
                    (
                        ErrorRange::Bytes(0..4),
                        VerboseErrorKind::Nom(ErrorKind::Tag),
                    ),
                    (
                        ErrorRange::Bytes(0..4),
                        VerboseErrorKind::Nom(ErrorKind::Alt),
                    ),
                ])),
            },
            ParseTestCase {
                name: "Ok: null (double zero) in string little endian",
                source_bits: bitvec_from_bytes(&[
                    0xff, 0xfe, // BOM LE
                    0x20, 0x00, // space
                    0x00, 0x04, // Ѐ
                    0x00, 0x00, // null
                ]),
                expected_result: Ok(" Ѐ".to_owned()),
            },
            ParseTestCase {
                name: "Ok: null (double zero) in string big endian",
                source_bits: bitvec_from_bytes(&[
                    0xfe, 0xff, // BOM BE
                    0x04, 0x00, // Ѐ
                    0x00, 0x20, // space
                    0x00, 0x00, // null
                ]),
                expected_result: Ok("Ѐ ".to_owned()),
            },
            ParseTestCase {
                name: "Err: invalid utf-16 little endian",
                source_bits: bitvec_from_bytes(&[
                    0xff, 0xfe, // BOM LE
                    0x00, 0xDC, // invalid char
                    0x00, 0x00, // null
                ]),
                expected_result: Err(ErrorSrc::Failure(vec![(
                    ErrorRange::Bytes(2..6),
                    VerboseErrorKind::Nom(ErrorKind::MapRes),
                )])),
            },
            ParseTestCase {
                name: "Err: invalid utf-16 big endian",
                source_bits: bitvec_from_bytes(&[
                    0xfe, 0xff, // BOM BE
                    0xDC, 0x00, // invalid char
                    0x00, 0x00, // null
                ]),
                expected_result: Err(ErrorSrc::Failure(vec![(
                    ErrorRange::Bytes(2..6),
                    VerboseErrorKind::Nom(ErrorKind::MapRes),
                )])),
            },
        ];

        check_parser_test_cases(
            super::null_terminated_string(TextEncoding::Utf16),
            &test_cases[..],
        );
    }

    #[test]
    fn null_terminated_string_utf16be() {
        let test_cases = vec![
            ParseTestCase {
                name: "Ok: cyrillic",
                source_bits: bitvec_from_bytes(&[
                    0x04, 0x10, // А
                    0x04, 0x11, // Б
                    0x04, 0x12, // В
                    0x04, 0x13, // Г
                    0x04, 0x14, // Д
                    0x00, 0x20, // space
                    0x04, 0x4b, // ы
                    0x04, 0x4c, // ь
                    0x04, 0x4d, // э
                    0x04, 0x4e, // ю
                    0x04, 0x4f, // я
                    0x00, 0x00, // null
                ]),
                expected_result: Ok("АБВГД ыьэюя".to_owned()),
            },
            ParseTestCase {
                name: "Ok: ascii",
                source_bits: bitvec_from_bytes(&[
                    0, b'i', // i
                    0, b'm', // m
                    0, b'a', // a
                    0, b'g', // g
                    0, b'e', // e
                    0, b'/', // /
                    0, b'p', // p
                    0, b'n', // n
                    0, b'g', // g
                    0, 0, // null
                ]),
                expected_result: Ok("image/png".to_owned()),
            },
            ParseTestCase {
                name: "Ok: null (double zero) in string",
                source_bits: bitvec_from_bytes(&[
                    0x04, 0x00, // Ѐ
                    0x00, 0x20, // space
                    0x00, 0x00, // null
                ]),
                expected_result: Ok("Ѐ ".to_owned()),
            },
            ParseTestCase {
                name: "Err: invalid utf-16k",
                source_bits: bitvec_from_bytes(&[
                    0xDC, 0x00, // invalid char
                    0x00, 0x00, // null
                ]),
                expected_result: Err(ErrorSrc::Error(vec![(
                    ErrorRange::Bytes(0..4),
                    VerboseErrorKind::Nom(ErrorKind::MapRes),
                )])),
            },
        ];

        check_parser_test_cases(
            super::null_terminated_string(TextEncoding::Utf16Be),
            &test_cases[..],
        );
    }
}

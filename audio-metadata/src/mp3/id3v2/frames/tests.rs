use bitvec::bitvec;
use bitvec::order::Msb0;
use nom::error::{ErrorKind, VerboseErrorKind};

use crate::mp3::id3v2::frames::{AttachedPicture, PictureType, TextEncoding};
use crate::mp3::test_helpers::{
    bitvec_from_byte_slices, check_parser_test_cases, ErrorRange, ErrorSrc, ParseTestCase,
};

#[test]
fn attached_picture() {
    let test_cases = vec![
        ParseTestCase {
            name: "Err: empty",
            source_bits: bitvec![Msb0, u8; 0; 0],
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..0),
                    VerboseErrorKind::Nom(ErrorKind::Tag),
                ),
                (
                    ErrorRange::Bytes(0..0),
                    VerboseErrorKind::Nom(ErrorKind::Alt),
                ),
                (
                    ErrorRange::Bytes(0..0),
                    VerboseErrorKind::Context("text_encoding"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Err: invalid encoding",
            source_bits: bitvec_from_byte_slices([
                &[5],                     // text encoding - invalid
                "image/png\0".as_bytes(), // mime type
                &[3],                     // other picture type
                b"Front cover\0",         // description
            ]),
            expected_result: Err(ErrorSrc::Error(vec![
                (
                    ErrorRange::Bytes(0..24),
                    VerboseErrorKind::Nom(ErrorKind::Tag),
                ),
                (
                    ErrorRange::Bytes(0..24),
                    VerboseErrorKind::Nom(ErrorKind::Alt),
                ),
                (
                    ErrorRange::Bytes(0..24),
                    VerboseErrorKind::Context("text_encoding"),
                ),
            ])),
        },
        ParseTestCase {
            name: "Ok: Empty UTF-16BE description",
            source_bits: bitvec_from_byte_slices([
                &[2],                     // text encoding: ISO-8859-1
                "image/png\0".as_bytes(), // mime type
                &[3],                     // other picture type
                b"\0\0",                  // description
                b"data",                  // data
            ]),
            expected_result: Ok(AttachedPicture {
                text_encoding: TextEncoding::Utf16Be,
                description: "".to_owned(),
                mime_type: "image/png".to_owned(),
                picture_type: PictureType::CoverFront,
                data_range: 14..18,
            }),
        },
        ParseTestCase {
            name: "Ok: ASCII description",
            source_bits: bitvec_from_byte_slices([
                &[0],                     // text encoding: ISO-8859-1
                "image/png\0".as_bytes(), // mime type
                &[3],                     // other picture type
                b"Front cover\0",         // description
                b"data",                  // data
            ]),
            expected_result: Ok(AttachedPicture {
                text_encoding: TextEncoding::Iso8859_1,
                description: "Front cover".to_owned(),
                mime_type: "image/png".to_owned(),
                picture_type: PictureType::CoverFront,
                data_range: 24..28,
            }),
        },
        ParseTestCase {
            name: "Ok: UTF-8 description",
            source_bits: bitvec_from_byte_slices([
                &[3],                      // text encoding: utf-8
                "image/jpeg\0".as_bytes(), // mime type
                &[4],                      // other picture type
                "Cover\0".as_bytes(),      // description
                b"data",                   // data
            ]),
            expected_result: Ok(AttachedPicture {
                text_encoding: TextEncoding::Utf8,
                description: "Cover".to_owned(),
                mime_type: "image/jpeg".to_owned(),
                picture_type: PictureType::CoverBack,
                data_range: 19..23,
            }),
        },
    ];

    check_parser_test_cases(super::attached_picture, &test_cases[..]);
}

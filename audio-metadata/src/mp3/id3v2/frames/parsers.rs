use super::string::{null_terminated_string, text_encoding, FromStringError};
use crate::mp3::errors::BytesError;
use crate::mp3::BytesInput;

use super::{AttachedPicture, PictureType, TextEncoding};

type BytesResult<'a, T, E> = nom::IResult<BytesInput<'a>, T, E>;

fn picture_type<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, PictureType, E>
where
    E: BytesError<'a>,
{
    use nom::number::complete::be_u8;

    let (input, value) = be_u8(input)?;

    let picture_type = match value {
        0x01 => PictureType::FileIcon,
        0x02 => PictureType::OtherFileIcon,
        0x03 => PictureType::CoverFront,
        0x04 => PictureType::CoverBack,
        other => PictureType::Other(other),
    };

    Ok((input, picture_type))
}

pub(crate) fn attached_picture<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, AttachedPicture, E>
where
    E: BytesError<'a> + FromStringError<'a>,
{
    use nom::{combinator::rest, error::context, sequence::tuple};

    let (input, text_encoding) = context("text_encoding", text_encoding)(input)?;

    let (input, (mime_type, picture_type, description, picture_view)) = tuple((
        context("mime_type", null_terminated_string(TextEncoding::Iso8859_1)),
        context("picture_type", picture_type),
        context("description", null_terminated_string(text_encoding)),
        context("picture_data", rest),
    ))(input)?;

    let data_range = picture_view.data_range();

    Ok((
        input,
        AttachedPicture {
            text_encoding,
            mime_type,
            picture_type,
            description,
            data_range,
        },
    ))
}

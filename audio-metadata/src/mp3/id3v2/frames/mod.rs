mod parsers;
mod string;

#[cfg(test)]
mod tests;

use std::ops::Range;

pub(crate) use parsers::*;

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum TextEncoding {
    Iso8859_1,
    Utf16,
    Utf16Be,
    Utf8,
}

impl TextEncoding {
    const ISO_8859_1_VALUE: u8 = 0x00;
    const UTF_16_VALUE: u8 = 0x01;
    const UTF_16_BE_VALUE: u8 = 0x02;
    const UTF_8_VALUE: u8 = 0x03;

    const NULL_ONE_BYTE: [u8; 1] = [0];
    const NULL_TWO_BYTES: [u8; 2] = [0, 0];

    const BOM_LE: &'static [u8] = &[0xff, 0xfe];
    const BOM_BE: &'static [u8] = &[0xfe, 0xff];
}

#[derive(Clone, Debug, PartialEq)]
pub enum PictureType {
    Other(u8),
    FileIcon,
    OtherFileIcon,
    CoverFront,
    CoverBack,
    CD,
    LeadArtist,
    Band,
}

#[derive(Clone, PartialEq, Debug)]
pub struct AttachedPicture {
    pub text_encoding: TextEncoding,
    pub mime_type: String,
    pub picture_type: PictureType,
    pub description: String,
    pub data_range: Range<usize>,
}

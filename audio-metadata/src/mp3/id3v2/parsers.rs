use crate::mp3::errors::{BitsError, BytesError, CombinedError};
use crate::mp3::parse_helpers::{define_bit_slices, parse_bool, with_bits};
use crate::mp3::{BitsInput, BitsResult, BytesInput, BytesResult};

// SynchsafeInteger

use super::SynchsafeInteger;

fn synchsafe_integer_byte<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, u8, E>
where
    E: BytesError<'a>,
{
    use nom::{combinator::verify, number::complete::be_u8};

    let is_valid = |byte: &u8| *byte <= SynchsafeInteger::BYTE_MASK;
    let (rest, byte) = verify(be_u8, is_valid)(input)?;

    Ok((rest, byte))
}

pub(crate) fn synchsafe_integer<'a, E>(
    input: BytesInput<'a>,
) -> BytesResult<'a, SynchsafeInteger, E>
where
    E: BytesError<'a>,
{
    use nom::{combinator::cut, multi::count};

    let (rest, integer_bytes) =
        cut(count(synchsafe_integer_byte, SynchsafeInteger::BYTES_SIZE))(input)?;

    let value = integer_bytes
        .into_iter()
        .fold(0, |acc, byte| (acc << 7) + u32::from(byte));

    Ok((rest, SynchsafeInteger(value)))
}

// HeaderFlags

use super::HeaderFlags;

define_bit_slices! {
    HEADER_FLAGS_MASK => [0, 0, 0, 0]
}

pub(crate) fn header_flags<'a, E>(input: BitsInput<'a>) -> BitsResult<'a, HeaderFlags, E>
where
    E: BitsError<'a>,
{
    use nom::{bytes::complete::tag, sequence::tuple};

    let (input, (unsynchronization, extended_header, experimental, footer, _reserved)) =
        tuple((
            parse_bool,
            parse_bool,
            parse_bool,
            parse_bool,
            tag(*HEADER_FLAGS_MASK),
        ))(input)?;

    let header_flags = HeaderFlags {
        unsynchronization,
        extended_header,
        experimental,
        footer,
    };

    Ok((input, header_flags))
}

// Common header

struct CommonHeader {
    version_major: u8,
    version_minor: u8,
    flags: HeaderFlags,
    size: SynchsafeInteger,
}

fn common_header<'a, E>(input: BytesInput<'a>, magic: [u8; 3]) -> BytesResult<'a, CommonHeader, E>
where
    E: CombinedError<'a>,
{
    use nom::{
        bytes::{complete::tag, streaming::take},
        combinator::all_consuming,
        error::context,
        number::complete::be_u8,
        sequence::tuple,
    };

    const HEADER_SIZE: usize = 10;

    let (rest, input) = take(HEADER_SIZE)(input)?;

    let (_input, (_magic, version_major, version_minor, flags, size)) = all_consuming(tuple((
        context("magic", tag(&magic[..])),
        context("version_major", be_u8),
        context("version_minor", be_u8),
        context("flags", with_bits(header_flags)),
        context("size", synchsafe_integer),
    )))(input)?;

    let header = CommonHeader {
        version_major,
        version_minor,
        flags,
        size,
    };

    Ok((rest, header))
}

// Header

use super::Header;

pub(crate) fn header<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, Header, E>
where
    E: CombinedError<'a>,
{
    const HEADER_MAGIC: [u8; 3] = *b"ID3";

    let (
        input,
        CommonHeader {
            version_major,
            version_minor,
            flags,
            size,
        },
    ) = common_header(input, HEADER_MAGIC)?;

    let header = Header {
        version_major,
        version_minor,
        flags,
        size,
    };

    Ok((input, header))
}

// Footer

use super::Footer;

pub(crate) fn footer<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, Footer, E>
where
    E: CombinedError<'a>,
{
    const FOOTER_MAGIC: [u8; 3] = *b"3DI";

    let (
        input,
        CommonHeader {
            version_major,
            version_minor,
            flags,
            size,
        },
    ) = common_header(input, FOOTER_MAGIC)?;

    let footer = Footer {
        version_major,
        version_minor,
        flags,
        size,
    };

    Ok((input, footer))
}

// ExtendedHeader

use super::ExtendedHeader;

pub(crate) fn extended_header<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, ExtendedHeader, E>
where
    E: BytesError<'a>,
{
    use nom::{
        bytes::complete::take,
        combinator::{cut, map, verify},
        error::context,
    };

    let (input, size) = cut(context(
        "size",
        verify(synchsafe_integer, ExtendedHeader::validate_size),
    ))(input)?;
    let data_size = ExtendedHeader::data_size(&size);
    let (input, data) = context("data", map(take(data_size), BytesData::new))(input)?;

    let extended_header = ExtendedHeader { size, data };

    Ok((input, extended_header))
}

// BytesData

use crate::mp3::utils::BytesData;

#[allow(clippy::needless_pass_by_value)]
pub(crate) fn parse_bytes_data<'a, E, C>(
    count: C,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, BytesData, E>
where
    E: BytesError<'a>,
    C: nom::ToUsize,
{
    use nom::{bytes::complete::take, combinator::map};

    let count = count.to_usize();

    move |input: BytesInput| -> nom::IResult<BytesInput<'a>, BytesData, E> {
        let (input, data) = map(take(count), BytesData::new)(input)?;

        Ok((input, data))
    }
}

// Frame size

fn validate_frame_size<C>(size: &C) -> bool
where
    C: nom::ToUsize,
{
    let size = size.to_usize();

    size > 0
}

// FrameV2

use super::{FrameId3, FrameV2};

fn frame_v2_size<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, u32, E>
where
    E: BytesError<'a>,
{
    use nom::bytes::complete::take;

    const SIZE_BYTES: usize = 3;

    let (input, size_bytes) = take(SIZE_BYTES)(input)?;

    let size = size_bytes
        .iter()
        .fold(0_u32, |acc, byte| (acc << 8) + u32::from(*byte));

    Ok((input, size))
}

fn take_array<'a, E, const N: usize>(input: BytesInput<'a>) -> BytesResult<'a, [u8; N], E>
where
    E: BytesError<'a>,
{
    use nom::bytes::complete::take;

    let (input, bytes) = take(N)(input)?;

    let mut array = [0_u8; N];
    for (index, byte) in bytes.iter().enumerate() {
        array[index] = *byte;
    }

    Ok((input, array))
}

pub(crate) fn frame_v2<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, FrameV2, E>
where
    E: BytesError<'a>,
{
    use nom::{
        combinator::{cut, map, verify},
        error::context,
        sequence::tuple,
    };

    let (input, (id, size)) = tuple((
        context(
            "id",
            verify(map(take_array, FrameId3::new), FrameId3::is_valid),
        ),
        cut(context("size", verify(frame_v2_size, validate_frame_size))),
    ))(input)?;
    let (input, data) = context("data", parse_bytes_data(size))(input)?;

    let frame = FrameV2 { id, size, data };

    Ok((input, frame))
}

// FrameId4

use super::FrameId4;

pub(crate) fn frame_id4<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, FrameId4, E>
where
    E: BytesError<'a>,
{
    use nom::combinator::{map, verify};

    let (input, id) = verify(map(take_array, FrameId4::new), FrameId4::is_valid)(input)?;

    Ok((input, id))
}

// FrameFlags
use super::FrameFlags;

pub(crate) fn frame_flags<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, FrameFlags, E>
where
    E: BytesError<'a>,
{
    use nom::{combinator::map, number::complete::be_u16};

    let (input, flags) = map(be_u16, FrameFlags::new)(input)?;

    Ok((input, flags))
}

// FrameV3

use super::FrameV3;

pub(crate) fn frame_v3<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, FrameV3, E>
where
    E: BytesError<'a>,
{
    use nom::{
        combinator::{cut, verify},
        error::context,
        number::complete::be_u32,
        sequence::tuple,
    };

    let (input, (id, size, flags)) = tuple((
        context("id", frame_id4),
        cut(context("size", verify(be_u32, validate_frame_size))),
        context("flags", frame_flags),
    ))(input)?;
    let (input, data) = context("data", parse_bytes_data(size))(input)?;

    let frame = FrameV3 {
        id,
        size,
        flags,
        data,
    };

    Ok((input, frame))
}

// FrameV4

use super::FrameV4;

pub(crate) fn frame_v4<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, FrameV4, E>
where
    E: BytesError<'a>,
{
    use nom::{
        combinator::{cut, verify},
        error::context,
        sequence::tuple,
    };

    let (input, (id, size, flags)) = tuple((
        context("id", frame_id4),
        context("size", cut(verify(synchsafe_integer, validate_frame_size))),
        context("flags", frame_flags),
    ))(input)?;
    let (input, data) = context("data", parse_bytes_data(size.value()))(input)?;

    let frame = FrameV4 {
        id,
        size,
        flags,
        data,
    };

    Ok((input, frame))
}

// Frame

use super::Frame;

pub(crate) fn frame<'a, E>(
    version_major: u8,
) -> impl Fn(BytesInput<'a>) -> BytesResult<'a, Frame, E>
where
    E: BytesError<'a>,
{
    use nom::{
        combinator::{cut, fail, map},
        error::context,
    };

    move |input| match version_major {
        2 => context("frame_v2", map(frame_v2, Frame::V2))(input),
        3 => context("frame_v3", map(frame_v3, Frame::V3))(input),
        4 => context("frame_v4", map(frame_v4, Frame::V4))(input),
        _ => cut(context("unknown", fail))(input),
    }
}

// ID3V2

use super::Id3V2;

pub(crate) fn id3v2<'a, E>(input: BytesInput<'a>) -> BytesResult<'a, Id3V2, E>
where
    E: CombinedError<'a>,
{
    use nom::{
        bytes::streaming::take,
        combinator::{cond, cut, map_parser, rest, verify},
        error::context,
        multi::many1,
        sequence::tuple,
    };

    let (input, header) = context("header", header)(input)?;
    let (input, (extended_header, frames, _padding)) = context(
        "body",
        map_parser(
            take(header.total_size()),
            tuple((
                context(
                    "extended_header",
                    cond(header.flags.extended_header, extended_header),
                ),
                context("frames", many1(frame(header.version_major))),
                context("padding", cut(verify(rest, BytesData::is_clean))),
            )),
        ),
    )(input)?;

    let (input, footer) = context("footer", cond(header.flags.footer, footer))(input)?;

    let id3v2 = Id3V2 {
        header,
        extended_header,
        frames,
        footer,
    };

    Ok((input, id3v2))
}

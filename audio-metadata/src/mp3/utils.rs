use nom::InputLength;

use super::BytesInput;

static EMPTY_BYTES_DATA: &[u8] = &[];

#[derive(PartialEq, Clone)]
pub struct BytesData<'a> {
    view: BytesInput<'a>,
}

impl<'a> BytesData<'a> {
    const MAX_PREVIEW_SIZE: usize = 20;

    pub(crate) fn new(view: impl Into<BytesInput<'a>>) -> Self {
        let view = view.into();
        Self { view }
    }

    pub(crate) fn is_clean(data: &BytesInput) -> bool {
        data.iter().all(|byte| *byte == 0)
    }

    pub(crate) fn data(&self) -> &[u8] {
        self.view.data()
    }

    pub(crate) fn view(&self) -> BytesInput {
        self.view.clone()
    }
}

impl BytesData<'static> {
    pub fn empty() -> Self {
        Self::new(EMPTY_BYTES_DATA)
    }
}

impl<'a> std::fmt::Debug for BytesData<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let print_len = Self::MAX_PREVIEW_SIZE.min(self.view.input_len());

        write!(f, "[")?;

        for byte in &self.view.data()[..print_len] {
            if byte.is_ascii_graphic() {
                write!(f, "'{}', ", char::from(*byte))?;
            } else {
                write!(f, "0x{:02X}, ", byte)?;
            }
        }

        if Self::MAX_PREVIEW_SIZE > print_len {
            write!(f, "..")?;
        }

        write!(f, "] ({len} total)", len = self.view.input_len())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub(crate) struct DebugValue<T>(T);

impl<T> std::ops::Deref for DebugValue<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

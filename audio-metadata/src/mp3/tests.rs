use pretty_assertions::assert_eq;

use super::{
    frame::{
        AsciiString, AudioFrame, BitRate, BitrateMethod, Channels, Emphasis, Header, Layer,
        MetaData, MetaDataFlags, ModeExtension, MpegVersionId, SamplingRate, TagRevision,
    },
    utils::BytesData,
    File,
};

#[test]
fn file_audio_frames() {
    let header = Header {
        bitrate: BitRate::Kbps256,
        channels: Channels::Stereo,
        copyright: false,
        crc_protection: false,
        emphasis: Emphasis::None,
        layer: Layer::Iii,
        mpeg_version_id: MpegVersionId::Mpeg1,
        mode_extension: ModeExtension::default(),
        original: false,
        padding: false,
        private: false,
        sampling_rate: SamplingRate::Hz32000,
    };

    let first_audio_frame = AudioFrame {
        header: header.clone(),
        payload: BytesData::new(&[0]),
    };
    let tail_audio_frames = vec![
        AudioFrame {
            header: header.clone(),
            payload: BytesData::new(&[1]),
        },
        AudioFrame {
            header,
            payload: BytesData::new(&[2]),
        },
    ];

    let file = File {
        id3v2: None,
        first_audio_frame: first_audio_frame.clone(),
        tail_audio_frames: tail_audio_frames.clone(),
    };

    let mut expected_audio_frames = vec![&first_audio_frame];
    expected_audio_frames.extend(tail_audio_frames.iter());

    let result_audio_frames = file.audio_frames();
    assert_eq!(
        expected_audio_frames,
        result_audio_frames.collect::<Vec<&AudioFrame>>(),
    );
}

#[test]
fn meta_data() {
    let payload = vec![0_u8; MetaData::MPEG1_NOT_MONO_PADDING_SIZE]
        .into_iter()
        .chain(b"Xing".to_vec())
        .chain(vec![0; 3]) // padding
        .chain(vec![0b0000_1111]) // padding high / flags
        .chain(vec![0, 0, 1, 0]) // frames
        .chain(vec![0, 1, 0, 0]) // bytes
        .chain(vec![127; MetaData::TOC_SIZE]) // TOC
        .chain(vec![0, 0, 0, 127]) // qc
        .chain(b"123456789".to_vec()) // extension
        .chain([0b0100_0001].to_vec()) // tag revision / bitrate method
        .chain(vec![0_u8; 794]) // suffix padding
        .collect::<Vec<u8>>();

    let header = Header {
        mpeg_version_id: MpegVersionId::Mpeg1,
        layer: Layer::Iii,
        crc_protection: false,
        bitrate: BitRate::Kbps320,
        sampling_rate: SamplingRate::Hz48000,
        padding: false,
        private: false,
        channels: Channels::Stereo,
        mode_extension: ModeExtension::default(),
        copyright: false,
        original: false,
        emphasis: Emphasis::None,
    };

    let first_audio_frame = AudioFrame {
        header,
        payload: BytesData::new(&payload[..]),
    };

    let file = File {
        id3v2: None,
        first_audio_frame,
        tail_audio_frames: Vec::new(),
    };

    let meta_data = file.meta_data();
    let expected_meta_data = Some(MetaData {
        magic: AsciiString::new(b"Xing".into()),
        flags: MetaDataFlags {
            bytes: true,
            frames: true,
            toc: true,
            quality_indicator: true,
        },
        bytes: Some(65536),
        frames: Some(256),
        toc: Some(vec![127; MetaData::TOC_SIZE]),
        quality_indicator: Some(127),
        extension_codec: AsciiString::new(b"123456789".into()),
        bitrate_method: BitrateMethod::ConstantBitrate,
        tag_revision: TagRevision::Revision(4),
    });

    assert_eq!(expected_meta_data, meta_data);
}

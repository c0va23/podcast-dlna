use std::fmt::Debug;
use std::ops::Range;

use bitvec::order::Msb0;
use bitvec::prelude::BitVec;
use bitvec::slice::BitSlice;
use bitvec::view::BitView;
use nom::error::VerboseErrorKind;
use nom::{InputLength, Needed};
use nom_bitvec::BSlice;
use pretty_assertions::assert_eq;

use crate::mp3::errors::{NomError, NomErrorValue, NomInput, NomVerboseError};

use super::{BitsInput, BytesInput};

pub(crate) struct InputSrc<'a>(&'a BitSlice<Msb0, u8>);

impl<'a> From<InputSrc<'a>> for BitsInput<'a> {
    fn from(input: InputSrc<'a>) -> Self {
        BSlice(input.0)
    }
}

impl<'a> From<InputSrc<'a>> for BytesInput<'a> {
    fn from(input: InputSrc<'a>) -> Self {
        Self::new(input.0.as_raw_slice())
    }
}

#[derive(Clone)]
pub(crate) enum ErrorRange {
    Bytes(Range<usize>),
    Bits(Range<usize>),
}

type ErrorVec = Vec<(ErrorRange, VerboseErrorKind)>;

#[derive(Clone)]
pub(crate) enum ErrorSrc {
    Error(ErrorVec),
    Failure(ErrorVec),
    Incomplete(usize),
}

pub(crate) struct ParseTestCase<T: Clone> {
    pub name: &'static str,
    pub source_bits: BitVec<Msb0, u8>,
    pub expected_result: Result<T, ErrorSrc>,
}

impl<'a, T> ParseTestCase<T>
where
    T: Clone,
{
    fn build_nom_error(&self, error_range: &ErrorRange, err_kind: &VerboseErrorKind) -> NomError {
        let input = match error_range {
            ErrorRange::Bits(range) => NomInput::Bits(self.source_bits[range.clone()].to_bitvec()),
            ErrorRange::Bytes(range) => {
                let input_src = self.source_bits.as_raw_slice();
                NomInput::Bytes(input_src[range.clone()].to_vec())
            }
        };

        let value = match err_kind {
            VerboseErrorKind::Context(ctx) => NomErrorValue::Context(ctx),
            VerboseErrorKind::Nom(kind) => NomErrorValue::Parse(*kind),
            VerboseErrorKind::Char(_) => unreachable!(),
        };

        NomError { input, value }
    }

    fn build_verbose_error(&self, errors: &[(ErrorRange, VerboseErrorKind)]) -> NomVerboseError {
        let errors = errors
            .iter()
            .map(|(error_range, err_kind)| self.build_nom_error(error_range, err_kind))
            .collect();
        NomVerboseError { errors }
    }

    fn build_expected_result(&'a self) -> Result<T, nom::Err<NomVerboseError>> {
        self.expected_result
            .as_ref()
            .map_err(|error_src| match error_src {
                ErrorSrc::Error(errors) => nom::Err::Error(self.build_verbose_error(errors)),
                ErrorSrc::Failure(errors) => nom::Err::Failure(self.build_verbose_error(errors)),
                ErrorSrc::Incomplete(size) => nom::Err::Incomplete(Needed::new(*size)),
            })
            .map(Clone::clone)
    }
}

type NomParserError = NomVerboseError;
type NomParserResult<'a, I, T> = nom::IResult<I, T, NomParserError>;

fn check_parser_test_case<'a, ParseInput, P, T>(mut nom_parser: P, test_case: &'a ParseTestCase<T>)
where
    ParseInput: InputLength + From<InputSrc<'a>>,
    P: for<'b> FnMut(ParseInput) -> NomParserResult<'b, ParseInput, T>,
    T: PartialEq + Debug + Clone,
{
    let input_src = InputSrc(&test_case.source_bits[..]);
    let actual_result_raw = nom_parser(input_src.into());

    if let Ok((ref rest, _)) = actual_result_raw {
        assert_eq!(
            0,
            rest.input_len(),
            "Rest. Test case: {name}",
            name = test_case.name,
        );
    }

    let actual_result = actual_result_raw.map(|(_, value)| value);
    let expected_result = test_case.build_expected_result();

    assert_eq!(
        expected_result,
        actual_result,
        "Assert. Test case: {name}",
        name = test_case.name,
    );
}

pub(crate) fn check_parser_test_cases<'a, ParseInput, P, T>(
    mut nom_parser: P,
    test_cases: &'a [ParseTestCase<T>],
) where
    ParseInput: InputLength + From<InputSrc<'a>>,
    P: for<'b> FnMut(ParseInput) -> NomParserResult<'b, ParseInput, T>,
    T: Clone + PartialEq + Debug,
{
    for test_case in test_cases {
        check_parser_test_case(&mut nom_parser, test_case);
    }
}

pub(crate) fn build_bitvec_from_bitslice_slice(
    bit_slice_list: &[&BitSlice<Msb0, u8>],
) -> BitVec<Msb0, u8> {
    let mut target_bitvec = BitVec::new();

    for bit_slice in bit_slice_list {
        target_bitvec.extend_from_bitslice(bit_slice);
    }

    target_bitvec
}

pub(crate) fn bitvec_from_bytes(bytes: &[u8]) -> BitVec<Msb0, u8> {
    bytes.view_bits().to_bitvec()
}

pub(crate) fn bitvec_from_byte_slices<'a>(
    byte_slices: impl IntoIterator<Item = &'a [u8]>,
) -> BitVec<Msb0, u8> {
    let mut target_bitvec = BitVec::new();
    for byte_slice in byte_slices {
        target_bitvec.extend_from_bitslice(byte_slice.view_bits::<Msb0>());
    }
    target_bitvec
}

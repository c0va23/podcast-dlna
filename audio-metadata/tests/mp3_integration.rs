use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::time::Duration;

use serde_derive::Deserialize;
use thiserror::Error as ThisError;

use audio_metadata::{mp3::id3v2::Id3V2, Mp3File, ParseDurationError, ParseError};

fn read_file_prefix(file: &mut std::fs::File, size: usize) -> (Vec<u8>, usize) {
    use std::io::Read;
    let mut buf = vec![0u8; size];
    let file_len = file.metadata().unwrap().len();
    file.read_exact(&mut buf).unwrap();
    (buf, file_len as usize)
}

#[derive(Debug, Deserialize, Clone)]
struct Id3Frame {
    tag: String,
    size: usize,
}

#[derive(Debug, Deserialize, Clone)]
struct Id3V2Tag {
    version_major: u8,
    version_minor: u8,
    frames: Vec<Id3Frame>,
}

#[derive(Debug, Deserialize, Clone)]
struct Mp3TestCase {
    url: String,
    file_name: String,
    duration: f64,
    note: Option<String>,
    id3v2: Option<Id3V2Tag>,
    bit_rate: Option<usize>,
}

#[derive(Debug, Deserialize)]
struct Mp3TestsSuite {
    #[serde(rename = "mp3_test_case")]
    test_cases: Vec<Mp3TestCase>,
}

#[derive(ThisError, Debug)]
#[allow(clippy::large_enum_variant)]
enum TestSuiteError {
    #[error("Read config error: {0}")]
    ReadError(#[from] std::io::Error),

    #[error("Toml error: {0}")]
    TomlParseError(#[from] toml::de::Error),

    #[error("Not uniq file name #{first:?} and #{second:?}")]
    NotUniqFileName {
        first: Mp3TestCase,
        second: Mp3TestCase,
    },
}

impl Mp3TestsSuite {
    fn validate(&self) -> Result<(), TestSuiteError> {
        self.validate_file_names()?;

        Ok(())
    }

    fn validate_file_names(&self) -> Result<(), TestSuiteError> {
        for (index, audio_file) in self.test_cases.iter().enumerate() {
            for second_audio_file in self.test_cases.iter().skip(index + 1) {
                if audio_file.file_name == second_audio_file.file_name {
                    return Err(TestSuiteError::NotUniqFileName {
                        first: audio_file.clone(),
                        second: second_audio_file.clone(),
                    });
                }
            }
        }

        Ok(())
    }
}

fn read_mp3_test_suite(audio_files_path: &str) -> Result<Mp3TestsSuite, TestSuiteError> {
    let audio_files_data = std::fs::read_to_string(audio_files_path)?;
    let audio_files = toml::from_str::<Mp3TestsSuite>(&audio_files_data)?;
    audio_files.validate()?;
    Ok(audio_files)
}

#[derive(ThisError, Debug)]
enum AudioFileError {
    #[error("File error: {0}")]
    Io(#[from] std::io::Error),

    #[error("Network error: {0}")]
    Reqwest(#[from] reqwest::Error),
}

fn download_audio_file(file_path: &Path, url: &str) -> Result<(), AudioFileError> {
    let temp_file_path = file_path.parent().unwrap();
    if !temp_file_path.exists() {
        eprintln!("Create temp dir {:?}", temp_file_path);

        std::fs::DirBuilder::new().create(temp_file_path)?;
    }

    eprintln!("Create file {:?}", file_path);
    let mut file = File::create(file_path)?;

    eprintln!("Download {}", url);
    let mut response = reqwest::blocking::get(url)?;

    eprintln!("Copy {} to {:?}", url, file_path);
    response.copy_to(&mut file)?;

    Ok(())
}

const TEMP_FILE_PATH: &str = "tests/test_data/temp/";

fn fetch_audio_file(audio_file: &Mp3TestCase) -> Result<File, AudioFileError> {
    let temp_file_path = Path::new(TEMP_FILE_PATH);
    let file_path = temp_file_path.join(Path::new(&audio_file.file_name));

    if !file_path.exists() {
        download_audio_file(&file_path, &audio_file.url)?;
    }

    let file = File::open(file_path)?;

    Ok(file)
}

const MP3_TESTS_SUITE_PATH: &str = "tests/test_data/mp3_tests_suite.toml";

#[derive(ThisError, Debug)]
enum TryParseDurationError {
    #[error("Parse MP3 file error: {0}")]
    Mp3Error(#[from] ParseError),

    #[error("Parse duration: {0}")]
    ParseDuration(#[from] ParseDurationError),

    #[error("Audio file error: {0}")]
    AudioFile(#[from] AudioFileError),
}

fn try_read_file(
    test_case: &Mp3TestCase,
    prefix_size: usize,
) -> Result<(Vec<u8>, usize), TryParseDurationError> {
    println!("Try {} with {} bytes", test_case.file_name, prefix_size);

    let mut file = fetch_audio_file(test_case)?;
    let (data, file_len) = read_file_prefix(&mut file, prefix_size);

    Ok((data, file_len))
}

fn try_parse_audio_data(
    data: &[u8],
    file_len: usize,
) -> Result<(Mp3File, Duration), TryParseDurationError> {
    let mp3_file = Mp3File::parse(data)?;
    let duration = mp3_file.duration_by_prefix(file_len)?;

    Ok((mp3_file, duration))
}

#[test]
#[ignore]
fn integration_mp3_duration() -> Result<(), Box<dyn Error>> {
    const DEFAULT_PREFIX_SIZE: usize = 64_000;

    let audio_data = read_mp3_test_suite(MP3_TESTS_SUITE_PATH)?;
    for test_case in audio_data.test_cases {
        println!(
            "Test case {file_name} ({note:?})",
            file_name = test_case.file_name,
            note = test_case.note,
        );

        let mut data: Vec<u8>;
        let mut next_min_len = DEFAULT_PREFIX_SIZE;
        let mut try_num = 0;
        let max_retry = 20;

        let (mp3_file, duration) = loop {
            let (tmp_data, file_len) = try_read_file(&test_case, next_min_len)?;
            data = tmp_data;

            match try_parse_audio_data(&data[..], file_len) {
                Err(TryParseDurationError::Mp3Error(ParseError::Incomplete(min_len))) => {
                    next_min_len = min_len
                }
                result => break result?,
            }

            try_num += 1;
            if try_num >= max_retry {
                panic!("Max retries");
            }
        };

        let expected_duration = Duration::from_secs_f64(test_case.duration);

        if let Some(expected_bit_rate) = test_case.bit_rate {
            assert_eq!(
                expected_bit_rate,
                mp3_file.bit_rate().value(),
                "unexpected bitrate on {file_name} ({note:?})",
                file_name = test_case.file_name,
                note = test_case.note,
            );
        }

        assert_eq!(
            expected_duration,
            duration,
            "check duration for {file_name}, expect {expect:?} got {got:?} ({note:?})",
            file_name = test_case.file_name,
            expect = expected_duration,
            got = duration,
            note = test_case.note,
        );

        assert_eq!(
            test_case.clone().id3v2.map(|id3v2| id3v2.version_major),
            mp3_file
                .id3v2
                .clone()
                .map(|id3v2| id3v2.header.version_major),
            "unexpected major version on {file_name} ({note:?})",
            file_name = test_case.file_name,
            note = test_case.note,
        );

        assert_eq!(
            test_case.id3v2.clone().map(|id3v2| id3v2.version_minor),
            mp3_file
                .id3v2
                .clone()
                .map(|id3v2| id3v2.header.version_minor),
            "unexpected minor version on {file_name} ({note:?})",
            file_name = test_case.file_name,
            note = test_case.note,
        );

        assert_eq!(
            test_case.id3v2.clone().map(|id3| id3.frames.len()),
            mp3_file.id3v2.clone().map(|id3| id3.frames.len()),
            "unexpected frame count on {file_name} ({note:?})",
            file_name = test_case.file_name,
            note = test_case.note,
        );

        for frame in mp3_file
            .id3v2
            .as_ref()
            .map(Id3V2::frames)
            .map(<&[Frame]>::into)
            .unwrap_or_else(Vec::new)
            .iter()
        {
            println!("{:?}", frame);
        }

        use audio_metadata::mp3::id3v2::Frame;
        for (index, expected_frame) in test_case
            .id3v2
            .map_or_else(Vec::new, |id3| id3.frames)
            .into_iter()
            .enumerate()
        {
            let frame: Option<Frame> = mp3_file
                .id3v2
                .clone()
                .and_then(|id3v2| id3v2.frames.get(index).map(Clone::clone));

            assert_eq!(
                Some(expected_frame.tag.clone()),
                frame.clone().map(|frame| frame.id().to_string()),
                "unexpected frame tag on {file_name} ({note:?})",
                file_name = test_case.file_name,
                note = test_case.note,
            );

            assert_eq!(
                Some(expected_frame.size),
                frame.map(|frame| frame.size()),
                "unexpected frame size on {file_name} ({note:?})",
                file_name = test_case.file_name,
                note = test_case.note,
            );
        }

        for picture in mp3_file.attached_pictures() {
            println!(
                "mime={mime_type} len={len} description={description}",
                description = picture.description,
                mime_type = picture.mime_type,
                len = picture.data_range.len(),
            );
        }
    }

    Ok(())
}

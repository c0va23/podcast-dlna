local rustVersion = '1.57.0';
local DOCKER_REGISTRY = 'cr.yandex';
local DOCKER_REPO_PREFIX = DOCKER_REGISTRY + '/crpqrdfl6aknpra9ttup';

local commonS3Settings = [
  { setting: 'endpoint', secret: 'endpoint' },
  { setting: 'access_key', secret: 'access_key' },
  { setting: 'secret_key', secret: 'secret_key' },
];

local cacheSettingToSecret = commonS3Settings + [
  { setting: 'root', secret: 'cache_bucket_name' },
];

local releaseSettingsToSecret = commonS3Settings + [
  { setting: 'bucket', secret: 'artifacts_bucket_name' },
];

local CARGO_HOME = '.cargo';

local buildSecretName(prefix, name) = prefix + '_' + name;

local buildSettingsFromSecret(prefix, settingToSecret) = {
  [pair.setting]: {
    from_secret: buildSecretName(prefix, pair.secret),
  } for pair in settingToSecret
};

local buildSecret(prefix, settingToSecret) = [
  {
    kind: 'secret',
    name: buildSecretName(prefix, pair.secret),
    get: {
      path: 'drone-minio',
      name: pair.secret,
    },
  } for pair in settingToSecret
];

local cacheSettingsFromSecret = buildSettingsFromSecret('cache', cacheSettingToSecret);
local cacheSecrets = buildSecret('cache', cacheSettingToSecret);

local releaseSettingsFromSecret = buildSettingsFromSecret('release', releaseSettingsToSecret);
local releaseSecrets = buildSecret('release', releaseSettingsToSecret);

local dockerSecretList = [
  'username',
  'password',
  'config_json',
];

local dockerSecrets = [
  {
    kind: 'secret',
    name: "docker_%s" % secretName,
    get: {
      path: 'drone-docker',
      name: secretName,
    },
  } for secretName in dockerSecretList
];

local promoteSecretList = [
  'drone_server',
  'drone_token',
  'podcast_dlna_deploy_repo',
];

local promoteSercets = [
  {
    kind: 'secret',
    name: secretName,
    get: {
      path: 'drone-promote',
      name: secretName,
    },
  } for secretName in promoteSecretList
];

local debVersions = [
  {
    linker: 'lld-7',
    os: 'debian',
    release: 'stretch', # 9
  },
  {
    linker: 'lld',
    os: "debian",
    release: 'buster', # 10
  },
  {
    linker: 'lld',
    os: "debian",
    release: 'bullseye', # 11
  },
  {
    linker: 'lld-10',
    os: 'ubuntu',
    release: 'bionic', # 18.04
  },
  {
    linker: 'lld',
    os: 'ubuntu',
    release: 'focal', # 20.04
  },
];

local oldestDebVersion = debVersions[0];

local platformTag(platform) = "%s-%s" % [platform.os, platform.arch];
local debReleaseTag(debianVersion) = "%s-%s" % [debianVersion.os, debianVersion.release];

local CacheStep(stepName, action, platform, trigger = {
  status: [
    'success',
    'failure',
  ],
}) = {
  name: stepName,
  image: 'plugins/s3-cache',
  settings: {
    mode: action,
    path: '${DRONE_REPO}/${DRONE_BRANCH}-' + platformTag(platform) + '-rust' + rustVersion,
    fallback_path: '${DRONE_REPO}/master-' + platformTag(platform) + '-rust' + rustVersion,
    mount: [
      // Target cached by volume
      // 'target',
      CARGO_HOME + '/bin/',
      CARGO_HOME + '/registry/index/',
      CARGO_HOME + '/registry/cache/',
      // CARGO_HOME + '/git/db/',
      '.cache/',
    ],
    trigger: trigger,
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  } + cacheSettingsFromSecret,
};

local nightlyTrigger = {
  event: ['cron'],
  cron: ['nightly'],
};

local releaseTrigger = {
  event: [
    'tag',
  ],
  ref: [
    "refs/tags/v*",
  ],
};

local imageTrigger = {
  event: [
    'tag',
  ],
  ref: [
    "refs/tags/image-*",
  ],
};

local ciTrigger = {
  event: [
    'push',
    'pull_request',
  ],
  ref: {
    exclude: [
      "refs/tags/**",
    ],
  },
};

local baseImageVersion = "bullseye";
local baseImage = "debian";

local rustCheckImage = DOCKER_REPO_PREFIX + '/rust-check';
local buildDebImage = DOCKER_REPO_PREFIX + '/build-deb';

local buildImagSteps(platform) = [
  {
    name: 'build rust-check image',
    image: 'plugins/docker',
    settings: {
      username: {
        from_secret: 'docker_username',
      },
      password: {
        from_secret: 'docker_password',
      },
      registry: DOCKER_REGISTRY,
      repo: rustCheckImage,
      tags: [
        platformTag(platform),
      ],
      dockerfile: 'dockerfiles/rust-check.Dockerfile',
      build_args: [
        'RUST_VERSION=%s' % rustVersion,
        'BASE_IMAGE=%s' % baseImageVersion,
      ],
      debug: true,
    },
  },
] + [
  {
    name: 'build deb %s-%s image' % [debVersion.os, debVersion.release],
    image: 'plugins/docker',
    settings: {
      username: {
        from_secret: 'docker_username',
      },
      password: {
        from_secret: 'docker_password',
      },
      registry: DOCKER_REGISTRY,
      repo: buildDebImage,
      tags: [
        "%s_%s" % [platformTag(platform), debReleaseTag(debVersion)]
      ],
      dockerfile: 'dockerfiles/deb.Dockerfile',
      build_args: [
        'RUST_VERSION=%s' % rustVersion,
        'BASE_IMAGE=%s:%s' % [debVersion.os, debVersion.release],
        'LLD_PACKAGE=%s' % debVersion.linker,
      ],
      debug: true,
    },
  } for debVersion in debVersions
];

local ciSteps(platform) = [
  CacheStep('restore cache', 'restore', platform),
  {
    name: 'rust-check',
    pull: 'always',
    image: "%s:%s" % [rustCheckImage, platformTag(platform)],
    resources: {
      requests: {
        cpu: 2048,
        memory: '2GiB',
      },
      limits: {
        cpu: 2048,
        memory: '2GiB',
      },
    },
    environment: {
      CARGO_HOME: CARGO_HOME,
      RUST_VERSION: rustVersion,
    },
    commands: [
      'make env-check',
      'make check-rust-version',
      'cargo fetch',

      'make lint',
      'make test',

      'cargo build',

      // Prevent fail cache without clean cache
      'mkdir -p $CARGO_HOME/bin/',
    ],
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  },
  {
    name: 'e2e',
    image: 'python:3.9-%s' % baseImageVersion,
    resources: {
      requests: {
        cpu: 512,
        memory: '512MiB',
      },
      limit: {
        cpu: 512,
        memory: '512MiB',
      },
    },
    environment: {
      CARGO_HOME: CARGO_HOME,
      E2E_TESTS_WRAPPER: '',
      E2E_PODCAST_DLNA_PATH: 'target/debug/podcast-dlna',
      PYTHONPATH: '/drone/src',
      PIP_CACHE_DIR: "/drone/src/.cache/pip",
    },
    commands: [
      './$${E2E_PODCAST_DLNA_PATH} --version',
      'make e2e-debian-deps',
      'make e2e-pip-deps',
      'make e2e-lint-python',
      'make e2e-tests',
    ],
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  },
  {
    name: 'Rust clean',
    image: 'rust:%s-%s' % [rustVersion, baseImageVersion],
    pull: 'always',
    when: nightlyTrigger,
    failure: 'ignore',
    environment: {
      CARGO_HOME: CARGO_HOME,
    },
    commands: [
      'cargo install --force --no-default-features --features ci-autoclean cargo-cache',
      'cargo cache',
    ],
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  },
  CacheStep('rebuild cache', 'rebuild', platform),
  CacheStep('flush cache', 'flush', platform, {
    branch: [
      'master',
    ],
    status: [
      'success',
    ],
  }),
];

local releaseSteps(platform) = [
  {
    name: 'build cargo-deb',
    image: '%s:%s_%s' % [buildDebImage, platformTag(platform), debReleaseTag(oldestDebVersion)],
    pull: 'always',
    commands: [
      # Move to image
      'export PATH=$PATH:$HOME/.cargo/bin',

      'mkdir -p target/debian',
      './scripts/get-version.sh > target/debian/latest-version',

      # Build
      'cargo deb --version',
    ],
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  },
] + [
  {
    name: 'Build deb %s:%s' % [debVersion.os, debVersion.release],
    image: '%s:%s_%s' % [buildDebImage, platformTag(platform), debReleaseTag(debVersion)],
    pull: 'always',
    environment: {
      CARGO_HOME: CARGO_HOME,
    },
    commands: [
      # Move to image
      'export PATH=$PATH:$HOME/.cargo/bin',

      # Force rebuild
      'touch podcast-dlna/src/main.rs',

      'make build',
      './scripts/check_version.sh ${DRONE_TAG}',

      'make build-deb',

      'ls target/debian/',

      # Validate installation
      'dpkg -i target/debian/$(./scripts/deb-package-name.sh)',
      'podcast-dlna --version',

      'make build-clean',
    ],
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      }
    ],
  } for debVersion in debVersions
] + [
  {
    name: "Upload deb",
    image: "plugins/s3",
    settings: {
      source: "target/debian/*.deb",
      target: "/podcast-dlna/debian/",
      path_style: true,
      strip_prefix:  'target/debian/',
      acl: 'public-read',
    } + releaseSettingsFromSecret,
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      },
    ],
  },
  {
    name: "Upload latest-version",
    image: "plugins/s3",
    settings: {
      source: "target/debian/latest-version",
      target: "/podcast-dlna/debian/",
      path_style: true,
      strip_prefix:  'target/debian/',
      acl: 'public-read',
    } + releaseSettingsFromSecret,
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      },
    ],
  },
  {
    name: 'Clean debs',
    image: "%s:%s" % [baseImage, baseImageVersion],
    commands: [
      "rm target/debian/*",
    ],
    environment: {
      CARGO_HOME: CARGO_HOME,
    },
    volumes: [
      {
        name: 'cache-volume',
        path: '/drone/src/target',
      },
    ],
  },
];

local promotePipeline = {
  kind: "pipeline",
  type: "kubernetes",
  name: "deploy",
  trigger: {
    event: [
      "promote",
    ],
  },
  clone: {
    disable: true,
  },
  steps: [
    {
      name: "trigger deploy",
      image: "plugins/downstream",
      settings: {
        server: { from_secret: "drone_server" },
        token: { from_secret: "drone_token" },
        fork: true,
        deploy: "production",
        last_successful: true,
        params: [
          "TAGS=podcast-dlna",
          "PODCAST_DLNA_VERSION=${DRONE_TAG}",
        ],
        repositories: { from_secret: "podcast_dlna_deploy_repo" },
      },
    },
  ]
};

local kubernetesPipeline(pipeline_suffix, platform, steps, trigger = {}) = {
  kind: 'pipeline',
  type: 'kubernetes',
  name: "%s-%s" % [platformTag(platform), pipeline_suffix],

  node_selector: {
    'kubernetes.io/arch': platform.arch,
    'kubernetes.io/os': platform.os,
  },

  trigger: trigger,

  steps: steps(platform),

  volumes: [
    {
      name: "cache-volume",
      claim: {
        name: "build-disk-" + platform.arch,
        read_only: false,
      },
    },
  ],
  image_pull_secrets: ['docker_config_json']
};

local PLATFORM_LINUX_AMD64 = {
  os: 'linux',
  arch: 'amd64',
};

local PLATFORM_LINUX_ARM64 = {
  os: 'linux',
  arch: 'arm64',
};

local PLATFORM_LINUX_ARM = {
  os: 'linux',
  arch: 'arm',
};

[
  kubernetesPipeline('ci', PLATFORM_LINUX_AMD64, ciSteps, ciTrigger),

  kubernetesPipeline('nightly', PLATFORM_LINUX_AMD64, ciSteps, nightlyTrigger),
  kubernetesPipeline('release', PLATFORM_LINUX_AMD64, releaseSteps, releaseTrigger),
  kubernetesPipeline('images', PLATFORM_LINUX_AMD64, buildImagSteps, imageTrigger),

  kubernetesPipeline('nightly', PLATFORM_LINUX_ARM64, ciSteps, nightlyTrigger),
  kubernetesPipeline('release', PLATFORM_LINUX_ARM64, releaseSteps, releaseTrigger),
  kubernetesPipeline('images', PLATFORM_LINUX_ARM64, buildImagSteps, imageTrigger),

  kubernetesPipeline('nightly', PLATFORM_LINUX_ARM, ciSteps, nightlyTrigger),
  kubernetesPipeline('release', PLATFORM_LINUX_ARM, releaseSteps, releaseTrigger),
  kubernetesPipeline('images', PLATFORM_LINUX_ARM, buildImagSteps, imageTrigger),

  promotePipeline,
] + cacheSecrets
  + releaseSecrets
  + dockerSecrets
  + promoteSercets

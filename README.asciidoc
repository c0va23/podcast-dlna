PodcastDLNA
-----------

Listening to podcasts (RSS) on DLNA / UPNP compatible media devices (such as PlayStation 4)

Docs
----
- link:./podcast-dlna/README.ru.asciidoc[PodcastDLNA - RU]
- link:./audio-metadata/README.ru.asciidoc[audio-metadata - RU]
- link:./upnp/README.ru.asciidoc[upnp - RU]
- link:./ssdp/README.ru.asciidoc[SSDP - RU]

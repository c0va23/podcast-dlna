PODCAST_DLNA_SRC_FILES := $(shell find podcast-dlna/ -name *.rs  -printf '%p ')
PODCAST_DLNA_SRC_FILES += $(shell find audio-metadata/ -name *.rs  -printf '%p ')

target/release/podcast-dlna: ${PODCAST_DLNA_SRC_FILES} audio-metadata/** Cargo.lock Cargo.toml
	cargo build --release

target/debug/podcast-dlna: ${PODCAST_DLNA_SRC_FILES} audio-metadata/** Cargo.lock Cargo.toml
	cargo build

podcast-dlna/debian/config.env: target/release/podcast-dlna
	./target/release/podcast-dlna env-config > $@

.PHONY: build-arm64
build-arm64:
	cross build --release --target aarch64-unknown-linux-gnu

.PHONY: build-armv7
build-armv7:
	cross build --release --target armv7-unknown-linux-gnueabihf

.PHONY: build-debian-buster
build-debian-buster:
	docker run --rm -v ${PWD}:/app -w /app \
		rust:${RUST_VERSION} cargo build --release

.PHONY: build-clean
build-clean:
	test -f target/release/podcast-dlna && rm target/release/podcast-dlna || true
	test -f target/debug/podcast-dlna && rm target/debug/podcast-dlna || true

.PHONY: build
build: target/release/podcast-dlna

bin/cargo-deb: ./scripts/cargo-deb-install.sh
	./scripts/cargo-deb-install.sh $@

DEB_GEN_DEPS := \
	podcast-dlna/debian/config.env \

DEB_DEPS := ${DEB_GEN_DEPS} \
	target/release/podcast-dlna

DEB_VARIANT = $(shell ./scripts/deb-variant.sh)

build-deb: ${DEB_DEPS}
	cargo deb -p podcast-dlna --variant "${DEB_VARIANT}" --no-build

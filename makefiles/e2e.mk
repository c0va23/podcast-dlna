E2E_TESTS_PATH := e2e
E2E_TESTS_REPORTS_PATH := target/reports

E2E_TESTS_TARGET ?= ""
E2E_PODCAST_DLNA_PATH ?= target/debug/podcast-dlna

E2E_PYLINT_VERSION := 2.8.2

export PYTHONPATH := ${PWD}

${E2E_TESTS_REPORTS_PATH}:
	mkdir -p $@

e2e-debian-deps:
	apt update
	# dbus provide machine-id file
	apt install -y make dbus

export VIRTUAL_ENV := ${PWD}/.venv
export PATH := ${VIRTUAL_ENV}/bin:${PATH}

.venv:
	python -m venv $@
	touch $@

.venv/bin/pylint:
	python3 -m pip install pylint==${E2E_PYLINT_VERSION}

.venv/bin/robot: .venv ${E2E_TESTS_PATH}/requirements.txt
	python3 -m pip install -r ${E2E_TESTS_PATH}/requirements.txt
	touch $@

.PHONY: e2e-pip-deps
e2e-pip-deps: .venv/bin/robot .venv/bin/pylint

.PHONY: e2e-lint-python
e2e-lint-python: .venv/bin/pylint .venv/bin/robot
	python3 -m pylint ${E2E_TESTS_PATH}/lib/*.py

.PHONY: e2e-tests
e2e-tests: ${E2E_TESTS_REPORTS_PATH} ${E2E_PODCAST_DLNA_PATH} .venv/bin/robot
	# robot --version exist with 251. grep fix it.
	robot --version | grep Robot

	PODCAST_DLNA_PATH=${E2E_PODCAST_DLNA_PATH} \
		flock ${E2E_TESTS_REPORTS_PATH} \
			robot \
				--outputdir ${E2E_TESTS_REPORTS_PATH} \
				--randomize tests \
				${E2E_TESTS_PATH}/tests/${E2E_TESTS_TARGET}

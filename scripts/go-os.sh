#!/usr/bin/env bash

source_os=$(uname --operating-system)

case "$source_os" in
    "GNU/Linux")
        echo "linux";;
    *)
        exit 1;;
esac

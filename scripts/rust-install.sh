#!/usr/bin/env bash

set -xe

RUST_VERSION=$1

if which rustup; then
    rustup self update
else
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | \
        sh -s -- -y --default-toolchain none

    source ./scripts/load-cargo.sh
fi

# Install expected rust version if not installed
if ! rustup list | grep "${RUST_VERSION}"; then
    rustup install --profile minimal "${RUST_VERSION}"
fi

# Select expected rust version if not selected
if ! rustup show | grep "${RUST_VERSION}"; then
    rustup default "${RUST_VERSION}"
fi

rustc --version

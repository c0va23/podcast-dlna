#!/usr/bin/env bash

set -e

raw_expected_version=$1

# Strip prefix "v"
expected_version=${raw_expected_version:1}

if [[ -z "$expected_version" ]]; then
    echo "Expected version not provided"

    exit 1
fi

echo "Check binary version"

actual_binary_version=$(./scripts/get-version.sh)

if [[ -z "$actual_binary_version" ]]; then
    echo "Actual version not detected"

    return 1
fi

if [[ "$actual_binary_version" != "$expected_version" ]]; then
    echo "Expected version ${expected_version}, but actual version ${actual_binary_version}"
    exit 1
else
    echo "Valid version ${actual_binary_version}"
fi

echo "Check Cargo.lock version"
actual_cargo_lock_version=$(./scripts/get-cargo-lock-version.sh)

if [[ "$actual_cargo_lock_version" != "$expected_version" ]]; then
    echo "Cargo.lock not updated"
    exit 1
else
    echo "Cargo.lock OK"
fi

#!/usr/bin/env bash

TARGET_DIR=podcast-dlna/src
EXCLUDE_DIR=upnp
SEARCH_PATTERN="^\s*pub\s"
RS_FILE_PATTERN="${TARGET_DIR}(/[a-z0-9_]+)+\.rs"
FIELD_PATTERN="${RS_FILE_PATTERN}:.*pub.*[a-z0-9_]+:\s*\S+"

# Find all pub items
pub_items=$(
    grep \
        --recursive \
        --exclude-dir="$EXCLUDE_DIR" \
        --extended-regexp \
        "$SEARCH_PATTERN" \
        "$TARGET_DIR" \
)

# Exclude pub fields into structs
pub_types=$(
    echo "$pub_items" | \
    grep \
        --invert-match \
        --extended-regexp \
        "$FIELD_PATTERN" \
)

if [[ -z "$pub_types" ]]; then
    echo "Successful. No pub types."
    exit 0
fi

echo "Error. Found pub types:"
echo "$pub_types"

exit 1

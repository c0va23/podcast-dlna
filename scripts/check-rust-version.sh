#!/usr/bin/env bash

if [[ -z "${RUST_VERSION}" ]]; then
    echo "Envvar RUST_VERSION required"
    exit 1
fi

test_rust_version() {
    rustc --version | grep ${RUST_VERSION}
}

if test_rust_version; then
    exit 0
fi

echo "Need rebuild image"

exit 1

#!/usr/bin/env bash

# Usage: `./scripts/tag-release.sh -f origin`

set -xe

flags="$*"

version=v$(./scripts/get-version.sh)

git tag -f "${version}" 1>/dev/stderr

echo "git push ${flags} ${version}"

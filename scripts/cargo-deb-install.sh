#!/usr/bin/env bash

set -xe

TARGET_CARGO_DEB_VERSION="1.34.0"

TARGET_BINARY="$1"

install_cargo_deb() {
    # Debug speedup build
    cargo install \
          --version="${TARGET_CARGO_DEB_VERSION}" \
          --root="$PWD" \
          --force \
          cargo-deb
}

if [[ !  -f "${TARGET_BINARY}" ]]; then
    echo "cargo-deb not installed"
    install_cargo_deb
    exit 0
fi

ACTUAL_CARGO_DEB_VERSIO=$(${TARGET_BINARY} --version)

if [[ "${ACTUAL_CARGO_DEB_VERSIO}" != "${TARGET_CARGO_DEB_VERSION}" ]]; then
    echo "cargo-deb have not expected version"
    install_cargo_deb
    exit 0
fi

echo "cargo-deb have expected version ${TARGET_CARGO_DEB_VERSION}"

#!/usr/bin/env bash

if [[ -n "$CARGO_HOME" ]] && [[ -d "$CARGO_HOME" ]]; then
    export PATH="$PATH:$CARGO_HOME/.cargo/bin"
else
    export PATH="$PATH:$HOME/.cargo/bin"
fi

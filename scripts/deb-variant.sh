#!/usr/bin/env bash

OS_RELEASE_PATH=/etc/os-release

if [[ ! -f "${OS_RELEASE_PATH}" ]]; then
    echo "default"
    exit 0
fi

set +a
source "${OS_RELEASE_PATH}"
set -a

case "${ID}" in
    ubuntu | debian) echo "${ID,,}-${VERSION_CODENAME,,}";;
    *) echo "default";;
esac

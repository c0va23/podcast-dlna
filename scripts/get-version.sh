#!/usr/bin/env bash

package_name=podcast-dlna

cargo_metadata=$(cargo metadata --format-version=1)
jq_query=".workspace_members[]"
package_triple_list=$(echo "$cargo_metadata" | jq -r "$jq_query")
package_triple=$(echo "$package_triple_list" | grep  -o -E -e "(${package_name}) (\S+)")
package_version=$(echo "$package_triple" | sed -e "s/${package_name} //")

echo "${package_version}"

#!/usr/bin/env bash

deb_variant=$(./scripts/deb-variant.sh)
deb_version=$(./scripts/get-version.sh | sed -e 's/-/~/')
deb_arch=$(dpkg --print-architecture)

echo "podcast-dlna-${deb_variant}_${deb_version}_${deb_arch}.deb"

#!/usr/bin/env bash

source_arch=$(uname --machine)

case "$source_arch" in
    x86_64)
        echo amd64;;
    aarch64)
        echo arm64;;
    armv7l)
        echo arm;;
    *)
        exit 1;;
esac

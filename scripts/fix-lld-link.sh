#!/usr/bin/env bash

# Workaround for lld for debian:stretch and ubuntu:bionic [arm64]

set -xe

LLD_PACKAGE=$1

if [[ -z "${LLD_PACKAGE}" ]]; then
    echo Provide lld package name
    exit 1
fi

LLD_ALIAS_TARGET="/usr/bin/ld.${LLD_PACKAGE}"

if [[ -f "${LLD_ALIAS_TARGET}" ]] && [[ ! -f "/usr/bin/ld.lld" ]]; then
    echo "Create link from ${LLD_ALIAS_TARGET} to /usr/bin/ld.lld"
    ln -s "${LLD_ALIAS_TARGET}" /usr/bin/ld.lld
    exit 0
fi

echo "Link /usr/bin/ld.lld exists"
exit 0

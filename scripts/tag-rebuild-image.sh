#!/usr/bin/env bash

# Usage: `./scripts/tag-rebuild-image.sh`

timecode=$(date +"%Y-%m-%dT%H-%M")
image_tag="image-${timecode}"

git tag "${image_tag}"
echo "git push origin ${image_tag}"

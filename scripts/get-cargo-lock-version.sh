#!/usr/bin/env bash

set -e

# Get podcast-dlna name and version lines
podcast_dlna_section=$(grep -A 1 -E '^name = "podcast-dlna"$' Cargo.lock)

# Truncate name line
version_line=$(echo "${podcast_dlna_section}" | tail -n 1)

# Extract version value
version_value=$(echo "${version_line}" | sed -E -e 's/^version = "([^"]+)"$/\1/')

echo "${version_value}"

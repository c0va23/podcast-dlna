fn main() {
    set_app_long_version_env();
}

fn set_app_long_version_env() {
    let git_output = std::process::Command::new("git")
        .arg("show")
        .arg("--format=%h %cI")
        .output()
        .unwrap();
    let git_data = String::from_utf8(git_output.stdout).unwrap();
    let git_data_parts = git_data.trim_end().split(' ').collect::<Vec<&str>>();
    let commit_hash = git_data_parts[0];
    let commit_time = git_data_parts[1];

    let long_version = format!(
        "{version} ({commit_hash} {commit_time})",
        version = env!("CARGO_PKG_VERSION"),
        commit_hash = commit_hash,
        commit_time = commit_time,
    );

    let env_name = "APP_LONG_VERSION";
    println!(
        "cargo:rustc-env={env_name}={long_version}",
        env_name = env_name,
        long_version = long_version,
    )
}

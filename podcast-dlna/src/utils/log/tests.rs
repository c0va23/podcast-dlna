use std::io::Write;
use std::sync::Arc;
use std::sync::Mutex;

use chrono::TimeZone;
use pretty_assertions::assert_eq;

#[derive(Clone)]
struct Buffer {
    inner: Arc<Mutex<Vec<u8>>>,
}

impl Buffer {
    fn new() -> Self {
        Self {
            inner: Arc::new(Mutex::new(Vec::new())),
        }
    }

    fn buffer(&self) -> Vec<u8> {
        self.inner.lock().expect("should not be poisoned").clone()
    }
}

impl Write for Buffer {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let mut inner = self.inner.lock().expect("should bot be poisoned");
        inner.extend(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

#[derive(Clone, Copy)]
struct MockClock {
    fixed_time: chrono::DateTime<chrono::Utc>,
}

impl MockClock {
    const fn new(fixed_time: chrono::DateTime<chrono::Utc>) -> Self {
        Self { fixed_time }
    }
}

impl super::Clock for MockClock {
    fn now(&self) -> chrono::DateTime<chrono::Utc> {
        self.fixed_time
    }
}

#[test]
#[allow(clippy::too_many_lines)]
fn test_logfmt_formatter() {
    use tracing::metadata::Level;
    use tracing_subscriber::prelude::*;

    use super::LogfmtLayer;

    struct TestCase {
        name: &'static str,
        message_args: std::fmt::Arguments<'static>,
        level: Level,
        expected_log_line: String,
    }

    let file = "log.rs";
    let line = 123;
    let module_path = "utils::log";
    let time = chrono::prelude::Utc
        .ymd(2021, 8, 26)
        .and_hms_milli(12, 34, 56, 789);
    let log_prefix = "time=2021-08-26T12:34:56.789Z ";
    let log_suffix = " module=utils::log file=log.rs line=123\n";

    let mock_clock = MockClock::new(time);

    let test_cases = vec![
        TestCase {
            name: "Simple",
            message_args: format_args!("Simple"),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned() + r#"level=INFO msg=Simple"# + log_suffix,
        },
        TestCase {
            name: "Simple Warm",
            message_args: format_args!("Simple"),
            level: Level::WARN,
            expected_log_line: log_prefix.to_owned() + r#"level=WARN msg=Simple"# + log_suffix,
        },
        TestCase {
            name: "Two words",
            message_args: format_args!("Two words"),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned() + r#"level=INFO msg="Two words""# + log_suffix,
        },
        TestCase {
            name: "Escaping double quote with spaces",
            message_args: format_args!(r#""key": 123"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned()
                + r#"level=INFO msg="\"key\": 123""#
                + log_suffix,
        },
        TestCase {
            name: "Escaping double quote without spaces",
            message_args: format_args!(r#""key":123"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned() + r#"level=INFO msg=\"key\":123"# + log_suffix,
        },
        TestCase {
            name: "Escaping slash",
            message_args: format_args!(r#"{{"key": "\"value\""}}"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned()
                + r#"level=INFO msg="{\"key\": \"\\\"value\\\"\"}""#
                + log_suffix,
        },
        TestCase {
            name: "Escaping new line",
            message_args: format_args!(r#"1\r\n2\n3"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned()
                + r#"level=INFO msg=1\\r\\n2\\n3"#
                + log_suffix,
        },
        TestCase {
            name: "Escaping equal",
            message_args: format_args!(r#"http://example.com/?param=value"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned()
                + r#"level=INFO msg=http://example.com/?param\=value"#
                + log_suffix,
        },
        TestCase {
            name: "Escaping tab",
            message_args: format_args!(r#"a\tb"#),
            level: Level::INFO,
            expected_log_line: log_prefix.to_owned() + r#"level=INFO msg=a\\tb"# + log_suffix,
        },
    ];

    for test_case in test_cases {
        let buffer = Buffer::new();

        let logfmt_layer = LogfmtLayer::new(buffer.clone()).with_clock(mock_clock);
        let subscriber = tracing_subscriber::registry().with(logfmt_layer);
        tracing::subscriber::with_default(subscriber, || match test_case.level {
            Level::WARN => tracing::event!(
                Level::WARN,
                log.target = module_path,
                log.module_path = module_path,
                log.file = file,
                log.line = line,
                "{}",
                test_case.message_args
            ),
            Level::INFO => tracing::event!(
                Level::INFO,
                log.target = module_path,
                log.module_path = module_path,
                log.file = file,
                log.line = line,
                "{}",
                test_case.message_args
            ),
            _ => unreachable!(),
        });

        assert_eq!(
            test_case.expected_log_line,
            String::from_utf8(buffer.buffer()).unwrap(),
            "Test case: {name}",
            name = test_case.name,
        );
    }
}

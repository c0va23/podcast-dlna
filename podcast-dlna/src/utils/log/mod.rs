use std::collections::LinkedList;
use std::fmt::Debug;
use std::fmt::Display;
use std::io::LineWriter;
use std::io::Write;
use std::marker::PhantomData;
use std::sync::Mutex;

use tracing::field::Field;
use tracing::field::Visit;
use tracing::span;
use tracing::Subscriber;
use tracing_subscriber::layer::Context;
use tracing_subscriber::registry::LookupSpan;
use tracing_subscriber::registry::SpanRef;
use tracing_subscriber::Layer;

fn escape_log_mgs(src_msg: &str) -> String {
    let escaped_msg = src_msg
        .replace('\\', "\\\\")
        .replace('"', "\\\"")
        .replace('=', "\\=")
        .replace('\t', "\\t")
        .replace('\n', "\\n")
        .replace('\r', "\\r");
    let need_quoted = escaped_msg.chars().any(|c| c == ' ');
    if need_quoted {
        format!("\"{}\"", escaped_msg)
    } else {
        escaped_msg
    }
}

type ChronoTime = chrono::DateTime<chrono::Utc>;
type ChronoDuration = chrono::Duration;

struct LogfmtRecord<'a> {
    time: ChronoTime,
    level: tracing::Level,
    module: &'a str,
    field_recorder: FieldRecorder,
    override_message: Option<String>,
    metrics: Option<MetricsHolder>,
}

impl<'a> LogfmtRecord<'a> {
    fn new(
        time: ChronoTime,
        metadata: &'a tracing::Metadata,
        field_recorder: FieldRecorder,
    ) -> Self {
        let level = *metadata.level();
        let module = metadata.module_path().unwrap_or_else(|| metadata.target());

        Self {
            time,
            level,
            module,
            field_recorder,
            override_message: None,
            metrics: None,
        }
    }

    // Clippy bug
    #[allow(clippy::missing_const_for_fn)]
    fn with_override_message(self, message: String) -> Self {
        Self {
            override_message: Some(message),
            ..self
        }
    }

    // Clippy bug
    #[allow(clippy::missing_const_for_fn)]
    fn with_metrics(self, metrics: Option<MetricsHolder>) -> Self {
        Self { metrics, ..self }
    }

    fn module(&self) -> &str {
        // Workaround for log module path.
        self.field_recorder
            .module
            .as_ref()
            .map_or(self.module, String::as_str)
    }

    fn message(&self) -> Option<&str> {
        self.override_message
            .as_deref()
            .or_else(|| self.field_recorder.message.as_deref())
    }
}

// TODO: remove derive Clone
#[derive(Clone)]
struct FieldRecorder {
    message: Option<String>,
    module: Option<String>,
    #[allow(clippy::linkedlist)]
    fields: LinkedList<(String, String)>,
    request_id: Option<String>,
    parent_span_id: Option<u64>,
}

impl FieldRecorder {
    const MESSAGE_FIELD_NAME: &'static str = "message";
    const MODULE_FIELD_NAME: &'static str = "module_path";
    const TARGET_FIELD_NAME: &'static str = "target";
    const LOG_FILED_PREFIX: &'static str = "log.";
    const REQUEST_ID: &'static str = "request_id";

    const fn new() -> Self {
        Self {
            message: None,
            module: None,
            fields: LinkedList::new(),
            request_id: None,
            parent_span_id: None,
        }
    }

    fn from_ctx<S>(ctx: &Context<'_, S>) -> Self
    where
        S: Subscriber + for<'a> LookupSpan<'a>,
    {
        let request_id = Self::request_id_from_ctx(ctx);
        let parent_span_id = Self::parent_span_id_from_ctx(ctx);

        Self {
            request_id,
            parent_span_id,
            ..Self::new()
        }
    }

    fn request_id_from_ctx<S>(ctx: &Context<'_, S>) -> Option<String>
    where
        S: Subscriber + for<'a> LookupSpan<'a>,
    {
        ctx.lookup_current().and_then(|span| {
            span.extensions()
                .get::<RequestIdHolder>()
                .map(RequestIdHolder::request_id)
        })
    }

    fn parent_span_id_from_ctx<S>(ctx: &Context<'_, S>) -> Option<u64>
    where
        S: Subscriber + for<'a> LookupSpan<'a>,
    {
        ctx.current_span().id().map(tracing::Id::into_u64)
    }

    #[allow(clippy::needless_pass_by_value)]
    fn prepends_field(self, field_name: impl ToString, field_value: impl ToString) -> Self {
        let mut fields = self.fields;

        fields.push_front((field_name.to_string(), field_value.to_string()));

        Self { fields, ..self }
    }

    fn field_name(field: &Field) -> &str {
        field.name().trim_start_matches(Self::LOG_FILED_PREFIX)
    }
}

impl Visit for FieldRecorder {
    fn record_debug(&mut self, field: &Field, value: &dyn Debug) {
        let formatted_value = format!("{:?}", value);
        let escaped_value = escape_log_mgs(&formatted_value);
        match Self::field_name(field) {
            Self::MESSAGE_FIELD_NAME => {
                self.message = Some(escaped_value);
            }
            Self::REQUEST_ID => {
                self.request_id = Some(escaped_value);
            }
            field_name => self.fields.push_back((field_name.into(), escaped_value)),
        }
    }

    fn record_str(&mut self, field: &Field, value: &str) {
        let escaped_value = escape_log_mgs(value);
        match Self::field_name(field) {
            // Skip fields
            Self::MODULE_FIELD_NAME | Self::TARGET_FIELD_NAME => {
                self.module = Some(escaped_value);
            }
            field_name => self.fields.push_back((field_name.into(), escaped_value)),
        }
    }
}

pub trait Clock {
    fn now(&self) -> ChronoTime;
}

pub struct SystemClock;

impl Clock for SystemClock {
    fn now(&self) -> ChronoTime {
        chrono::Utc::now()
    }
}

#[derive(Clone)]
struct RequestIdHolder {
    request_id: String,
}

impl RequestIdHolder {
    const fn new(request_id: String) -> Self {
        Self { request_id }
    }

    fn request_id(&self) -> String {
        self.request_id.clone()
    }
}

struct MetricsHolder {
    open_time: ChronoTime,
    close_time: Option<ChronoTime>,
    enter_time: Option<ChronoTime>,
    busy_duration: ChronoDuration,
}

impl MetricsHolder {
    fn new(start_time: ChronoTime) -> Self {
        Self {
            open_time: start_time,
            close_time: None,
            enter_time: None,
            busy_duration: ChronoDuration::seconds(0),
        }
    }

    #[allow(clippy::missing_const_for_fn)]
    fn with_close_time(&self, close_time: ChronoTime) -> Self {
        Self {
            close_time: Some(close_time),
            ..*self
        }
    }

    fn duration(&self) -> Option<std::time::Duration> {
        self.close_time
            .and_then(|close_time| (close_time - self.open_time).to_std().ok())
    }

    fn busy_duration(&self) -> Option<std::time::Duration> {
        self.busy_duration.to_std().ok()
    }

    fn track_enter_time(&mut self, enter_time: ChronoTime) {
        self.enter_time = Some(enter_time);
    }

    fn track_exit_time(&mut self, exit_time: ChronoTime) {
        if let Some(enter_time) = self.enter_time {
            self.busy_duration = self.busy_duration + (exit_time - enter_time);
            self.enter_time = None;
        }
    }
}

pub struct LogfmtLayer<S, W, C>
where
    S: Subscriber + for<'a> LookupSpan<'a>,
    W: Write,
    C: Clock,
{
    line_writer: Mutex<LineWriter<W>>,
    clock: C,
    subscriber: PhantomData<S>,
}

impl<S, W> LogfmtLayer<S, W, SystemClock>
where
    S: Subscriber + for<'a> LookupSpan<'a>,
    W: Write,
{
    pub(crate) fn new(writer: W) -> Self {
        Self {
            line_writer: Mutex::new(LineWriter::new(writer)),
            clock: SystemClock,
            subscriber: PhantomData::<S>,
        }
    }

    #[cfg(test)]
    fn with_clock<NC>(self, clock: NC) -> LogfmtLayer<S, W, NC>
    where
        NC: Clock,
    {
        LogfmtLayer {
            subscriber: self.subscriber,
            line_writer: self.line_writer,
            clock,
        }
    }
}

// Format and write log records
impl<S, W, C> LogfmtLayer<S, W, C>
where
    Self: 'static,
    S: Subscriber + for<'a> LookupSpan<'a>,
    W: Write,
    C: Clock,
{
    fn write_record(&self, record: &LogfmtRecord) -> std::io::Result<()> {
        let mut line_writer = self.line_writer.lock().expect("log writer poisoned");

        Self::write_common_fields(&mut line_writer, record)?;

        Self::write_recorded_fields(&mut line_writer, &record.field_recorder)?;

        if let Some(ref metrics) = record.metrics {
            Self::write_metrics(&mut line_writer, metrics)?;
        }

        writeln!(line_writer)
    }

    fn write_common_fields(
        line_writer: &mut LineWriter<W>,
        record: &LogfmtRecord,
    ) -> std::io::Result<()> {
        let time_value = record
            .time
            .to_rfc3339_opts(chrono::SecondsFormat::Millis, true);
        write!(line_writer, "time={} ", time_value)?;
        write!(line_writer, "level={} ", record.level)?;

        if let Some(msg) = record.message() {
            write!(line_writer, "msg={}", msg)?;
        }

        Self::write_display_field(line_writer, "module", &record.module())?;

        Ok(())
    }

    fn write_metrics(
        line_writer: &mut LineWriter<W>,
        metrics: &MetricsHolder,
    ) -> std::io::Result<()> {
        if let Some(duration) = metrics.duration() {
            Self::write_duration_field(line_writer, "duration", duration)?;
        }

        if let Some(busy_duration) = metrics.busy_duration() {
            Self::write_duration_field(line_writer, "busy_duration", busy_duration)?;
        }

        Ok(())
    }

    fn write_recorded_fields(
        line_writer: &mut LineWriter<W>,
        field_recorder: &FieldRecorder,
    ) -> std::io::Result<()> {
        if let Some(ref parent_span_id) = field_recorder.parent_span_id {
            Self::write_display_field(line_writer, "parent_span_id", parent_span_id)?;
        }

        if let Some(ref request_id) = field_recorder.request_id {
            Self::write_display_field(line_writer, "request_id", request_id)?;
        }

        for (field, value) in &field_recorder.fields {
            Self::write_display_field(line_writer, field, value)?;
        }

        Ok(())
    }

    fn write_display_field(
        line_writer: &mut LineWriter<W>,
        field_name: &str,
        field_value: &dyn Display,
    ) -> std::io::Result<()> {
        write!(line_writer, " {}={}", field_name, field_value)
    }

    fn write_duration_field(
        line_writer: &mut LineWriter<W>,
        field_name: &str,
        field_value: std::time::Duration,
    ) -> std::io::Result<()> {
        write!(
            line_writer,
            " {}={:.6}",
            field_name,
            field_value.as_secs_f64(),
        )
    }
}

// Helpers
impl<S, W, C> LogfmtLayer<S, W, C>
where
    Self: 'static,
    S: Subscriber + for<'a> LookupSpan<'a>,
    W: Write,
    C: Clock,
{
    fn get_span<'a>(id: &'a span::Id, ctx: &'a Context<'_, S>) -> SpanRef<'a, S> {
        ctx.span(id).expect("span not found, bug")
    }

    fn log_event(&self, event: &tracing::Event<'_>, ctx: &Context<'_, S>) {
        let time = self.clock.now();

        let mut recorder = FieldRecorder::from_ctx(ctx);

        event.record(&mut recorder);

        let record = LogfmtRecord::new(time, event.metadata(), recorder);

        self.write_record(&record).expect("write error");
    }

    fn track_busy_time(
        &self,
        id: &span::Id,
        ctx: &Context<'_, S>,
        action: impl Fn(&mut MetricsHolder, ChronoTime),
    ) {
        let time = self.clock.now();

        let span = Self::get_span(id, ctx);

        let mut extensions = span.extensions_mut();
        if let Some(metrics_holder) = extensions.get_mut::<MetricsHolder>() {
            action(metrics_holder, time);
        }
    }

    fn build_span_record<'a>(&self, span: &SpanRef<'a, S>, message: &str) -> LogfmtRecord<'a> {
        let time = self.clock.now();

        let extensions = span.extensions();
        let escaped_msg = escape_log_mgs(message);

        let recorder = extensions
            .get::<FieldRecorder>()
            .map_or_else(FieldRecorder::new, Clone::clone)
            .prepends_field("span_name", span.name())
            .prepends_field("span_id", span.id().into_non_zero_u64());

        let metrics = extensions
            .get::<MetricsHolder>()
            .map(|metrics| metrics.with_close_time(time));

        LogfmtRecord::new(time, span.metadata(), recorder)
            .with_metrics(metrics)
            .with_override_message(escaped_msg)
    }

    fn log_span(&self, id: &span::Id, ctx: &Context<'_, S>, message: &str) {
        let span = Self::get_span(id, ctx);

        let record = self.build_span_record(&span, message);

        self.write_record(&record).expect("write error");
    }
}

impl<S, W, C> Layer<S> for LogfmtLayer<S, W, C>
where
    Self: 'static,
    S: Subscriber + for<'a> LookupSpan<'a>,
    W: Write,
    C: Clock,
{
    fn on_new_span(&self, attrs: &span::Attributes<'_>, id: &span::Id, ctx: Context<'_, S>) {
        let time = self.clock.now();

        let span = Self::get_span(id, &ctx);
        let mut recorder = FieldRecorder::from_ctx(&ctx);

        attrs.record(&mut recorder);

        let mut extensions = span.extensions_mut();

        if let Some(ref request_id) = recorder.request_id {
            let request_id_holder = RequestIdHolder::new(request_id.clone());
            extensions.insert(request_id_holder);
        }

        extensions.insert(recorder);

        let metrics_holder = MetricsHolder::new(time);
        extensions.insert(metrics_holder);
    }

    fn on_event(&self, event: &tracing::Event<'_>, ctx: Context<'_, S>) {
        self.log_event(event, &ctx);
    }

    fn on_enter(&self, id: &span::Id, ctx: Context<'_, S>) {
        self.track_busy_time(id, &ctx, MetricsHolder::track_enter_time);
    }

    fn on_exit(&self, id: &span::Id, ctx: Context<'_, S>) {
        self.track_busy_time(id, &ctx, MetricsHolder::track_exit_time);
    }

    fn on_close(&self, id: span::Id, ctx: Context<'_, S>) {
        self.log_span(&id, &ctx, "span close");
    }
}

#[cfg(test)]
mod tests;

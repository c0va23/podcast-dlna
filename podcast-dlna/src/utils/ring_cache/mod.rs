use std::future::Future;

pub struct Bucket<K, V> {
    ring: Vec<(K, V)>,
    index: usize,
    capacity: usize,
    allocated: usize,
}

impl<K, V> Bucket<K, V> {
    pub(crate) fn new(capacity: usize) -> Self {
        Self {
            ring: Vec::with_capacity(capacity),
            allocated: 0,
            index: 0,
            capacity,
        }
    }

    pub(crate) fn get(&self, find_key: &K) -> Option<V>
    where
        K: PartialEq,
        V: Clone,
    {
        self.ring.iter().take(self.allocated).find_map(|(key, v)| {
            // `find_key` should be on left side in this equal.
            if find_key == key {
                Some(v.clone())
            } else {
                None
            }
        })
    }

    pub(crate) fn push(&mut self, key: K, value: V)
    where
        K: PartialEq,
        V: Clone,
    {
        let find_key = &key;
        if let Some((index, _)) = self
            .ring
            .iter()
            .take(self.allocated)
            .enumerate()
            .find(|(_, (key, _))| key == find_key)
        {
            self.ring[index] = (key, value);
        } else if self.capacity == self.allocated {
            self.ring[self.index] = (key, value);
        } else {
            self.ring.push((key, value));
            self.allocated += 1;
        }
        self.index = (self.index + 1) % self.capacity;
    }

    pub(crate) async fn get_or_push<F, E>(&mut self, key: K, perform: F) -> Result<V, E>
    where
        K: PartialEq + Sync + Send,
        V: Clone + Sync + Send,
        F: Future<Output = Result<V, E>> + Send,
    {
        if let Some(value) = self.get(&key) {
            Ok(value)
        } else {
            let value = perform.await?;
            self.push(key, value.clone());
            Ok(value)
        }
    }
}

#[cfg(test)]
mod tests;

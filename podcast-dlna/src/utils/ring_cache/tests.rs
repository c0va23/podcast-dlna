use super::Bucket;

#[test]
#[allow(clippy::too_many_lines)]
fn test_bucket() {
    enum Action {
        Get {
            key: String,
            expected_value: Option<String>,
        },
        Push {
            key: String,
            value: String,
        },
    }

    struct TestSuite {
        capacity: usize,
        actions: Vec<Action>,
    }

    let test_suites = vec![
        TestSuite {
            capacity: 1,
            actions: vec![
                Action::Push {
                    key: "key".into(),
                    value: "value".into(),
                },
                Action::Get {
                    key: "key".into(),
                    expected_value: Some("value".into()),
                },
                Action::Push {
                    key: "key2".into(),
                    value: "value2".into(),
                },
                Action::Get {
                    key: "key2".into(),
                    expected_value: Some("value2".into()),
                },
                Action::Get {
                    key: "key".into(),
                    expected_value: None,
                },
            ],
        },
        TestSuite {
            capacity: 2,
            actions: vec![
                Action::Get {
                    key: "key1".into(),
                    expected_value: None,
                },
                Action::Get {
                    key: "key2".into(),
                    expected_value: None,
                },
                Action::Push {
                    key: "key1".into(),
                    value: "value1".into(),
                },
                Action::Push {
                    key: "key2".into(),
                    value: "value2".into(),
                },
                Action::Get {
                    key: "key1".into(),
                    expected_value: Some("value1".into()),
                },
                Action::Get {
                    key: "key2".into(),
                    expected_value: Some("value2".into()),
                },
                Action::Push {
                    key: "key3".into(),
                    value: "value3".into(),
                },
                Action::Get {
                    key: "key3".into(),
                    expected_value: Some("value3".into()),
                },
                Action::Get {
                    key: "key2".into(),
                    expected_value: Some("value2".into()),
                },
                Action::Get {
                    key: "key1".into(),
                    expected_value: None,
                },
                Action::Push {
                    key: "key4".into(),
                    value: "value4".into(),
                },
                Action::Get {
                    key: "key4".into(),
                    expected_value: Some("value4".into()),
                },
                Action::Get {
                    key: "key3".into(),
                    expected_value: Some("value3".into()),
                },
                Action::Get {
                    key: "key2".into(),
                    expected_value: None,
                },
                Action::Push {
                    key: "key4".into(),
                    value: "value4-updated".into(),
                },
                Action::Get {
                    key: "key3".into(),
                    expected_value: Some("value3".into()),
                },
                Action::Get {
                    key: "key4".into(),
                    expected_value: Some("value4-updated".into()),
                },
            ],
        },
    ];

    for test_suite in test_suites {
        let mut bucket = Bucket::new(test_suite.capacity);
        for action in test_suite.actions {
            match action {
                Action::Push { key, value } => {
                    bucket.push(key, value);
                }
                Action::Get {
                    key,
                    expected_value,
                } => {
                    let value = bucket.get(&key);
                    assert_eq!(value, expected_value);
                }
            }
        }
    }
}

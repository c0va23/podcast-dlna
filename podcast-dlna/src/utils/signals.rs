pub async fn wait_signal() -> Result<(), std::io::Error> {
    let mut term_signal =
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate()).unwrap();

    let mut int_signal =
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::interrupt()).unwrap();

    tokio::select! {
        _ = term_signal.recv() => {
            info!("Receive TERM signal");
            Ok(())
        },
        _ = int_signal.recv() => {
            info!("Receive INT signal");
            Ok(())
        },
    }
}

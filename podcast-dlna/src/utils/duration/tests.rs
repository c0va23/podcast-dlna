use super::*;

#[test]
fn test_parse_iso8601_duration() {
    struct TestCase {
        source: String,
        expected_result: Option<(u64, u64)>,
    }

    let test_cases = vec![
        TestCase {
            source: "PT1H".into(),
            expected_result: Some((HOUR, 0)),
        },
        TestCase {
            source: "PT15M".into(),
            expected_result: Some((15 * MINUTE, 0)),
        },
        TestCase {
            source: "PT3H".into(),
            expected_result: Some((3 * HOUR, 0)),
        },
        TestCase {
            source: "P2W".into(),
            expected_result: Some((2 * WEEK, 0)),
        },
        TestCase {
            source: "P2DT12H".into(),
            expected_result: Some((2 * DAY + 12 * HOUR, 0)),
        },
        TestCase {
            source: "PT2.5S".into(),
            expected_result: Some((2, 500)),
        },
        TestCase {
            source: "P3H".into(),
            expected_result: None,
        },
    ];

    for test_case in &test_cases {
        let result = parse_iso8601(test_case.source.as_str())
            .ok()
            .map(|d| (d.as_secs(), u64::from(d.subsec_millis())));
        assert_eq!(test_case.expected_result, result);
    }
}

#[test]
fn format_colons_with_hours() {
    let duration = Duration::from_secs(9116);
    let formatted_duration = format_colons(duration);
    assert_eq!("02:31:56".to_owned(), formatted_duration);
}

#[test]
fn format_colons_with_minutes() {
    let duration = Duration::from_secs(1916);
    let formatted_duration = format_colons(duration);
    assert_eq!("00:31:56".to_owned(), formatted_duration);
}

#[test]
fn format_colons_with_seconds() {
    let duration = Duration::from_secs(56);
    let formatted_duration = format_colons(duration);
    assert_eq!("00:00:56".to_owned(), formatted_duration);
}

#[test]
fn test_parse_colons() {
    struct TestCase {
        description: &'static str,
        source: &'static str,
        expected_result: Option<Duration>,
    }

    let test_cases = vec![
        TestCase {
            description: "Parse all parts",
            source: "02:31:56",
            expected_result: Some(Duration::from_secs(2 * HOUR + 31 * MINUTE + 56)),
        },
        TestCase {
            description: "Parse two parts with padding",
            source: "00:31:56",
            expected_result: Some(Duration::from_secs(31 * MINUTE + 56)),
        },
        TestCase {
            description: "Parse two parts without padding",
            source: "31:56",
            expected_result: Some(Duration::from_secs(31 * MINUTE + 56)),
        },
        TestCase {
            description: "Parse one parts with padding",
            source: "00:00:56",
            expected_result: Some(Duration::from_secs(56)),
        },
        TestCase {
            description: "Parse one parts without padding",
            source: "56",
            expected_result: Some(Duration::from_secs(56)),
        },
        TestCase {
            description: "Error with day",
            source: "01:02:31:56",
            expected_result: None,
        },
        TestCase {
            description: "Error with invalid value",
            source: "invalid",
            expected_result: None,
        },
    ];

    for test_case in test_cases {
        let parse_result = parse_colons(test_case.source).ok();
        assert_eq!(
            test_case.expected_result, parse_result,
            "Test cases: {}: Expected: {:?}; Got: {:?}",
            test_case.description, test_case.expected_result, parse_result,
        );
    }
}

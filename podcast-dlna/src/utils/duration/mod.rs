// imports
use std::num::ParseIntError;
use std::str::FromStr;
use std::time::Duration;

// modules
#[cfg(test)]
mod tests;

const MINUTE: u64 = 60;
const HOUR: u64 = 60 * MINUTE;
const DAY: u64 = 24 * HOUR;
const WEEK: u64 = 7 * DAY;

#[derive(thiserror::Error, Debug)]
#[error("parse iso8601 error: {message}")]
pub struct Iso8601Error {
    message: String,
}

impl Iso8601Error {
    const fn new(message: String) -> Self {
        Self { message }
    }
}

pub fn parse_iso8601(raw_duration: &str) -> Result<Duration, Iso8601Error> {
    let iso8601_duration = iso8601::duration(raw_duration).map_err(Iso8601Error::new)?;

    match iso8601_duration {
        iso8601::Duration::Weeks(week) => Ok(Duration::from_secs(u64::from(week) * WEEK)),
        iso8601::Duration::YMDHMS {
            year: _,
            month: _,
            day,
            hour,
            minute,
            second,
            millisecond,
        } => Ok(Duration::from_millis(u64::from(millisecond))
            + Duration::from_secs(
                u64::from(second)
                    + MINUTE * u64::from(minute)
                    + HOUR * u64::from(hour)
                    + DAY * u64::from(day),
            )),
    }
}

pub fn format_colons(duration: Duration) -> String {
    let second_total = duration.as_secs();

    let hour_count = second_total / HOUR;

    let minute_count = (second_total - hour_count * HOUR) / MINUTE;

    let second_count = second_total - hour_count * HOUR - minute_count * MINUTE;

    format!("{:02}:{:02}:{:02}", hour_count, minute_count, second_count)
}

const DURATION_MAX_PARTS: usize = 3;
const DURATION_PART_MULTIPLEXER: u64 = 60;

/// Parse from format "HH:MM:SS". Milliseconds and days not parsed.
/// Ignore part overflow, `"100:60"` - valid value.
pub fn parse_colons(source_duration: &str) -> Result<Duration, ParseIntError> {
    let seconds = source_duration
        .splitn(DURATION_MAX_PARTS, ':')
        .map(FromStr::from_str)
        .collect::<Result<Vec<u64>, ParseIntError>>()?
        .into_iter()
        .fold(0, |value, item| value * DURATION_PART_MULTIPLEXER + item);

    Ok(Duration::from_secs(seconds))
}

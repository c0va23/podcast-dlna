#![allow(clippy::default_trait_access)]

#[cfg(test)]
use mockall::automock;

use thiserror::Error as ThisError;

#[derive(ThisError, Debug)]
pub enum Error {
    #[error("Device UUID file read error: {0}")]
    FileRead(#[from] std::io::Error),

    #[error("Device UUID format error: {0}")]
    Uuid(#[from] uuid::Error),

    #[error("Call hostname error: {0}")]
    CallHostname(std::io::Error),

    #[error("Get uname error: {0}")]
    Uname(std::io::Error),
}

#[cfg(unix)]
pub fn os_name_version() -> Result<String, Error> {
    let info = uname::uname().map_err(Error::Uname)?;

    Ok(format!(
        "{os}/{version}",
        os = info.sysname,
        version = info.release,
    ))
}

#[cfg(not(unix))]
pub(crate) fn os_name_version() -> Result<String, Error> {
    unimplemented!()
}

#[derive(Debug, clap::Parser)]
pub struct Config {
    /// Path to device uuid
    #[clap(long, env, default_value = "/etc/machine-id")]
    #[cfg(target_os = "linux")]
    device_uuid_path: String,

    /// Path to device uuid (DBus fallback)
    #[clap(long, env, default_value = "/var/lib/dbus/machine-id")]
    #[cfg(target_os = "linux")]
    device_uuid_fallback_path: String,

    /// Server name (show into discovery)
    #[clap(long, env, default_value = "Podcast DLNA")]
    server_name: String,

    /// Not add hostname to server name
    #[clap(long, env)]
    server_name_without_hostname: bool,

    /// UPNP version into User agent and Server header
    #[clap(long, env, default_value = "1.0")]
    upnp_version: String,
}

#[cfg_attr(test, automock)]
pub trait DeviceUuidFetcher {
    fn device_uuid(&self) -> Result<uuid::Uuid, Error>;
}

#[cfg_attr(test, automock)]
pub trait ServerNameFetcher {
    fn server_name(&self) -> Result<String, Error>;
}

#[cfg_attr(test, automock)]
pub trait UserAgentFetcher {
    fn user_agent(&self) -> Result<String, Error>;
}

pub struct DeviceInfo {
    config: Config,
}

impl DeviceInfo {
    pub(crate) const fn new(config: Config) -> Self {
        Self { config }
    }

    #[cfg(target_os = "linux")]
    fn try_machine_id(machine_id_path: &str) -> Result<uuid::Uuid, Error> {
        use std::fs::File;
        use std::io::Read;

        let mut file = File::open(machine_id_path)?;

        let mut machine_id = String::new();
        file.read_to_string(&mut machine_id)?;

        let uuid = uuid::Uuid::parse_str(machine_id.trim_end())?;

        Ok(uuid)
    }
}

impl DeviceUuidFetcher for DeviceInfo {
    #[cfg(target_os = "linux")]
    fn device_uuid(&self) -> Result<uuid::Uuid, Error> {
        let uuid = Self::try_machine_id(self.config.device_uuid_path.as_str())
            .or_else(|_| Self::try_machine_id(self.config.device_uuid_fallback_path.as_str()))?;

        info!("Device UUID: {}", uuid);

        Ok(uuid)
    }

    #[cfg(not(target_os = "linux"))]
    fn device_uuid(&self) -> Result<uuid::Uuid, Error> {
        unimplemented!()
    }
}

impl ServerNameFetcher for DeviceInfo {
    fn server_name(&self) -> Result<String, Error> {
        let server_name = if self.config.server_name_without_hostname {
            self.config.server_name.clone()
        } else {
            let hostname = hostname::get()
                .map_err(Error::CallHostname)?
                .as_os_str()
                .to_string_lossy()
                .to_string();
            format!("{} on {}", self.config.server_name, hostname)
        };
        Ok(server_name)
    }
}

impl UserAgentFetcher for DeviceInfo {
    fn user_agent(&self) -> Result<String, Error> {
        Ok(format!(
            "{os} UPnP/{upnp_version} Podcast/{version}",
            os = os_name_version()?,
            upnp_version = self.config.upnp_version,
            version = clap::crate_version!(),
        ))
    }
}

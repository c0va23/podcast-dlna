use http::StatusCode;
use std::error::Error;
use std::sync::Arc;
use upnp::media_server::connection_manager;
use upnp::media_server::content_directory;
use upnp::media_server::device_description::{
    device_description, Config, IconConfig, ServiceConfig,
};
use warp::reply::Reply;

use crate::app::{FeedFetcher, FeedFetcherError, Image};
use crate::server::HandlerResult;
use crate::utils::device_info::{DeviceUuidFetcher, ServerNameFetcher};

#[derive(Clone)]
pub struct DeviceDescriptionHandler {
    device_uuid: uuid::Uuid,
    server_name: String,
}

impl DeviceDescriptionHandler {
    pub(crate) fn new<Fetcher>(fetcher: &Fetcher) -> Result<Self, Box<dyn Error>>
    where
        Fetcher: DeviceUuidFetcher + ServerNameFetcher,
    {
        Ok(Self {
            device_uuid: fetcher.device_uuid()?,
            server_name: fetcher.server_name()?,
        })
    }
}

#[async_trait::async_trait]
impl crate::server::DeviceDescriptionHandler for DeviceDescriptionHandler {
    #[tracing::instrument(name = "DeviceHandler::device_describe", skip(self))]
    async fn device_describe(self: Arc<Self>) -> HandlerResult {
        let device_description = device_description(&Config {
            device_uuid: self.device_uuid,
            manufacturer: "HomeMade".into(),
            name: self.server_name.clone(),
            icons: vec![
                IconConfig {
                    depth: 24,
                    size: 32,
                    mime_type: "image/png".into(),
                    url: "/icons/icon32.png".into(),
                },
                IconConfig {
                    depth: 24,
                    size: 24,
                    mime_type: "image/png".into(),
                    url: "/icons/icon24.png".into(),
                },
            ],
            services: vec![
                ServiceConfig {
                    service_type: "urn:schemas-upnp-org:service:ContentDirectory:1".into(),
                    service_id: "urn:upnp-org:serviceId:ContentDirectory".into(),
                    description_url: "/content-directory/description.xml".into(),
                    control_url: "/content-directory/control.xml".into(),
                    event_sub_url: None,
                },
                ServiceConfig {
                    service_type: "urn:schemas-upnp-org:service:ConnectionManager:1".into(),
                    service_id: "urn:upnp-org:serviceId:ConnectionManager".into(),
                    description_url: "/connection-manager/description.xml".into(),
                    control_url: "/connection-manager/control.xml".into(),
                    event_sub_url: None,
                },
            ],
        });

        info!("Return device description");
        Ok(Box::new(
            yaserde::ser::to_string(&device_description)
                .unwrap()
                .into_response(),
        ))
    }
}

#[derive(Clone)]
pub struct ContentDirectoryHandler {
    content_directory: content_directory::ContentDirectory,
}

impl ContentDirectoryHandler {
    pub(crate) const fn new(content_directory: content_directory::ContentDirectory) -> Self {
        Self { content_directory }
    }
}

#[async_trait::async_trait]
impl crate::server::ContentDirectoryHandler for ContentDirectoryHandler {
    #[tracing::instrument(name = "ContentDirectoryHandler::description", skip(self))]
    async fn description(self: Arc<Self>) -> HandlerResult {
        let service_description = self.content_directory.describe();

        Ok(Box::new(
            yaserde::ser::to_string(&service_description)
                .unwrap()
                .into_response(),
        ))
    }

    #[tracing::instrument(
        name = "ContentDirectoryHandler::control",
        skip(self, envelope, _soap_action),
        fields(soap_action = _soap_action.trim_matches('"'))
    )]
    async fn control(
        self: Arc<Self>,
        _soap_action: String,
        envelope: content_directory::Envelope,
    ) -> HandlerResult {
        tracing::trace!(data = ?envelope, "Envelope");

        let response = self.content_directory.control(&envelope).await;

        debug!("Response with {}", &response.body.kind());
        tracing::trace!(data = ?response, "Response");

        Ok(Box::new(
            yaserde::ser::to_string(&response).unwrap().into_response(),
        ))
    }
}

pub struct ConnectionManagerHandler {}

impl ConnectionManagerHandler {
    pub const fn new() -> Self {
        Self {}
    }
}

#[async_trait::async_trait]
impl crate::server::ConnectionManagerHandler for ConnectionManagerHandler {
    #[tracing::instrument(name = "ConnectionManagerHandler::description", skip(self))]
    async fn description(self: Arc<Self>) -> HandlerResult {
        let service_description = connection_manager::ConnectionManager::describe();

        Ok(Box::new(
            yaserde::ser::to_string(&service_description)
                .unwrap()
                .into_response(),
        ))
    }

    #[tracing::instrument(name = "ConnectionManagerHandler::control", skip(self))]
    async fn control(self: Arc<Self>) -> HandlerResult {
        Ok(Box::new(warp::reply::with_status(
            warp::reply(),
            http::StatusCode::NOT_IMPLEMENTED,
        )))
    }
}

pub struct IconsHandler {}

impl IconsHandler {
    pub const fn new() -> Self {
        Self {}
    }
}

#[async_trait::async_trait]
impl crate::server::IconsHandler for IconsHandler {
    #[tracing::instrument(name = "IconsHandler::icon24", skip(self))]
    async fn icon24(self: Arc<Self>) -> HandlerResult {
        Ok(Box::new(
            crate::assets::icons::FEED_ICON_24_PNG.into_response(),
        ))
    }

    #[tracing::instrument(name = "IconsHandler::icon32", skip(self))]
    async fn icon32(self: Arc<Self>) -> HandlerResult {
        Ok(Box::new(
            crate::assets::icons::FEED_ICON_32_PNG.into_response(),
        ))
    }
}

#[derive(thiserror::Error, Debug)]
enum PictureError {
    #[error("picture not found")]
    PictureNotFound,

    #[error("Feed error: {0}")]
    FeedFetch(#[from] FeedFetcherError),
}

pub struct PicturesHandler {
    feed_fetcher: Arc<dyn FeedFetcher>,
}

// pub(create) implementation
impl PicturesHandler {
    pub(crate) fn new(feed_fetcher: Arc<dyn FeedFetcher>) -> Self {
        Self { feed_fetcher }
    }
}

#[async_trait::async_trait]
impl crate::server::PicturesHandler for PicturesHandler {
    #[tracing::instrument(
        name = "PicturesHandler::handle_track",
        skip(self),
        fields(channel_id = channel_id.as_str(), track_id = track_id.as_str())
    )]
    async fn handle_track(self: Arc<Self>, channel_id: String, track_id: String) -> HandlerResult {
        Ok(Box::new(Self::build_response(
            self.fetch_track_picture(channel_id, track_id).await,
        )))
    }

    #[tracing::instrument(
        name = "PicturesHandler::handle_channel",
        skip(self),
        fields(channel_id = channel_id.as_str())
    )]
    async fn handle_channel(self: Arc<Self>, channel_id: String) -> HandlerResult {
        Ok(Box::new(Self::build_response(
            self.fetch_channel_picture(channel_id).await,
        )))
    }
}

// private implementation
impl PicturesHandler {
    fn build_response(result: Result<Image, PictureError>) -> impl warp::Reply {
        use crate::app::DataStreamer;

        let response_builder = http::response::Builder::new();

        match result {
            Ok(picture) => {
                let mut response_builder = response_builder.status(StatusCode::OK);
                if let Some(content_type) = picture.mime_type() {
                    response_builder = response_builder.header("Content-Type", content_type);
                }

                let stream = picture.data_stream::<std::io::Error>();
                response_builder.body(hyper::Body::wrap_stream(stream))
            }
            Err(PictureError::PictureNotFound) => response_builder
                .status(StatusCode::NOT_FOUND)
                .body(hyper::Body::empty()),
            Err(other_error) => response_builder
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(hyper::Body::from(other_error.to_string())),
        }
    }

    async fn fetch_track_picture(
        &self,
        channel_id: String,
        track_id: String,
    ) -> Result<Image, PictureError> {
        let (_channel, track) = self
            .feed_fetcher
            .fetch_track(&channel_id, &track_id)
            .await?;

        Ok(track.image.ok_or(PictureError::PictureNotFound)?)
    }

    async fn fetch_channel_picture(&self, channel_id: String) -> Result<Image, PictureError> {
        let (channel, _tracks) = self.feed_fetcher.fetch_tracks(&channel_id).await?;

        Ok(channel.image.ok_or(PictureError::PictureNotFound)?)
    }
}

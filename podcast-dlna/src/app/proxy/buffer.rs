use std::pin::Pin;

use futures::stream::{Stream, StreamExt};
use tokio::sync::mpsc::channel;
use tokio_stream::wrappers::ReceiverStream;

pub(super) trait SizedBytes {
    fn len(&self) -> Option<usize>;
}

pub(super) fn infinity<I>(
    packet_buffer_size: usize,
    stream: Box<dyn Stream<Item = I> + Send + Unpin>,
) -> impl Stream<Item = I> + Send
where
    I: Send + SizedBytes + 'static,
{
    let span = tracing::info_span!("infinity");

    let (sender, receiver) = channel(packet_buffer_size);

    tokio::spawn(async move {
        let _enter = span.enter();

        let mut stream = Pin::new(stream);

        let mut total_bytes_sended = 0;
        while let Some(value) = stream.next().await {
            trace!("Send stream item {:?}", value.len());
            total_bytes_sended += value.len().unwrap_or(0);
            if let Err(err) = sender.send(value).await {
                warn!("Send buffer error: {}", err);
                break;
            }
        }

        span.record("total_bytes_sended", &total_bytes_sended);
    });

    ReceiverStream::new(receiver)
}

#[cfg(test)]
mod tests {
    use super::infinity;
    use futures::stream::StreamExt;

    #[tokio::test]
    async fn test_infinity_buffer() {
        #[derive(Clone, Debug, PartialEq)]
        struct FakeBytes {
            value: usize,
        }

        impl super::SizedBytes for FakeBytes {
            fn len(&self) -> Option<usize> {
                Some(self.value)
            }
        }
        let items = vec![0, 1, 2]
            .into_iter()
            .map(|value| FakeBytes { value })
            .collect::<Vec<FakeBytes>>();
        let source_stream = futures::stream::iter(items.clone().into_iter());
        let buffered_stream = infinity(2, Box::new(source_stream));

        let result_items = buffered_stream.collect::<Vec<FakeBytes>>().await;

        assert_eq!(items, result_items);
    }
}

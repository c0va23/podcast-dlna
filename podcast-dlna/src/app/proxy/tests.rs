#![allow(clippy::too_many_lines)]

use bytes::Bytes;
use http::{header::HeaderName, HeaderValue};
use hyper::body::HttpBody;
use warp::Reply;

use std::sync::Arc;

use super::Proxy;

#[tokio::test]
async fn test_handler() {
    use http::StatusCode;
    use hyper::Method;

    use super::batch::{Error, MockStreamBuilder, PacketStream};
    use super::Headers;
    use super::{range::Batch, StreamBuildParams};
    use crate::app::{Channel, FeedFetcherError, MockFeedFetcher, MusicTrack};
    use crate::app::{
        MockPreflighter, PreflightError, PreflightErrorKind, PreflightParams, PreflightResponse,
    };
    use crate::server::ProxyHandler;

    use mockall::predicate::eq;

    const CONTENT_TYPE: &str = "audio/mpeg";
    const TRACK_SIZE: usize = 1000;
    static BODY_PREFIX: &[u8] = b"STATIC BODY PREFIX";

    static CHANNEL_ID: &str = "channel_id";
    static CHANNEL_URL: &str = "http://example.org/channels/channel_id";

    static TRACK_ID: &str = "track_id";
    static TRACK_URL: &str = "http://example.org/tracks/track_id.mp3";
    static TRACK_DIRECTED_URL: &str = "http://node0.example.org/tracks/track_id.mp3";
    static TRACK_ID_WITH_EXTENSION: &str = "track_id.mp3";

    struct StreamBuildArgs {
        range: Batch,
        result: Result<PacketStream, Error>,
    }

    struct TargetResponse {
        status_code: StatusCode,
        include_headers: Vec<(HeaderName, HeaderValue)>,
        body: Option<Bytes>,
    }

    struct TargetError {
        not_found: bool,
    }

    struct TestCase {
        description: &'static str,

        request_method: http::Method,
        request_headers: Headers,

        feed_fetcher_result: Result<(Channel, MusicTrack), FeedFetcherError>,
        preflight_result: Option<Result<PreflightResponse, PreflightError>>,

        stream_build: Option<StreamBuildArgs>,

        expected_response: Result<TargetResponse, TargetError>,
    }

    let feed_fetcher_response = (
        Channel {
            id: CHANNEL_ID.to_owned(),
            url: CHANNEL_URL.to_owned(),
            title: "Channel title".into(),
            tracks_count: 1,
            update_time: None,
            image: None,
        },
        MusicTrack {
            id: TRACK_ID.to_owned(),
            url: TRACK_URL.to_owned(),
            content_type: CONTENT_TYPE.to_owned(),
            creators: None,
            date: None,
            duration: None,
            title: "Track title".into(),
            size: Some(TRACK_SIZE),
            image: None,
            description: None,
        },
    );

    let preflight_response = PreflightResponse {
        content_length: TRACK_SIZE,
        duration: None,
        headers: vec![
            (http::header::CONTENT_TYPE, "audio/mpeg".parse().unwrap()),
            (http::header::CONTENT_LENGTH, "1000".parse().unwrap()),
        ]
        .into_iter()
        .collect(),
        status: hyper::StatusCode::OK,
        url: TRACK_DIRECTED_URL.into(),
        version: http::Version::HTTP_11,
        body_prefix: Bytes::from(BODY_PREFIX),
        attached_picture: None,
    };

    let test_cases = vec![
        TestCase {
            description: "Channel id not found",

            request_method: Method::GET,
            request_headers: Headers::new(),

            feed_fetcher_result: Err(FeedFetcherError::ChannelNotFound),
            preflight_result: None,

            stream_build: None,

            expected_response: Err(TargetError { not_found: true }),
        },
        TestCase {
            description: "Track id not found",

            request_method: Method::GET,
            request_headers: Headers::new(),

            feed_fetcher_result: Err(FeedFetcherError::TrackNotFound),
            preflight_result: None,

            stream_build: None,

            expected_response: Err(TargetError { not_found: true }),
        },
        TestCase {
            description: "Preflight return error",

            request_method: Method::GET,
            request_headers: Headers::new(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Err(PreflightError {
                kind: PreflightErrorKind::UnknownContentLength,
                url: TRACK_URL.into(),
            })),

            stream_build: None,

            expected_response: Err(TargetError { not_found: false }),
        },
        TestCase {
            description: "Preflight return not found",

            request_method: Method::GET,
            request_headers: Headers::new(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Ok(PreflightResponse {
                status: StatusCode::NOT_FOUND,
                body_prefix: Bytes::from(b"not found".to_vec()),
                headers: vec![("x-custom".parse().unwrap(), "value".parse().unwrap())]
                    .into_iter()
                    .collect(),
                ..preflight_response.clone()
            })),

            stream_build: None,

            expected_response: Ok(TargetResponse {
                status_code: StatusCode::NOT_FOUND,
                include_headers: vec![("x-custom".parse().unwrap(), "value".parse().unwrap())],
                body: Some(Bytes::from_static(b"not found")),
            }),
        },
        TestCase {
            description: "Invalid range error",

            request_method: Method::GET,
            request_headers: vec![(http::header::RANGE, "bytes=0-1000".parse().unwrap())]
                .into_iter()
                .collect(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Ok(preflight_response.clone())),

            stream_build: None,

            expected_response: Ok(TargetResponse {
                status_code: StatusCode::RANGE_NOT_SATISFIABLE,
                include_headers: vec![].into_iter().collect(),
                body: None,
            }),
        },
        TestCase {
            description: "Success response without range",

            request_method: Method::GET,
            request_headers: Headers::new(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Ok(preflight_response.clone())),

            stream_build: Some(StreamBuildArgs {
                range: Batch {
                    start: 0,
                    end: TRACK_SIZE - 1,
                    content_length: TRACK_SIZE,
                },
                result: Ok(Box::new(futures::stream::iter(
                    vec![Ok(Bytes::from(BODY_PREFIX.to_vec()))].into_iter(),
                ))),
            }),

            expected_response: Ok(TargetResponse {
                status_code: StatusCode::OK,
                include_headers: vec![
                    (http::header::CONTENT_LENGTH, "1000".parse().unwrap()),
                    (http::header::CONTENT_TYPE, "audio/mpeg".parse().unwrap()),
                ]
                .into_iter()
                .collect(),
                body: Some(Bytes::from(BODY_PREFIX.to_vec())),
            }),
        },
        TestCase {
            description: "Success partial response",

            request_method: Method::GET,
            request_headers: vec![(http::header::RANGE, "bytes=0-499".parse().unwrap())]
                .into_iter()
                .collect(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Ok(preflight_response.clone())),

            stream_build: Some(StreamBuildArgs {
                range: Batch {
                    start: 0,
                    end: 499,
                    content_length: TRACK_SIZE,
                },
                result: Ok(Box::new(futures::stream::iter(
                    vec![Ok(Bytes::from(BODY_PREFIX.to_vec()))].into_iter(),
                ))),
            }),

            expected_response: Ok(TargetResponse {
                status_code: StatusCode::PARTIAL_CONTENT,
                include_headers: vec![
                    (
                        http::header::CONTENT_RANGE,
                        "bytes 0-499/1000".parse().unwrap(),
                    ),
                    (http::header::CONTENT_LENGTH, "500".parse().unwrap()),
                    (http::header::CONTENT_TYPE, "audio/mpeg".parse().unwrap()),
                ],
                body: Some(Bytes::from(BODY_PREFIX.to_vec())),
            }),
        },
        TestCase {
            description: "Success head partial response",

            request_method: Method::HEAD,
            request_headers: vec![(http::header::RANGE, "bytes=0-499".parse().unwrap())]
                .into_iter()
                .collect(),

            feed_fetcher_result: Ok(feed_fetcher_response.clone()),
            preflight_result: Some(Ok(preflight_response.clone())),

            stream_build: Some(StreamBuildArgs {
                range: Batch {
                    start: 0,
                    end: 499,
                    content_length: TRACK_SIZE,
                },
                result: Ok(Box::new(futures::stream::iter(
                    vec![Ok(Bytes::from(BODY_PREFIX.to_vec()))].into_iter(),
                ))),
            }),

            expected_response: Ok(TargetResponse {
                status_code: StatusCode::PARTIAL_CONTENT,
                include_headers: vec![
                    (
                        http::header::CONTENT_RANGE,
                        "bytes 0-499/1000".parse().unwrap(),
                    ),
                    (http::header::CONTENT_LENGTH, "500".parse().unwrap()),
                    (http::header::CONTENT_TYPE, "audio/mpeg".parse().unwrap()),
                ],
                body: None,
            }),
        },
    ];

    for test_case in test_cases {
        let TestCase {
            description,

            request_method,
            request_headers,

            feed_fetcher_result,

            preflight_result,

            stream_build,

            expected_response,
        } = test_case;

        let mut preflighter = MockPreflighter::new();
        let mut batch_stream_builder = MockStreamBuilder::new();
        let mut feed_fetcher = MockFeedFetcher::new();

        feed_fetcher
            .expect_fetch_track()
            .with(eq(CHANNEL_ID), eq(TRACK_ID))
            .return_once(|_, _| feed_fetcher_result);

        if let Some(preflight_result) = preflight_result {
            preflighter
                .expect_perform()
                .with(eq(PreflightParams {
                    feed_url: CHANNEL_URL.into(),
                    url: TRACK_URL.into(),
                    parse_body: false,
                }))
                .return_once(|_| preflight_result);
        }

        if let Some(StreamBuildArgs { range, result }) = stream_build {
            batch_stream_builder
                .expect_build_stream()
                .with(eq(StreamBuildParams {
                    body_prefix: Bytes::from(BODY_PREFIX),
                    headers: Headers::new(),
                    url: TRACK_DIRECTED_URL.into(),
                    range,
                }))
                .return_once(|_params| result);
        }

        let proxy = Arc::new(Proxy::new(
            Arc::new(preflighter),
            Arc::new(batch_stream_builder),
            Arc::new(feed_fetcher),
        ));
        let handler_result = proxy
            .handle(
                request_method,
                CHANNEL_ID.into(),
                TRACK_ID_WITH_EXTENSION.into(),
                request_headers,
            )
            .await;

        match expected_response {
            Ok(target_response) => {
                assert!(
                    handler_result.is_ok(),
                    "Case {}: Response not success",
                    description,
                );

                let mut reply = handler_result.unwrap().into_response();
                assert_eq!(
                    target_response.status_code,
                    reply.status(),
                    "Case {}: Status code has unexpected value",
                    description,
                );

                for (header_name, header_value) in target_response.include_headers {
                    assert_eq!(
                        Some(header_value.clone()),
                        reply.headers().get(&header_name).map(Clone::clone),
                        "Case {}: Response not include header {:?} with value {:?}",
                        description,
                        header_name,
                        header_value,
                    );
                }

                let body = reply.body_mut();
                let data = body.data().await;
                if let Some(expected_body) = target_response.body {
                    assert!(
                        data.is_some(),
                        "Case {}: Response body not exists",
                        description,
                    );

                    let data = data.unwrap();
                    assert!(
                        data.is_ok(),
                        "Case {}: Response body has error",
                        description,
                    );

                    let data_bytes = data.unwrap();
                    assert_eq!(
                        expected_body, data_bytes,
                        "Case {}: Body has unexpected value",
                        description,
                    );
                } else {
                    assert!(
                        data.is_none(),
                        "Case {}: Response body should be empty",
                        description,
                    );
                }
            }
            Err(rejection) => {
                assert!(
                    handler_result.is_err(),
                    "Case {}: Response not error",
                    description,
                );

                if let Err(response_error) = handler_result {
                    assert_eq!(
                        rejection.not_found,
                        response_error.is_not_found(),
                        "Case {}: Unexpected rejection reason",
                        description,
                    );
                }
            }
        }
    }
}

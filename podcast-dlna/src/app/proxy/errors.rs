use std::error::Error as StdError;
use thiserror::Error as ThisError;

use super::range::Error as RangeError;

#[derive(ThisError, Debug, PartialEq)]
#[error("End after content length ({end} > {max_end})")]
pub(super) struct InvalidRangeValue {
    pub end: usize,
    pub max_end: usize,
}

#[derive(Debug, ThisError)]
pub(super) enum Error {
    // Not depend direct from Reqwest error. Allow mock reqwest errors.
    #[error("Reqwest error: {0}")]
    Reqwest(Box<dyn StdError + Sync + Send>),

    #[error("Build request error")]
    Http(#[from] http::Error),

    #[error("Parse Range header error")]
    RangeHeader(#[from] RangeError),

    #[error("Build Range header error")]
    HeaderToStr(#[from] http::header::ToStrError),

    #[error("Parse range value error")]
    ParseInt(#[from] std::num::ParseIntError),

    #[error("Preflight error")]
    Preflight(#[from] crate::app::PreflightError),

    #[error("Batch error: {0}")]
    Batch(#[from] super::batch::Error),
}

impl warp::reject::Reject for Error {}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Self::Reqwest(Box::new(error))
    }
}

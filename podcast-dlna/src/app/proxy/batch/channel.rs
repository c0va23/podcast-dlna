pub enum BatchSender<MessageType> {
    Bounded(tokio::sync::mpsc::Sender<MessageType>),
    Unbounded(tokio::sync::mpsc::UnboundedSender<MessageType>),
}

impl<MessageType> BatchSender<MessageType>
where
    MessageType: Send,
{
    pub(crate) async fn send(
        &self,
        value: MessageType,
    ) -> Result<(), tokio::sync::mpsc::error::SendError<MessageType>> {
        match self {
            Self::Bounded(ref sender) => sender.send(value).await,
            Self::Unbounded(ref sender) => sender.send(value),
        }
    }
}

#[pin_project::pin_project(project = BatchReceiverProject)]
pub enum BatchReceiver<MessageType> {
    Bounded(#[pin] tokio_stream::wrappers::ReceiverStream<MessageType>),
    Unbounded(#[pin] tokio_stream::wrappers::UnboundedReceiverStream<MessageType>),
}

impl<MessageType> futures::Stream for BatchReceiver<MessageType> {
    type Item = MessageType;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        match self.project() {
            BatchReceiverProject::Bounded(receiver_stream) => receiver_stream.poll_next(cx),
            BatchReceiverProject::Unbounded(receiver_stream) => receiver_stream.poll_next(cx),
        }
    }
}

pub fn create<MessageType>(
    buffer_size: Option<usize>,
) -> (BatchSender<MessageType>, BatchReceiver<MessageType>) {
    use tokio::sync::mpsc::{channel, unbounded_channel};
    use tokio_stream::wrappers::{ReceiverStream, UnboundedReceiverStream};

    match buffer_size {
        None => {
            let (tx, rx) = unbounded_channel();

            (
                BatchSender::Unbounded(tx),
                BatchReceiver::Unbounded(UnboundedReceiverStream::new(rx)),
            )
        }
        Some(buffer_size) => {
            let (tx, rx) = channel(buffer_size);

            (
                BatchSender::Bounded(tx),
                BatchReceiver::Bounded(ReceiverStream::new(rx)),
            )
        }
    }
}

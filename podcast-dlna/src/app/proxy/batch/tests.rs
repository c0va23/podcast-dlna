use super::super::Batch;

use pretty_assertions::assert_eq;

use std::{ops::Range, sync::Arc, time::Duration};

use bytes::Bytes;
use futures::StreamExt;
use mockall::predicate;
use rand::RngCore;

use super::{
    ContinuableRequester, MockRequester, PacketStream, RemoteStreamBuilder,
    RemoteStreamBuilderConfig, Requester, StreamBuildParams, StreamBuilder,
};

async fn stream_response(stream: PacketStream) -> Vec<Bytes> {
    type Result<T> = std::result::Result<T, super::Error>;

    stream
        .collect::<Vec<Result<Bytes>>>()
        .await
        .into_iter()
        .collect::<Result<Vec<Bytes>>>()
        .unwrap()
}

fn response_size(responses: &[Bytes]) -> usize {
    responses.iter().fold(0, |acc, body| acc + body.len())
}

fn response_data(responses: &[Bytes]) -> Vec<u8> {
    use std::io::Write;

    let mut response_data = Vec::new();
    let mut cursor = std::io::Cursor::new(&mut response_data);
    responses
        .iter()
        .for_each(|body| cursor.write_all(body.as_ref()).unwrap());
    response_data
}

#[tokio::test]
async fn simple_remote_stream_builder() {
    let mut data = [0_u8; 1000];
    rand::thread_rng().fill_bytes(&mut data[..]);

    let url = "https://example.com/test.mp3".to_owned();
    let headers = super::super::Headers::new();

    let range = Batch {
        start: 0,
        end: data.len() - 1,
        content_length: 2500,
    };

    let mut mock_requester = MockRequester::new();
    let data_mock = data;
    mock_requester
        .expect_batch_request()
        .with(
            predicate::always(),
            predicate::eq(url.clone()),
            predicate::eq(headers.clone()),
        )
        .times(2)
        .returning(move |range, _, _| {
            let batch_src = &data_mock[range.start..=range.end];
            let batch_body = Bytes::copy_from_slice(batch_src);

            let stream = futures::stream::iter(vec![Result::Ok(batch_body)]);
            Ok(Box::new(stream) as PacketStream)
        });

    let body_prefix = Bytes::from(data[..250].to_vec());

    let remote_stream_builder_config = RemoteStreamBuilderConfig {
        batch_size: 500,
        packet_buffer_size: 3,
        buffered_batch_count: 2,
    };
    let remote_stream_builder =
        RemoteStreamBuilder::new(Arc::new(mock_requester), remote_stream_builder_config);
    let stream = remote_stream_builder
        .build_stream(StreamBuildParams {
            url,
            headers,
            range,
            body_prefix,
        })
        .await
        .unwrap();

    let responses = stream_response(stream).await;

    assert_eq!(3, responses.len());

    let response_len = response_size(&responses);
    assert_eq!(data.len(), response_len);

    let response_data = response_data(&responses);
    assert_eq!(response_data.as_slice(), &data[..]);
}

#[tokio::test]
async fn zero_remote_stream_builder() {
    let mut data = [0_u8; 1000];
    rand::thread_rng().fill_bytes(&mut data[..]);

    let url = "https://example.com/test.mp3".to_owned();
    let headers = super::super::Headers::new();

    let range = Batch {
        start: 0,
        end: 0,
        content_length: 2500,
    };

    let body_prefix = Bytes::from(data[..250].to_vec());

    let mock_requester = MockRequester::new();

    let remote_stream_builder_config = RemoteStreamBuilderConfig {
        batch_size: 500,
        packet_buffer_size: 3,
        buffered_batch_count: 2,
    };
    let remote_stream_builder =
        RemoteStreamBuilder::new(Arc::new(mock_requester), remote_stream_builder_config);
    let stream = remote_stream_builder
        .build_stream(StreamBuildParams {
            url,
            headers,
            range,
            body_prefix,
        })
        .await
        .unwrap();
    let responses = stream_response(stream).await;

    assert_eq!(1, responses.len());

    let response_len = response_size(&responses);
    assert_eq!(1, response_len);

    let response_data = response_data(&responses);
    assert_eq!(response_data.as_slice(), &data[range.start..=range.end]);
}

#[tokio::test]
async fn only_prefix_remote_stream_builder() {
    let mut data = [0_u8; 1000];
    rand::thread_rng().fill_bytes(&mut data[..]);

    let url = "https://example.com/test.mp3".to_owned();
    let headers = super::super::Headers::new();

    let prefix_size = 250;
    let range = Batch {
        start: 0,
        end: prefix_size - 1,
        content_length: 2500,
    };

    let body_prefix = Bytes::from(data[..prefix_size].to_vec());

    let mock_requester = MockRequester::new();

    let remote_stream_builder_config = RemoteStreamBuilderConfig {
        batch_size: 500,
        packet_buffer_size: 3,
        buffered_batch_count: 2,
    };
    let remote_stream_builder =
        RemoteStreamBuilder::new(Arc::new(mock_requester), remote_stream_builder_config);
    let stream = remote_stream_builder
        .build_stream(StreamBuildParams {
            url,
            headers,
            range,
            body_prefix,
        })
        .await
        .unwrap();
    let responses = stream_response(stream).await;

    assert_eq!(1, responses.len());

    let response_len = response_size(&responses);
    assert_eq!(range.size(), response_len);

    let response_data = response_data(&responses);
    assert_eq!(response_data.as_slice(), &data[range.start..=range.end]);
}

#[tokio::test]
async fn without_prefix_remote_stream_builder() {
    type Result<T> = std::result::Result<T, super::Error>;

    let mut data = [0_u8; 1000];
    rand::thread_rng().fill_bytes(&mut data[..]);

    let url = "https://example.com/test.mp3".to_owned();
    let headers = super::super::Headers::new();

    let batch_size = 500;
    let prefix_size = 250;
    let range = Batch {
        start: prefix_size,
        end: prefix_size + batch_size - 1,
        content_length: 2500,
    };

    let body_prefix = Bytes::from(data[..prefix_size].to_vec());

    let mut mock_requester = MockRequester::new();
    let data_mock = data;
    mock_requester
        .expect_batch_request()
        .with(
            predicate::always(),
            predicate::eq(url.clone()),
            predicate::eq(headers.clone()),
        )
        .times(1)
        .returning(move |range, _, _| {
            let batch_src = &data_mock[range.start..=range.end];
            let batch_body = Bytes::copy_from_slice(batch_src);

            let stream = futures::stream::iter(vec![Result::Ok(batch_body)]);
            Ok(Box::new(stream) as PacketStream)
        });

    let remote_stream_builder_config = RemoteStreamBuilderConfig {
        batch_size: 500,
        packet_buffer_size: 3,
        buffered_batch_count: 2,
    };
    let remote_stream_builder =
        RemoteStreamBuilder::new(Arc::new(mock_requester), remote_stream_builder_config);
    let stream = remote_stream_builder
        .build_stream(StreamBuildParams {
            url,
            headers,
            range,
            body_prefix,
        })
        .await
        .unwrap();
    let responses = stream_response(stream).await;

    assert_eq!(1, responses.len());

    let response_len = response_size(&responses);
    assert_eq!(range.size(), response_len);

    let response_data = response_data(&responses);
    assert_eq!(response_data.as_slice(), &data[range.start..=range.end]);
}

#[allow(clippy::too_many_lines)]
#[tokio::test]
async fn continuable_requester() {
    use super::Error;
    use super::{continuable::RepeaterConfig, ContinuableRequesterConfig};

    type Result<T> = std::result::Result<T, Error>;

    struct RequesterMockData {
        request_range: Range<usize>,
        requester_result: Result<Vec<Result<Range<usize>>>>,
    }

    struct ExpectedValue {
        batches: Vec<Result<Range<usize>>>,
    }

    struct TestCase {
        description: &'static str,
        config: ContinuableRequesterConfig,
        requester_results: Vec<RequesterMockData>,
        expected_result: Result<ExpectedValue>,
    }

    #[derive(thiserror::Error, Debug, Clone)]
    #[error("Reqwest error")]
    struct ReqwestError;

    const BATCH_SIZE: usize = 100;
    let mut data = vec![0_u8; BATCH_SIZE];
    rand::thread_rng().fill_bytes(&mut data[..]);

    let url = "https://example.com/test.mp3".to_owned();
    let headers = super::super::Headers::new();

    let range = Batch {
        start: 0,
        end: BATCH_SIZE - 1,
        content_length: BATCH_SIZE,
    };

    let config = ContinuableRequesterConfig {
        repeater_config: RepeaterConfig {
            max_attempts: 3,
            fail_fast: false,
            retry_delay: None,
        },
        packet_buffer_size: None,
    };

    let reqwest_error = |index| Error::Reqwest(format!("Reqwest {} error", index));

    let test_cases = vec![
        TestCase {
            description: "Success one batch",
            config,
            requester_results: vec![RequesterMockData {
                request_range: 0..BATCH_SIZE - 1,
                requester_result: Ok(vec![Ok(0..BATCH_SIZE)]),
            }],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE)],
            }),
        },
        TestCase {
            description: "Success two batch",
            config,
            requester_results: vec![RequesterMockData {
                request_range: 0..BATCH_SIZE - 1,
                requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2), Ok(BATCH_SIZE / 2..BATCH_SIZE)]),
            }],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Ok(BATCH_SIZE / 2..BATCH_SIZE)],
            }),
        },
        TestCase {
            description: "First batch with one batch and error, second batch success",
            config,
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))]),
                },
                RequesterMockData {
                    request_range: BATCH_SIZE / 2..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(BATCH_SIZE / 2..BATCH_SIZE)]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Ok(BATCH_SIZE / 2..BATCH_SIZE)],
            }),
        },
        TestCase {
            description:
                "First batch with one batch and error, second batch return error with 2 attempts",
            config: ContinuableRequesterConfig {
                repeater_config: RepeaterConfig {
                    max_attempts: 2,
                    ..config.repeater_config
                },
                ..config
            },
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))]),
                },
                RequesterMockData {
                    request_range: BATCH_SIZE / 2..BATCH_SIZE - 1,
                    requester_result: Err(reqwest_error(1)),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(1))],
            }),
        },
        TestCase {
            description:
                "First batch with one batch and error, second batch return error with 1 attempt",
            config: ContinuableRequesterConfig {
                repeater_config: RepeaterConfig {
                    max_attempts: 1,
                    ..config.repeater_config
                },
                ..config
            },
            requester_results: vec![RequesterMockData {
                request_range: 0..BATCH_SIZE - 1,
                requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))]),
            }],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))],
            }),
        },
        TestCase {
            description: "First two batch with one batch and error, third batch success",
            config,
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE / 3), Err(reqwest_error(0))]),
                },
                RequesterMockData {
                    request_range: BATCH_SIZE / 3..BATCH_SIZE - 1,
                    requester_result: Ok(vec![
                        Ok(BATCH_SIZE / 3..BATCH_SIZE / 3 * 2),
                        Err(reqwest_error(1)),
                    ]),
                },
                RequesterMockData {
                    request_range: BATCH_SIZE / 3 * 2..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(BATCH_SIZE / 3 * 2..BATCH_SIZE)]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![
                    Ok(0..BATCH_SIZE / 3),
                    Ok(BATCH_SIZE / 3..BATCH_SIZE / 3 * 2),
                    Ok(BATCH_SIZE / 3 * 2..BATCH_SIZE),
                ],
            }),
        },
        TestCase {
            description: "Error on first batch with fail-fast",
            config: ContinuableRequesterConfig {
                repeater_config: RepeaterConfig {
                    fail_fast: true,
                    ..config.repeater_config
                },
                ..config
            },
            requester_results: vec![RequesterMockData {
                request_range: 0..BATCH_SIZE - 1,
                requester_result: Err(reqwest_error(0)),
            }],
            expected_result: Err(reqwest_error(0)),
        },
        TestCase {
            description: "Without fail fast (default) first batch error, second success",
            config,
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Err(reqwest_error(0)),
                },
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE)]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE)],
            }),
        },
        TestCase {
            description: "with delay: first batch error, second success",
            config: ContinuableRequesterConfig {
                repeater_config: RepeaterConfig {
                    retry_delay: Some(Duration::from_secs_f64(0.01)),
                    ..config.repeater_config
                },
                ..config
            },
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Err(reqwest_error(0)),
                },
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE)]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE)],
            }),
        },
        TestCase {
            description:
                "first batch return partial data and not return error, second return full tail",
            config,
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2)]),
                },
                RequesterMockData {
                    request_range: BATCH_SIZE / 2..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(BATCH_SIZE / 2..BATCH_SIZE)]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Ok(BATCH_SIZE / 2..BATCH_SIZE)],
            }),
        },
        TestCase {
            description: "number of attempts exceeded",
            config,
            requester_results: vec![
                RequesterMockData {
                    request_range: 0..BATCH_SIZE - 1,
                    requester_result: Ok(vec![Ok(0..(BATCH_SIZE / 4)), Err(reqwest_error(0))]),
                },
                RequesterMockData {
                    request_range: (BATCH_SIZE / 4)..(BATCH_SIZE - 1),
                    requester_result: Ok(vec![
                        Ok((BATCH_SIZE / 4)..(BATCH_SIZE / 2)),
                        Err(reqwest_error(1)),
                    ]),
                },
                RequesterMockData {
                    request_range: (BATCH_SIZE / 2)..(BATCH_SIZE - 1),
                    requester_result: Ok(vec![
                        Ok((BATCH_SIZE / 2)..(BATCH_SIZE / 4 * 3)),
                        Err(reqwest_error(2)),
                    ]),
                },
            ],
            expected_result: Ok(ExpectedValue {
                batches: vec![
                    Ok(0..(BATCH_SIZE / 4)),
                    Ok((BATCH_SIZE / 4)..(BATCH_SIZE / 2)),
                    Ok((BATCH_SIZE / 2)..(BATCH_SIZE / 4 * 3)),
                    Err(reqwest_error(2)),
                ],
            }),
        },
        TestCase {
            description: "first batch with one batch and error  with 1 attempt and fail-fast",
            config: ContinuableRequesterConfig {
                repeater_config: RepeaterConfig {
                    max_attempts: 1,
                    fail_fast: true,
                    ..config.repeater_config
                },
                ..config
            },
            requester_results: vec![RequesterMockData {
                request_range: 0..BATCH_SIZE - 1,
                requester_result: Ok(vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))]),
            }],
            expected_result: Ok(ExpectedValue {
                batches: vec![Ok(0..BATCH_SIZE / 2), Err(reqwest_error(0))],
            }),
        },
    ];

    for test_case in test_cases {
        println!("Test Case: {}", test_case.description);

        let mut mock_requester = MockRequester::new();
        let url = url.clone();
        let headers = headers.clone();
        for response_mock_data in test_case.requester_results {
            let RequesterMockData {
                request_range,
                requester_result,
            } = response_mock_data;
            let url = url.clone();
            let headers = headers.clone();
            let data = data.clone();

            let request_batch = Batch {
                start: request_range.start,
                end: request_range.end,
                content_length: BATCH_SIZE,
            };

            println!("mock request {:?}", request_range);

            mock_requester
                .expect_batch_request()
                .with(
                    predicate::eq(request_batch),
                    predicate::eq(url),
                    predicate::eq(headers.clone()),
                )
                .times(1)
                .return_once(move |_, _, _| {
                    let data = Arc::new(data.clone());

                    requester_result.map(move |result| {
                        Box::new(futures::stream::iter(result.into_iter().map(
                            move |range_results| {
                                range_results.map(|range| Bytes::copy_from_slice(&data[range]))
                            },
                        ))) as PacketStream
                    })
                });
        }

        let continuable_requester = Arc::new(ContinuableRequester::new(
            Arc::new(mock_requester),
            test_case.config,
        ));

        let requester_result = continuable_requester
            .batch_request(range, url, headers)
            .await;

        let data = data.clone();

        let expected_result = test_case.expected_result.map(|expected_value| {
            expected_value
                .batches
                .into_iter()
                .map(|range_result| range_result.map(|range| Bytes::copy_from_slice(&data[range])))
                .collect::<Vec<Result<Bytes>>>()
        });

        let actual_result = match requester_result {
            Ok(packet_stream) => Ok(packet_stream.collect::<Vec<Result<Bytes>>>().await),
            Err(err) => Err(err),
        };

        assert_eq!(expected_result, actual_result);
    }
}

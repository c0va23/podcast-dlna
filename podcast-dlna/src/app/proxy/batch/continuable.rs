use std::sync::Arc;

use crate::utils::parse_iso8601_duration;
use std::time::Duration;

use super::channel::BatchSender;
use super::{Batch, Error, Headers, PacketResult, PacketStream, PacketStreamResult};

#[derive(Debug, Clone, Copy, clap::Parser)]
pub struct RequesterConfig {
    #[clap(flatten)]
    pub(super) repeater_config: RepeaterConfig,

    /// Buffer size of batch. By default - unbounded.
    #[clap(
        name = "batch-packet-buffer-size",
        long = "batch-packet-size",
        env = "BATCH_PACKET_BUFFER_SIZE"
    )]
    pub(super) packet_buffer_size: Option<usize>,
}

pub struct Requester {
    requester: Arc<dyn super::Requester>,
    config: RequesterConfig,
}

impl Requester {
    pub(crate) fn new(requester: Arc<dyn super::Requester>, config: RequesterConfig) -> Self {
        Self { requester, config }
    }
}

#[async_trait::async_trait]
impl super::Requester for Requester {
    #[tracing::instrument(
        name = "Requester::batch_request",
        skip_all,
        fields(url = url.as_str(), batch = %batch)
    )]
    async fn batch_request(
        self: Arc<Self>,
        batch: Batch,
        url: String,
        headers: Headers,
    ) -> PacketStreamResult {
        let (sender, receiver) = super::channel::create(self.config.packet_buffer_size);

        let repeater = ContinuableRequesterRepeater {
            url,
            headers,
            sender,
            requester: self.requester.clone(),
            config: self.config.repeater_config,
        };

        repeater.start_retry_loop(batch).await?;

        Ok(Box::new(receiver))
    }
}

enum RepeaterAttempt {
    Finished,
    NotRepeatableAttempt { error: Option<Error> },
    RepeatableAttempt { next_batch: Batch, error: Error },
}

enum ChunkError {
    Read(Error),
    Write,
}

#[derive(Clone, Copy, Debug, clap::Parser)]
pub struct RepeaterConfig {
    /// Maximum retries count to fetch batch
    #[clap(
        name = "batch-proxy-attempt-count",
        long = "batch-proxy-attempt-count",
        env = "BATCH_PROXY_ATTEMPT_COUNT",
        default_value = "3"
    )]
    pub(super) max_attempts: usize,

    /// Not retry on first batch request error.
    /// Fast error response on network issue or server error.
    #[clap(
        name = "batch-proxy-fail-fast",
        long = "batch-proxy-fail-fast",
        env = "BATCH_PROXY_FAIL_FAST"
    )]
    pub(super) fail_fast: bool,

    /// Delay before retry
    #[clap(
        name = "batch-proxy-retry-delay",
        long = "batch-proxy-retry-delay",
        env = "BATCH_PROXY_RETRY_DELAY",
        parse(try_from_str = parse_iso8601_duration),
    )]
    pub(super) retry_delay: Option<Duration>,
}

struct ContinuableRequesterRepeater {
    url: String,
    headers: Headers,
    requester: Arc<dyn super::Requester>,
    config: RepeaterConfig,
    sender: BatchSender<PacketResult>,
}

impl ContinuableRequesterRepeater {
    async fn start_retry_loop(self, batch: Batch) -> Result<(), Error> {
        // Fail if first attempt return error
        let bytes_stream_result = self.perform_batch_request(batch).await;

        if self.config.fail_fast {
            if let Err(err) = bytes_stream_result {
                return Err(err);
            }
        }

        let current_span = tracing::Span::current();
        tokio::spawn(async move {
            current_span
                .in_scope(|| async {
                    self.retry_loop(batch, bytes_stream_result).await;
                })
                .await;
        });

        Ok(())
    }

    #[tracing::instrument(
        name = "ContinuableRequesterRepeater::retry_loop",
        skip_all,
        fields(batch)
    )]
    async fn retry_loop(self, batch: Batch, mut packet_stream_result: PacketStreamResult) {
        let (attempts_count, mut batch, mut last_error) =
            match self.try_first_attempt(batch, packet_stream_result).await {
                Ok(_) => return,
                Err(data) => data,
            };

        for attempt in 0_usize..attempts_count {
            tracing::trace!(attempt, batch = %batch, "Try attempt");

            packet_stream_result = self.perform_batch_request(batch).await;

            match self.try_attempt(batch, packet_stream_result).await {
                RepeaterAttempt::Finished => return,
                RepeaterAttempt::NotRepeatableAttempt { error } => {
                    last_error = error;
                    break;
                }
                RepeaterAttempt::RepeatableAttempt { next_batch, error } => {
                    batch = next_batch;
                    last_error = Some(error);
                }
            }
        }

        if let Some(error) = last_error {
            self.send_error_packet(error).await;
        }
    }

    async fn perform_batch_request(&self, batch: Batch) -> PacketStreamResult {
        let batch_stream_result = self
            .requester
            .clone()
            .batch_request(batch, self.url.clone(), self.headers.clone())
            .await;

        match batch_stream_result {
            Ok(butch_stream) => {
                trace!("Success start batch");
                Ok(butch_stream)
            }
            Err(err) => {
                tracing::error!(err = %err, "Batch attempt error");
                Err(err)
            }
        }
    }

    async fn try_attempt(
        &self,
        batch: Batch,
        source_stream_result: PacketStreamResult,
    ) -> RepeaterAttempt {
        let source_stream = match source_stream_result {
            Ok(source_stream) => source_stream,
            Err(err) => {
                tracing::warn!(
                    batch = %batch,
                    err = %err,
                    "Retry batch after error",
                );

                let processed_bytes_count = 0;
                return self
                    .build_next_attempt(batch, processed_bytes_count, err)
                    .await;
            }
        };

        self.process_stream(batch, source_stream).await
    }

    async fn try_first_attempt(
        &self,
        batch: Batch,
        packet_stream_result: PacketStreamResult,
    ) -> Result<(), (usize, Batch, Option<Error>)> {
        match self.try_attempt(batch, packet_stream_result).await {
            RepeaterAttempt::Finished => Ok(()),
            RepeaterAttempt::NotRepeatableAttempt { error } => Err((0, batch, error)),
            RepeaterAttempt::RepeatableAttempt { next_batch, error } => {
                Err((
                    self.config.max_attempts - 1, // exclude current
                    next_batch,
                    Some(error),
                ))
            }
        }
    }

    async fn process_stream(
        &self,
        batch: Batch,
        mut source_packet_stream: PacketStream,
    ) -> RepeaterAttempt {
        use futures::stream::StreamExt;

        let mut processed_bytes_count: usize = 0;

        while let Some(packet_result) = source_packet_stream.next().await {
            match self.send_packet_result(packet_result).await {
                Ok(bytes_len) => {
                    processed_bytes_count += bytes_len;

                    trace!(
                        "Success send {} (total {})",
                        bytes_len,
                        processed_bytes_count,
                    );
                }
                Err(chunk_error) => {
                    return self
                        .build_error(batch, processed_bytes_count, chunk_error)
                        .await;
                }
            }
        }

        trace!("Batch read finished");

        self.build_finished_attempt(batch, processed_bytes_count)
            .await
    }

    async fn build_error(
        &self,
        batch: Batch,
        processed_bytes_count: usize,
        chunk_error: ChunkError,
    ) -> RepeaterAttempt {
        match chunk_error {
            ChunkError::Read(error) => {
                warn!("Retry batch after: {}", processed_bytes_count);

                self.build_next_attempt(batch, processed_bytes_count, error)
                    .await
            }
            ChunkError::Write => {
                warn!("Client close connection");

                RepeaterAttempt::NotRepeatableAttempt { error: None }
            }
        }
    }

    async fn build_finished_attempt(
        &self,
        batch: Batch,
        processed_bytes_count: usize,
    ) -> RepeaterAttempt {
        if processed_bytes_count > batch.size() {
            warn!(
                "Process {} bytes, greater then batch size {}",
                processed_bytes_count,
                batch.size(),
            );
        }

        if processed_bytes_count >= batch.size() {
            tracing::debug!(batch = %batch, "Batch successful finished");
            return RepeaterAttempt::Finished;
        }

        warn!("Steam not finished and not return error");
        self.build_next_attempt(batch, processed_bytes_count, Error::StreamNotFinished)
            .await
    }

    async fn build_next_attempt(
        &self,
        batch: Batch,
        processed_bytes_count: usize,
        error: Error,
    ) -> RepeaterAttempt {
        let config = self.config;

        let next_batch = Batch {
            start: batch.start + processed_bytes_count,
            end: batch.end,
            content_length: batch.content_length,
        };

        trace!("Next batch: {:?}", next_batch);

        if config.fail_fast {
            warn!("Stop batch on fail fast");
            return RepeaterAttempt::NotRepeatableAttempt { error: Some(error) };
        }

        if let Some(delay) = config.retry_delay {
            debug!("Retry request {} second", delay.as_secs_f64());
            tokio::time::sleep(delay).await;
        }

        RepeaterAttempt::RepeatableAttempt { next_batch, error }
    }

    async fn send_packet_result(&self, packet_result: PacketResult) -> Result<usize, ChunkError> {
        match packet_result {
            Ok(batch_bytes) => {
                let bytes_len = batch_bytes.len();
                trace!("Batch chunk read read {} bytes", bytes_len);

                self.sender.send(Ok(batch_bytes)).await.map_err(|err| {
                    error!("Write chunk error: {}", err);
                    ChunkError::Write
                })?;

                Ok(bytes_len)
            }
            Err(err) => {
                warn!("Batch chunk read error: {}", err);

                Err(ChunkError::Read(err))
            }
        }
    }

    async fn send_error_packet(&self, error: Error) {
        debug!("Send error package {}", error);

        match self.sender.send(Err(error)).await {
            Ok(()) => {
                debug!("Success send error");
            }
            Err(err) => {
                error!("Error on send error: {}", err);
            }
        }
    }
}

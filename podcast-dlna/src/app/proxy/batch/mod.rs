#![allow(clippy::default_trait_access)]

use std::sync::Arc;

use super::range::Batch;

use async_trait::async_trait;
use bytes::Bytes;
use futures::stream::StreamExt;
use futures::stream::TryStreamExt;
use futures::TryFutureExt;
use http::{HeaderMap, HeaderValue};

mod channel;

mod continuable;

pub use continuable::{
    Requester as ContinuableRequester, RequesterConfig as ContinuableRequesterConfig,
};

type Headers = HeaderMap<HeaderValue>;

type PacketResult = Result<bytes::Bytes, Error>;

pub(super) type PacketStream = Box<dyn futures::Stream<Item = PacketResult> + Send + Sync + Unpin>;

type PacketStreamResult = Result<PacketStream, Error>;

#[derive(thiserror::Error, Debug, PartialEq)]
pub enum Error {
    #[error("Request status error: {0}")]
    RequestStatus(http::StatusCode),

    // TODO: replace string with custom error type
    #[error("Request error: {0}")]
    Reqwest(String),

    #[error("Stream not finished")]
    StreamNotFinished,
}

impl From<reqwest::Error> for Error {
    fn from(reqwest_error: reqwest::Error) -> Self {
        Self::Reqwest(reqwest_error.to_string())
    }
}

#[cfg_attr(test, mockall::automock)]
#[async_trait]
pub trait Requester: Send + Sync {
    async fn batch_request(
        self: Arc<Self>,
        range: Batch,
        url: String,
        headers: Headers,
    ) -> PacketStreamResult;
}

#[derive(Clone)]
pub struct ReqwestRequester {
    http_client: reqwest::Client,
}

impl ReqwestRequester {
    pub(crate) const fn new(http_client: reqwest::Client) -> Self {
        Self { http_client }
    }
}

impl Default for ReqwestRequester {
    fn default() -> Self {
        let http_client = reqwest::Client::new();

        Self::new(http_client)
    }
}

#[async_trait]
impl Requester for ReqwestRequester {
    #[tracing::instrument(
        name = "ReqwestRequester::batch_request",
        skip_all,
        fields(url, range, range_size = range.size()),
        err
    )]
    async fn batch_request(
        self: Arc<Self>,
        range: Batch,
        url: String,
        headers: Headers,
    ) -> PacketStreamResult {
        let range_header = range.to_request_header_value();

        let response = self
            .http_client
            .clone()
            .get(&url)
            .headers(headers.clone())
            .header(http::header::RANGE, range_header)
            .send()
            .await?;

        if !response.status().is_success() {
            error!("Batch response with status: {}", response.status());
            return Err(Error::RequestStatus(response.status()));
        }

        if let Some(content_length) = response.content_length() {
            tracing::debug!(content_length, "Range content length");
        }

        let bytes_stream = response.bytes_stream().map_err(|err| {
            error!("Batch request stream err: {}", err);
            err.into()
        });

        Ok(Box::new(bytes_stream))
    }
}

impl super::buffer::SizedBytes for PacketResult {
    fn len(&self) -> Option<usize> {
        if let Ok(ref bytes) = self {
            return Some(bytes.len());
        }
        None
    }
}

#[derive(Debug, PartialEq)]
pub struct StreamBuildParams {
    pub(super) url: String,
    pub(super) headers: Headers,
    pub(super) range: Batch,
    pub(super) body_prefix: Bytes,
}

#[cfg_attr(test, mockall::automock)]
#[async_trait]
pub trait StreamBuilder: Send + Sync {
    async fn build_stream(&self, params: StreamBuildParams) -> Result<PacketStream, Error>;
}

#[derive(Clone, Copy, Debug, clap::Parser)]
#[clap()]
pub struct RemoteStreamBuilderConfig {
    /// Batch size for proxy.
    #[clap(
        name = "proxy-batch-size",
        long = "proxy-batch-size",
        env = "PROXY_BATCH_SIZE",
        default_value = "5000000"
    )]
    batch_size: usize,

    /// Proxy packet buffer size.
    /// Less value - less memory consumption.
    /// More value means more reliable connection.
    #[clap(
        name = "proxy-packet-buffer-size",
        long = "proxy-packet-buffer-size",
        env = "PROXY_PACKET_BUFFER_SIZE",
        default_value = "10"
    )]
    packet_buffer_size: usize,

    /// Count of ready buffered batches.
    /// Values:
    /// 0 - stop request next batch, when client not read current.
    /// 1 - allow request next batch, when client read current.
    /// n (2 and above) - allow request next (n-1) batches.
    #[clap(
        name = "proxy-buffered-batch-count",
        long = "proxy-buffered-batch-count",
        env = "PROXY_BUFFERED_BATCH_COUNT",
        default_value = "2"
    )]
    buffered_batch_count: usize,
}

#[derive(Clone)]
pub struct RemoteStreamBuilder {
    batch_requester: Arc<dyn Requester>,
    config: RemoteStreamBuilderConfig,
}

impl RemoteStreamBuilder {
    pub(crate) fn new(
        batch_requester: Arc<dyn Requester>,
        config: RemoteStreamBuilderConfig,
    ) -> Self {
        Self {
            batch_requester,
            config,
        }
    }

    fn build_first_batch(body_prefix: Bytes, range: Batch) -> (PacketStream, Option<Batch>) {
        use crate::app::DataStreamer;

        let prefix_len = body_prefix.len();

        let (first_batch_range, tail_batch_range) = range.split_to(prefix_len);

        #[allow(clippy::option_if_let_else)]
        if let Some(first_batch_range) = first_batch_range {
            tracing::trace!(?first_batch_range, ?tail_batch_range, "Split first range");
            let first_range_end = first_batch_range.end + 1;
            let first_range = first_batch_range.start..first_range_end;

            let stream = (body_prefix, first_range).data_stream();
            (stream, tail_batch_range)
        } else {
            trace!("Skip first batch");

            (Box::new(futures::stream::empty()), tail_batch_range)
        }
    }

    fn empty_batch_ranges() -> Box<dyn std::iter::Iterator<Item = Batch> + Send> {
        Box::new(std::iter::empty())
    }
}

#[async_trait]
impl StreamBuilder for RemoteStreamBuilder {
    #[tracing::instrument(
        name = "RemoteStreamBuilder::build_stream"
        skip_all,
        err
    )]
    async fn build_stream(&self, params: StreamBuildParams) -> Result<PacketStream, Error> {
        let headers = params.headers.clone();

        let batch_requester = self.batch_requester.clone();

        let (first_batch, tail_batch_range) =
            Self::build_first_batch(params.body_prefix, params.range);

        let tail_batch_ranges = tail_batch_range
            .map_or_else(Self::empty_batch_ranges, |tail_batch_range| {
                tail_batch_range.split_by(self.config.batch_size)
            });

        let current_span = tracing::Span::current();
        let batch_futures = tail_batch_ranges.map(move |range| {
            current_span.in_scope(|| {
                batch_requester
                    .clone()
                    .batch_request(range, params.url.clone(), headers.clone())
                    .map_err(|err| {
                        tracing::error!(err = %err, "Batch request error");
                        err
                    })
            })
        });

        let batch_stream = futures::stream::iter(batch_futures)
            .buffered(self.config.buffered_batch_count)
            .try_flatten();

        let stream = super::buffer::infinity(
            self.config.packet_buffer_size,
            Box::new(first_batch.chain(batch_stream)),
        );

        Ok(Box::new(stream))
    }
}

#[cfg(test)]
mod tests;

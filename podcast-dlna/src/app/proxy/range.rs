use std::fmt::{Display, Formatter, Result as FmtResult};
use std::num::ParseIntError;
use std::str::FromStr;

use thiserror::Error as ThisError;

use super::errors::InvalidRangeValue;

// Errors

#[derive(Debug, ThisError)]
pub(super) enum Error {
    #[error("Unexpected header format. Expected bytes=<start>-<end>, got {0}.")]
    UnexpectedFormat(String),

    #[error("Parse header value error")]
    ParseInt(#[from] ParseIntError),

    #[error("Build Range Header error")]
    InvalidHeader(#[from] http::header::InvalidHeaderValue),
}

// Range

const RANGE_HEADER_PREFIX: &str = "bytes=";
const RANGE_DELIMITER: char = '-';

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub(super) struct Range {
    pub start: usize,
    pub end: Option<usize>,
}

impl FromStr for Range {
    type Err = Error;

    fn from_str(range_str: &str) -> Result<Self, Self::Err> {
        if !range_str.starts_with(RANGE_HEADER_PREFIX) {
            return Err(Error::UnexpectedFormat(range_str.into()));
        }

        let values = range_str[RANGE_HEADER_PREFIX.len()..]
            .split(RANGE_DELIMITER)
            .collect::<Vec<&str>>();

        match values.as_slice() {
            [start, ""] => Ok(Self {
                start: start.parse()?,
                end: None,
            }),
            [start, end] => Ok(Self {
                start: start.parse()?,
                end: Some(end.parse()?),
            }),
            _ => Err(Error::UnexpectedFormat(range_str.into())),
        }
    }
}

impl Display for Range {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        if let Some(end) = self.end {
            write!(fmt, "{}..{}", self.start, end)
        } else {
            write!(fmt, "{}..", self.start)
        }
    }
}

// Request

pub(super) struct Request {
    pub range: Option<Range>,
    pub content_length: usize,
    content_length_last: usize,
}

// pub(super) implementation
impl Request {
    pub(super) const fn new(range: Option<Range>, content_length: usize) -> Self {
        // -1 for inclusion range behavior
        let content_length_last = content_length - 1;

        Self {
            range,
            content_length,
            content_length_last,
        }
    }

    pub(super) fn batch(&self) -> Result<Batch, InvalidRangeValue> {
        self.range.map_or_else(
            || {
                debug!(
                    "Range {first}-{last} (full file)",
                    first = 0,
                    last = self.content_length_last,
                );

                Ok(Batch {
                    start: 0,
                    end: self.content_length_last,
                    content_length: self.content_length,
                })
            },
            |range| self.batch_range(range),
        )
    }
}

// private implementation
impl Request {
    fn batch_range(&self, range: Range) -> Result<Batch, InvalidRangeValue> {
        range.end.map_or_else(
            || {
                debug!(
                    "Range {first}-{last} (file end)",
                    first = range.start,
                    last = self.content_length_last,
                );

                Ok(Batch {
                    start: range.start,
                    end: self.content_length_last,
                    content_length: self.content_length,
                })
            },
            |end| self.batch_range_with_end(range, end),
        )
    }

    fn batch_range_with_end(&self, range: Range, end: usize) -> Result<Batch, InvalidRangeValue> {
        if end > self.content_length_last {
            warn!(
                "Invalid range end ({end} > {max_end})",
                end = end,
                max_end = self.content_length_last,
            );

            Err(InvalidRangeValue {
                max_end: self.content_length_last,
                end,
            })
        } else {
            debug!(
                "Range {first} to {last} (from request)",
                first = range.start,
                last = end,
            );

            Ok(Batch {
                start: range.start,
                end,
                content_length: self.content_length,
            })
        }
    }
}

// Batch

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Batch {
    pub start: usize,
    pub end: usize,
    pub content_length: usize,
}

impl std::fmt::Display for Batch {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        let value = format!("{}..{}/{}", self.start, self.end, self.content_length);
        f.write_str(&value)
    }
}

impl Batch {
    pub(super) fn to_request_header_value(self) -> String {
        format!("bytes={}-{}", self.start, self.end)
    }

    pub(super) fn to_response_header_value(self) -> String {
        format!(
            "bytes {first}-{last}/{size}",
            first = self.start,
            last = self.end,
            size = self.content_length,
        )
    }

    pub(super) const fn size(&self) -> usize {
        // End is inclusion
        self.end - self.start + 1
    }

    pub(super) const fn split_to(&self, right_start: usize) -> (Option<Self>, Option<Self>) {
        if right_start <= self.start {
            (None, Some(*self))
        } else if right_start > self.end {
            (Some(*self), None)
        } else {
            let content_length = self.content_length;
            let left_end = right_start - 1;

            (
                Some(Self {
                    start: self.start,
                    end: left_end,
                    content_length,
                }),
                Some(Self {
                    start: right_start,
                    end: self.end,
                    content_length,
                }),
            )
        }
    }

    pub(super) fn split_by(
        self,
        batch_size: usize,
    ) -> Box<dyn std::iter::Iterator<Item = Self> + Send> {
        let total_size = self.size();
        let content_length = self.content_length;

        let batch_count = total_size / batch_size;

        let iter = (0..batch_count)
            .map(move |index| {
                let start = index * batch_size + self.start;
                let end = start + batch_size - 1;

                Self {
                    start,
                    end,
                    content_length,
                }
            })
            .chain(self.batch_tail(batch_size).into_iter());

        Box::new(iter)
    }

    fn batch_tail(&self, batch_size: usize) -> Vec<Self> {
        let content_length = self.content_length;
        let batch_tail_size = self.size() % batch_size;

        if batch_tail_size > 0 {
            let end = self.end;
            let start = end - (batch_tail_size - 1);

            vec![Self {
                start,
                end,
                content_length,
            }]
        } else {
            vec![]
        }
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use super::{Batch, Range, Request};

    #[test]
    fn parse_closed_range() {
        let range: Range = "bytes=1-1000".parse().unwrap();

        let expected_range = Range {
            start: 1,
            end: Some(1000),
        };

        assert_eq!(expected_range, range);
    }

    #[test]
    fn parse_open_range() {
        let range: Range = "bytes=1-".parse().unwrap();

        let expected_range = Range {
            start: 1,
            end: None,
        };

        assert_eq!(expected_range, range);
    }

    #[test]
    fn batch_range_to_request_header_value() {
        let batch_range = Batch {
            start: 0,
            end: 999,
            content_length: 2000,
        };

        let header_value = batch_range.to_request_header_value();
        let expected_header_value = "bytes=0-999";
        assert_eq!(expected_header_value, header_value);
    }

    #[test]
    fn batch_range_to_response_header_value() {
        let batch_range = Batch {
            start: 0,
            end: 999,
            content_length: 2000,
        };

        let header_value = batch_range.to_response_header_value();
        let expected_header_value = "bytes 0-999/2000";
        assert_eq!(expected_header_value, header_value);
    }

    #[test]
    fn batch_split_to() {
        struct Case {
            description: String,
            batch: Batch,
            target: usize,
            expected_left_batch: Option<Batch>,
            expected_right_batch: Option<Batch>,
        }

        let cases = vec![
            Case {
                description: "Target less start".into(),
                batch: Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                },
                target: 50,
                expected_left_batch: None,
                expected_right_batch: Some(Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                }),
            },
            Case {
                description: "Target equal start".into(),
                batch: Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                },
                target: 100,
                expected_left_batch: None,
                expected_right_batch: Some(Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                }),
            },
            Case {
                description: "Target between start and end".into(),
                batch: Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                },
                target: 150,
                expected_left_batch: Some(Batch {
                    start: 100,
                    content_length: 200,
                    end: 149,
                }),
                expected_right_batch: Some(Batch {
                    start: 150,
                    end: 199,
                    content_length: 200,
                }),
            },
            Case {
                description: "Target equal end".into(),
                batch: Batch {
                    start: 100,
                    end: 199,
                    content_length: 200,
                },
                target: 199,
                expected_left_batch: Some(Batch {
                    start: 100,
                    end: 198,
                    content_length: 200,
                }),
                expected_right_batch: Some(Batch {
                    start: 199,
                    end: 199,
                    content_length: 200,
                }),
            },
        ];

        for case in &cases {
            let (left_batch, right_batch) = case.batch.split_to(case.target);

            assert_eq!(case.expected_left_batch, left_batch, "{}", case.description);
            assert_eq!(
                case.expected_right_batch, right_batch,
                "{}",
                case.description,
            );
        }
    }

    #[test]
    fn test_detect_batch() {
        use super::super::errors::InvalidRangeValue;

        struct Case {
            description: String,
            request_range: Option<Range>,
            content_length: usize,
            expected_batch: Result<Batch, InvalidRangeValue>,
        }

        let cases = vec![
            Case {
                description: "Without request range".into(),

                request_range: None,
                content_length: 1000,
                expected_batch: Ok(Batch {
                    start: 0,
                    end: 999,
                    content_length: 1000,
                }),
            },
            Case {
                description: "Request range without end".into(),

                request_range: Some(Range {
                    start: 500,
                    end: None,
                }),
                content_length: 1000,
                expected_batch: Ok(Batch {
                    start: 500,
                    end: 999,
                    content_length: 1000,
                }),
            },
            Case {
                description: "Request range with end before content-length".into(),

                request_range: Some(Range {
                    start: 500,
                    end: Some(750),
                }),
                content_length: 1000,
                expected_batch: Ok(Batch {
                    start: 500,
                    end: 750,
                    content_length: 1000,
                }),
            },
            Case {
                description: "Request range with end equal content-length".into(),

                request_range: Some(Range {
                    start: 250,
                    end: Some(999),
                }),
                content_length: 1000,
                expected_batch: Ok(Batch {
                    start: 250,
                    end: 999,
                    content_length: 1000,
                }),
            },
            Case {
                description: "Request range with end after content-length".into(),

                request_range: Some(Range {
                    start: 250,
                    end: Some(1000),
                }),
                content_length: 1000,
                expected_batch: Err(InvalidRangeValue {
                    max_end: 999,
                    end: 1000,
                }),
            },
        ];

        for case in &cases {
            let request = Request::new(case.request_range, case.content_length);
            let batch = request.batch();

            assert_eq!(case.expected_batch, batch, "{}", case.description);
        }
    }

    #[allow(clippy::too_many_lines)]
    #[test]
    fn test_batch_split_by() {
        struct Case {
            description: String,
            batch_range: Batch,
            batch_size: usize,
            expected_batch_ranges: Vec<Batch>,
        }

        let content_length = 2500;

        let cases = vec![
            Case {
                description: "Simple".into(),
                batch_range: Batch {
                    start: 0,
                    end: 2499,
                    content_length,
                },
                batch_size: 1000,
                expected_batch_ranges: vec![
                    Batch {
                        start: 000,
                        end: 999,
                        content_length,
                    },
                    Batch {
                        start: 1000,
                        end: 1999,
                        content_length,
                    },
                    Batch {
                        start: 2000,
                        end: 2499,
                        content_length,
                    },
                ],
            },
            Case {
                description: "Padded".into(),
                batch_range: Batch {
                    start: 250,
                    end: 2549,
                    content_length,
                },
                batch_size: 1000,
                expected_batch_ranges: vec![
                    Batch {
                        start: 250,
                        end: 1249,
                        content_length,
                    },
                    Batch {
                        start: 1250,
                        end: 2249,
                        content_length,
                    },
                    Batch {
                        start: 2250,
                        end: 2549,
                        content_length,
                    },
                ],
            },
            Case {
                description: "One byte".into(),
                batch_range: Batch {
                    start: 0,
                    end: 0,
                    content_length,
                },
                batch_size: 1000,
                expected_batch_ranges: vec![Batch {
                    start: 0,
                    end: 0,
                    content_length,
                }],
            },
            Case {
                description: "One batch".into(),
                batch_range: Batch {
                    start: 0,
                    end: 999,
                    content_length,
                },
                batch_size: 1000,
                expected_batch_ranges: vec![Batch {
                    start: 0,
                    end: 999,
                    content_length,
                }],
            },
            Case {
                description: "One batch with one byte".into(),
                batch_range: Batch {
                    start: 0,
                    end: 1000,
                    content_length,
                },
                batch_size: 1000,
                expected_batch_ranges: vec![
                    Batch {
                        start: 0,
                        end: 999,
                        content_length,
                    },
                    Batch {
                        start: 1000,
                        end: 1000,
                        content_length,
                    },
                ],
            },
        ];

        for case in &cases {
            let batch_ranges = case
                .batch_range
                .split_by(case.batch_size)
                .collect::<Vec<Batch>>();
            assert_eq!(
                case.expected_batch_ranges, batch_ranges,
                "{}",
                case.description,
            );
        }
    }
}

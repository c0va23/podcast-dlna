use std::str::FromStr;
use std::sync::Arc;

use http::{HeaderMap, HeaderValue, StatusCode};
use hyper::Body;

use crate::app::{FeedFetcher, FeedFetcherError};
use crate::app::{PreflightParams, PreflightResponse, Preflighter};

mod batch;
mod buffer;
mod errors;
mod range;

#[cfg(test)]
mod tests;

use batch::StreamBuildParams;
pub use batch::{
    ContinuableRequester, ContinuableRequesterConfig, RemoteStreamBuilder,
    RemoteStreamBuilderConfig, ReqwestRequester, StreamBuilder as BatchStreamBuilder,
};

use errors::Error;
use range::{Batch, Range, Request};

type Headers = http::HeaderMap<http::HeaderValue>;

pub struct Proxy {
    preflighter: Arc<dyn Preflighter>,
    batch_stream_builder: Arc<dyn BatchStreamBuilder>,
    feed_fetcher: Arc<dyn FeedFetcher>,
}

impl Proxy {
    pub(crate) fn new(
        preflighter: Arc<dyn Preflighter>,
        batch_stream_builder: Arc<dyn BatchStreamBuilder>,
        feed_fetcher: Arc<dyn FeedFetcher>,
    ) -> Self {
        Self {
            preflighter,
            batch_stream_builder,
            feed_fetcher,
        }
    }

    fn truncate_extension(track_id: &str) -> String {
        track_id
            .split_once('.')
            .map_or(track_id, |pair| pair.0)
            .to_owned()
    }
}

#[async_trait::async_trait]
impl crate::server::ProxyHandler for Proxy {
    #[tracing::instrument(
        name = "Proxy::handle",
        skip_all,
        fields(method, channel_id, track_id_with_extension)
    )]
    async fn handle(
        self: Arc<Self>,
        method: hyper::Method,
        channel_id: String,
        track_id_with_extension: String,
        headers: Headers,
    ) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
        let range_header = Self::fetch_range_header(&headers)?;

        let track_id = Self::truncate_extension(&track_id_with_extension);

        let range = range_header
            .map(|range_header| Range::from_str(&range_header))
            .transpose()
            .map_err(Error::RangeHeader)?;

        let (channel, track) = self
            .feed_fetcher
            .fetch_track(channel_id.as_str(), track_id.as_str())
            .await
            .map_err(|err| Self::map_feed_fetcher_error(&err))?;

        let headers = Self::filter_request_headers(&headers);

        let preflight_response = self.preflight_request(channel.url, track.url).await?;

        let request = Request::new(range, preflight_response.content_length);

        let reply = self
            .build_response(method, preflight_response, headers, request)
            .await?;

        Ok(Box::new(reply))
    }
}

// private implementation
impl Proxy {
    fn map_feed_fetcher_error(error: &FeedFetcherError) -> warp::Rejection {
        use FeedFetcherError::{ChannelNotFound, Other, TrackNotFound};

        match error {
            ChannelNotFound | TrackNotFound => warp::reject::not_found(),
            Other(_) => warp::reject::reject(),
        }
    }

    async fn preflight_request(
        &self,
        feed_url: String,
        url: String,
    ) -> Result<PreflightResponse, Error> {
        let response = self
            .preflighter
            .perform(PreflightParams {
                feed_url,
                url,
                parse_body: false,
            })
            .await?;

        Ok(response)
    }

    #[tracing::instrument(name = "Proxy::build_response", skip_all, err)]
    async fn build_response(
        &self,
        method: hyper::Method,
        src_response: PreflightResponse,
        request_headers: Headers,
        request: Request,
    ) -> Result<impl warp::Reply, Error> {
        let mut response_builder = http::Response::builder().version(src_response.version);

        Self::add_response_headers(&mut response_builder, &src_response.headers);

        let (body, status) = if src_response.status.as_u16() >= 300 {
            Self::error_preflight_response(&src_response)
        } else if let Ok(batch) = request.batch() {
            let headers = response_builder.headers_mut().unwrap();
            let status = if request.range.is_some() {
                Self::write_partial_headers(headers, batch)
            } else {
                Self::write_ok_headers(headers, batch)
            };

            let body = self
                .success_body(method, src_response, request_headers, batch)
                .await?;

            (body, status)
        } else {
            Self::error_range_response()
        };

        let response = response_builder.status(status).body(body)?;

        tracing::debug!(status = %response.status(), hreaders = ?response.headers() ,"Response");

        Ok(response)
    }

    const EXCLUDE_RESPONSE_HEADERS: &'static [&'static str] = &["content-length", "content-range"];

    fn add_response_headers(
        response_builder: &mut http::response::Builder,
        request_headers: &HeaderMap<http::HeaderValue>,
    ) {
        let headers = response_builder.headers_mut().unwrap();

        for (key, value) in request_headers.iter() {
            if !Self::EXCLUDE_RESPONSE_HEADERS.contains(&key.to_string().as_str()) {
                headers.append(key, value.clone());
            }
        }
    }

    fn error_preflight_response(src_response: &PreflightResponse) -> (Body, StatusCode) {
        use crate::app::DataStreamer;

        tracing::debug!(status = %src_response.status, "Return text body");

        let text = src_response.body_prefix.data_stream::<batch::Error>();
        (hyper::Body::wrap_stream(text), src_response.status)
    }

    fn write_partial_headers(headers: &mut HeaderMap<HeaderValue>, batch: Batch) -> StatusCode {
        headers.append(http::header::CONTENT_LENGTH, batch.size().into());
        headers.append(
            http::header::CONTENT_RANGE,
            batch.to_response_header_value().parse().unwrap(),
        );

        http::StatusCode::PARTIAL_CONTENT
    }

    fn write_ok_headers(headers: &mut HeaderMap<HeaderValue>, batch: Batch) -> StatusCode {
        headers.append(http::header::CONTENT_LENGTH, batch.content_length.into());

        http::StatusCode::OK
    }

    async fn success_body(
        &self,
        method: hyper::Method,
        src_response: PreflightResponse,
        request_headers: Headers,
        batch_range: Batch,
    ) -> Result<Body, Error> {
        let body = if method == http::Method::HEAD {
            debug!("Empty response on HEAD request");
            hyper::Body::empty()
        } else {
            debug!("Return batch body");
            let batch_response_stream = self
                .batch_stream_builder
                .build_stream(StreamBuildParams {
                    url: src_response.url,
                    headers: request_headers,
                    range: batch_range,
                    body_prefix: src_response.body_prefix,
                })
                .await?;
            hyper::Body::wrap_stream(batch_response_stream)
        };

        Ok(body)
    }

    fn error_range_response() -> (Body, StatusCode) {
        (
            hyper::Body::empty(),
            http::StatusCode::RANGE_NOT_SATISFIABLE,
        )
    }

    const EXCLUDE_REQUEST_HEADERS: &'static [&'static str] = &["host", "if-match", "range"];

    fn filter_request_headers(headers: &Headers) -> Headers {
        tracing::debug!(?headers, "Source headers");

        let filtered_headers = headers
            .iter()
            .filter_map(|(key, value)| {
                // Not pass Host header
                let header = key.to_string();
                if Self::EXCLUDE_REQUEST_HEADERS.contains(&header.as_str()) {
                    None
                } else {
                    Some((key.clone(), value.clone()))
                }
            })
            .collect();

        tracing::debug!(headers = ?filtered_headers, "Filtered headers");

        filtered_headers
    }

    fn fetch_range_header(headers: &Headers) -> Result<Option<String>, Error> {
        let range_header = headers
            .get(http::header::RANGE)
            .map(http::HeaderValue::to_str)
            .transpose()?
            .map(str::to_string);

        tracing::debug!(?range_header, "Range header");

        Ok(range_header)
    }
}

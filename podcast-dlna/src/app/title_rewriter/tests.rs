use super::{RegexTitleRewriter, TitleRewriter};

#[test]
fn test_regexp_title_rewriter_new() {
    struct TestCase<'a> {
        source_title: &'a str,
        expected_title: &'a str,
    }

    enum TestSuiteCases<'a> {
        Error,
        Success(&'a [TestCase<'a>]),
    }

    struct TestSuite<'a> {
        pattern: &'a str,
        template: &'a str,
        test_cases: TestSuiteCases<'a>,
    }

    let test_suites = &[
        TestSuite {
            pattern: "(.+) — Episode (\\d+",
            template: "${1} - ${2}",
            test_cases: TestSuiteCases::Error,
        },
        TestSuite {
            pattern: "(.+) — Episode (\\d+)",
            template: "Devzen ${2} — ${1}",
            test_cases: TestSuiteCases::Success(&[
                TestCase {
                    source_title: "IDE и интервью — Episode 0331",
                    expected_title: "Devzen 0331 — IDE и интервью",
                },
                TestCase {
                    source_title: "Формальный ко-ко-кок — Episode 0330",
                    expected_title: "Devzen 0330 — Формальный ко-ко-кок",
                },
            ]),
        },
        TestSuite {
            pattern: "(?P<title>.+) — Episode (?P<number>\\d+)",
            template: "Devzen $number — $title",
            test_cases: TestSuiteCases::Success(&[
                TestCase {
                    source_title: "IDE и интервью — Episode 0331",
                    expected_title: "Devzen 0331 — IDE и интервью",
                },
                TestCase {
                    source_title: "Формальный ко-ко-кок — Episode 0330",
                    expected_title: "Devzen 0330 — Формальный ко-ко-кок",
                },
            ]),
        },
    ];

    for test_suite in test_suites {
        let rewriter = RegexTitleRewriter::new(test_suite.pattern, test_suite.template.to_owned());

        match test_suite.test_cases {
            TestSuiteCases::Error => {
                assert!(rewriter.is_err());
            }
            TestSuiteCases::Success(test_cases) => {
                assert!(rewriter.is_ok());
                let rewriter = rewriter.unwrap();
                for test_case in test_cases {
                    let target_title = rewriter.rewrite_title(test_case.source_title);

                    assert_eq!(test_case.expected_title, target_title);
                }
            }
        }
    }
}

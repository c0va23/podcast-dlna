#![allow(clippy::module_name_repetitions)]

// imports
use regex::Regex;
use std::collections::HashMap;

use super::config::Config;

// modules
#[cfg(test)]
mod tests;

trait TitleRewriter {
    fn rewrite_title(&self, title: &str) -> String;
}

#[derive(Clone)]
struct RegexTitleRewriter {
    regex: Regex,
    template: String,
}

impl RegexTitleRewriter {
    #[allow(dead_code)]
    /// Create new `RegexTitleRewriter`
    ///
    /// Errors:
    /// - `regex::Error` if pattern invalid regexp
    fn new(pattern: &str, template: String) -> Result<Self, regex::Error> {
        let regex = Regex::new(pattern)?;

        Ok(Self { regex, template })
    }
}

impl TitleRewriter for RegexTitleRewriter {
    fn rewrite_title(&self, title: &str) -> String {
        self.regex
            .replace(title, self.template.as_str())
            .into_owned()
    }
}

struct PassTitleRewriter {}

impl PassTitleRewriter {
    const fn new() -> Self {
        Self {}
    }
}

impl TitleRewriter for PassTitleRewriter {
    fn rewrite_title(&self, title: &str) -> String {
        title.to_owned()
    }
}

pub trait ChannelTitleRewriter {
    fn rewrite_title(&self, url: &str, title: &str) -> String;
}

pub struct DefaultChannelTitleRewriter {
    rewriters: HashMap<String, RegexTitleRewriter>,
}

impl DefaultChannelTitleRewriter {
    pub(crate) fn from_config(config: &Config) -> Result<Self, regex::Error> {
        let rewriters = config
            .podcasts
            .iter()
            .filter_map(|podcast| {
                podcast
                    .name_rewrite_rule
                    .clone()
                    .map(|rewrite_rule| (podcast.url.clone(), rewrite_rule))
                    .map(|(url, rewrite_rule)| {
                        RegexTitleRewriter::new(
                            rewrite_rule.regex.as_str(),
                            rewrite_rule.template.clone(),
                        )
                        .map(|rewriter| (url, rewriter))
                    })
            })
            .collect::<Result<HashMap<String, RegexTitleRewriter>, regex::Error>>()?;

        Ok(Self { rewriters })
    }
}

impl ChannelTitleRewriter for DefaultChannelTitleRewriter {
    fn rewrite_title(&self, url: &str, title: &str) -> String {
        match self.rewriters.get(url) {
            Some(title_rewriter) => title_rewriter.rewrite_title(title),
            None => PassTitleRewriter::new().rewrite_title(title),
        }
    }
}

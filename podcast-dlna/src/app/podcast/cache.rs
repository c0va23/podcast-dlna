use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use tokio::sync::RwLock;
use ttl_cache::TtlCache;

use super::Channel;
use crate::utils::parse_iso8601_duration;

#[derive(Debug, Clone, clap::Parser)]
pub struct Config {
    /// RSS feed caching duration
    #[clap(
        name = "feed-cache-ttl",
        long = "feed-cache-ttl",
        env = "FEED_CACHE_TTL",
        default_value = "PT1H",
        parse(try_from_str = parse_iso8601_duration),
    )]
    pub cache_duration: Duration,
}

pub struct ChannelFetcher {
    backend_fetcher: Arc<dyn super::ChannelFetcher + Send + Sync>,
    cache: RwLock<TtlCache<String, Channel>>,
    config: Config,
}

impl ChannelFetcher {
    pub(crate) fn new(
        backend_fetcher: Arc<dyn super::ChannelFetcher + Send + Sync>,
        cache_size: usize,
        config: Config,
    ) -> Self {
        let cache = RwLock::new(TtlCache::new(cache_size));

        Self {
            backend_fetcher,
            cache,
            config,
        }
    }
}

#[async_trait]
impl super::ChannelFetcher for ChannelFetcher {
    async fn fetch(&self, url: String) -> anyhow::Result<Channel> {
        let backend_fetcher = self.backend_fetcher.clone();

        if let Some(channel) = self.cache.read().await.get(&url) {
            debug!("Return value from cache");

            return Ok(channel.clone());
        }

        let channel = backend_fetcher.fetch(url.clone()).await?;

        debug!("Write cache for {}", &url);

        // TODO: Possible double execution

        self.cache
            .write()
            .await
            .insert(url.clone(), channel.clone(), self.config.cache_duration);

        Ok(channel)
    }
}

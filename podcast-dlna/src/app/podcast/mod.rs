// Fix clippy warning for automock
#![cfg_attr(test, allow(clippy::default_trait_access))]
use async_trait::async_trait;

mod cache;
mod rss;

pub use self::rss::ChannelFetcher as RssChannelFetcher;
pub use cache::{ChannelFetcher as CachedChannelFetcher, Config as CacheConfig};

pub type DateTime = chrono::DateTime<chrono::FixedOffset>;
pub type Duration = std::time::Duration;

#[derive(Debug, PartialEq, Clone)]
pub struct Channel {
    pub url: String,
    pub title: String,
    pub tracks: Vec<MusicTrack>,
    pub image_url: Option<String>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct MusicTrack {
    pub title: String,
    pub url: String,
    pub content_type: String,
    pub size: Option<usize>,
    pub date: Option<DateTime>,
    pub creators: Option<String>,
    pub duration: Option<Duration>,
    pub image_url: Option<String>,
    pub description: Option<String>,
}

impl MusicTrack {
    pub(crate) const fn date(&self) -> Option<DateTime> {
        self.date
    }
}

#[cfg_attr(test, mockall::automock)]
#[async_trait]
pub trait ChannelFetcher {
    async fn fetch(&self, url: String) -> anyhow::Result<Channel>;
}

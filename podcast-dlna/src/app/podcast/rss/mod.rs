use std::num::ParseIntError;

use bytes::Bytes;

use bytes::Buf;
use rss::extension::itunes::ITunesChannelExtension;
use thiserror::Error as ThisError;

use super::{Channel, DateTime, MusicTrack};
use crate::utils::duration::parse_colons as parse_duration;

#[derive(Debug, ThisError)]
pub enum Error {
    #[error("Podcast reqwest error: {0}")]
    Reqwest(#[from] reqwest::Error),

    #[error("Parse RSS error: {0}")]
    Rss(#[from] ::rss::Error),

    #[error("Parse size error: {0}")]
    Convert(#[from] ParseIntError),

    #[error("Parse pubDate error: {0}")]
    ParseDatetime(#[from] chrono::format::ParseError),

    #[error("Invalid duration: {0}")]
    InvalidDuration(ParseIntError),
}

#[derive(Clone)]
pub struct ChannelFetcher {
    http_client: reqwest::Client,
}

impl ChannelFetcher {
    pub(crate) fn new() -> Self {
        Self {
            http_client: reqwest::Client::new(),
        }
    }

    async fn get_rss(&self, url: &str) -> Result<Bytes, Error> {
        let response = self.http_client.get(url).send().await?;

        debug!("Wait body");
        let body = response.bytes().await?;

        Ok(body)
    }
}

#[async_trait::async_trait]
impl super::ChannelFetcher for ChannelFetcher {
    async fn fetch(&self, url: String) -> anyhow::Result<Channel> {
        let body = self.get_rss(&url).await?;

        Ok(parse_feed(&url, body.reader())?)
    }
}

fn parse_feed(rss_url: &str, reader: impl std::io::BufRead) -> Result<Channel, Error> {
    debug!("Read channel");

    let channel = rss::Channel::read_from(reader)?;

    debug!("Iter items");

    let url = rss_url.to_owned();
    let title = channel.title().to_owned();
    let mut tracks = channel
        .items()
        .iter()
        .filter_map(|item| {
            item.enclosure()
                .map(|enclosure| parse_item(item, enclosure))
        })
        .collect::<Result<Vec<MusicTrack>, Error>>()?;

    tracks.sort_by(order_music_track);

    let image_url = channel
        .image()
        .map(rss::Image::url)
        .map(ToOwned::to_owned)
        .or_else(|| {
            channel
                .itunes_ext()
                .and_then(ITunesChannelExtension::image)
                .map(ToOwned::to_owned)
        });

    Ok(Channel {
        url,
        title,
        tracks,
        image_url,
    })
}

fn order_music_track(
    music_track_left: &MusicTrack,
    music_track_right: &MusicTrack,
) -> std::cmp::Ordering {
    match (music_track_left.date(), music_track_right.date()) {
        (Some(date_left), Some(date_right)) => date_right.cmp(&date_left),
        _ => std::cmp::Ordering::Equal,
    }
}

fn parse_data_time(src_data_time: &str) -> Result<chrono::DateTime<chrono::FixedOffset>, Error> {
    Ok(DateTime::parse_from_rfc2822(src_data_time)
        .or_else(|_| DateTime::parse_from_rfc3339(src_data_time))?)
}

fn parse_size(size: &str) -> Result<Option<usize>, Error> {
    let size_value = if size.is_empty() {
        None
    } else {
        Some(size.parse()?)
    };

    Ok(size_value)
}

fn parse_item(item: &rss::Item, enclosure: &rss::Enclosure) -> Result<MusicTrack, Error> {
    use rss::extension::itunes::ITunesItemExtension;

    let date = if let Some(pub_date) = item.pub_date() {
        Some(parse_data_time(pub_date)?)
    } else {
        None
    };
    let creators = item
        .itunes_ext()
        .and_then(ITunesItemExtension::author)
        .or_else(|| item.author())
        .map(ToOwned::to_owned);
    let duration = item
        .itunes_ext()
        .and_then(ITunesItemExtension::duration)
        .map(parse_duration)
        .transpose()
        .map_err(Error::InvalidDuration)?;
    let image_url = item
        .itunes_ext()
        .and_then(ITunesItemExtension::image)
        .map(ToOwned::to_owned);

    let description = item
        .description()
        .or_else(|| item.itunes_ext().and_then(ITunesItemExtension::summary))
        .map(ToOwned::to_owned);

    Ok(MusicTrack {
        size: parse_size(enclosure.length())?,
        title: item.title().unwrap_or("untitled").to_owned(),
        url: enclosure.url().to_owned(),
        content_type: enclosure.mime_type().to_owned(),
        creators,
        duration,
        date,
        image_url,
        description,
    })
}

#[cfg(test)]
#[allow(clippy::non_ascii_literal)]
mod tests;

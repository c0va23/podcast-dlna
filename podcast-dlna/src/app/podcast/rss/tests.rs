use super::super::Duration;
use super::*;
use chrono::TimeZone;

use pretty_assertions::assert_eq;

const MINUTE: u64 = 60;
const HOUR: u64 = 60 * MINUTE;

#[test]
fn parse_simple_feed() {
    let example_feed = include_str!("examples/example-feed.xml");
    let example_feed_bytes = example_feed.as_bytes();
    let cursor = std::io::Cursor::new(example_feed_bytes);
    let buffer = std::io::BufReader::new(cursor);

    let url = "https://radio-t.com/p/2020/08/22//podcast-716/";
    let music_tracks = parse_feed(url, buffer).unwrap().tracks;
    let expected_track = MusicTrack {
        title: "Радио-Т 716".into(),
        url: "http://cdn.radio-t.com/rt_podcast716.mp3".into(),
        content_type: "audio/mp3".into(),
        size: Some(94_637_163),
        duration: None,
        creators: Some("Umputun, Bobuk, Gray, Ksenks, Alek.sys".into()),
        description: Some(example_feed[2370..4075].to_string()),
        date: Some(
            chrono::FixedOffset::west(5 * 3600)
                .ymd(2020, 8, 22)
                .and_hms(18, 33, 45),
        ),
        image_url: Some("https://radio-t.com/images/radio-t/rt716.jpg".into()),
    };

    assert_eq!(expected_track, music_tracks[0]);
}

#[test]
fn parse_simple_feed_with_duration() {
    let example_feed = include_str!("examples/feed-with-duration.xml");
    let example_feed_bytes = example_feed.as_bytes();
    let cursor = std::io::Cursor::new(example_feed_bytes);
    let buffer = std::io::BufReader::new(cursor);

    let url = "https://devzen.ru/?p=3884";
    let music_tracks = parse_feed(url, buffer).unwrap().tracks;
    let expected_track = MusicTrack {
        title: "Холистический уж-паук — Episode 0302".into(),
        url: "https://devzen.ru/download/2020/devzen-0302-2020-08-29-e6b5cd8fa7b53cd7.mp3".into(),
        content_type: "audio/mpeg".into(),
        size: Some(84_084_736),
        duration: Some(Duration::from_secs(2 * HOUR + 20 * MINUTE + 4)),
        creators: Some("DevZen Podcast".into()),
        date: Some(
            chrono::FixedOffset::west(0)
                .ymd(2020, 8, 30)
                .and_hms(7, 50, 41),
        ),
        image_url: None,
        description: Some(example_feed[2293..2947].to_string()),
    };

    assert_eq!(expected_track, music_tracks[0]);
}

#[test]
fn parse_feed_with_iso_data() {
    let example_feed = include_str!("examples/feed-with-iso-date.xml");
    let example_feed_bytes = example_feed.as_bytes();
    let cursor = std::io::Cursor::new(example_feed_bytes);
    let buffer = std::io::BufReader::new(cursor);

    let url = "https://feeds.podcastmirror.com/razborpoletov";
    let music_tracks = parse_feed(url, buffer).unwrap().tracks;
    let expected_track = MusicTrack {
        title: "Episode 238 — Interview - Интервью с Борисовым".into(),
        url: "http://media.blubrry.com/razborpoletov/traffic.libsyn.com/".to_owned()
            + "razborpoletov/razbor_238.mp3",
        content_type: "audio/mpeg".into(),
        size: None,
        duration: None,
        creators: None,
        date: Some(
            chrono::FixedOffset::west(0)
                .ymd(2021, 10, 23)
                .and_hms(0, 0, 0),
        ),
        image_url: Some("http://razborpoletov.com/images/razbor_238_text.jpg".into()),
        description: Some(example_feed[2028..4989].to_string()),
    };

    assert_eq!(expected_track, music_tracks[0]);
}

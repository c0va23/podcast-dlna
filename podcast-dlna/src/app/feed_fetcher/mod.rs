#![allow(clippy::default_trait_access)]

use std::fmt::Debug;
use std::result::Result as StdResult;
use std::time::Duration;

use async_trait::async_trait;
use thiserror::Error as ThisError;

mod image;
mod podcast;
mod url;

pub use image::{
    CachedFetcher as CachedImageFetcher, Fetcher as ImageFetcher, Image,
    ReqwestFetcher as ReqwestImageFetcher,
};
pub use podcast::{Config as PodcastConfig, FeedFetcher as PodcastFetcher};
pub use url::Sha2UrlEncoder;

#[cfg(test)]
pub(self) use url::MockEncoder as MockUrlEncoder;
#[cfg(test)]
mod tests;

type DateTime = chrono::DateTime<chrono::FixedOffset>;

type ChannelId = String;

#[derive(Debug, PartialEq, Clone)]
pub struct Channel {
    pub id: ChannelId,
    pub url: String,
    pub title: String,
    pub tracks_count: usize,
    pub update_time: Option<DateTime>,
    pub image: Option<Image>,
}

impl Channel {
    pub(crate) const fn update_time(&self) -> Option<DateTime> {
        self.update_time
    }
}

type TrackId = String;

#[derive(Debug, PartialEq, Clone)]
pub struct MusicTrack {
    pub id: TrackId,
    pub title: String,
    pub url: String,
    pub content_type: String,
    pub size: Option<usize>,
    pub date: Option<DateTime>,
    pub creators: Option<String>,
    // TODO: change type to std::time::Duration and remove Option
    pub duration: Option<Duration>,
    pub image: Option<Image>,
    pub description: Option<String>,
}

impl MusicTrack {
    pub(crate) fn extension(&self) -> String {
        match self.content_type.as_str() {
            "audio/mpeg" | "audio/mp3" => "mp3",
            _ => "tmp",
        }
        .to_owned()
    }
}

#[derive(ThisError, Debug)]
pub enum Error {
    #[error("Channel not found")]
    ChannelNotFound,

    #[error("Track not found")]
    TrackNotFound,

    #[error("Other error: {0}")]
    Other(#[from] anyhow::Error),
}

type Result<T> = StdResult<T, Error>;

#[cfg_attr(test, mockall::automock)]
#[async_trait]
pub trait FeedFetcher: Sync + Send {
    async fn fetch_channels(&self) -> Result<Vec<Channel>>;

    async fn fetch_tracks(&self, channel_id: &str) -> Result<(Channel, Vec<MusicTrack>)>;

    async fn fetch_track(&self, channel_id: &str, track_id: &str) -> Result<(Channel, MusicTrack)>;
}

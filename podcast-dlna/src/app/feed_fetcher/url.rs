#![allow(clippy::default_trait_access)]
#[cfg_attr(test, mockall::automock)]
pub trait Encoder {
    fn encode_url(&self, url: &str) -> String;
}

pub struct Sha2UrlEncoder {
    random_data: [u8; 32],
}

impl Sha2UrlEncoder {
    pub(crate) fn new() -> Self {
        let random_data = rand::random();
        Self { random_data }
    }

    pub(crate) fn new_with_seed(seed: u64) -> Self {
        use rand::Rng;
        let mut rnd: rand::rngs::StdRng = rand::SeedableRng::seed_from_u64(seed);
        let random_data = rnd.gen();
        Self { random_data }
    }
}

impl Encoder for Sha2UrlEncoder {
    fn encode_url(&self, url: &str) -> String {
        use sha2::Digest;
        let mut hesher = sha2::Sha256::new();
        hesher.update(self.random_data);
        hesher.update(url);
        let bytes = hesher.finalize();
        base64::encode_config(bytes.as_slice(), base64::URL_SAFE_NO_PAD)
    }
}

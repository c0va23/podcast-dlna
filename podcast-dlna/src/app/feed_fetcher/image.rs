use std::collections::BTreeMap;
use std::ops::Range;
use std::sync::Arc;

use tokio::sync::RwLock;

use crate::app::data_stream::DataStreamer;
use crate::utils::ring_cache::Bucket;

#[derive(Clone, Debug, PartialEq)]
pub struct Image {
    image_data: bytes::Bytes,
    mime_type: Option<String>,
}

impl Image {
    pub(crate) fn new(image_data: bytes::Bytes, mime_type: Option<String>) -> Self {
        Self {
            image_data,
            mime_type,
        }
    }

    pub(crate) fn mime_type(&self) -> Option<String> {
        self.mime_type.clone()
    }
}

impl DataStreamer for Image {
    fn fetch_data(&self) -> (bytes::Bytes, Range<usize>) {
        (self.image_data.clone(), 0..self.image_data.len())
    }
}

#[cfg_attr(test, mockall::automock)]
#[async_trait::async_trait]
pub trait Fetcher {
    async fn fetch_channel_image(&self, image_url: &str) -> anyhow::Result<Image>;
    async fn fetch_track_image(&self, feed_id: &str, image_url: &str) -> anyhow::Result<Image>;
}

#[derive(Clone)]
pub struct ReqwestFetcher {
    http_client: reqwest::Client,
}

impl ReqwestFetcher {
    pub(crate) const fn new(http_client: reqwest::Client) -> Self {
        Self { http_client }
    }

    async fn fetch(&self, image_url: &str) -> anyhow::Result<Image> {
        let response = self.http_client.get(image_url).send().await?;
        let mime_type = response
            .headers()
            .get("Content-Type")
            .map(http::HeaderValue::to_str)
            .transpose()?
            .map(ToOwned::to_owned);
        let image_data = response.bytes().await?;

        Ok(Image::new(image_data, mime_type))
    }
}

#[async_trait::async_trait]
impl Fetcher for ReqwestFetcher {
    async fn fetch_channel_image(&self, image_url: &str) -> anyhow::Result<Image> {
        Ok(self.fetch(image_url).await?)
    }

    async fn fetch_track_image(&self, _feed_id: &str, image_url: &str) -> anyhow::Result<Image> {
        Ok(self.fetch(image_url).await?)
    }
}

pub struct CachedFetcher {
    track_cache: RwLock<BTreeMap<String, Bucket<String, Image>>>,
    channel_cache: RwLock<BTreeMap<String, Image>>,
    backend_fetcher: Arc<dyn Fetcher + Sync + Send>,
    bucket_capacity: usize,
}

impl CachedFetcher {
    pub(crate) fn new(
        backend_fetcher: Arc<dyn Fetcher + Sync + Send>,
        bucket_capacity: usize,
    ) -> Self {
        let track_cache = RwLock::new(BTreeMap::new());
        let channel_cache = RwLock::new(BTreeMap::new());

        Self {
            track_cache,
            channel_cache,
            backend_fetcher,
            bucket_capacity,
        }
    }
}

#[async_trait::async_trait]
impl Fetcher for CachedFetcher {
    async fn fetch_track_image(&self, feed_id: &str, image_url: &str) -> anyhow::Result<Image> {
        let bucket_key = image_url.to_string();

        if let Some(image) = self
            .track_cache
            .read()
            .await
            .get(feed_id)
            .and_then(|bucket| bucket.get(&bucket_key))
        {
            return Ok(image);
        }

        let image = self
            .backend_fetcher
            .fetch_track_image(feed_id, image_url)
            .await?;

        self.track_cache
            .write()
            .await
            .entry(feed_id.to_owned())
            .or_insert_with(|| Bucket::new(self.bucket_capacity))
            .push(bucket_key, image.clone());

        Ok(image)
    }

    async fn fetch_channel_image(&self, image_url: &str) -> anyhow::Result<Image> {
        if let Some(image) = self.channel_cache.read().await.get(image_url) {
            return Ok(image.clone());
        }

        let image = self.backend_fetcher.fetch_channel_image(image_url).await?;

        self.channel_cache
            .write()
            .await
            .insert(image_url.to_string(), image.clone());

        Ok(image)
    }
}

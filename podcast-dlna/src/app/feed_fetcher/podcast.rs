use std::result::Result as StdResult;
use std::sync::Arc;
use std::time::Duration;

use async_trait::async_trait;
use futures::stream::{FuturesUnordered, StreamExt};
use futures::TryStreamExt;

use super::url::Encoder as UrlEncoder;
use super::{
    Channel, ChannelId, DateTime, Error as FeedFetcherError, Image, ImageFetcher, MusicTrack,
    Result,
};

use crate::app::podcast::{Channel as ChannelSrc, ChannelFetcher, MusicTrack as MusicTrackSrc};
use crate::app::preflighter::{PreflightParams, Preflighter};

#[derive(Debug, Clone, clap::Parser)]
pub struct Config {
    /// Truncate feed items size.
    /// Value: positive integer
    #[clap(
        name = "feed-limit",
        long = "feed-limit",
        env = "FEED_LIMIT",
        default_value = "10"
    )]
    pub(crate) limit: usize,

    /// Skip not valid feed items (RSS and MP3).
    /// Possible value: true
    #[clap(
        name = "feed-skip-error",
        long = "feed-skip-error",
        env = "FEED_SKIP_ERROR"
    )]
    pub(super) skip_error: bool,
}

type ChannelUrl = String;

pub struct FeedFetcher {
    channel_id_map: Vec<(ChannelId, ChannelUrl)>,
    config: Config,
    channel_fetcher: Arc<dyn ChannelFetcher + Sync + Send>,
    preflighter: Arc<dyn Preflighter + Sync + Send>,
    url_encoder: Arc<dyn UrlEncoder + Sync + Send>,
    image_fetcher: Arc<dyn ImageFetcher + Sync + Send>,
}

impl FeedFetcher {
    pub(crate) fn new(
        feed_urls: Vec<String>,
        config: Config,
        channel_fetcher: Arc<dyn ChannelFetcher + Sync + Send>,
        preflighter: Arc<dyn Preflighter + Sync + Send>,
        url_encoder: Arc<dyn UrlEncoder + Sync + Send>,
        image_fetcher: Arc<dyn ImageFetcher + Sync + Send>,
    ) -> Self {
        let channel_id_map = feed_urls
            .into_iter()
            .map(|url| (url_encoder.encode_url(&url), url))
            .collect();

        Self {
            channel_id_map,
            config,
            channel_fetcher,
            preflighter,
            url_encoder,
            image_fetcher,
        }
    }

    async fn fetch_track_image(&self, channel_url: &str, track: &MusicTrackSrc) -> Option<Image> {
        let image_url = track.image_url.as_ref()?;

        match self
            .image_fetcher
            .fetch_track_image(channel_url, image_url)
            .await
        {
            Ok(image) => Some(image),
            Err(err) => {
                warn!(
                    "Fetch channel {channel_url} track {track_url} image {image_url} error: {err}",
                    channel_url = channel_url,
                    track_url = track.url,
                    image_url = image_url,
                    err = err
                );

                None
            }
        }
    }

    async fn preflight_track(
        &self,
        feed_url: String,
        track: &MusicTrackSrc,
    ) -> anyhow::Result<(Option<Duration>, Option<Image>, usize)> {
        let response = self
            .preflighter
            .perform(PreflightParams {
                feed_url,
                url: track.url.clone(),
                parse_body: true,
            })
            .await?;

        let image = response.attached_picture.map(move |picture| {
            let image_data = response.body_prefix.slice(picture.data_range);
            Image::new(image_data, Some(picture.mime_type))
        });

        Ok((response.duration, image, response.content_length))
    }

    // If feed not have duration, then prefetch track headers and parse duration
    async fn parse_track(
        &self,
        feed_url: String,
        track: &MusicTrackSrc,
    ) -> StdResult<MusicTrack, anyhow::Error> {
        let rss_image = self.fetch_track_image(&feed_url, track).await;

        let (duration, image, size) =
            if track.duration.is_some() && track.size.is_some() && rss_image.is_some() {
                (track.duration, rss_image, track.size)
            } else {
                self.preflight_track(feed_url.clone(), track)
                    .await
                    // RSS image preferred over MP3 image
                    .map(|(duration, image, size)| (duration, rss_image.or(image), Some(size)))?
            };

        let id = self.url_encoder.encode_url(track.url.as_str());

        Ok(MusicTrack {
            id,
            title: track.title.clone(),
            url: track.url.clone(),
            content_type: track.content_type.clone(),
            size,
            date: track.date,
            creators: track.creators.clone(),
            description: track.description.clone(),
            duration,
            image,
        })
    }

    fn filter_result<T>(&self, item_result: &StdResult<T, anyhow::Error>) -> bool {
        if !self.config.skip_error {
            // Not skip all items
            return true;
        }

        match item_result {
            Ok(_) => true,
            Err(err) => {
                warn!("Skip track error: {}", err);
                false
            }
        }
    }

    fn detect_channel_update_time(channel_src: &ChannelSrc) -> Option<DateTime> {
        channel_src
            .tracks
            .iter()
            .filter_map(MusicTrackSrc::date)
            .max()
    }

    async fn fetch_channel_image(&self, channel_src: &ChannelSrc) -> Option<Image> {
        let image_url = channel_src.image_url.as_ref()?;

        match self.image_fetcher.fetch_channel_image(image_url).await {
            Ok(image) => Some(image),
            Err(err) => {
                warn!(
                    "Fetch channel {channel_url} image {image_url} error: {err}",
                    channel_url = channel_src.url,
                    image_url = image_url,
                    err = err,
                );

                None
            }
        }
    }

    async fn build_channel(&self, channel_src: ChannelSrc, id: String) -> anyhow::Result<Channel> {
        let update_time = Self::detect_channel_update_time(&channel_src);

        // TODO: may return invalid value when channel contain track with invalid duration.
        let tracks_count = channel_src.tracks.len().min(self.config.limit);

        let image = self.fetch_channel_image(&channel_src).await;

        Ok(Channel {
            id,
            update_time,
            tracks_count,
            image,
            url: channel_src.url,
            title: channel_src.title,
        })
    }

    fn feed_url_by_id(&self, target_id: &str) -> Result<ChannelUrl> {
        self.channel_id_map
            .iter()
            .find_map(|(id, url)| {
                if id == target_id {
                    Some(url.clone())
                } else {
                    None
                }
            })
            .ok_or(FeedFetcherError::ChannelNotFound)
    }

    async fn fetch_channel(&self, feed_url: String) -> anyhow::Result<ChannelSrc> {
        let channel_src = self.channel_fetcher.fetch(feed_url.clone()).await?;
        Ok(channel_src)
    }

    async fn fetch_channel_src_list(&self) -> anyhow::Result<Vec<(ChannelSrc, ChannelId)>> {
        use futures::future::ok;
        use futures::TryFutureExt;

        let channel_futures = self.channel_id_map.iter().map(|(id, feed_url)| {
            self.fetch_channel(feed_url.clone())
                .and_then(move |channel| ok((channel, id.clone())))
        });

        futures::future::join_all(channel_futures)
            .await
            .into_iter()
            .collect()
    }
}

#[async_trait]
impl super::FeedFetcher for FeedFetcher {
    async fn fetch_channels(&self) -> Result<Vec<Channel>> {
        let channel_src_list = self.fetch_channel_src_list().await?;

        let channel_stream = channel_src_list
            .into_iter()
            .map(|(channel_src, id)| self.build_channel(channel_src, id))
            .collect::<FuturesUnordered<_>>();

        Ok(channel_stream.try_collect().await?)
    }

    // TODO: Not return channel. Return album into MusicTrack.
    async fn fetch_tracks(&self, channel_id: &str) -> Result<(Channel, Vec<MusicTrack>)> {
        use futures::future::ready;

        let feed_url = self.feed_url_by_id(channel_id)?;
        let channel_src = self.fetch_channel(feed_url.clone()).await?;

        let track_stream = channel_src
            .tracks
            .iter()
            .take(self.config.limit)
            .map(|track| self.parse_track(feed_url.clone(), track))
            .collect::<FuturesUnordered<_>>();

        let tracks: Vec<MusicTrack> = track_stream
            .filter(|track| ready(self.filter_result(track)))
            .try_collect()
            .await?;

        let channel = self
            .build_channel(channel_src, channel_id.to_owned())
            .await?;

        Ok((channel, tracks))
    }

    async fn fetch_track(&self, channel_id: &str, track_id: &str) -> Result<(Channel, MusicTrack)> {
        let feed_url = self.feed_url_by_id(channel_id)?;
        let channel_src = self.fetch_channel(feed_url.clone()).await?;

        let url_encoder = self.url_encoder.clone();

        let track = channel_src
            .tracks
            .iter()
            .find(move |track| url_encoder.encode_url(&track.url) == track_id)
            .ok_or(FeedFetcherError::TrackNotFound)?;

        let track = self.parse_track(feed_url.clone(), track).await?;

        let channel = self
            .build_channel(channel_src, channel_id.to_owned())
            .await?;

        Ok((channel, track))
    }
}

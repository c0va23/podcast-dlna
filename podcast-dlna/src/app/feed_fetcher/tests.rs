#![allow(clippy::too_many_lines)]

use std::ops::Range;
use std::sync::Arc;
use std::time::Duration;

use audio_metadata::mp3::id3v2::{AttachedPicture, PictureType, TextEncoding};
use bytes::Bytes;
use mockall::predicate;
use pretty_assertions::assert_eq;

use super::{
    Channel, FeedFetcher, Image, MockUrlEncoder, MusicTrack, PodcastConfig, PodcastFetcher,
};
use crate::app::podcast::{Channel as ChannelSrc, MockChannelFetcher, MusicTrack as MusicTrackSrc};
use crate::app::preflighter::{
    Error as PreflightError, ErrorKind as PreflightErrorKind, MockPreflighter, PreflightParams,
    PreflightResponse,
};

const FIRST_FEED_URL: &str = "https://podcast-one.com/rss.xml";
const FIRST_FEED_ID: &str = "feed1";
const FIRST_FEED_TITLE: &str = "feed one";

const SECOND_FEED_URL: &str = "https://podcast-two.com/rss.xml";
const SECOND_FEED_ID: &str = "feed2";
const SECOND_FEED_TITLE: &str = "feed two";

const FIRST_TRACK_URL: &str = "https://podcast-one.com/track1.mp3";
const FIRST_TRACK_ID: &str = "track1";
const FIRST_TRACK_TITLE: &str = "track-one";
const FIRST_TRACK_BODY_PREFIX: &[u8] = b"Track1MP3Header: JPG apic1";
const FIRST_TRACK_SIZE: usize = 1024;

const SECOND_TRACK_URL: &str = "https://podcast-one.com/track2.mp3";
const SECOND_TRACK_ID: &str = "track2";
const SECOND_TRACK_TITLE: &str = "track-two";
const SECOND_TRACK_BODY_PREFIX: &[u8] = b"Track2MP3Header: PNG apic2";
const SECOND_TRACK_SIZE: usize = 2048;

const FIRST_IMAGE_URL: &str = "https://podcast-one.com/track1.jpeg";
const FIRST_IMAGE_MIME: &str = "image/jpeg";
const FIRST_IMAGE_DATA: &[u8] = b"JPG apic1";
const FIRST_IMAGE_DATA_RANGE: Range<usize> = 17..26;

const SECOND_IMAGE_URL: &str = "https://podcast-two.com/track2.png";
const SECOND_IMAGE_MIME: &str = "image/png";
const SECOND_IMAGE_DATA: &[u8] = b"PNG apic2";
const SECOND_IMAGE_DATA_RANGE: Range<usize> = 17..26;

type TrackFetcherResult = anyhow::Result<ChannelSrc>;
type TrackFetcherCall = (String, TrackFetcherResult);

fn build_track_fetcher_mock(calls: Vec<TrackFetcherCall>) -> Arc<MockChannelFetcher> {
    let mut track_fetcher = MockChannelFetcher::new();

    for (feed_url, channel_src_result) in calls {
        track_fetcher
            .expect_fetch()
            .with(predicate::eq(feed_url))
            .times(1)
            .return_once(move |_| channel_src_result);
    }

    Arc::new(track_fetcher)
}

type UrlEncodeCall = (&'static str, String);

#[test]
fn assert_constants() {
    assert_eq!(
        FIRST_IMAGE_DATA, &FIRST_TRACK_BODY_PREFIX[FIRST_IMAGE_DATA_RANGE],
        "first",
    );
    assert_eq!(
        SECOND_IMAGE_DATA, &SECOND_TRACK_BODY_PREFIX[SECOND_IMAGE_DATA_RANGE],
        "second",
    );
}

fn build_url_encoder_mock(calls: Vec<UrlEncodeCall>) -> Arc<MockUrlEncoder> {
    let mut url_encoder = MockUrlEncoder::new();

    for (url, id) in calls {
        url_encoder
            .expect_encode_url()
            .with(predicate::eq(url))
            .times(1)
            .returning(move |_| id.to_string());
    }

    Arc::new(url_encoder)
}

struct PreflighterCall {
    feed_url: &'static str,
    track_url: &'static str,
    body_prefix: &'static [u8],
    duration_result: Result<Option<Duration>, PreflightError>,
    attached_picture: Option<AttachedPicture>,
    content_length: usize,
}

fn build_preflighter_mock(calls: Vec<PreflighterCall>) -> Arc<MockPreflighter> {
    let mut preflighter = MockPreflighter::new();

    for PreflighterCall {
        feed_url,
        track_url,
        body_prefix,
        duration_result,
        attached_picture,
        content_length,
    } in calls
    {
        preflighter
            .expect_perform()
            .with(predicate::eq(PreflightParams {
                feed_url: feed_url.to_owned(),
                url: track_url.to_owned(),
                parse_body: true,
            }))
            .times(1)
            .return_once(move |_| match duration_result {
                Ok(duration) => Ok(PreflightResponse {
                    duration,
                    version: hyper::Version::HTTP_11,
                    status: http::StatusCode::OK,
                    headers: http::HeaderMap::new(),
                    content_length,
                    url: track_url.to_owned(),
                    body_prefix: Bytes::from_static(body_prefix),
                    attached_picture,
                }),
                Err(err) => Err(err),
            });
    }

    Arc::new(preflighter)
}

fn build_feed_fetcher_error() -> anyhow::Error {
    anyhow::anyhow!("invalid")
}

struct ImageFetcherCall {
    feed_url: &'static str,
    image_url: &'static str,
    result: anyhow::Result<Image>,
}

fn build_image_fetcher_mock(calls: Vec<ImageFetcherCall>) -> Arc<super::image::MockFetcher> {
    let mut fetcher_mock = super::image::MockFetcher::new();

    for ImageFetcherCall {
        feed_url,
        image_url,
        result,
    } in calls
    {
        fetcher_mock
            .expect_fetch_track_image()
            .with(predicate::eq(feed_url), predicate::eq(image_url))
            .times(1)
            .return_once(move |_, _| result);
    }

    Arc::new(fetcher_mock)
}

#[tokio::test]
async fn test_podcast_fetch_channels() {
    struct TestCase {
        name: &'static str,
        feed_urls: Vec<String>,
        track_fetcher_calls: Vec<TrackFetcherCall>,
        url_encode_calls: Vec<UrlEncodeCall>,
        config: PodcastConfig,
        expected_result: Option<Vec<Channel>>,
    }

    let first_channel_src = ChannelSrc {
        url: FIRST_FEED_URL.to_string(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };
    let second_channel_src = ChannelSrc {
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };

    let first_channel = Channel {
        id: FIRST_FEED_ID.to_owned(),
        url: FIRST_FEED_URL.to_owned(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };
    let second_channel = Channel {
        id: SECOND_FEED_ID.to_owned(),
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };

    let test_cases = vec![
        TestCase {
            name: "without feed urls",
            feed_urls: vec![],
            track_fetcher_calls: vec![],
            url_encode_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: true,
            },
            expected_result: Some(vec![]),
        },
        TestCase {
            name: "with one valid feed url",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(FIRST_FEED_URL.to_owned(), Ok(first_channel_src.clone()))],
            url_encode_calls: vec![(FIRST_FEED_URL, FIRST_FEED_ID.to_owned())],
            config: PodcastConfig {
                limit: 2,
                skip_error: true,
            },
            expected_result: Some(vec![first_channel.clone()]),
        },
        TestCase {
            name: "with other valid feed url",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(SECOND_FEED_URL.to_owned(), Ok(second_channel_src.clone()))],
            url_encode_calls: vec![(SECOND_FEED_URL, SECOND_FEED_ID.to_owned())],
            config: PodcastConfig {
                limit: 2,
                skip_error: true,
            },
            expected_result: Some(vec![second_channel.clone()]),
        },
        TestCase {
            name: "with two valid feed url",
            feed_urls: vec![FIRST_FEED_URL.to_owned(), SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![
                (FIRST_FEED_URL.to_owned(), Ok(first_channel_src.clone())),
                (SECOND_FEED_URL.to_owned(), Ok(second_channel_src.clone())),
            ],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
            ],
            config: PodcastConfig {
                limit: 2,
                skip_error: true,
            },
            expected_result: Some(vec![first_channel.clone(), second_channel.clone()]),
        },
        TestCase {
            name: "with one invalid feed url and without skip error",
            feed_urls: vec![FIRST_FEED_URL.to_owned(), SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![
                (FIRST_FEED_URL.to_owned(), Ok(first_channel_src.clone())),
                (SECOND_FEED_URL.to_owned(), Err(build_feed_fetcher_error())),
            ],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
            ],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            expected_result: None,
        },
    ];

    for TestCase {
        name,
        feed_urls,
        track_fetcher_calls,
        url_encode_calls,
        config,
        expected_result,
    } in test_cases
    {
        let track_fetcher = build_track_fetcher_mock(track_fetcher_calls);
        let preflighter = build_preflighter_mock(vec![]);
        let url_encoder = build_url_encoder_mock(url_encode_calls);
        let image_fetcher = build_image_fetcher_mock(vec![]);
        let feed_fetcher = PodcastFetcher::new(
            feed_urls,
            config,
            track_fetcher,
            preflighter,
            url_encoder,
            image_fetcher,
        );

        let channels = feed_fetcher.fetch_channels().await;

        assert_eq!(
            expected_result,
            channels.ok().map(|mut channels| {
                channels.sort_by_key(|channel| channel.id.clone());
                channels
            }),
            "test '{}' failed",
            name,
        );
    }
}

#[tokio::test]
async fn test_podcast_fetch_tracks() {
    struct TestCase {
        name: &'static str,
        feed_urls: Vec<String>,
        track_fetcher_calls: Vec<TrackFetcherCall>,
        url_encode_calls: Vec<UrlEncodeCall>,
        preflighter_calls: Vec<PreflighterCall>,
        image_fetcher_calls: Vec<ImageFetcherCall>,
        config: PodcastConfig,
        channel_id: &'static str,
        expected_result: Option<(Channel, Vec<MusicTrack>)>,
    }

    let first_image = Image::new(
        Bytes::from_static(FIRST_TRACK_BODY_PREFIX).slice(FIRST_IMAGE_DATA_RANGE),
        Some(FIRST_IMAGE_MIME.into()),
    );

    let second_image = Image::new(
        Bytes::from_static(SECOND_TRACK_BODY_PREFIX).slice(SECOND_IMAGE_DATA_RANGE),
        Some(SECOND_IMAGE_MIME.into()),
    );

    let first_attached_picture = AttachedPicture {
        description: "".to_owned(),
        mime_type: FIRST_IMAGE_MIME.into(),
        picture_type: PictureType::CoverFront,
        text_encoding: TextEncoding::Utf8,
        data_range: FIRST_IMAGE_DATA_RANGE,
    };

    let second_attached_picture = AttachedPicture {
        description: "".to_owned(),
        mime_type: SECOND_IMAGE_MIME.into(),
        picture_type: PictureType::CoverFront,
        text_encoding: TextEncoding::Utf8,
        data_range: SECOND_IMAGE_DATA_RANGE,
    };

    let first_channel_src = ChannelSrc {
        url: FIRST_FEED_URL.to_string(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };

    let second_channel_src = ChannelSrc {
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };

    let first_channel = Channel {
        id: FIRST_FEED_ID.to_owned(),
        url: FIRST_FEED_URL.to_owned(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };
    let second_channel = Channel {
        id: SECOND_FEED_ID.to_owned(),
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };

    let first_track_src = MusicTrackSrc {
        title: FIRST_TRACK_TITLE.to_owned(),
        url: FIRST_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(FIRST_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image_url: Some(FIRST_IMAGE_URL.to_owned()),
        description: None,
    };

    let first_track = MusicTrack {
        id: FIRST_TRACK_ID.to_owned(),
        title: FIRST_TRACK_TITLE.to_owned(),
        url: FIRST_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(FIRST_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image: Some(first_image.clone()),
        description: None,
    };

    let first_track_duration = Duration::new(2 * 3600 + 3 * 60, 0);

    let second_track_src = MusicTrackSrc {
        title: SECOND_TRACK_TITLE.to_owned(),
        url: SECOND_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(SECOND_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image_url: Some(SECOND_IMAGE_URL.to_owned()),
        description: None,
    };

    let second_track = MusicTrack {
        id: SECOND_TRACK_ID.to_owned(),
        title: SECOND_TRACK_TITLE.to_owned(),
        url: SECOND_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(SECOND_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image: Some(second_image.clone()),
        description: None,
    };

    let second_track_duration = Duration::new(3600 + 23 * 60 + 45, 0);

    let test_cases = vec![
        TestCase {
            name: "with channel and without tracks",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(FIRST_FEED_URL.to_owned(), Ok(first_channel_src.clone()))],
            preflighter_calls: vec![],
            url_encode_calls: vec![(FIRST_FEED_URL, FIRST_FEED_ID.to_owned())],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: Some((first_channel.clone(), vec![])),
        },
        TestCase {
            name: "with one channel and with one track with duration",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: Some(second_track_duration),
                        ..second_track_src.clone()
                    }],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: SECOND_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..second_channel.clone()
                },
                vec![MusicTrack {
                    duration: Some(second_track_duration),
                    ..second_track.clone()
                }],
            )),
        },
        TestCase {
            name: "with one channel and with two track and limit 1",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![
                        MusicTrackSrc {
                            duration: Some(second_track_duration),
                            ..second_track_src.clone()
                        },
                        first_track_src.clone(),
                    ],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: SECOND_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 1,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..second_channel.clone()
                },
                vec![MusicTrack {
                    duration: Some(second_track_duration),
                    ..second_track.clone()
                }],
            )),
        },
        TestCase {
            name: "with one channel and with two track and limit 2, success parse duration",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![
                        MusicTrackSrc {
                            duration: None,
                            ..first_track_src.clone()
                        },
                        MusicTrackSrc {
                            duration: Some(second_track_duration),
                            ..second_track_src.clone()
                        },
                    ],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: SECOND_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(first_track_duration)),
                attached_picture: None,
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![
                ImageFetcherCall {
                    feed_url: SECOND_FEED_URL,
                    image_url: FIRST_IMAGE_URL,
                    result: Ok(first_image.clone()),
                },
                ImageFetcherCall {
                    feed_url: SECOND_FEED_URL,
                    image_url: SECOND_IMAGE_URL,
                    result: Ok(second_image.clone()),
                },
            ],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 2,
                    ..second_channel.clone()
                },
                vec![
                    MusicTrack {
                        duration: Some(first_track_duration),
                        ..first_track.clone()
                    },
                    MusicTrack {
                        duration: Some(second_track_duration),
                        ..second_track.clone()
                    },
                ],
            )),
        },
        TestCase {
            name: "with first channel and with two track and limit 2, success parse duration",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![
                        MusicTrackSrc {
                            duration: Some(first_track_duration),
                            ..first_track_src.clone()
                        },
                        MusicTrackSrc {
                            duration: None,
                            ..second_track_src.clone()
                        },
                    ],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: SECOND_TRACK_URL,
                body_prefix: SECOND_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(second_track_duration)),
                attached_picture: None,
                content_length: SECOND_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![
                ImageFetcherCall {
                    feed_url: FIRST_FEED_URL,
                    image_url: FIRST_IMAGE_URL,
                    result: Ok(first_image.clone()),
                },
                ImageFetcherCall {
                    feed_url: FIRST_FEED_URL,
                    image_url: SECOND_IMAGE_URL,
                    result: Ok(second_image.clone()),
                },
            ],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 2,
                    ..first_channel.clone()
                },
                vec![
                    MusicTrack {
                        duration: Some(first_track_duration),
                        ..first_track.clone()
                    },
                    MusicTrack {
                        duration: Some(second_track_duration),
                        ..second_track.clone()
                    },
                ],
            )),
        },
        TestCase {
            name: "rss track have image, but mp3 have another image",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(first_track_duration)),
                attached_picture: Some(second_attached_picture.clone()),
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: FIRST_IMAGE_URL,
                result: Ok(first_image.clone()),
            }],
            config: PodcastConfig {
                limit: 1,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                vec![MusicTrack {
                    duration: Some(first_track_duration),
                    image: Some(first_image.clone()),
                    ..first_track.clone()
                }],
            )),
        },
        TestCase {
            name: "rss track not have image, but mp3 have image",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        image_url: None,
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(first_track_duration)),
                attached_picture: Some(first_attached_picture.clone()),
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 1,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                vec![MusicTrack {
                    duration: Some(first_track_duration),
                    image: Some(first_image.clone()),
                    ..first_track.clone()
                }],
            )),
        },
        TestCase {
            name: "rss track have image url, but it return error",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(first_track_duration)),
                attached_picture: Some(first_attached_picture),
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: FIRST_IMAGE_URL,
                result: Err(anyhow::anyhow!("fetch image error")),
            }],
            config: PodcastConfig {
                limit: 1,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                vec![MusicTrack {
                    duration: Some(first_track_duration),
                    image: Some(first_image),
                    ..first_track.clone()
                }],
            )),
        },
        TestCase {
            name: "channel not found",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![],
            preflighter_calls: vec![],
            url_encode_calls: vec![(FIRST_FEED_URL, FIRST_FEED_ID.to_owned())],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            expected_result: None,
        },
        TestCase {
            name: "parse duration error",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..second_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: SECOND_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Err(PreflightError {
                    url: SECOND_TRACK_URL.to_owned(),
                    kind: PreflightErrorKind::NoDuration,
                }),
                attached_picture: None,
                content_length: SECOND_TRACK_SIZE,
            }],
            url_encode_calls: vec![(FIRST_FEED_URL, FIRST_FEED_ID.to_owned())],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 1,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            expected_result: None,
        },
    ];

    for TestCase {
        name,
        feed_urls,
        track_fetcher_calls,
        preflighter_calls,
        url_encode_calls,
        image_fetcher_calls,
        config,
        channel_id,
        expected_result,
    } in test_cases
    {
        println!("Test case: {}", name);

        let track_fetcher = build_track_fetcher_mock(track_fetcher_calls);
        let preflighter = build_preflighter_mock(preflighter_calls);
        let url_encoder = build_url_encoder_mock(url_encode_calls);
        let image_fetcher = build_image_fetcher_mock(image_fetcher_calls);
        let feed_fetcher = PodcastFetcher::new(
            feed_urls,
            config,
            track_fetcher,
            preflighter,
            url_encoder,
            image_fetcher,
        );

        let result = feed_fetcher.fetch_tracks(channel_id).await;

        assert_eq!(expected_result, result.ok(), "test '{}' failed", name);
    }
}

#[tokio::test]
async fn test_podcast_fetch_track() {
    struct TestCase {
        name: &'static str,
        feed_urls: Vec<String>,
        track_fetcher_calls: Vec<TrackFetcherCall>,
        url_encode_calls: Vec<UrlEncodeCall>,
        preflighter_calls: Vec<PreflighterCall>,
        image_fetcher_calls: Vec<ImageFetcherCall>,
        config: PodcastConfig,
        channel_id: &'static str,
        track_id: &'static str,
        expected_result: Option<(Channel, MusicTrack)>,
    }

    let first_image = Image::new(Bytes::from(FIRST_IMAGE_DATA), Some(FIRST_IMAGE_MIME.into()));

    let second_image = Image::new(
        Bytes::from(SECOND_IMAGE_DATA),
        Some(SECOND_IMAGE_MIME.into()),
    );

    let first_channel_src = ChannelSrc {
        url: FIRST_FEED_URL.to_string(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };

    let second_channel_src = ChannelSrc {
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks: vec![],
        image_url: None,
    };

    let first_channel = Channel {
        id: FIRST_FEED_ID.to_owned(),
        url: FIRST_FEED_URL.to_owned(),
        title: FIRST_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };
    let second_channel = Channel {
        id: SECOND_FEED_ID.to_owned(),
        url: SECOND_FEED_URL.to_owned(),
        title: SECOND_FEED_TITLE.to_owned(),
        tracks_count: 0,
        update_time: None,
        image: None,
    };

    let first_track_src = MusicTrackSrc {
        title: FIRST_TRACK_TITLE.to_owned(),
        url: FIRST_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(FIRST_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image_url: Some(FIRST_IMAGE_URL.to_owned()),
        description: None,
    };

    let first_track = MusicTrack {
        id: FIRST_TRACK_ID.to_owned(),
        title: FIRST_TRACK_TITLE.to_owned(),
        url: FIRST_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(FIRST_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image: Some(first_image.clone()),
        description: None,
    };

    let first_track_duration = Duration::new(2 * 3600 + 3 * 60, 0);

    let second_track_src = MusicTrackSrc {
        title: SECOND_TRACK_TITLE.to_owned(),
        url: SECOND_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(SECOND_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image_url: Some(SECOND_IMAGE_URL.to_owned()),
        description: None,
    };

    let second_track = MusicTrack {
        id: SECOND_TRACK_ID.to_owned(),
        title: SECOND_TRACK_TITLE.to_owned(),
        url: SECOND_TRACK_URL.to_owned(),
        content_type: "audio/mp3".to_owned(),
        size: Some(SECOND_TRACK_SIZE),
        date: None,
        creators: None,
        duration: None,
        image: Some(second_image.clone()),
        description: None,
    };

    let second_track_duration = Duration::new(3600 + 23 * 60 + 45, 0);

    let test_cases = vec![
        TestCase {
            name: "with first channel and with first track with duration",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: Some(first_track_duration),
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                // Called twice into find and into build_track
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: FIRST_IMAGE_URL,
                result: Ok(first_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            track_id: FIRST_TRACK_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                MusicTrack {
                    duration: Some(first_track_duration),
                    ..first_track.clone()
                },
            )),
        },
        TestCase {
            name: "with first channel and first without size",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        size: None,
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(None),
                attached_picture: None,
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                // Called twice into find and into build_track
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: FIRST_IMAGE_URL,
                result: Ok(first_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            track_id: FIRST_TRACK_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                first_track.clone(),
            )),
        },
        TestCase {
            name: "with second channel and with second track with duration",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: Some(second_track_duration),
                        ..second_track_src.clone()
                    }],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                // Called twice by find and parse
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: SECOND_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            track_id: SECOND_TRACK_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..second_channel.clone()
                },
                MusicTrack {
                    duration: Some(second_track_duration),
                    ..second_track.clone()
                },
            )),
        },
        TestCase {
            name: "with first channel and with first track without duration",
            feed_urls: vec![FIRST_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                FIRST_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..first_track_src.clone()
                    }],
                    ..first_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: FIRST_FEED_URL,
                track_url: FIRST_TRACK_URL,
                body_prefix: FIRST_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(first_track_duration)),
                attached_picture: None,
                content_length: FIRST_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (FIRST_FEED_URL, FIRST_FEED_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
                (FIRST_TRACK_URL, FIRST_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: FIRST_FEED_URL,
                image_url: FIRST_IMAGE_URL,
                result: Ok(first_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            track_id: FIRST_TRACK_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..first_channel.clone()
                },
                MusicTrack {
                    duration: Some(first_track_duration),
                    ..first_track.clone()
                },
            )),
        },
        TestCase {
            name: "with second channel and with second track without duration",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..second_track_src.clone()
                    }],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: SECOND_FEED_URL,
                track_url: SECOND_TRACK_URL,
                body_prefix: SECOND_TRACK_BODY_PREFIX,
                duration_result: Ok(Some(second_track_duration)),
                attached_picture: None,
                content_length: SECOND_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                // TODO: fix double call of url encoder
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: SECOND_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            track_id: SECOND_TRACK_ID,
            expected_result: Some((
                Channel {
                    tracks_count: 1,
                    ..second_channel.clone()
                },
                MusicTrack {
                    duration: Some(second_track_duration),
                    ..second_track.clone()
                },
            )),
        },
        TestCase {
            name: "not found track",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..second_track_src.clone()
                    }],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            track_id: FIRST_TRACK_ID,
            expected_result: None,
        },
        TestCase {
            name: "not found channel",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![],
            preflighter_calls: vec![],
            url_encode_calls: vec![(SECOND_FEED_URL, SECOND_FEED_ID.to_owned())],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: FIRST_FEED_ID,
            track_id: FIRST_TRACK_ID,
            expected_result: None,
        },
        TestCase {
            name: "preflight error",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Ok(ChannelSrc {
                    tracks: vec![MusicTrackSrc {
                        duration: None,
                        ..second_track_src.clone()
                    }],
                    ..second_channel_src.clone()
                }),
            )],
            preflighter_calls: vec![PreflighterCall {
                feed_url: SECOND_FEED_URL,
                track_url: SECOND_TRACK_URL,
                body_prefix: SECOND_TRACK_BODY_PREFIX,
                duration_result: Err(PreflightError {
                    url: SECOND_TRACK_URL.to_owned(),
                    kind: PreflightErrorKind::NoDuration,
                }),
                attached_picture: None,
                content_length: SECOND_TRACK_SIZE,
            }],
            url_encode_calls: vec![
                (SECOND_FEED_URL, SECOND_FEED_ID.to_owned()),
                (SECOND_TRACK_URL, SECOND_TRACK_ID.to_owned()),
            ],
            image_fetcher_calls: vec![ImageFetcherCall {
                feed_url: SECOND_FEED_URL,
                image_url: SECOND_IMAGE_URL,
                result: Ok(second_image.clone()),
            }],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            track_id: SECOND_TRACK_ID,
            expected_result: None,
        },
        TestCase {
            name: "feed fetcher error",
            feed_urls: vec![SECOND_FEED_URL.to_owned()],
            track_fetcher_calls: vec![(
                SECOND_FEED_URL.to_owned(),
                Err(build_feed_fetcher_error()),
            )],
            preflighter_calls: vec![],
            url_encode_calls: vec![(SECOND_FEED_URL, SECOND_FEED_ID.to_owned())],
            image_fetcher_calls: vec![],
            config: PodcastConfig {
                limit: 2,
                skip_error: false,
            },
            channel_id: SECOND_FEED_ID,
            track_id: SECOND_TRACK_ID,
            expected_result: None,
        },
    ];

    for TestCase {
        name,
        feed_urls,
        track_fetcher_calls,
        preflighter_calls,
        url_encode_calls,
        image_fetcher_calls,
        config,
        channel_id,
        track_id,
        expected_result,
    } in test_cases
    {
        println!("Test case: {}", name);

        let track_fetcher = build_track_fetcher_mock(track_fetcher_calls);
        let preflighter = build_preflighter_mock(preflighter_calls);
        let url_encoder = build_url_encoder_mock(url_encode_calls);
        let image_fetcher = build_image_fetcher_mock(image_fetcher_calls);
        let feed_fetcher = PodcastFetcher::new(
            feed_urls,
            config,
            track_fetcher,
            preflighter,
            url_encoder,
            image_fetcher,
        );

        let result = feed_fetcher.fetch_track(channel_id, track_id).await;

        assert_eq!(expected_result, result.ok(), "test '{}' failed", name);
    }
}

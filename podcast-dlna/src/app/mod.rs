// imports

// sub modules

mod cli;
mod config;
mod content_directory;
mod data_stream;
mod feed_fetcher;
mod handlers;
mod podcast;
mod preflighter;
mod proxy;
mod ssdp_data;
mod title_rewriter;

// reexport

pub use cli::App;

pub(self) use data_stream::DataStreamer;
pub(self) use feed_fetcher::{Error as FeedFetcherError, FeedFetcher, Image};
pub(self) use preflighter::{
    Error as PreflightError, PreflightParams, PreflightResponse, Preflighter,
};

#[cfg(test)]
pub(self) use feed_fetcher::{Channel, MockFeedFetcher, MusicTrack};
#[cfg(test)]
pub(self) use preflighter::{ErrorKind as PreflightErrorKind, MockPreflighter};

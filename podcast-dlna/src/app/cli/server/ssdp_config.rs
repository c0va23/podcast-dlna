use std::net::IpAddr;
use std::time::Duration;

use crate::utils::parse_iso8601_duration;

#[derive(Debug, clap::Parser, Clone)]
#[clap()]
pub struct NotifierConfig {
    /// SSDP notification message TTL.
    #[clap(
        env = "SSDP_NOTIFIER_TTL",
        long = "ssdp-notifier-ttl",
        default_value = "PT1M",
        parse(try_from_str = parse_iso8601_duration),
    )]
    ttl: Duration,

    /// Used to prevent the expiration of MTSP notifications.
    /// Notifications will be sent at intervals equal to TTL - TTL_OVERLAY.
    #[clap(
        env = "SSDP_NOTIFIER_TTL_OVERLAY",
        long = "ssdp-notifier-ttl-overlay",
        default_value = "PT3S",
        parse(try_from_str = parse_iso8601_duration)
    )]
    ttl_overlay: Duration,

    /// SSDP notifier binding address.
    #[clap(
        name = "ssdp-notifier-bind-addr",
        env = "SSDP_NOTIFIER_BIND_ADDR",
        long = "ssdp-notifier-bind-addr",
        default_value = "0.0.0.0"
    )]
    bind_addr: IpAddr,

    /// SSDP notifier binding port.
    /// 0 - random port.
    #[clap(
        name = "ssdp-notifier-bind-port",
        env = "SSDP_NOTIFIER_BIND_PORT",
        long = "ssdp-notifier-bind-port",
        default_value = "0"
    )]
    bind_port: u16,

    /// SSDP listener UDP multicast TTL.
    #[clap(
        name = "ssdp-notifier-udp-multicast-ttl",
        long = "ssdp-notifier-udp-multicast-ttl",
        env = "SSDP_NOTIFIER_UDP_MULTICAST_TTL",
        default_value = "3"
    )]
    multicast_ttl: u32,

    /// SSDP listener UDP multicast loop.
    #[clap(
        name = "ssdp-notifier-udp-multicast-loop",
        long = "ssdp-notifier-udp-multicast-loop",
        env = "SSDP_NOTIFIER_UDP_MULTICAST_LOOP"
    )]
    multicast_loop: bool,
}

impl From<NotifierConfig> for ssdp::NotifierConfig {
    fn from(config: NotifierConfig) -> Self {
        Self {
            ttl: config.ttl,
            ttl_overlay: config.ttl_overlay,
            bind_addr: config.bind_addr,
            bind_port: config.bind_port,
            multicast_ttl: config.multicast_ttl,
            multicast_loop: config.multicast_loop,
        }
    }
}

#[derive(Debug, clap::Parser, Clone)]
#[clap()]
pub struct ListenerConfig {
    /// SSDP listener bind address (IP).
    /// Currently only IPv4 supported.
    #[clap(
        name = "ssdp-listener-bind-addr",
        env = "SSDP_LISTENER_BIND_ADDR",
        long = "ssdp-listener-bind-addr",
        default_value = "0.0.0.0"
    )]
    bind_addr: IpAddr,

    /// SSDP listener response TTL.
    /// Currently only IPv4 supported.
    #[clap(
        env = "SSDP_LISTENER_RESPONSE_TTL",
        long = "ssdp-listener-response-ttl",
        default_value = "PT1M",
        parse(try_from_str = parse_iso8601_duration),
    )]
    response_ttl: Duration,

    /// SSDP listener UDP multicast TTL.
    #[clap(
        name = "ssdp-listener-udp-multicast-ttl",
        long = "ssdp-listener-udp-multicast-ttl",
        env = "SSDP_LISTENER_UDP_MULTICAST_TTL",
        default_value = "3"
    )]
    multicast_ttl: u32,

    /// SSDP listener UDP multicast loop.
    #[clap(
        name = "ssdp-listener-udp-multicast-loop",
        long = "ssdp-listener-udp-multicast-loop",
        env = "SSDP_LISTENER_UDP_MULTICAST_LOOP"
    )]
    multicast_loop: bool,

    /// Print received SSDP notify messages
    #[clap(
        name = "ssdp-listener-debug-notify",
        env = "SSDP_LISTENER_DEBUG_NOTIFY",
        long = "ssdp-listener-debug-notify"
    )]
    debug_notify: bool,

    /// SSDP listener max message size
    #[clap(
        name = "ssdp-listener-max-message-size",
        env = "SSDP_LISTENER_MAX_MESSAGE_SIZE",
        long = "ssdp-listener-max-message-size",
        default_value = "1024"
    )]
    max_message_size: usize,

    /// SSDP listener stop retry delay
    #[clap(
        name = "ssdp-listener-stop-retry-delay",
        long = "ssdp-listener-stop-retry-delay",
        env = "SSDP_LISTENER_STOP_RETRY_DELAY",
        default_value = "PT0.5S",
        parse(try_from_str = parse_iso8601_duration),
    )]
    stop_retry_delay: Duration,
}

impl From<ListenerConfig> for ssdp::ListenerConfig {
    fn from(config: ListenerConfig) -> Self {
        Self {
            bind_addr: config.bind_addr,
            response_ttl: config.response_ttl,
            multicast_ttl: config.multicast_ttl,
            multicast_loop: config.multicast_loop,
            debug_notify: config.debug_notify,
            max_message_size: config.max_message_size,
            stop_retry_delay: config.stop_retry_delay,
        }
    }
}

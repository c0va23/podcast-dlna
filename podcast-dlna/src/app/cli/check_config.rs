#[derive(clap::Parser, Debug)]
pub(super) struct CheckConfigArgs {
    /// Path to config TOML file with podcasts source definitions.
    #[clap(short, long, env, default_value = "config.toml")]
    config_path: String,
}

pub(super) fn run(args: &CheckConfigArgs) -> Result<(), anyhow::Error> {
    let _config = crate::app::config::Config::load(args.config_path.as_str())?;

    println!("Configuration is valid");

    Ok(())
}

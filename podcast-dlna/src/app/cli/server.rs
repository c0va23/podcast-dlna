use std::error::Error;
use std::sync::Arc;

use crate::app::handlers;
use crate::app::proxy;
use crate::app::{
    config, content_directory, feed_fetcher, podcast, preflighter, ssdp_data, title_rewriter,
};
use crate::server;
use crate::utils::device_info;

mod ssdp_config;

pub(super) async fn run(args: ServerArgs) -> Result<(), Box<dyn Error>> {
    let stop_future = crate::utils::wait_signal();

    let app = Server::new(args).await?;

    app.start().await?;

    if let Err(err) = stop_future.await {
        error!("Wait signal error: {}", err);
    };

    app.stop().await?;

    Ok(())
}

/// Server command start HTTP server (Podcast proxy + UPNP API provider) and
/// SSDP (publisher and consumer)
#[derive(clap::Parser, Debug)]
pub(super) struct ServerArgs {
    /// Path to config TOML file with podcasts source definitions.
    #[clap(short, long, env, default_value = "config.toml")]
    config_path: String,

    /// Seed of url hashing salt.
    /// Can be provided for stable urls on proxy.
    #[clap(long, env)]
    url_encoder_seed: Option<u64>,

    #[clap(flatten)]
    ssdp_listener_config: ssdp_config::ListenerConfig,

    #[clap(flatten)]
    ssdp_notifier_config: ssdp_config::NotifierConfig,

    #[clap(flatten)]
    http_listener_config: server::HttpListenerConfig,

    #[clap(flatten)]
    http_server_config: server::Config,

    #[clap(flatten)]
    http_preflighter_config: preflighter::HttpConfig,

    #[clap(flatten)]
    feed_fetcher_config: feed_fetcher::PodcastConfig,

    #[clap(flatten)]
    feed_cache_config: podcast::CacheConfig,

    #[clap(flatten)]
    stream_builder_config: proxy::RemoteStreamBuilderConfig,

    #[clap(flatten)]
    continuable_requester_config: proxy::ContinuableRequesterConfig,

    #[clap(flatten)]
    device_info_config: device_info::Config,
}

pub(super) struct Server {
    http_server: server::Server,
    ssdp_listener: ssdp::Listener,
    ssdp_notifier: ssdp::Notifier,
}

impl Server {
    pub(super) async fn new(args: ServerArgs) -> Result<Self, Box<dyn Error>> {
        let http_client = reqwest::Client::new();

        let config = config::Config::load(args.config_path.as_str())?;

        let device_info_fetcher = device_info::DeviceInfo::new(args.device_info_config);

        let device_description_handler = Arc::new(handlers::DeviceDescriptionHandler::new(
            &device_info_fetcher,
        )?);

        let connection_manager_handler = Arc::new(handlers::ConnectionManagerHandler::new());

        let feed_urls = config.feed_urls();

        let channel_fetcher = build_channel_fetcher(&feed_urls, args.feed_cache_config);

        let preflighter = build_preflighter(
            &device_info_fetcher,
            &http_client,
            args.http_preflighter_config.clone(),
            args.feed_fetcher_config.limit,
        )?;

        let http_listener_builder =
            Arc::new(server::HttpListenerBuilder::new(args.http_listener_config));

        let url_encoder = Arc::new(args.url_encoder_seed.map_or_else(
            feed_fetcher::Sha2UrlEncoder::new,
            feed_fetcher::Sha2UrlEncoder::new_with_seed,
        ));

        let image_fetcher = build_image_fetcher(&http_client, args.feed_fetcher_config.limit);

        let feed_fetcher = Arc::new(feed_fetcher::PodcastFetcher::new(
            feed_urls,
            args.feed_fetcher_config,
            channel_fetcher,
            preflighter.clone(),
            url_encoder,
            image_fetcher,
        ));

        let content_directory_handler = Arc::new(build_content_directory_handler(
            &config,
            http_listener_builder.clone(),
            feed_fetcher.clone(),
        )?);

        let picture_handler = Arc::new(handlers::PicturesHandler::new(feed_fetcher.clone()));

        let icons_handler = Arc::new(handlers::IconsHandler::new());

        let reqwest_batch_requester = Arc::new(proxy::ReqwestRequester::new(http_client));

        let continuable_requester = Arc::new(proxy::ContinuableRequester::new(
            reqwest_batch_requester,
            args.continuable_requester_config,
        ));

        let batch_stream_builder = Arc::new(proxy::RemoteStreamBuilder::new(
            continuable_requester,
            args.stream_builder_config,
        ));

        let proxy = Arc::new(proxy::Proxy::new(
            preflighter,
            batch_stream_builder,
            feed_fetcher,
        ));

        let http_router = server::Router::new(
            content_directory_handler,
            device_description_handler,
            picture_handler,
            connection_manager_handler,
            icons_handler,
            proxy,
            &device_info_fetcher,
        )?;

        let http_server = server::Server::new(
            args.http_server_config.clone(),
            http_router,
            http_listener_builder.clone(),
        );

        let ssdp_data_builder = Arc::new(ssdp_data::LazySsdpDataBuilder::new(
            &device_info_fetcher,
            http_listener_builder,
        )?);

        let ssdp_listener =
            ssdp::Listener::new(args.ssdp_listener_config.into(), ssdp_data_builder.clone());

        let ssdp_notifier =
            ssdp::Notifier::new(args.ssdp_notifier_config.into(), ssdp_data_builder);

        Ok(Self {
            http_server,
            ssdp_listener,
            ssdp_notifier,
        })
    }

    async fn start(&self) -> Result<(), Box<dyn Error>> {
        info!("Starting begin");

        debug!("Start HTTP server");
        self.http_server.start().await?;

        debug!("Start SSDP listener server");
        self.ssdp_listener.start().await?;

        debug!("Start SSDP notifier server");
        self.ssdp_notifier.start().await?;

        info!("Starting finished");

        Ok(())
    }

    async fn stop(&self) -> Result<(), Box<dyn Error>> {
        info!("Stopping begin");

        debug!("Stop SSDP notifier");
        self.ssdp_notifier.stop().await;

        debug!("Stop SSDP listener");
        self.ssdp_listener.stop().await;

        debug!("Stop HTTP server");
        self.http_server.stop().await;

        info!("Stopping finished");

        Ok(())
    }
}

fn build_content_directory_handler(
    config: &config::Config,
    http_listener_builder: Arc<server::HttpListenerBuilder>,
    feed_fetcher: Arc<feed_fetcher::PodcastFetcher>,
) -> Result<handlers::ContentDirectoryHandler, Box<dyn Error>> {
    let channel_title_rewriter = Arc::new(
        title_rewriter::DefaultChannelTitleRewriter::from_config(config)?,
    );
    let content_directory_backend = Arc::new(content_directory::PodcastBackend::new(
        http_listener_builder,
        feed_fetcher,
        channel_title_rewriter,
    ));
    let content_directory =
        upnp::media_server::content_directory::ContentDirectory::new(content_directory_backend);
    let content_directory_handler = handlers::ContentDirectoryHandler::new(content_directory);
    Ok(content_directory_handler)
}

fn build_channel_fetcher(
    feed_urls: &[String],
    feed_cache_config: podcast::CacheConfig,
) -> Arc<impl podcast::ChannelFetcher> {
    let channel_fetcher = Arc::new(podcast::RssChannelFetcher::new());
    Arc::new(podcast::CachedChannelFetcher::new(
        channel_fetcher,
        feed_urls.len(),
        feed_cache_config,
    ))
}

fn build_preflighter(
    device_info_fetcher: &device_info::DeviceInfo,
    http_client: &reqwest::Client,
    http_preflighter_config: preflighter::HttpConfig,
    tracks_limit: usize,
) -> Result<Arc<impl preflighter::Preflighter>, Box<dyn Error>> {
    let http = Arc::new(preflighter::Http::new(
        http_client.clone(),
        device_info_fetcher,
        http_preflighter_config,
    )?);
    let cached = Arc::new(preflighter::Cached::new(http, tracks_limit));
    Ok(cached)
}

fn build_image_fetcher(
    http_client: &reqwest::Client,
    tracks_limit: usize,
) -> Arc<impl feed_fetcher::ImageFetcher> {
    let reqwest_image_fetcher =
        Arc::new(feed_fetcher::ReqwestImageFetcher::new(http_client.clone()));
    Arc::new(feed_fetcher::CachedImageFetcher::new(
        reqwest_image_fetcher,
        tracks_limit,
    ))
}

use std::error::Error;

mod check_config;
mod env_config;
mod server;

#[derive(clap::Parser, Debug)]
#[clap(
    author,
    about,
    version,
    long_version = env!("APP_LONG_VERSION"),
    setting = clap::AppSettings::DeriveDisplayOrder
)]
pub struct App {
    #[clap(subcommand)]
    sub_command: SubCommand,
}

impl App {
    pub(crate) async fn start(self) -> Result<(), Box<dyn Error>> {
        self.sub_command.start().await
    }
}

#[allow(clippy::large_enum_variant)]
#[derive(clap::Parser, Debug)]
enum SubCommand {
    Server(server::ServerArgs),
    EnvConfig,
    CheckConfig(check_config::CheckConfigArgs),
}

impl SubCommand {
    async fn start(self) -> Result<(), Box<dyn Error>> {
        match self {
            Self::Server(args) => {
                server::run(args).await?;
            }
            Self::EnvConfig => {
                env_config::run()?;
            }
            Self::CheckConfig(args) => {
                check_config::run(&args)?;
            }
        }

        Ok(())
    }
}

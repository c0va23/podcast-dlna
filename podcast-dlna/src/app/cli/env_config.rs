struct EnvArg {
    name: String,
    default_value: Option<String>,
    required: bool,
    about: Option<String>,
}

const REQUIRED_FLAG: &str = "REQUIRED";

fn parse_server_args() -> anyhow::Result<Vec<EnvArg>> {
    use clap::IntoApp;

    let clap_app = super::server::ServerArgs::into_app();

    let env_regex = regex::Regex::new("env:\\s(Some\\(\\(\"([^\"]+)\",\\sNone\\)\\)|None)")?;
    let default_value_regex = regex::Regex::new("default_vals:\\s\\[\"([^\\]]+)\"\\]")?;
    let settings_regex = regex::Regex::new("settings:\\sArgFlags\\(([^\\)]+)\\)")?;
    let about_regex = regex::Regex::new("about:\\sSome\\(\"([^\"]+)\"\\)")?;

    let envs = clap_app.get_arguments().filter_map(|arg| {
        // Workaround to read Clap configuration.
        // Clap not provide public API to read configuration.
        // But debug of clap::Arg expose configuration.
        let arg_debug = format!("{:?}", arg);
        let default_value = default_value_regex
            .captures(&arg_debug)
            .and_then(|captures| captures.get(1))
            .map(|default_value_match| default_value_match.as_str().to_owned());

        let required = settings_regex
            .captures(&arg_debug)
            .and_then(|captures| captures.get(1))
            .map_or(false, |settings| {
                settings
                    .as_str()
                    .split(" | ")
                    .any(|flag| flag == REQUIRED_FLAG)
            });

        let about = about_regex
            .captures(&arg_debug)
            .and_then(|captures| captures.get(1))
            .map(|about_match| about_match.as_str().to_owned());

        env_regex
            .captures(&arg_debug)
            .and_then(|captures| captures.get(2))
            .map(|env_name_match| env_name_match.as_str().to_owned())
            .map(|env_name| EnvArg {
                name: env_name,
                default_value,
                required,
                about,
            })
    });

    Ok(envs.collect())
}

#[allow(clippy::print_stdout)]
pub(super) fn run() -> Result<(), anyhow::Error> {
    let env_args = parse_server_args()?;

    println!("## Logging level");
    println!("RUST_LOG=info");

    for env_arg in env_args {
        println!();

        let EnvArg {
            name,
            default_value,
            required,
            about,
        } = env_arg;

        if let Some(about) = about {
            println!("## {}", about);
        }

        if required {
            println!("{}=", name);
        } else if let Some(default_value) = default_value {
            println!("# {}={:?}", name, default_value);
        } else {
            println!("# {}=", name);
        }
    }

    Ok(())
}

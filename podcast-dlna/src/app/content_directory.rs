use std::convert::TryInto;
use std::net::SocketAddr;
use std::sync::Arc;

use async_trait::async_trait;
use thiserror::Error;

use upnp::didl::{Container, DidlLite, Item, ProtocolInfo, Resource, TransportType};
use upnp::media_server::content_directory::{
    Backend, Browse, BrowseResponse, GetSearchCapabilities, GetSearchCapabilitiesResponse,
    GetSortCapabilities, GetSortCapabilitiesResponse, BROWSE_DIRECT_CHILDREN, BROWSE_METADATA,
};

use crate::app::title_rewriter::ChannelTitleRewriter;
use crate::server::{HttpListenerError, HttpListenerLocator, PROXY_PREFIX};

use super::feed_fetcher::{Channel, Error as FeedFetcherError, FeedFetcher, MusicTrack};
use crate::utils::duration::format_colons;

#[derive(Error, Debug)]
enum BackendError {
    #[error("Cast number error: {0}")]
    TryFromInt(#[from] std::num::TryFromIntError),

    #[error("Serialization error: {0}")]
    Yarde(String),

    #[error("Feed fetcher error: {0}")]
    FeedFetcher(#[from] FeedFetcherError),

    #[error("Listener locator error: {0}")]
    HttpListenerLocator(#[from] HttpListenerError),
}

type Result<T> = anyhow::Result<T, BackendError>;

pub struct PodcastBackend {
    feed_fetcher: Arc<dyn FeedFetcher + Sync + Send>,
    http_listener_locator: Arc<dyn HttpListenerLocator>,
    channel_title_rewriter: Arc<dyn ChannelTitleRewriter + Sync + Send>,
}

#[async_trait]
impl Backend for PodcastBackend {
    async fn browse(&self, browse: &Browse) -> anyhow::Result<BrowseResponse> {
        debug!("browse flags {:?}", browse.browse_flag);
        debug!("object id {:?}", browse.object_id);

        let response = self
            .route_request(&browse.browse_flag, &browse.object_id)
            .await?;
        Ok(response)
    }

    #[allow(clippy::used_underscore_binding)]
    async fn get_search_capabilities(
        &self,
        _get_search_capabilities: &GetSearchCapabilities,
    ) -> anyhow::Result<GetSearchCapabilitiesResponse> {
        Ok(GetSearchCapabilitiesResponse {
            search_caps: "".into(),
        })
    }

    #[allow(clippy::used_underscore_binding)]
    async fn get_sort_capabilities(
        &self,
        _get_sort_capabilities: &GetSortCapabilities,
    ) -> anyhow::Result<GetSortCapabilitiesResponse> {
        Ok(GetSortCapabilitiesResponse {
            sort_caps: "".into(),
        })
    }
}

const OBJECT_ID_SEPARATOR: &str = "/";

impl PodcastBackend {
    pub(crate) fn new(
        http_listener_locator: Arc<dyn HttpListenerLocator>,
        feed_fetcher: Arc<dyn FeedFetcher + Sync + Send>,
        channel_title_rewriter: Arc<dyn ChannelTitleRewriter + Sync + Send>,
    ) -> Self {
        Self {
            feed_fetcher,
            http_listener_locator,
            channel_title_rewriter,
        }
    }

    async fn route_request(&self, browse_flag: &str, object_id: &str) -> Result<BrowseResponse> {
        let parts = object_id.split(OBJECT_ID_SEPARATOR).collect::<Vec<&str>>();

        match (browse_flag, parts.as_slice()) {
            (BROWSE_DIRECT_CHILDREN, [""]) => self.podcast_list("").await,
            (BROWSE_DIRECT_CHILDREN, ["0"]) => self.podcast_list("0").await,
            (BROWSE_DIRECT_CHILDREN, [channel_id]) => self.podcast_items(channel_id).await,
            (BROWSE_METADATA, [channel_id, track_id]) => {
                self.podcast_item(channel_id, track_id).await
            }
            (browse_flag, unknown_parts) => Self::unknown_handler(browse_flag, unknown_parts),
        }
    }

    fn last_channel_update<'a>(
        channels: impl Iterator<Item = &'a Channel>,
    ) -> Option<chrono::DateTime<chrono::FixedOffset>> {
        channels.filter_map(Channel::update_time).max()
    }

    fn build_containers(
        channels: impl IntoIterator<Item = Channel>,
        parent_id: &str,
        http_server_addr: SocketAddr,
    ) -> Vec<Container> {
        channels
            .into_iter()
            .map(|ref channel| Self::build_container(parent_id, channel, http_server_addr))
            .collect::<Vec<Container>>()
    }

    async fn podcast_list(&self, parent_id: &str) -> Result<BrowseResponse> {
        info!("Browse containers");

        let channels = self.feed_fetcher.fetch_channels().await?;

        let http_server_addr = self.http_listener_locator.local_addr().await?;

        let update_time = Self::last_channel_update(channels.iter());

        let update_id = Self::build_update_id(update_time)?;
        let containers = Self::build_containers(channels, parent_id, http_server_addr);
        let containers_count: u32 = containers.len().try_into()?;

        let didl = DidlLite {
            containers,
            items: Vec::new(),
        };

        Ok(BrowseResponse {
            result: yaserde::ser::to_string(&didl).map_err(BackendError::Yarde)?,
            number_returned: containers_count,
            total_matches: containers_count,
            update_id,
        })
    }

    fn build_update_id(update_time: Option<chrono::DateTime<chrono::FixedOffset>>) -> Result<u32> {
        let update_id = update_time
            .map_or_else(
                || chrono::MIN_DATETIME.naive_utc(),
                |datetime| datetime.naive_utc(),
            )
            .timestamp()
            .try_into()?;
        Ok(update_id)
    }

    fn build_container(
        parent_id: &str,
        channel: &Channel,
        http_server_addr: SocketAddr,
    ) -> Container {
        let album_art_uri = channel
            .image
            .as_ref()
            .map(|_| Self::channel_picture_url(channel.id.as_str(), http_server_addr));

        Container {
            id: channel.id.clone(),
            parent_id: parent_id.to_owned(),
            title: channel.title.clone(),
            child_count: Some(channel.tracks_count as i64),

            class: "object.container.album.musicAlbum".into(),
            searchable: Some("0".into()),
            restricted: "1".into(),
            storage_used: -1,
            album_art_uri,
        }
    }

    async fn podcast_items(&self, container_id: &str) -> Result<BrowseResponse> {
        info!("Browse items {}", container_id);

        let (channel, tracks) = self.feed_fetcher.fetch_tracks(container_id).await?;

        trace!("tracks {:?}", tracks);

        let items = self.build_items(&channel, tracks).await?;

        Self::build_items_response(&channel, items)
    }

    async fn podcast_item(&self, channel_id: &str, track_id: &str) -> Result<BrowseResponse> {
        info!("Browse items {} / {}", channel_id, track_id);

        let (channel, track) = self.feed_fetcher.fetch_track(channel_id, track_id).await?;

        trace!("track {:?}", track);

        let items = self.build_items(&channel, vec![track]).await?;

        Self::build_items_response(&channel, items)
    }

    async fn build_items(&self, channel: &Channel, tracks: Vec<MusicTrack>) -> Result<Vec<Item>> {
        let http_server_addr = self.http_listener_locator.local_addr().await?;

        Ok(tracks
            .iter()
            .map(|track| self.build_item(channel, track, http_server_addr))
            .collect())
    }

    fn build_item(
        &self,
        channel: &Channel,
        track: &MusicTrack,
        http_server_addr: SocketAddr,
    ) -> Item {
        let id = [channel.id.as_str(), track.id.as_str()].join(OBJECT_ID_SEPARATOR);
        let date = track
            .date
            .map(|date| date.date().format("%y-%m-%d").to_string());
        let content_type = Self::map_content_type(track.content_type.as_ref());
        let title = self
            .channel_title_rewriter
            .rewrite_title(channel.url.as_str(), track.title.as_str());

        let album_art_uri = if track.image.is_some() {
            Some(Self::icon_url(&channel.id, track, http_server_addr))
        } else {
            None
        };

        Item {
            id,
            parent_id: channel.id.clone(),
            class: "object.item.audioItem.musicTrack".to_owned(),
            creator: track.creators.clone(),
            artist: track.creators.clone(),
            restricted: "1".into(),
            title,
            date,
            album: Some(channel.title.clone()),
            album_art_uri,
            description: None,
            long_description: track.description.clone(),
            resource: Resource {
                uri: Self::wrap_url(&channel.id, track, http_server_addr),
                size: track.size,
                duration: track.duration.map(format_colons),
                protocol_info: ProtocolInfo {
                    content_type,
                    transport_type: TransportType::HttpGet,
                },
            },
        }
    }

    fn build_items_response(channel: &Channel, items: Vec<Item>) -> Result<BrowseResponse> {
        let total: u32 = items.len().try_into()?;
        let update_id = Self::build_update_id(channel.update_time)?;

        let didl = DidlLite {
            containers: Vec::new(),
            items,
        };

        Ok(BrowseResponse {
            result: yaserde::ser::to_string(&didl).map_err(BackendError::Yarde)?,
            number_returned: total,
            total_matches: total,
            update_id,
        })
    }

    fn map_content_type(src_content_type: &str) -> String {
        match src_content_type {
            "audio/mp3" => "audio/mpeg".to_owned(),
            _ => src_content_type.to_owned(),
        }
    }

    fn icon_url(channel_id: &str, track: &MusicTrack, http_server_addr: SocketAddr) -> String {
        // TODO: Share base url building
        format!(
            "http://{server_addr}/{prefix}/{channel_id}/{track_id}",
            server_addr = http_server_addr,
            // TODO: use prefix from constant shared with http server
            prefix = "pictures",
            channel_id = channel_id,
            track_id = track.id,
        )
    }

    fn channel_picture_url(channel_id: &str, http_server_addr: SocketAddr) -> String {
        // TODO: Share base url building
        format!(
            "http://{server_addr}/{prefix}/{channel_id}",
            server_addr = http_server_addr,
            // TODO: use prefix from constant shared with http server
            prefix = "pictures",
            channel_id = channel_id,
        )
    }

    fn wrap_url(channel_id: &str, track: &MusicTrack, http_server_addr: SocketAddr) -> String {
        let extension = track.extension();

        format!(
            "http://{server_addr}/{prefix}/{channel_id}/{track_id}.{extension}",
            server_addr = http_server_addr,
            prefix = PROXY_PREFIX,
            channel_id = channel_id,
            track_id = track.id,
            extension = extension,
        )
    }

    fn unknown_handler(browse_flag: &str, parts: &[&str]) -> Result<BrowseResponse> {
        info!("Browse items {:?} {:?}", browse_flag, parts);

        let didl = DidlLite {
            containers: Vec::new(),
            items: Vec::new(),
        };

        Ok(BrowseResponse {
            result: yaserde::ser::to_string(&didl).map_err(BackendError::Yarde)?,
            number_returned: 0,
            total_matches: 0,
            update_id: 1,
        })
    }
}

use std::error::Error as StdError;
use std::sync::Arc;

use async_trait::async_trait;
use ssdp::{
    Data as SsdpData, DataBuilder as SsdpDataBuilder, Device, Error as SSDPError,
    Result as SSDPResult,
};

use crate::server::HttpListenerLocator;
use crate::utils::device_info::DeviceUuidFetcher;
use crate::utils::device_info::UserAgentFetcher;

pub struct LazySsdpDataBuilder {
    device_uuid: uuid::Uuid,
    user_agent: String,
    listener_locator: Arc<dyn HttpListenerLocator>,
}

impl LazySsdpDataBuilder {
    pub(crate) fn new<DeviceInfoFetcher>(
        device_info_fetcher: &DeviceInfoFetcher,
        listener_locator: Arc<dyn HttpListenerLocator>,
    ) -> Result<Self, Box<dyn StdError>>
    where
        DeviceInfoFetcher: UserAgentFetcher + DeviceUuidFetcher,
    {
        let device_uuid = device_info_fetcher.device_uuid()?;
        let user_agent = device_info_fetcher.user_agent()?;

        Ok(Self {
            device_uuid,
            user_agent,
            listener_locator,
        })
    }
}

#[async_trait]
impl SsdpDataBuilder for LazySsdpDataBuilder {
    async fn build(&self) -> SSDPResult<SsdpData> {
        let http_server_local_addr = self
            .listener_locator
            .local_addr()
            .await
            .map_err(|err| SSDPError::Other(Box::new(err)))?;

        let location = format!("http://{}/root.xml", http_server_local_addr);
        let device = Device {
            uuid: format!("uuid:{}", self.device_uuid),
            name: "urn:schemas-upnp-org:device:MediaServer:1".to_owned(),
            services: vec!["urn:schemas-upnp-org:service:ContentDirectory:1".to_owned()],
        };
        let user_agent = self.user_agent.clone();

        Ok(SsdpData {
            location,
            user_agent,
            device,
        })
    }
}

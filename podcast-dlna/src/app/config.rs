use serde_derive::Deserialize;
use thiserror::Error as ThisError;

#[derive(Deserialize, Debug, Clone)]
pub struct RewriteRule {
    pub regex: String,
    pub template: String,
}

#[derive(Deserialize, Debug)]
pub struct Podcast {
    pub url: String,
    pub name_rewrite_rule: Option<RewriteRule>,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub podcasts: Vec<Podcast>,
}

#[derive(ThisError, Debug)]
pub enum Error {
    #[error("Config file {path} read error: {cause}")]
    NotFound { cause: std::io::Error, path: String },

    #[error("Config parse error: {0}")]
    Toml(#[from] toml::de::Error),
}

impl Config {
    pub(crate) fn load(config_path: &str) -> Result<Self, Error> {
        let config_data = std::fs::read_to_string(config_path).map_err(|err| Error::NotFound {
            cause: err,
            path: config_path.to_string(),
        })?;
        let config = toml::from_str::<Self>(&config_data)?;

        debug!("Config: {:?}", config);

        Ok(config)
    }

    pub(crate) fn feed_urls(&self) -> Vec<String> {
        self.podcasts
            .iter()
            .map(|podcast| podcast.url.clone())
            .collect::<Vec<String>>()
    }
}

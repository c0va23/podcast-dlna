use std::collections::HashMap;
use std::sync::Arc;

use async_trait::async_trait;
use tokio::sync::RwLock;

use super::{Error, PreflightParams, PreflightResponse, Preflighter};
use crate::utils::ring_cache::Bucket;

#[cfg(test)]
mod tests;

struct CacheKey {
    track_url: String,
    with_duration: bool,
}

impl PartialEq for CacheKey {
    fn eq(&self, other: &Self) -> bool {
        // if this cache key not expect duration, then ignore duration on other.
        // Otherwise other should be have duration.
        let ignore_duration = !self.with_duration;
        self.track_url == other.track_url && (ignore_duration || other.with_duration)
    }
}

impl From<&PreflightParams> for CacheKey {
    fn from(params: &PreflightParams) -> Self {
        Self {
            track_url: params.url.clone(),
            with_duration: params.parse_body,
        }
    }
}

impl AsRef<Self> for PreflightParams {
    fn as_ref(&self) -> &Self {
        self
    }
}

type BucketLock = Arc<RwLock<Bucket<CacheKey, PreflightResponse>>>;

pub struct CachedPreflighter {
    cache: RwLock<HashMap<String, BucketLock>>,
    preflighter: Arc<dyn Preflighter + Send + Sync>,
    bucket_size: usize,
}

impl CachedPreflighter {
    pub(crate) fn new(preflighter: Arc<dyn Preflighter + Sync + Send>, bucket_size: usize) -> Self {
        let cache = RwLock::new(HashMap::new());

        Self {
            cache,
            preflighter,
            bucket_size,
        }
    }

    // Read or crate bucket for feed. Release lock after exit function.
    async fn get_bucket(&self, params: &PreflightParams) -> BucketLock {
        // Fast path. Return bucket if exists.
        if let Some(bucket) = self.cache.read().await.get(params.feed_url.as_str()) {
            return bucket.clone();
        }

        // Slow path. Only one thread insert bucket.
        self.cache
            .write()
            .await
            .entry(params.feed_url.clone())
            .or_insert_with(|| self.build_bucket())
            .clone()
    }

    fn build_bucket(&self) -> BucketLock {
        Arc::new(RwLock::new(Bucket::new(self.bucket_size)))
    }

    async fn perform_read(
        &self,
        bucket: BucketLock,
        params: &PreflightParams,
    ) -> Option<PreflightResponse> {
        let key = params.into();
        let response = bucket.read().await.get(&key)?;
        tracing::debug!(
            track_url = params.url.as_str(),
            "Return response from cache",
        );
        Some(response)
    }

    async fn perform_update(
        &self,
        bucket: BucketLock,
        params: PreflightParams,
    ) -> Result<PreflightResponse, Error> {
        let key = params.as_ref().into();
        let preflighter = self.preflighter.clone();
        tracing::debug!(track_url = params.url.as_str(), "Try perform preflight");
        bucket
            .write()
            .await
            .get_or_push(key, async move {
                tracing::debug!(track_url = params.url.as_str(), "Write response to cache");
                preflighter.perform(params).await
            })
            .await
    }
}

#[async_trait]
impl Preflighter for CachedPreflighter {
    #[tracing::instrument(
        name = "CachedPreflighter::perform",
        skip(self, params),
        fields(
            feed_url = params.feed_url.as_str(),
            track_url = params.url.as_str(),
            parse_body = params.parse_body
        )
    )]
    async fn perform(&self, params: PreflightParams) -> Result<PreflightResponse, Error> {
        let bucket = self.get_bucket(&params).await;

        // Fast path / cache hit
        if let Some(response) = self.perform_read(bucket.clone(), &params).await {
            return Ok(response);
        }

        // Slow path / cache miss
        self.perform_update(bucket, params).await
    }
}

use std::sync::Arc;
use std::time::Duration;

use super::super::{Headers, MockPreflighter, PreflightParams, PreflightResponse, Preflighter};
use super::CachedPreflighter;

use bytes::Bytes;
use mockall::predicate;

#[tokio::test]
async fn cache_single_value() {
    let mut mocked_preflighter = MockPreflighter::new();

    let params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: false,
    };
    let expected_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    {
        let expected_response = expected_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_response = Arc::new(expected_response.clone());
                Ok(expected_response.as_ref().clone())
            });
    }

    let bucket_size = 10;
    let preflighter = CachedPreflighter::new(Arc::new(mocked_preflighter), bucket_size);

    let response_one = preflighter.perform(params.clone()).await.unwrap();
    assert_eq!(expected_response, response_one);

    let response_two = preflighter.perform(params.clone()).await.unwrap();
    assert_eq!(expected_response, response_two);
}

#[tokio::test]
async fn refresh_cache_when_evicted() {
    let mut mocked_preflighter = MockPreflighter::new();

    let first_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: false,
    };
    let expected_first_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: first_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    let second_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-90.mp3".to_string(),
        parse_body: false,
    };
    let expected_second_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: second_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    {
        let expected_response = expected_first_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(first_params.clone()))
            .times(2)
            .returning(move |_| {
                let expected_response = Arc::new(expected_response.clone());
                Ok(expected_response.as_ref().clone())
            });
    }

    {
        let expected_response = expected_second_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(second_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_response = Arc::new(expected_response.clone());
                Ok(expected_response.as_ref().clone())
            });
    }

    let bucket_size = 1;
    let preflighter = CachedPreflighter::new(Arc::new(mocked_preflighter), bucket_size);

    let response_one = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, response_one, "1-1");

    let response_two = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_second_response, response_two, "2-1");

    let response_three = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, response_three, "1-2");
}

#[tokio::test]
async fn cache_two_bucket() {
    let mut mocked_preflighter = MockPreflighter::new();

    let first_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed1.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: false,
    };
    let expected_first_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: first_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    let second_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed2.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-90.mp3".to_string(),
        parse_body: false,
    };
    let expected_second_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: second_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    {
        let expected_response = expected_first_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(first_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_response = Arc::new(expected_response.clone());
                Ok(expected_response.as_ref().clone())
            });
    }

    {
        let expected_response = expected_second_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(second_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_response = Arc::new(expected_response.clone());
                Ok(expected_response.as_ref().clone())
            });
    }

    let bucket_size = 1;
    let preflighter = CachedPreflighter::new(Arc::new(mocked_preflighter), bucket_size);

    let response_one = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, response_one);

    let response_two = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_second_response, response_two);

    let response_three = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, response_three);

    let response_four = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_second_response, response_four);
}

#[tokio::test]
async fn update_cache_with_duration() {
    let mut mocked_preflighter = MockPreflighter::new();

    let first_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: false,
    };
    let expected_first_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: first_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: None,
        content_length: 0,
        attached_picture: None,
    };

    let second_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: true,
    };
    let expected_second_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: first_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: Some(Duration::new(12, 34)),
        content_length: 0,
        attached_picture: None,
    };

    {
        let expected_first_response = expected_first_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(first_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_first_response = Arc::new(expected_first_response.clone());
                Ok(expected_first_response.as_ref().clone())
            });
    }

    {
        let expected_second_response = expected_second_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(second_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_second_response = Arc::new(expected_second_response.clone());
                Ok(expected_second_response.as_ref().clone())
            });
    }

    let bucket_size = 10;
    let preflighter = CachedPreflighter::new(Arc::new(mocked_preflighter), bucket_size);

    let first_response_one = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, first_response_one, "1-1");

    let first_response_two = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, first_response_two, "1-2");

    let second_response_one = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_second_response, second_response_one, "2-1");

    let second_response_two = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_second_response, second_response_two, "2-2");
}

#[tokio::test]
async fn not_update_cache_without_duration() {
    let mut mocked_preflighter = MockPreflighter::new();

    let first_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: true,
    };
    let expected_first_response = PreflightResponse {
        version: hyper::Version::HTTP_11,
        status: http::StatusCode::PARTIAL_CONTENT,
        headers: Headers::new(),
        url: first_params.url.clone(),
        body_prefix: Bytes::new(),
        duration: Some(Duration::new(12, 34)),
        content_length: 0,
        attached_picture: None,
    };

    let second_params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: false,
    };

    {
        let expected_first_response = expected_first_response.clone();
        mocked_preflighter
            .expect_perform()
            .with(predicate::eq(first_params.clone()))
            .times(1)
            .returning(move |_| {
                let expected_first_response = Arc::new(expected_first_response.clone());
                Ok(expected_first_response.as_ref().clone())
            });
    }

    let bucket_size = 10;
    let preflighter = CachedPreflighter::new(Arc::new(mocked_preflighter), bucket_size);

    let first_response_one = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, first_response_one);

    let first_response_two = preflighter.perform(first_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, first_response_two);

    let second_response_one = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, second_response_one);

    let second_response_two = preflighter.perform(second_params.clone()).await.unwrap();
    assert_eq!(expected_first_response, second_response_two);
}

use std::time::Duration;

use pretty_assertions::assert_eq;

use super::super::{PreflightParams, Preflighter};
use super::{HttpPreflighter, HttpPreflighterConfig};
use crate::utils::device_info::MockUserAgentFetcher;

const USER_AGENT: &str = "Linux/5.8.13-arch1-1 UPnP/1.0 Podcast/0.1.1";

fn build_user_agent_fetcher() -> MockUserAgentFetcher {
    let mut user_agent_fetcher = MockUserAgentFetcher::new();
    user_agent_fetcher
        .expect_user_agent()
        .times(1)
        .returning(|| Ok(USER_AGENT.to_owned()));
    user_agent_fetcher
}

#[tokio::test]
async fn preflight_simple() {
    // TODO: up local server on tests
    // TODO: move to integration tests
    let timeout = Duration::from_secs(10);
    let prefix_size = usize::from(std::u16::MAX);

    let user_agent_fetcher = build_user_agent_fetcher();

    let http_client = reqwest::Client::new();
    let config = HttpPreflighterConfig {
        preflight_prefix_size: prefix_size,
        preflight_max_prefix_size: prefix_size * 2,
        preflight_timeout: timeout,
        preflight_attempt_count: 3,
    };
    let preflighter = HttpPreflighter::new(http_client, &user_agent_fetcher, config).unwrap();

    let params = PreflightParams {
        feed_url: "http://feeds.rucast.net/radio-t".to_string(),
        url: "http://cdn.radio-t.com/rt_podcast719.mp3".to_string(),
        parse_body: true,
    };

    let response = preflighter.perform(params).await.unwrap();

    let expected_duration = Duration::new(8005, 485_750_000);
    assert_eq!(Some(expected_duration), response.duration);

    assert_eq!(hyper::Version::HTTP_11, response.version);

    assert!(response.attached_picture.is_some());
}

#[tokio::test]
async fn preflight_without_pictures_with_http2() {
    // TODO: up local server on tests
    // TODO: move to integration tests
    let timeout = Duration::from_secs(10);
    let prefix_size = usize::from(std::u16::MAX);

    let user_agent_fetcher = build_user_agent_fetcher();

    let http_client = reqwest::Client::new();
    let config = HttpPreflighterConfig {
        preflight_prefix_size: prefix_size,
        preflight_max_prefix_size: prefix_size * 2,
        preflight_timeout: timeout,
        preflight_attempt_count: 3,
    };
    let preflighter = HttpPreflighter::new(http_client, &user_agent_fetcher, config).unwrap();

    let params = PreflightParams {
        feed_url: "dm".to_string(),
        url: "https://otvratitelnye-muzhiki.podster.fm/200/download/audio.mp3".to_string(),
        parse_body: true,
    };

    let response = preflighter.perform(params).await.unwrap();

    let expected_duration = Duration::new(5_722, 44_281_250);
    assert_eq!(Some(expected_duration), response.duration);

    assert_eq!(hyper::Version::HTTP_2, response.version);

    assert!(response.attached_picture.is_none());
}

#[tokio::test]
async fn preflight_with_big_cover() {
    // TODO: up local server on tests
    // TODO: move to integration tests
    let timeout = Duration::from_secs(10);
    let prefix_size = usize::from(std::u16::MAX);
    let max_prefix_size = 4 * 1024 * 1024;
    let user_agent_fetcher = build_user_agent_fetcher();

    let http_client = reqwest::Client::new();
    let config = HttpPreflighterConfig {
        preflight_prefix_size: prefix_size,
        preflight_max_prefix_size: max_prefix_size,
        preflight_timeout: timeout,
        preflight_attempt_count: 3,
    };
    let preflighter = HttpPreflighter::new(http_client, &user_agent_fetcher, config).unwrap();

    let params = PreflightParams {
        feed_url: "https://scalalaz.ru/rss/feed.xml".to_string(),
        url: "https://scalalaz.ru/mp3/scalalaz-podcast-89.mp3".to_string(),
        parse_body: true,
    };

    let response = preflighter.perform(params).await.unwrap();

    let expected_duration = Duration::new(3885, 944_000_000);
    assert_eq!(Some(expected_duration), response.duration);

    assert_eq!(hyper::Version::HTTP_11, response.version);

    assert!(response.attached_picture.is_some());
}

#[test]
fn content_length_from_range_success() {
    use std::convert::TryInto;

    let mut headers = http::header::HeaderMap::new();
    headers.append(
        http::header::CONTENT_RANGE,
        "bytes 0-999/10000".try_into().unwrap(),
    );
    let content_length = HttpPreflighter::content_length_from_range(&headers).unwrap();
    assert_eq!(10000, content_length);
}

#[test]
fn content_length_from_range_unknown_size() {
    use std::convert::TryInto;

    let mut headers = http::header::HeaderMap::new();
    headers.append(
        http::header::CONTENT_RANGE,
        "bytes 0-999/*".try_into().unwrap(),
    );
    let result = HttpPreflighter::content_length_from_range(&headers);

    assert!(result.is_err());
}

#[test]
fn content_length_from_range_invalid_format() {
    use std::convert::TryInto;

    let mut headers = http::header::HeaderMap::new();
    headers.append(
        http::header::CONTENT_RANGE,
        "bytes 0-999/invalid".try_into().unwrap(),
    );
    let result = HttpPreflighter::content_length_from_range(&headers);

    assert!(result.is_err());
}

#[test]
fn content_length_from_range_invalid_size() {
    use std::convert::TryInto;

    let mut headers = http::header::HeaderMap::new();
    headers.append(
        http::header::CONTENT_RANGE,
        "bytes 0-999/10000000000000000000000".try_into().unwrap(),
    );
    let result = HttpPreflighter::content_length_from_range(&headers);

    assert!(result.is_err());
}

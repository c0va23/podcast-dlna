use std::error::Error as StdError;
use std::time::Duration;

use async_trait::async_trait;
use audio_metadata::mp3::id3v2::AttachedPicture;
use bytes::{Bytes, BytesMut};

use crate::utils::device_info::UserAgentFetcher;
use crate::utils::parse_iso8601_duration;

use super::{Error, ErrorKind, PreflightParams, PreflightResponse, Preflighter};

type Headers = http::HeaderMap<http::HeaderValue>;

#[derive(Debug, Clone, clap::Parser)]
#[clap()]
pub struct HttpPreflighterConfig {
    /// Default prefix size. Used as cache for fast start track playing.
    /// If track not have duration into RSS feed, then prefix cache used for parsing duration from
    /// MP3 headers.
    #[clap(long, env, default_value = "65535")]
    preflight_prefix_size: usize,

    /// If duration header not exists into first prefix size bytes, then requested next prefix
    /// size bytes. Max prefix size is limit it.
    #[clap(long, env, default_value = "1048576")]
    preflight_max_prefix_size: usize,

    /// HTTP preflight request timeout.
    #[clap(long, env, default_value = "PT10S", parse(try_from_str = parse_iso8601_duration))]
    preflight_timeout: Duration,

    /// Max preflight attempt performed before return error.
    #[clap(long, env, default_value = "3")]
    preflight_attempt_count: usize,
}

pub struct HttpPreflighter {
    http_client: reqwest::Client,
    common_headers: Headers,
    config: HttpPreflighterConfig,
}

impl HttpPreflighter {
    pub(crate) fn new<Fetcher>(
        http_client: reqwest::Client,
        fetcher: &Fetcher,
        config: HttpPreflighterConfig,
    ) -> Result<Self, Box<dyn StdError>>
    where
        Fetcher: UserAgentFetcher,
    {
        let common_headers = Self::build_common_headers(fetcher)?;
        Ok(Self {
            http_client,
            common_headers,
            config,
        })
    }
}

// private implementation
impl HttpPreflighter {
    fn build_common_headers<Fetcher>(fetcher: &Fetcher) -> Result<Headers, Box<dyn StdError>>
    where
        Fetcher: UserAgentFetcher,
    {
        let mut headers = http::header::HeaderMap::new();
        let user_agent = fetcher.user_agent()?;

        headers.append(http::header::USER_AGENT, user_agent.parse().unwrap());

        Ok(headers)
    }

    fn headers(&self, prefix_start: usize, prefix_size: usize) -> Headers {
        let mut headers = self.common_headers.clone();

        let prefix_end = prefix_start + prefix_size;
        debug!("Request {}-{}", prefix_start, prefix_end);
        let range_header_value = format!("bytes={}-{}", prefix_start, prefix_end)
            .parse()
            .unwrap();
        headers.append(http::header::RANGE, range_header_value);

        headers
    }

    fn build_request(
        &self,
        url: &str,
        prefix_start: usize,
        prefix_size: usize,
    ) -> Result<reqwest::Request, reqwest::Error> {
        self.http_client
            .request(reqwest::Method::GET, url)
            .headers(self.headers(prefix_start, prefix_size))
            .timeout(self.config.preflight_timeout)
            .build()
    }

    #[tracing::instrument(
        name = "HttpPreflighter::parse_response",
        level = "trace",
        skip(prev_body_prefix)
    )]
    async fn parse_response(
        response: reqwest::Response,
        prev_body_prefix: Bytes,
        parse_body: bool,
    ) -> Result<PreflightResponse, ErrorKind> {
        let version = response.version();
        let status = response.status();
        let headers = response.headers().clone();
        let url = response.url().to_string();
        let body_prefix = Self::fetch_body_prefix(response, prev_body_prefix).await?;
        let content_length = Self::content_length_from_range(&headers)?;

        let (body_prefix, duration, attached_picture) = if parse_body {
            Self::parse_response_body(body_prefix, &url, content_length)
                .map(|(body_prefix, duration, picture)| (body_prefix, Some(duration), picture))?
        } else {
            (body_prefix, None, None)
        };

        Ok(PreflightResponse {
            version,
            status,
            headers,
            url,
            body_prefix,
            duration,
            content_length,
            attached_picture,
        })
    }

    #[tracing::instrument(
        name = "HttpPreflighter::fetch_body_prefix",
        level = "trace",
        skip(prev_body_prefix, response)
    )]
    async fn fetch_body_prefix(
        response: reqwest::Response,
        prev_body_prefix: Bytes,
    ) -> Result<Bytes, ErrorKind> {
        let body_prefix = response.bytes().await?;

        if prev_body_prefix.is_empty() {
            Ok(body_prefix)
        } else {
            let mut tmp_body_prefix =
                BytesMut::with_capacity(prev_body_prefix.len() + body_prefix.len());
            tmp_body_prefix.extend_from_slice(prev_body_prefix.as_ref());
            tmp_body_prefix.extend_from_slice(body_prefix.as_ref());
            Ok(tmp_body_prefix.freeze())
        }
    }

    #[tracing::instrument(
        name = "HttpPreflighter::parse_response_body",
        level = "trace",
        skip(body_prefix, url),
        fields(body_prefix_len=body_prefix.len())
    )]
    fn parse_response_body(
        body_prefix: Bytes,
        url: &str,
        full_size: usize,
    ) -> Result<(Bytes, Duration, Option<AttachedPicture>), ErrorKind> {
        use audio_metadata::{Mp3File, ParseError};

        match Mp3File::parse(&body_prefix[..]) {
            Ok(mp3_file) => {
                let duration = mp3_file.duration_by_prefix(full_size).map_err(|error| {
                    let url = url.to_string();

                    ErrorKind::Duration { error, url }
                })?;

                let picture = mp3_file.attached_pictures().into_iter().next();

                Ok((body_prefix, duration, picture))
            }
            Err(error) => {
                let url = url.to_owned();

                Err(match error {
                    ParseError::Incomplete(min_len) => ErrorKind::IncompleteMp3File {
                        body_prefix,
                        url,
                        min_len,
                    },
                    ParseError::Error(error) => ErrorKind::ParseAudioFile { error, url },
                })
            }
        }
    }

    fn content_length_from_range(headers: &http::HeaderMap) -> Result<usize, ErrorKind> {
        let re = regex::Regex::new(r"bytes .+/(?P<size>\d+|\*)").unwrap();

        let is_known_size = |size: &&str| *size != "*";

        headers
            .get(http::header::CONTENT_RANGE)
            .map(http::header::HeaderValue::to_str)
            .transpose()?
            .and_then(|content_range| re.captures(content_range))
            .and_then(|captures| captures.name("size"))
            .as_ref()
            .map(regex::Match::as_str)
            .filter(is_known_size)
            .map(str::parse)
            .transpose()?
            .ok_or(ErrorKind::UnknownContentLength)
    }

    #[tracing::instrument(
        name = "HttpPreflighter::try_perform_request",
        level = "debug",
        skip(self, prev_prefix_body, params),
        fields(
            prev_prefix_body_len = prev_prefix_body.len(),
            track_url = params.url.as_str(),
            parse_body = params.parse_body,
        )
    )]
    async fn try_perform_request(
        &self,
        params: PreflightParams,
        prev_prefix_body: Bytes,
        prefix_size: usize,
    ) -> Result<PreflightResponse, ErrorKind> {
        let request =
            self.build_request(params.url.as_str(), prev_prefix_body.len(), prefix_size)?;

        match self.http_client.execute(request).await {
            Ok(response) => {
                Self::parse_response(response, prev_prefix_body, params.parse_body).await
            }
            Err(error) => Err(ErrorKind::RetryableReqwest {
                error,
                body_prefix: prev_prefix_body,
            }),
        }
    }

    async fn perform_request(
        &self,
        params: PreflightParams,
    ) -> Result<PreflightResponse, ErrorKind> {
        let mut prev_prefix_body = Bytes::new();
        let mut prefix_size = self.config.preflight_prefix_size;
        let mut params = params;
        let mut attempt = 0;

        loop {
            if prefix_size >= self.config.preflight_max_prefix_size {
                debug!(
                    "Duration prefix limit reached. Expected {}, but max {}",
                    prefix_size, self.config.preflight_max_prefix_size,
                );
                warn!(" Increase PREFLIGHT_MAX_PREFIX_SIZE value to fix it.");
                break;
            }

            match self
                .try_perform_request(params.clone(), prev_prefix_body, prefix_size)
                .await
            {
                Err(ErrorKind::IncompleteMp3File {
                    body_prefix,
                    url,
                    min_len,
                    ..
                }) => {
                    prev_prefix_body = body_prefix;
                    prefix_size = min_len;
                    params = PreflightParams { url, ..params };
                    continue;
                }
                Err(ErrorKind::RetryableReqwest { error, body_prefix }) => {
                    attempt += 1;
                    if attempt == self.config.preflight_attempt_count {
                        return Err(ErrorKind::Reqwest(error));
                    }
                    prev_prefix_body = body_prefix;
                    tracing::warn!(
                        track_url = params.url.as_str(),
                        attempt = attempt,
                        err = ?error,
                        "Retry request after error",
                    );
                }
                Ok(response) => {
                    tracing::debug!(
                        track_url = response.url.as_str(),
                        prefix_size = response.body_prefix.len(),
                        "Success preflight",
                    );
                    return Ok(response);
                }
                Err(err) => return Err(err),
            }
        }

        Err(ErrorKind::NoDuration)
    }
}

#[async_trait]
impl Preflighter for HttpPreflighter {
    #[tracing::instrument(name = "HttpPreflighter::perform", skip(self, params))]
    async fn perform(&self, params: PreflightParams) -> Result<PreflightResponse, Error> {
        self.perform_request(params.clone())
            .await
            .map_err(|kind| Error {
                kind,
                url: params.url,
            })
    }
}

#[cfg(test)]
mod tests;

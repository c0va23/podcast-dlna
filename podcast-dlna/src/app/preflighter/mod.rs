#![allow(clippy::default_trait_access)]
use std::num::TryFromIntError;
use std::time::Duration;

use async_trait::async_trait;
use audio_metadata::mp3::id3v2::AttachedPicture;
use bytes::Bytes;
#[cfg(test)]
use mockall::automock;
use thiserror::Error as ThisError;

mod cache;
mod reqwest;

pub use self::reqwest::{HttpPreflighter as Http, HttpPreflighterConfig as HttpConfig};
pub use cache::CachedPreflighter as Cached;

type Headers = http::HeaderMap<http::HeaderValue>;

#[derive(Debug)]
pub struct Error {
    pub kind: ErrorKind,
    pub url: String,
}

// Workaround for rust-analyzer. Derive thiserror::Error on struct provide
// `unresolved-import` error.
impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Preflight {url} error: {kind}",
            url = self.url,
            kind = self.kind,
        )
    }
}

#[derive(ThisError, Debug)]
pub enum ErrorKind {
    #[error("Reqwest error {0}")]
    Reqwest(#[from] ::reqwest::Error),

    #[error("Reqwest error {error}")]
    RetryableReqwest {
        error: ::reqwest::Error,
        body_prefix: Bytes,
    },

    #[error("Incomplete MP3 {url} error: need {min_len} bytes")]
    IncompleteMp3File {
        url: String,
        body_prefix: Bytes,
        min_len: usize,
    },

    #[error("Duration on {url} error: {error}")]
    Duration {
        error: audio_metadata::ParseDurationError,
        url: String,
    },

    #[error("Parse audio file on {url} error: {error}")]
    ParseAudioFile {
        error: audio_metadata::ParseVerboseError,
        url: String,
    },

    #[error("Convert header value error {0}")]
    HeaderToStr(#[from] hyper::header::ToStrError),

    #[error("Parse content size error {0}")]
    ParseContentSize(#[from] std::num::ParseIntError),

    #[error("Unknown content length")]
    UnknownContentLength,

    #[error("Duration not detected")]
    NoDuration,

    #[error("Invalid prefix size: {0}")]
    InvalidPrefixSize(#[from] TryFromIntError),
}

#[derive(Clone, PartialEq, Debug)]
pub struct PreflightResponse {
    pub version: hyper::Version,
    pub status: http::StatusCode,
    pub headers: Headers,
    pub url: String,
    pub body_prefix: Bytes,
    pub duration: Option<Duration>,
    pub content_length: usize,
    pub attached_picture: Option<AttachedPicture>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct PreflightParams {
    pub feed_url: String,
    pub url: String,
    pub parse_body: bool,
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait Preflighter: Send + Sync {
    async fn perform(&self, params: PreflightParams) -> Result<PreflightResponse, Error>;
}

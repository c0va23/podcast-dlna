use bytes::Bytes;
use pretty_assertions::assert_eq;
use std::ops::Range;

use super::{DataStream, CHUNK_SIZE};

#[tokio::test]
async fn image_stream() {
    use futures::TryStreamExt;
    use rand::prelude::*;

    struct TestCase {
        name: &'static str,
        image_size: usize,
        sub_range: Option<Range<usize>>,
    }

    #[derive(Debug)]
    struct Error {}

    let test_cases = &[
        TestCase {
            name: "0.5x chunk",
            image_size: CHUNK_SIZE / 2,
            sub_range: None,
        },
        TestCase {
            name: "1x-1 chunk",
            image_size: CHUNK_SIZE - 1,
            sub_range: None,
        },
        TestCase {
            name: "1x chunk",
            image_size: CHUNK_SIZE,
            sub_range: None,
        },
        TestCase {
            name: "1x+1 chunk",
            image_size: CHUNK_SIZE + 1,
            sub_range: None,
        },
        TestCase {
            name: "1.5x chunk",
            image_size: CHUNK_SIZE / 2 * 3,
            sub_range: None,
        },
        TestCase {
            name: "2x-1 chunk",
            image_size: CHUNK_SIZE * 2 - 1,
            sub_range: None,
        },
        TestCase {
            name: "2x chunk",
            image_size: CHUNK_SIZE * 2,
            sub_range: None,
        },
        TestCase {
            name: "2x+1 chunk",
            image_size: CHUNK_SIZE * 2 + 1,
            sub_range: None,
        },
        TestCase {
            name: "1x chunk (sub range)",
            image_size: CHUNK_SIZE,
            sub_range: Some((CHUNK_SIZE / 4)..(CHUNK_SIZE / 4 * 3)),
        },
        TestCase {
            name: "2x chunk (sub range)",
            image_size: CHUNK_SIZE * 2,
            sub_range: Some((CHUNK_SIZE / 2)..(CHUNK_SIZE / 2 * 3)),
        },
    ];

    for test_case in test_cases {
        println!("Test case {}", test_case.name);

        let mut data = vec![0_u8; test_case.image_size];
        thread_rng().fill_bytes(&mut data[..]);

        let sub_range = test_case.sub_range.clone().unwrap_or(0..data.len());
        let image_data = Bytes::from(data.clone());
        let image_stream = DataStream::<Error>::new(&image_data, sub_range.clone());

        let chunks: Vec<Bytes> = image_stream.try_collect().await.unwrap();
        let result_data: Vec<u8> = chunks
            .into_iter()
            .flat_map(|chunk| chunk[..].to_vec())
            .collect();

        assert_eq!(data[sub_range].to_vec(), result_data);
    }
}

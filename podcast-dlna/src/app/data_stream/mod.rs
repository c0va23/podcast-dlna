// imports
use std::error::Error as StdError;
use std::marker::PhantomData;
use std::ops::Range;

use bytes::Bytes;
use futures::Stream;

// modules

#[cfg(test)]
mod tests;

const CHUNK_SIZE: usize = 16384;

pub struct DataStream<E> {
    data: Bytes,
    chunk_count: usize,
    current_chunk_index: usize,
    _err: PhantomData<E>,
}

impl<E> DataStream<E> {
    pub(crate) fn new(data: &Bytes, data_range: Range<usize>) -> Self {
        let data = data.slice(data_range);
        let data_len = data.len();
        let last_chunk = if data_len % CHUNK_SIZE > 0 { 1 } else { 0 };

        let chunk_count = data_len / CHUNK_SIZE + last_chunk;
        let current_chunk_index = 0;

        Self {
            data,
            chunk_count,
            current_chunk_index,
            _err: PhantomData::<E>,
        }
    }

    fn next_chunk(&mut self) -> (usize, usize) {
        let current_chunk_index = self.current_chunk_index;
        let next_chunk_index = current_chunk_index + 1;
        self.current_chunk_index = next_chunk_index;
        (current_chunk_index, next_chunk_index)
    }
}

impl<E> Stream for DataStream<E>
where
    E: Unpin,
{
    type Item = Result<Bytes, E>;

    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        // Break stream on last chunk
        if self.current_chunk_index == self.chunk_count {
            return std::task::Poll::Ready(None);
        }

        let (current_chunk_index, next_chunk_index) = self.next_chunk();
        let range_start = current_chunk_index * CHUNK_SIZE;
        let range_end = next_chunk_index * CHUNK_SIZE;
        // Truncate last chunk
        let range_end = range_end.min(self.data.len());
        let current_range = range_start..range_end;
        let current_chunk = self.data.slice(current_range);

        std::task::Poll::Ready(Some(Ok(current_chunk)))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let remain = self.chunk_count - self.current_chunk_index;
        (remain, Some(remain))
    }
}

pub trait DataStreamer {
    fn fetch_data(&self) -> (Bytes, Range<usize>);

    fn data_stream<E>(&self) -> Box<dyn Stream<Item = Result<Bytes, E>> + Send + Sync + Unpin>
    where
        E: StdError + Sync + Send + Unpin + 'static,
    {
        let (data, data_range) = self.fetch_data();
        let stream = DataStream::new(&data, data_range);

        Box::new(stream)
    }
}

impl DataStreamer for (Bytes, Range<usize>) {
    fn fetch_data(&self) -> (Bytes, Range<usize>) {
        (self.0.clone(), self.1.clone())
    }
}

impl DataStreamer for Bytes {
    fn fetch_data(&self) -> (Bytes, Range<usize>) {
        let range = 0..self.len();
        (self.clone(), range)
    }
}

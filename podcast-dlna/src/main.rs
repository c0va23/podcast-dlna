#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![deny(clippy::print_stdout)]
#![deny(clippy::dbg_macro)]
#![warn(clippy::cargo)]
#![allow(clippy::non_ascii_literal)]
#![warn(clippy::nursery)]

#[macro_use]
extern crate log;

use std::error::Error;

mod app;
mod assets;
mod server;
mod utils;

fn init_tracing() -> Result<(), Box<dyn Error>> {
    use tracing_subscriber::prelude::*;
    let env_filter = tracing_subscriber::filter::EnvFilter::from_default_env();

    let logfmt_layer = utils::log::LogfmtLayer::new(std::io::stderr());

    tracing_subscriber::registry()
        .with(env_filter)
        .with(logfmt_layer)
        .try_init()?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    use clap::Parser;

    init_tracing()?;

    let app = app::App::parse();
    debug!("Args {:?}", app);

    if let Err(err) = app.start().await {
        error!("App error: {}", err);
        return Err(err);
    }

    Ok(())
}

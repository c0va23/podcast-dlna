use std::net::{IpAddr, SocketAddr};
use std::time::Duration;

use async_trait::async_trait;
use socket2::{Domain, Protocol, SockAddr, Socket, TcpKeepalive, Type};
use thiserror::Error as ThisError;
use tokio::net::TcpListener;
use tokio::sync::RwLock;

use crate::utils::parse_iso8601_duration;

#[derive(Debug, clap::Parser)]
pub struct Config {
    /// HTTP server listen address (IP). It address advertised via SSDP notifications.
    /// Not set 0.0.0.0 or ::, its broke SSDP advertising.
    #[clap(
        name = "http-server-bind-addr",
        long = "http-server-bind-addr",
        env = "HTTP_SERVER_BIND_ADDR"
    )]
    bind_addr: std::net::IpAddr,

    /// HTTP server listen port. 0 - random free port.
    /// Full server address advertised by SSDP.
    /// Using random port minimize probability of port conflict.
    #[clap(
        name = "http-server-bind-port",
        long = "http-server-bind-port",
        env = "HTTP_SERVER_BIND_PORT",
        default_value = "0"
    )]
    bind_port: u16,

    /// HTTP server keep alive.
    #[clap(
        name = "http-server-keep-alive",
        long = "http-server-keep-alive",
        env = "HTTP_SERVER_KEEP_ALIVE",
        default_value = "PT15S",
        parse(try_from_str = parse_iso8601_duration),
    )]
    keep_alive: Duration,

    /// HTTP server read timeout.
    #[clap(
        name = "http-server-read-timeout",
        long = "http-server-read-timeout",
        env = "HTTP_SERVER_READ_TIMEOUT",
        default_value = "PT1M",
        parse(try_from_str = parse_iso8601_duration),
    )]
    read_timeout: Duration,

    /// HTTP server write timeout.
    #[clap(
        name = "http-server-write-timeout",
        long = "http-server-write-timeout",
        env = "HTTP_SERVER_WRITE_TIMEOUT",
        default_value = "PT1M",
        parse(try_from_str = parse_iso8601_duration),
    )]
    write_timeout: Duration,

    /// HTTP server socket linger timeout.
    /// If not set then linger not configured.
    #[clap(
        name = "http-server-linger-timeout",
        long = "http-server-linger-timeout",
        env = "HTTP_SERVER_LINGER_TIMEOUT",
        parse(try_from_str = parse_iso8601_duration),
    )]
    linger_timeout: Option<Duration>,

    /// HTTP server socket reuse address.
    /// Useful if you need to quickly restart a service with a fixed port.
    #[clap(
        name = "http-server-reuse-address",
        long = "http-server-reuse-address",
        env = "HTTP_SERVER_REUSE_ADDRESS"
    )]
    reuse_address: bool,

    /// HTTP server write timeout.
    #[clap(
        name = "http-server-backlog",
        long = "http-server-backlog",
        env = "HTTP_SERVER_BACKLOG",
        default_value = "10"
    )]
    backlog: i32,
}

#[derive(ThisError, Debug)]
pub enum Error {
    #[error("Bind HTTP listener error: {0}")]
    Bind(#[from] std::io::Error),

    #[error("Listener not started")]
    ListenerNotStarted,

    #[error("Listener already started")]
    ListenerAlreadyStarted,
}

#[async_trait]
pub trait Locator: Sync + Send {
    async fn local_addr(&self) -> Result<SocketAddr, Error>;
}

pub struct Builder {
    config: Config,
    local_addr: RwLock<Option<SocketAddr>>,
}

impl Builder {
    pub(crate) fn new(config: Config) -> Self {
        let local_addr = RwLock::new(None);

        Self { config, local_addr }
    }

    pub(crate) async fn build(&self) -> Result<tokio::net::TcpListener, Error> {
        let mut local_adder = self.local_addr.write().await;

        if local_adder.is_some() {
            return Err(Error::ListenerAlreadyStarted);
        }

        let socket_addr: SockAddr =
            SocketAddr::new(self.config.bind_addr, self.config.bind_port).into();
        let domain = match self.config.bind_addr {
            IpAddr::V4(_) => Domain::IPV4,
            IpAddr::V6(_) => Domain::IPV6,
        };

        let socket = Socket::new(domain, Type::STREAM, Some(Protocol::TCP))?;

        socket.set_tcp_keepalive(&TcpKeepalive::new().with_time(self.config.keep_alive))?;

        socket.set_read_timeout(Some(self.config.read_timeout))?;
        socket.set_write_timeout(Some(self.config.write_timeout))?;
        socket.set_nonblocking(true)?;
        socket.set_linger(self.config.linger_timeout)?;
        socket.set_reuse_address(self.config.reuse_address)?;

        socket.bind(&socket_addr)?;

        socket.listen(self.config.backlog)?;

        *local_adder = socket.local_addr()?.as_socket();

        let listener = TcpListener::from_std(socket.into())?;

        Ok(listener)
    }
}

#[async_trait]
impl Locator for Builder {
    async fn local_addr(&self) -> Result<SocketAddr, Error> {
        self.local_addr
            .read()
            .await
            .ok_or(Error::ListenerNotStarted)
    }
}

use std::error::Error;
use std::sync::Arc;
use std::time::Duration;

use crate::utils::parse_iso8601_duration;

mod body;
mod listener;
mod router;

// Reexport

pub use listener::{
    Builder as HttpListenerBuilder, Config as HttpListenerConfig, Error as HttpListenerError,
    Locator as HttpListenerLocator,
};
pub use router::{
    ConnectionManagerHandler, ContentDirectoryHandler, DeviceDescriptionHandler, HandlerResult,
    IconsHandler, PicturesHandler, ProxyHandler, Router, PROXY_PREFIX,
};

#[derive(Debug, Clone, clap::Parser)]
pub struct Config {
    /// HTTP server stopping timeout.
    /// Force stopping after timeout when server have active connection.
    #[clap(
        name = "http-server-stop-timeout",
        long = "http-server-stop-timeout",
        env = "HTTP_SERVER_STOP_TIMEOUT",
        parse(try_from_str = parse_iso8601_duration),
    )]
    stop_timeout: Option<Duration>,

    /// HTTP server keep alive.
    #[clap(
        name = "http-server-keep-alive-time",
        long = "http-server-keep-alive-time",
        env = "HTTP_SERVER_KEEP_ALIVE_TIME",
        parse(try_from_str = parse_iso8601_duration),
    )]
    keep_alive: Option<Duration>,
}

#[derive(Clone)]
pub struct Server {
    config: Config,
    router: router::Router,
    listener_builder: Arc<listener::Builder>,
    close_notify: Arc<tokio::sync::Notify>,
    stop_notify: Arc<tokio::sync::Notify>,
}

impl Server {
    pub(crate) fn new(
        config: Config,
        router: router::Router,
        listener_builder: Arc<listener::Builder>,
    ) -> Self {
        let close_notify = Arc::new(tokio::sync::Notify::new());
        let stop_notify = Arc::new(tokio::sync::Notify::new());

        Self {
            config,
            router,
            listener_builder,
            close_notify,
            stop_notify,
        }
    }

    pub(crate) async fn start(&self) -> Result<(), Box<dyn Error>> {
        use futures::StreamExt;

        let listener = self.listener_builder.build().await?;
        let routes = self.router.routes();

        let close_notify = self.close_notify.clone();
        let stop_notify = self.stop_notify.clone();

        let socket_addr = listener.local_addr()?;
        info!("Start server listener on {}", socket_addr);

        let keep_alive = self.config.keep_alive;

        let listener =
            tokio_stream::wrappers::TcpListenerStream::new(listener).map(move |conn_result| {
                conn_result.and_then(move |conn| {
                    let conn2: socket2::Socket = conn.into_std()?.into();

                    if let Some(keep_alive) = keep_alive {
                        let ka_params = socket2::TcpKeepalive::new().with_time(keep_alive);
                        conn2.set_tcp_keepalive(&ka_params)?;
                    }

                    tokio::net::TcpStream::from_std(conn2.into())
                })
            });

        let server = warp::serve(routes);
        let server_stop = server.serve_incoming_with_graceful_shutdown(listener, async move {
            close_notify.clone().notified().await;
        });

        tokio::spawn(async move {
            server_stop.await;
            stop_notify.notify_one();
        });

        Ok(())
    }

    pub(crate) async fn stop(&self) {
        info!("HTTP server begin graceful shutdown");
        self.close_notify.clone().notify_one();

        if let Some(stop_timeout) = self.config.stop_timeout {
            let stop_notify = self.stop_notify.clone();
            if let Err(err) = tokio::time::timeout(stop_timeout, async move {
                stop_notify.notified().await;
            })
            .await
            {
                warn!("HTTP server stop timeout: {}", err);
            }
        } else {
            self.stop_notify.notified().await;
        }

        info!("HTTP server stopped");
    }
}

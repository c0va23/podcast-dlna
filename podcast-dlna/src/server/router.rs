use std::error::Error;
use std::sync::Arc;

use warp::{filters::BoxedFilter, Filter};

use crate::utils::device_info::UserAgentFetcher;
use upnp::media_server::content_directory;

use super::body;

pub const PROXY_PREFIX: &str = "proxy";

pub type HandlerResult = Result<Box<dyn warp::Reply>, std::convert::Infallible>;

#[async_trait::async_trait]
pub trait ContentDirectoryHandler: Sync + Send {
    async fn description(self: Arc<Self>) -> HandlerResult;

    async fn control(
        self: Arc<Self>,
        soap_action: String,
        envelope: content_directory::Envelope,
    ) -> HandlerResult;
}

#[async_trait::async_trait]
pub trait DeviceDescriptionHandler: Sync + Send {
    async fn device_describe(self: Arc<Self>) -> HandlerResult;
}

#[async_trait::async_trait]
pub trait PicturesHandler: Sync + Send {
    async fn handle_track(self: Arc<Self>, channel_id: String, track_id: String) -> HandlerResult;

    async fn handle_channel(self: Arc<Self>, channel_id: String) -> HandlerResult;
}

#[async_trait::async_trait]
pub trait ConnectionManagerHandler: Sync + Send {
    async fn description(self: Arc<Self>) -> HandlerResult;

    async fn control(self: Arc<Self>) -> HandlerResult;
}

#[async_trait::async_trait]
pub trait IconsHandler: Sync + Send {
    async fn icon24(self: Arc<Self>) -> HandlerResult;

    async fn icon32(self: Arc<Self>) -> HandlerResult;
}

#[async_trait::async_trait]
pub trait ProxyHandler: Sync + Send {
    async fn handle(
        self: Arc<Self>,
        method: hyper::Method,
        channel_id: String,
        track_id_with_extension: String,
        headers: http::HeaderMap<http::HeaderValue>,
    ) -> Result<Box<dyn warp::Reply>, warp::Rejection>;
}

#[derive(Clone)]
pub struct Router {
    content_directory_handler: Arc<dyn ContentDirectoryHandler>,
    device_description_handler: Arc<dyn DeviceDescriptionHandler>,
    picture_handler: Arc<dyn PicturesHandler>,
    connection_manager_handler: Arc<dyn ConnectionManagerHandler>,
    icons_handler: Arc<dyn IconsHandler>,
    proxy: Arc<dyn ProxyHandler>,
    user_agent: String,
}

// pub(crate) implementation
impl Router {
    pub(crate) fn new<UA>(
        content_directory_handler: Arc<dyn ContentDirectoryHandler>,
        device_description_handler: Arc<dyn DeviceDescriptionHandler>,
        picture_handler: Arc<dyn PicturesHandler>,
        connection_manager_handler: Arc<dyn ConnectionManagerHandler>,
        icons_handler: Arc<dyn IconsHandler>,
        proxy: Arc<dyn ProxyHandler>,
        user_agent_fetcher: &UA,
    ) -> Result<Self, Box<dyn Error>>
    where
        UA: UserAgentFetcher,
    {
        let user_agent = user_agent_fetcher.user_agent()?;
        Ok(Self {
            content_directory_handler,
            device_description_handler,
            picture_handler,
            connection_manager_handler,
            icons_handler,
            proxy,
            user_agent,
        })
    }

    pub(crate) fn routes(&self) -> BoxedFilter<(impl warp::Reply,)> {
        self.root_path()
            .or(self.content_directory_description_path())
            .or(self.content_directory_control_path())
            .or(self.track_picture_path())
            .or(self.channel_picture_path())
            .or(self.proxy_path())
            .or(self.connection_manager_description_path())
            .or(self.connection_manager_control_path())
            .or(self.icon24_path())
            .or(self.icon32_path())
            .with(self.server_header())
            .with(warp::trace(Self::trace))
            .boxed()
    }
}

// private implementation
impl Router {
    fn root_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path("root.xml")
            .and(warp::get())
            .and(Self::with(self.device_description_handler.clone()))
            .and_then(DeviceDescriptionHandler::device_describe)
            .with(Self::xml_content_type_header())
            .boxed()
    }

    fn content_directory_description_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("content-directory" / "description.xml")
            .and(warp::get())
            .and(Self::with(self.content_directory_handler.clone()))
            .and_then(ContentDirectoryHandler::description)
            .with(Self::xml_content_type_header())
            .boxed()
    }

    fn content_directory_control_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("content-directory" / "control.xml")
            .and(warp::post())
            .and(Self::with(self.content_directory_handler.clone()))
            .and(Self::soap_action_header())
            .and(body::xml::<content_directory::Envelope>())
            .and_then(ContentDirectoryHandler::control)
            .with(Self::xml_content_type_header())
            .with(Self::ext_header())
            .boxed()
    }

    fn proxy_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::any()
            .and(warp::path(PROXY_PREFIX.to_string()))
            .and(Self::with(self.proxy.clone()))
            .and(warp::method())
            .and(warp::path::param::<String>())
            .and(warp::path::param::<String>())
            .and(warp::header::headers_cloned())
            .and_then(ProxyHandler::handle)
            .boxed()
    }

    fn with<V>(value: Arc<V>) -> BoxedFilter<(Arc<V>,)>
    where
        V: Sync + Send + ?Sized + 'static,
    {
        warp::any().map(move || value.clone()).boxed()
    }

    fn track_picture_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::get()
            .and(Self::with(self.picture_handler.clone()))
            .and(warp::path!("pictures" / String / String))
            .and_then(PicturesHandler::handle_track)
            .boxed()
    }

    fn channel_picture_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::get()
            .and(Self::with(self.picture_handler.clone()))
            .and(warp::path!("pictures" / String))
            .and_then(PicturesHandler::handle_channel)
            .boxed()
    }

    fn connection_manager_description_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("connection-manager" / "description.xml")
            .and(warp::get())
            .and(Self::with(self.connection_manager_handler.clone()))
            .and_then(ConnectionManagerHandler::description)
            .with(Self::xml_content_type_header())
            .boxed()
    }

    fn connection_manager_control_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("connection-manager" / "control.xml")
            .and(warp::post())
            .and(Self::with(self.connection_manager_handler.clone()))
            .and_then(ConnectionManagerHandler::control)
            .with(Self::xml_content_type_header())
            .with(Self::ext_header())
            .boxed()
    }

    fn icon24_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("icons" / "icon24.png")
            .and(warp::get())
            .and(Self::with(self.icons_handler.clone()))
            .and_then(IconsHandler::icon24)
            .with(Self::png_content_type_header())
            .boxed()
    }

    fn icon32_path(&self) -> BoxedFilter<(impl warp::Reply,)> {
        warp::path!("icons" / "icon32.png")
            .and(warp::get())
            .and(Self::with(self.icons_handler.clone()))
            .and_then(IconsHandler::icon32)
            .with(Self::png_content_type_header())
            .boxed()
    }

    fn soap_action_header() -> BoxedFilter<(String,)> {
        warp::filters::header::header("SOAPACTION").boxed()
    }

    fn xml_content_type_header() -> warp::filters::reply::WithHeader {
        warp::reply::with::header("Content-Type", "text/xml; charset=\"utf-8\"")
    }

    fn png_content_type_header() -> warp::filters::reply::WithHeader {
        warp::reply::with::header("Content-Type", "image/png")
    }

    fn ext_header() -> warp::filters::reply::WithHeader {
        warp::reply::with::header("EXT", "")
    }

    fn server_header(&self) -> warp::filters::reply::WithHeader {
        warp::reply::with::header("Server", self.user_agent.clone())
    }

    #[allow(clippy::needless_pass_by_value)]
    fn trace(info: warp::filters::trace::Info) -> tracing::Span {
        use tracing::field::display;

        let request_id = uuid::Uuid::new_v4();

        let span = tracing::info_span!(
            "http-request",
            method = %info.method(),
            path = %info.path(),
            version  = ?info.version(),
            %request_id,
        );

        if let Some(client_addr) = info.remote_addr() {
            span.record("client_addr", &display(client_addr));
        }

        if let Some(user_agent) = info.user_agent() {
            span.record("user_agent", &user_agent);
        }

        span
    }
}

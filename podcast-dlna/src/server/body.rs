use bytes::Buf;
use std::error::Error as StdError;
use std::io::Read;
use warp::Filter;
use yaserde::YaDeserialize;

struct XmlBody;

#[derive(Debug)]
struct InvalidXmlBoxy {
    message: String,
}

impl std::fmt::Display for InvalidXmlBoxy {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Invalid XML body: {}", self.message)
    }
}

impl StdError for InvalidXmlBoxy {}

impl warp::reject::Reject for InvalidXmlBoxy {}

impl XmlBody {
    fn decode<R: Read, T: YaDeserialize>(reader: R) -> Result<T, InvalidXmlBoxy> {
        yaserde::de::from_reader(reader).map_err(|err| InvalidXmlBoxy { message: err })
    }
}

pub(super) fn xml<T>() -> impl Filter<Extract = (T,), Error = warp::reject::Rejection> + Copy
where
    T: YaDeserialize,
{
    warp::filters::body::bytes().and_then(|buf: bytes::Bytes| async {
        let reader = buf.reader();
        XmlBody::decode(reader).map_err(warp::reject::custom)
    })
}

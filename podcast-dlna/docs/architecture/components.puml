@startuml Podcast DLNA components

skinparam Nodesep 150
skinparam Ranksep 200

package utils {
    component SignalWaiter
}

package utils::device_info {
    interface UserAgentFetcher
    interface DeviceUuidFetcher

    component DeviceInfo

    UserAgentFetcher -- DeviceInfo
    DeviceUuidFetcher -- DeviceInfo
}

package ssdp::data {
    interface SsdpDataBuilder
}

package ssdp::listener {
    component SsdpListener
    component SsdpListenerStarter

    SsdpListenerStarter -> SsdpListener
    SsdpListenerStarter --( SsdpDataBuilder
}

package ssdp::notifier {
    component SsdpNotifier
    component SsdpNotifierStarter

    SsdpNotifierStarter -> SsdpNotifier
    SsdpNotifierStarter --( SsdpDataBuilder
}

package app::podcast {
    interface ChannelFetcher
    component RssChannelFetcher

    RssChannelFetcher - ChannelFetcher

    component CachedChannelFetcher

    CachedChannelFetcher -up-( ChannelFetcher : impl
    CachedChannelFetcher -up- ChannelFetcher : use
}

package upnp::media_server::content_directory {
    interface Backend as ContentDirectoryBackend

    component ContentDirectory

    ContentDirectory -( ContentDirectoryBackend
}

package app::preflighter {
    interface Preflighter

    component HttpPreflighter

    HttpPreflighter - Preflighter
    component CachedPreflighter

    CachedPreflighter -up-( Preflighter : impl
    CachedPreflighter -up- Preflighter : use
}

package app::services::feed_fetcher {
    interface UrlEncoder
    component Sha2UrlEncoder

    Sha2UrlEncoder - UrlEncoder

    interface ImageFetcher
    component ReqwestImageFetcher

    ReqwestImageFetcher - ImageFetcher

    component CachedImageFetcher
    CachedImageFetcher -up-( ImageFetcher : impl
    CachedImageFetcher -up- ImageFetcher : use


    interface FeedFetcher

    component PodcastFetcher

    PodcastFetcher - FeedFetcher

    PodcastFetcher --( ChannelFetcher
    PodcastFetcher --( Preflighter
    PodcastFetcher --( UrlEncoder
    PodcastFetcher --( ImageFetcher
}

package server::handlers {
    component DeviceHandler

    DeviceHandler --( DeviceUuidFetcher

    component ContentDirectoryHandler

    ContentDirectoryHandler --> ContentDirectory

    component ConnectionManagerHandler

    component IconsHandler
    component PicturesHandler
    PicturesHandler --( FeedFetcher
}

package server::proxy::batch {
    interface Requester

    component ReqwestRequester
    component ContinuableRequester

    ReqwestRequester - Requester
    ContinuableRequester -up-( Requester : impl
    ContinuableRequester -up- Requester : use


    interface BatchStreamBuilder
    component RemoteStreamBuilder

    RemoteStreamBuilder - BatchStreamBuilder
    RemoteStreamBuilder --( Requester
}

package server::proxy {
    component Proxy

    Proxy --( BatchStreamBuilder
    Proxy --( FeedFetcher
    Proxy --( Preflighter
}

package server::router {
    component Router

    Router --> ContentDirectoryHandler
    Router --> ConnectionManagerHandler
    Router --> DeviceHandler
    Router --> Proxy
    Router --> PicturesHandler
    Router --> IconsHandler

    Router --( UserAgentFetcher
}

package server::listener {
    interface HttpListenerLocator

    component HttpListenerBuilder

    HttpListenerBuilder - HttpListenerLocator
}

package server {
    component HttpServer

    HttpServer --> HttpListenerBuilder

    HttpServer --> Router
}

package app::config {
    component Config
}

package app::title_rewriter {
    interface TitleRewriter
    component RegexTitleRewriter
    component PassTitleRewriter

    RegexTitleRewriter - TitleRewriter
    PassTitleRewriter - TitleRewriter

    interface ChannelTitleRewriter
    component DefaultChannelTitleRewriter

    DefaultChannelTitleRewriter - ChannelTitleRewriter
    DefaultChannelTitleRewriter --> RegexTitleRewriter
    DefaultChannelTitleRewriter --> PassTitleRewriter
    DefaultChannelTitleRewriter -up-> Config : use
}

package app::ssdp_data {
    component LazySsdpDataBuilder

    SsdpDataBuilder - LazySsdpDataBuilder

    LazySsdpDataBuilder --( HttpListenerLocator
    LazySsdpDataBuilder --( UserAgentFetcher
    LazySsdpDataBuilder --( DeviceUuidFetcher
}

package app::content_directory {
    component PodcastBackend

    PodcastBackend -up- ContentDirectoryBackend

    PodcastBackend --( HttpListenerLocator
    PodcastBackend --( ChannelTitleRewriter
    PodcastBackend --( FeedFetcher
}

package app {
    component App
    component Args

    App -> Args

    App --> HttpServer : start / stop
    App --> SsdpListenerStarter : start / stop
    App --> SsdpNotifierStarter : start / stop
    App --> Config : create
}

component Main

Main --> Args : parse
Main --> SignalWaiter
Main --> App : start / stop
@enduml

*** Settings ***

Resource  common.resource

Library  XML
Library  RequestsLibrary
Library  e2e.lib.UpnpLibrary
...  current_device_location=${PODCAST_DLNA_URL}/root.xml
...  WITH NAME  UpnpLibrary

Suite Setup  Start PodcastDLNA with examples
Suite Teardown  Stop Podcast DNLA

Test Timeout  ${TEST_TIMEOUT}

*** Variables ***

${TEST_TIMEOUT}  10

*** Test Cases ***
Podcast DNLA discovery
  UpnpLibrary.Upnp discover
  UpnpLibrary.Upnp select device  ${PODCAST_DNAL_FRIENDLY_NAME}
  UpnpLibrary.Upnp device have type  urn:schemas-upnp-org:device:MediaServer:1

Podcast DNLA check default device type
  UpnpLibrary.Upnp device have type  urn:schemas-upnp-org:device:MediaServer:1

Podcast DNLA list container with object_id 0
  Select Didl Item By ObjectID  0
  UpnpLibrary.Upnp select didl container  title=DevZen Podcast

Podcast DNLA list container with empty object_id
  Select Didl Item By ObjectID  ${EMPTY}
  UpnpLibrary.Upnp select didl container  title=Радио-Т

Podcast DNLA open container
  Select Didl Item By ObjectID  ${EMPTY}
  ${container} =  UpnpLibrary.Upnp select didl container  index=0
  BuiltIn.Log  ${container.title}
  BuiltIn.Should Start With  ${container.album_art_uri}  ${PODCAST_DLNA_URL}
  ${channel_cover_response} =  RequestsLibrary.GET  ${container.album_art_uri}
  RequestsLibrary.Status Should Be  200  response=${channel_cover_response}
  BuiltIn.Should Match Regexp  ${channel_cover_response.headers['Content-Type']}  image/(jpeg|png)
  Select Didl Item By ObjectID  object_id=${container.id}
  ${item} =  UpnpLibrary.Upnp select didl container  index=0
  BuiltIn.Log  ${item.title}
  BuiltIn.Log  ${item.resources[0].uri}
  BuiltIn.Should Not Be Empty  ${item.title}
  BuiltIn.Should Start With  ${item.album_art_uri}  ${PODCAST_DLNA_URL}
  ${track_cover_response} =  RequestsLibrary.GET  ${item.album_art_uri}
  RequestsLibrary.Status Should Be  200  response=${track_cover_response}
  BuiltIn.Should Match Regexp  ${track_cover_response.headers['Content-Type']}  image/(jpeg|png)

Podcast DNLA connection manager
  [Tags]  fast
  RequestsLibrary.POST  ${PODCAST_DLNA_URL}/connection-manager/control.xml  expected_status=501

  ${description_response} =
  ...  RequestsLibrary.GET  ${PODCAST_DLNA_URL}/connection-manager/description.xml
  BuiltIn.Should Be Equal
  ...  ${description_response.headers['Content-Type']}
  ...  text/xml; charset="utf-8"

  ${description_xml} =  XML.Parse Xml  ${description_response.text}

  XML.Element Attribute Should Be  ${description_xml}  xmlns  urn:schemas-upnp-org:service-1-0

  XML.Element Text Should Be  ${description_xml}  1  xpath=specVersion/major
  XML.Element Text Should Be  ${description_xml}  0  xpath=specVersion/minor

  ${action_list} =  XML.Get Child Elements  ${description_xml}  xpath=actionList
  ${action_list_count} =  BuiltIn.Get Length  ${action_list}
  BuiltIn.Should Be Equal As Integers  ${action_list_count}  0

  ${service_state_table} =  XML.Get Child Elements  ${description_xml}  xpath=serviceStateTable
  ${service_state_table_count} =  BuiltIn.Get Length  ${service_state_table}
  BuiltIn.Should Be Equal As Integers  ${service_state_table_count}  0

Podcast DNLA icons
  [Tags]  fast

  ${icon_response} =   RequestsLibrary.GET  ${PODCAST_DLNA_URL}/icons/icon24.png
  BuiltIn.Should Be Equal  ${icon_response.headers['Content-Type']}  image/png

  ${icon_response} =   RequestsLibrary.GET  ${PODCAST_DLNA_URL}/icons/icon32.png
  BuiltIn.Should Be Equal  ${icon_response.headers['Content-Type']}  image/png

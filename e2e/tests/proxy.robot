*** Settings ***

Resource  common.resource
Library  RequestsLibrary
Library  e2e.lib.UpnpLibrary
...  current_device_location=${PODCAST_DLNA_URL}/root.xml
...  WITH NAME  UpnpLibrary

Suite Setup  Start Podcast DLNA and create session
Suite Teardown  Stop Podcast DNLA

Test Timeout  ${TEST_TIMEOUT}


*** Variables ***

${REQUEST_TIMEOUT}  30
${TEST_TIMEOUT}  60


*** Test Cases ***

Proxy Get start range (small)
  Proxy Get start range  ${{2 ** 10}}

Proxy Get start range (mid)
  Proxy Get start range  ${{2 ** 16}}

Proxy Get start range (long)
  Proxy Get start range  ${{10 ** 6}}

Proxy Get mid range
  ${response} =  Perform proxy request  range_start=1000001  range_end=1100000
  RequestsLibrary.Status Should Be  206  ${response}
  BuiltIn.Length Should Be  ${response.content}  100000
  BuiltIn.Should Match Regexp  ${response.headers['Content-Range']}  bytes 1000001-1100000/\\d{6,}
  BuiltIn.Should Match Regexp  ${response.headers['Content-Length']}  100000
  BuiltIn.Should Be Equal  ${response.headers['Content-Type']}  audio/mpeg
  BuiltIn.Should Not Be Empty  ${response.headers['Last-Modified']}
  BuiltIn.Should Not Be Empty  ${response.headers['Date']}

Proxy Get Not Found Resource
  ${response} =  RequestsLibrary.GET On Session  proxy  /proxy/not_found/not_found.mp3
  ...  expected_status=404
  RequestsLibrary.Status Should Be  404  ${response}
  BuiltIn.Should Be Equal  ${response.headers['Content-Length']}  0
  BuiltIn.Should Be Empty  ${response.content}

Proxy Head without range
  ${response} =  RequestsLibrary.HEAD On Session  proxy  ${MP3_URL}
  RequestsLibrary.Status Should Be  200  ${response}
  BuiltIn.Should Be Empty  ${response.content}
  BuiltIn.Should Match Regexp  ${response.headers['Content-Length']}  \\d{6,}
  BuiltIn.Should Be Equal  ${response.headers['Content-Type']}  audio/mpeg
  BuiltIn.Should Not Be Empty  ${response.headers['Last-Modified']}
  BuiltIn.Should Not Be Empty  ${response.headers['Date']}

Proxy Get end range (short)
  Proxy Get end range  ${{2**10}}

Proxy Get end range (mid)
  Proxy Get end range  ${{2**16}}

Proxy Get end range (long)
  Proxy Get end range  ${{10**6}}

# Proxy Get Full

Proxy Get Invalid end
  ${probe_response} =  RequestsLibrary.HEAD On Session  proxy  ${MP3_URL}
  ${content_length} =  BuiltIn.Convert To Integer  ${probe_response.headers['Content-Length']}
  &{headers} =  Create Dictionary  Range=bytes=0-${{${content_length} + 1}}
  ${response} =  RequestsLibrary.GET On Session  proxy  ${MP3_URL}
  ...  expected_status=416
  ...  headers=&{headers}
  ...  timeout=5
  RequestsLibrary.Status Should Be  416  ${response}

Proxy Head Invalid end
  ${probe_response} =  RequestsLibrary.HEAD On Session  proxy  ${MP3_URL}
  ${content_length} =  BuiltIn.Convert To Integer  ${probe_response.headers['Content-Length']}
  &{headers} =  Create Dictionary  Range=bytes=0-${{${content_length} + 1}}
  ${response} =  RequestsLibrary.Head On Session  proxy  ${MP3_URL}
  ...  expected_status=416
  ...  headers=&{headers}
  ...  timeout=5
  RequestsLibrary.Status Should Be  416  ${response}


*** Keywords ***

Find podcast URL
  Select Didl Item By ObjectID  ${EMPTY}
  ${container} =  UpnpLibrary.Upnp select didl container  index=0
  BuiltIn.Log  ${container.title}
  Select Didl Item By ObjectID  object_id=${container.id}
  ${item} =  UpnpLibrary.Upnp select didl container  index=0
  BuiltIn.Log  ${item.title}
  ${mp3_url} =  Set Variable  ${item.resources[0].uri}
  BuiltIn.Log  ${mp3_url}
  [Return]  ${mp3_url}

Start Podcast DLNA and create session
  Start PodcastDLNA with examples
  RequestsLibrary.Create Session  proxy  ${PODCAST_DLNA_URL}
  ${url} =  Find podcast URL
  Set Suite Variable  ${MP3_URL}  ${url}

Perform proxy request
  [Arguments]  ${range_start}=0  ${range_end}=
  &{headers} =  Create Dictionary  Range=bytes=${range_start}-${range_end}
  ${response} =  RequestsLibrary.GET On Session  proxy  ${MP3_URL}
  ...  headers=&{headers}
  ...  stream=False
  ...  timeout=${REQUEST_TIMEOUT}
  [Return]  ${response}

Proxy Get end range
  [Arguments]  ${range_length}
  ${probe_response} =  RequestsLibrary.HEAD On Session  proxy  ${MP3_URL}
  ${content_length} =  BuiltIn.Convert To Integer  ${probe_response.headers['Content-Length']}
  ${range_start} =  Set Variable  ${{${content_length} - ${range_length}}}
  ${range_end} =  Set Variable  ${{${content_length} - 1}}
  ${response} =  Perform proxy request  range_start=${range_start}  range_end=${range_end}
  RequestsLibrary.Status Should Be  206  ${response}
  BuiltIn.Should Be Equal
  ...  ${response.headers['Content-Range']}
  ...  bytes ${range_start}-${range_end}/${content_length}
  ...  msg=Should return valid Content-Range header
  BuiltIn.Should Be Equal As Integers  ${response.headers['Content-Length']}  ${range_length}
  ...  msg=Should return valid Content-Length header
  BuiltIn.Should Be Equal  ${response.headers['Content-Type']}  audio/mpeg
  ...  msg=Should return valid Content-Type header
  BuiltIn.Should Not Be Empty  ${response.headers['Last-Modified']}
  BuiltIn.Should Not Be Empty  ${response.headers['Date']}
  BuiltIn.Length Should Be  ${response.content}  ${range_length}

Proxy Get start range
  [Arguments]  ${range_length}
  ${range_end} =  Set Variable  ${{${range_length} - 1}}
  ${response} =  Perform proxy request  range_end=${range_end}
  RequestsLibrary.Status Should Be  206  ${response}
  BuiltIn.Length Should Be  ${response.content}  ${range_length}
  BuiltIn.Should Match Regexp  ${response.headers['Content-Range']}  bytes 0-${range_end}/\\d{6,}
  BuiltIn.Should Be Equal As Integers  ${response.headers['Content-Length']}  ${range_length}
  BuiltIn.Should Be Equal  ${response.headers['Content-Type']}  audio/mpeg
  BuiltIn.Should Not Be Empty  ${response.headers['Last-Modified']}
  BuiltIn.Should Not Be Empty  ${response.headers['Date']}

*** Settings ***

Resource  common.resource


*** Variables ***

${ISO8601_PATTERN}  \\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}:\\d{2}\\+\\d{2}:\\d{2}
${GIT_HASH_PATTERN}  [0-9a-f]{7}
${SEMVER_PATTER}  \\d+\.\\d+\.\\d+(-[a-z]+\.\\d+)?


*** Test Cases ***

Podcast DLNA version
  ${podcast_version} =  Process.Run Process  ${PODCAST_DLNA_PATH}  --version
  BuiltIn.Should Match Regexp
  ...  ${podcast_version.stdout}
  ...  podcast-dlna ${SEMVER_PATTER} \\(${GIT_HASH_PATTERN}  ${ISO8601_PATTERN}\\)
  BuiltIn.Should Be Equal as Integers  0  ${podcast_version.rc}

Podcast DLNA help
  ${podcast_help} =  Process.Run Process  ${PODCAST_DLNA_PATH}  --help
  BuiltIn.Should Contain  ${podcast_help.stdout}  USAGE:
  BuiltIn.Should Contain  ${podcast_help.stdout}
  ...  podcast-dlna <SUBCOMMAND>
  BuiltIn.Should Contain  ${podcast_help.stdout}  SUBCOMMANDS
  BuiltIn.Should Contain  ${podcast_help.stdout}  server

Podcast DLNA server help
  ${podcast_help} =  Process.Run Process  ${PODCAST_DLNA_PATH}  server  --help
  BuiltIn.Should Contain  ${podcast_help.stdout}  USAGE:
  BuiltIn.Should Contain  ${podcast_help.stdout}
  ...  podcast-dlna server [OPTIONS] --http-server-bind-addr <http-server-bind-addr>

Podcast DLNA server error on missed config
  ${process} =  Process.Run Process  ${PODCAST_DLNA_PATH}  server
  ...  --http-server-bind-addr  127.0.0.1
  ...  env:RUST_LOG=debug

  BuiltIn.Should Be Equal As Integers  1  ${process.rc}
  BuiltIn.Log  ${process.stderr}
  BuiltIn.Should Contain  ${process.stderr}  Config file config.toml read error
  BuiltIn.Should Contain  ${process.stderr}  No such file or directory

Podcast DLNA server error on invalid config
  ${process} =  Process.Run Process  ${PODCAST_DLNA_PATH}  server
  ...  --http-server-bind-addr  127.0.0.1
  ...  --config-path  /etc/hostname

  BuiltIn.Should Be Equal As Integers  1  ${process.rc}
  BuiltIn.Should Contain  ${process.stderr}  Config parse error


Podcast DLNA server error on invalid args
  ${process} =  Process.Run Process  ${PODCAST_DLNA_PATH}  server
  ...  --http-server-bind-addr  127.0.0.1000

  BuiltIn.Should Be Equal As Integers  2  ${process.rc}
  BuiltIn.Should Contain  ${process.stderr}  invalid IP address syntax

Podcast DLNA server handle TERM signal
  ${process} =  Start PodcastDLNA
  Process.Process Should Be Running  handle=${process}

  Stop Podcast DNLA  process=${process}
  ${process_result} =  Process.Get Process Result  handle=${process}

  Process.Process Should Be Stopped  handle=${process}

  BuiltIn.Should Be Equal As Integers  0  ${process_result.rc}

Podcast DLNA server handle INT signal
  ${process} =  Start PodcastDLNA
  Process.Process Should Be Running  handle=${process}

  Stop Podcast DNLA  process=${process}  signal=INT
  ${process_result} =  Process.Get Process Result  handle=${process}

  Process.Process Should Be Stopped  handle=${process}

  BuiltIn.Should Be Equal As Integers  0  ${process_result.rc}

Podcast DLNA env-config
  ${env_config_process} =  Process.Run Process  ${PODCAST_DLNA_PATH}  env-config
  BuiltIn.Log  ${env_config_process.stderr}
  BuiltIn.Should Be Equal as Integers  0  ${env_config_process.rc}
  BuiltIn.Should Match Regexp  ${env_config_process.stdout}
  ...  (?m)^(?:##\\s.+\\n(?:#\\s)?[A-Z_0-9]+=.*(?:$|\\n))+

Podcast DLNA check config
  ${check_config} =  Process.Run Process  ${PODCAST_DLNA_PATH}  check-config
  ...  --config-path  examples/config.toml
  BuiltIn.Should Contain  ${check_config.stdout}  Configuration is valid
  BuiltIn.Should Be Equal as Integers  0  ${check_config.rc}

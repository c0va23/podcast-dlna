# pylint: disable=missing-module-docstring

from typing import Optional, Union, List, Dict

from upnpclient import ValidationError, Device, Service, Action
import upnpclient

from didl_lite import didl_lite
from didl_lite.didl_lite import DidlObject, Descriptor, Container

import robot.api.logger as logger

from robot.api.exceptions import Failure

ActionResult = Dict[str, any]

DidlItem = Union[DidlObject, Descriptor]

# pylint: disable=too-many-instance-attributes
class UpnpLibrary:
    # pylint: disable=missing-function-docstring
    # pylint: disable=missing-class-docstring

    _current_device_location: Optional[str]
    _current_device: Optional[Device] = None
    _current_service: Optional[Service] = None
    _current_action: Optional[Action] = None
    _last_action_result: Optional[ActionResult] = None
    _last_didl_items: list[DidlItem] = []
    _current_didl_container: Optional[Container] = None
    _discovered_devices: list[Device] = []

    ROBOT_LIBRARY_SCOPE = 'TEST'

    def __init__(self, timeout: int = 3, current_device_location: Optional[str] = None):
        self._timeout = timeout

        self._current_device_location = current_device_location


    def upnp_discover(self) -> None:
        self._discovered_devices = upnpclient.discover(self._timeout)


    def upnp_select_device(self, friendly_name: str) -> Device:
        device = _find_device_by_name(self._discovered_devices, friendly_name)

        if device is None:
            raise Failure(f'Device with name {friendly_name} not exists')

        self._current_device = device
        return device


    def upnp_device_have_type(self, device_type: str, device: Device = None) -> None:
        device = self._device(device)

        if device.device_type != device_type:
            raise Failure(f'Device with name {device.friendly_name} have type \
                {device.device_type} by expected {device_type}')


    def upnp_select_service(self, service_type: str, device: Device = None) -> Service:
        device = self._device(device)

        service = _find_service_by_type(device, service_type)

        if service is None:
            raise Failure(f'Device with name {device.friendly_name} should \
                have service {service_type} but not have')

        self._current_service = service
        return service


    def upnp_select_action(self, action_name: str, service: Service = None) -> Action:
        service = self._service(service)

        action = service.find_action(action_name)

        if action is None:
            raise Failure(f'Service with type {service.service_type} not have \
                action {action_name}')

        self._current_action = action
        return action

    def upnp_action_call(self, action: str = None, **kwargs) -> ActionResult:
        try:
            logger.info(kwargs)

            action = self._action(action)

            action_result = action(**kwargs)

            self._last_action_result = action_result
            return action_result
        except ValidationError as validation_error:
            raise Failure(f"Action call validation error: {validation_error.reasons}") \
                from validation_error
        except Exception as error:
            raise Failure(f"Unknown action call error: {error}") from error


    def upnp_parse_didl_lite(self, action_result: Optional[ActionResult] = None) -> List[DidlItem]:
        try:
            action_result = self._action_result(action_result)
            didl_items = didl_lite.from_xml_string(action_result['Result'])
            self._last_didl_items = didl_items
            return didl_items
        except Exception as error:
            raise Failure("Invalid didl-lite response") from error


    def upnp_select_didl_container(self,
                                   title: Optional[str] = None,
                                   index: Optional[int] = None,
                                   didl_items: Optional[List[DidlItem]] = None) -> DidlItem:
        didl_items = didl_items or self._last_didl_items
        container = None

        if title is not None:
            container = _find_didl_container_by_title_or_raise(didl_items, title)
        if index is not None:
            container = _find_didl_container_by_index_or_raise(didl_items, index)

        if container is None:
            raise Failure("title or index should be passed")

        self._current_didl_container = container

        return container


    def _device(self, device: Optional[Device]) -> Device:
        if device is not None:
            return device

        if self._current_device is not None:
            return self._current_device

        if self._current_device_location is not None:
            self._current_device = Device(self._current_device_location)
            return self._current_device

        raise Failure("Device not found")


    def _service(self, service: Optional[Service]) -> Service:
        if service is not None:
            return service

        if self._current_service is not None:
            return self._current_service

        raise Failure("Service not found")


    def _action(self, action: Optional[Action]) -> Action:
        if action is not None:
            return action

        if self._current_action is not None:
            return self._current_action

        raise Failure("Action not found")


    def _action_result(self, action_result: Optional[ActionResult]) -> ActionResult:
        if action_result is not None:
            return action_result

        if self._last_action_result is not None:
            return self._last_action_result

        raise Failure("Action result not found")


def _find_device_by_name(devices: List[Device], friendly_name: str) -> Optional[Device]:
    for device in devices:
        if device.friendly_name == friendly_name:
            return device

    return None


def _find_service_by_type(device: Device, service_type: str) -> Optional[Service]:
    for service in device.services:
        if service.service_type == service_type:
            return service

    return None


def _find_didl_container_by_title(containers: List[DidlItem], title: str) -> Optional[DidlItem]:
    for container in containers:
        if container.title == title:
            return container

    return None

def _find_didl_container_by_title_or_raise(containers: List[DidlItem], title: str) -> DidlItem:
    container = _find_didl_container_by_title(containers, title)

    if container is None:
        raise Failure(f"container with title '{title}' not found")

    return container

def _find_didl_container_by_index_or_raise(containers: List[DidlItem], index: int) -> DidlItem:
    try:
        return containers[index]
    except IndexError as error:
        raise Failure(f"container with index '{index}' not found") from error

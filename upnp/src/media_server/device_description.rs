use super::super::common::device_description::{
    Device, DeviceDescription, DeviceType, Icon, IconList, Service, ServiceList, SpecVersion, Udn,
};

pub struct IconConfig {
    pub size: u64,
    pub depth: u64,
    pub mime_type: String,
    pub url: String,
}

pub struct ServiceConfig {
    pub service_type: String,
    pub service_id: String,
    pub description_url: String,
    pub control_url: String,
    pub event_sub_url: Option<String>,
}

pub struct Config {
    pub name: String,
    pub manufacturer: String,
    pub device_uuid: uuid::Uuid,
    pub icons: Vec<IconConfig>,
    pub services: Vec<ServiceConfig>,
}

pub fn device_description(config: &Config) -> DeviceDescription {
    let icons = config
        .icons
        .iter()
        .map(|icon| Icon {
            depth: icon.depth,
            height: icon.size,
            width: icon.size,
            mimetype: icon.mime_type.clone(),
            url: icon.url.clone(),
        })
        .collect();
    DeviceDescription {
        spec_version: SpecVersion::new(1, 0),
        config_id: 1,
        device: Device {
            device_type: DeviceType::new("MediaServer".to_owned(), 1),
            friendly_name: config.name.clone(),
            manufacturer: config.manufacturer.clone(),
            manufacturer_url: None,
            model_description: None,
            model_name: config.name.clone(),
            model_number: None,
            model_url: None,
            serial_number: None,
            udn: Udn::new(config.device_uuid),
            upc: None,
            icon_list: IconList { icons },
            service_list: ServiceList {
                services: services(config),
            },
            device_list: Vec::new(),
            presentation_url: None,
        },
    }
}

fn services(config: &Config) -> Vec<Service> {
    config
        .services
        .iter()
        .map(|service_config| Service {
            service_type: service_config.service_type.clone(),
            service_id: service_config.service_id.clone(),
            scpd_url: service_config.description_url.clone(),
            control_url: service_config.control_url.clone(),
            event_sub_url: service_config.event_sub_url.clone().unwrap_or_default(),
        })
        .collect()
}

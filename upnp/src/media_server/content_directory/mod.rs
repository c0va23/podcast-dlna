use std::sync::Arc;

use async_trait::async_trait;

pub mod description;
pub mod requests;
pub mod responses;

pub use requests::*;
pub use responses::*;

use super::super::common::service_description::ServiceDescription;
use super::super::common::Result;

#[async_trait]
pub trait Backend {
    async fn browse(&self, browse: &Browse) -> Result<BrowseResponse>;

    async fn get_search_capabilities(
        &self,
        get_search_capabilities: &GetSearchCapabilities,
    ) -> Result<GetSearchCapabilitiesResponse>;

    async fn get_sort_capabilities(
        &self,
        get_sort_capabilities: &GetSortCapabilities,
    ) -> Result<GetSortCapabilitiesResponse>;
}

#[derive(Clone)]
pub struct ContentDirectory {
    backend: Arc<dyn Backend + Send + Sync>,
}

impl ContentDirectory {
    pub fn new(backend: Arc<dyn Backend + Send + Sync>) -> Self {
        Self { backend }
    }

    #[allow(clippy::unused_self)]
    pub fn describe(&self) -> ServiceDescription {
        description::description()
    }

    pub async fn control(&self, envelope: &Envelope) -> EnvelopeResponse {
        let result = match envelope.body {
            Body::Browse(ref browse) => self
                .backend
                .browse(browse)
                .await
                .map(BodyResponse::BrowseResponse),
            Body::GetSearchCapabilities(ref get_search_capabilities) => self
                .backend
                .get_search_capabilities(get_search_capabilities)
                .await
                .map(BodyResponse::GetSearchCapabilitiesResponse),
            Body::GetSortCapabilities(ref get_sort_capabilities) => self
                .backend
                .get_sort_capabilities(get_sort_capabilities)
                .await
                .map(BodyResponse::GetSortCapabilitiesResponse),
        };

        let body_response = result.unwrap_or_else(|err| {
            error!("Action failed: {}", &err);

            BodyResponse::Fault(Fault {
                fault_code: "s:Client".into(),
                fault_string: "UPnPError".into(),
                detail: FaultDetail {
                    value: FaultDetailValue::UPnPError {
                        upnp_error: UPnPError {
                            error_code: UPNP_ERROR_CODE_ACTION_FAILED,
                            error_description: Some(err.to_string()),
                        },
                    },
                },
            })
        });

        EnvelopeResponse {
            encoding_style: Some("http://schemas.xmlsoap.org/soap/encoding/".into()),
            body: body_response,
        }
    }
}

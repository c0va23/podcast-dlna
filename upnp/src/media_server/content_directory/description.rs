use super::super::super::common::service_description::{
    Action, ActionList, AllowedValueList, Argument, ArgumentDirection, ArgumentList, DataType,
    DataValue, ServiceDescription, ServiceStateTable, SpecVersion, StateVariable,
};

#[allow(non_upper_case_globals)]
const A_ARG_TYPE_ObjectID: &str = "A_ARG_TYPE_ObjectID";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_Result: &str = "A_ARG_TYPE_Result";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_BrowseFlag: &str = "A_ARG_TYPE_BrowseFlag";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_Filter: &str = "A_ARG_TYPE_Filter";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_SortCriteria: &str = "A_ARG_TYPE_SortCriteria";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_Index: &str = "A_ARG_TYPE_Index";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_Count: &str = "A_ARG_TYPE_Count";
#[allow(non_upper_case_globals)]
const A_ARG_TYPE_UpdateID: &str = "A_ARG_TYPE_UpdateID";
#[allow(non_upper_case_globals)]
const SearchCapabilities: &str = "SearchCapabilities";
#[allow(non_upper_case_globals)]
const SortCapabilities: &str = "SortCapabilities";
#[allow(non_upper_case_globals)]
const SystemUpdateID: &str = "SystemUpdateID";

#[allow(clippy::too_many_lines)]
pub fn description() -> ServiceDescription {
    ServiceDescription {
        spec_version: SpecVersion::new(1, 0),
        config_id: 0,
        action_list: ActionList {
            actions: vec![
                Action {
                    name: "GetSearchCapabilities".into(),
                    argument_list: ArgumentList {
                        arguments: vec![Argument {
                            name: "SearchCaps".into(),
                            direction: ArgumentDirection::Out,
                            related_state_variable: SearchCapabilities.into(),
                        }],
                    },
                },
                Action {
                    name: "GetSortCapabilities".into(),
                    argument_list: ArgumentList {
                        arguments: vec![Argument {
                            name: "SortCaps".into(),
                            direction: ArgumentDirection::Out,
                            related_state_variable: SortCapabilities.into(),
                        }],
                    },
                },
                Action {
                    name: "GetSystemUpdateID".into(),
                    argument_list: ArgumentList {
                        arguments: vec![Argument {
                            name: "Id".into(),
                            direction: ArgumentDirection::Out,
                            related_state_variable: SystemUpdateID.into(),
                        }],
                    },
                },
                Action {
                    name: "Browse".into(),
                    argument_list: ArgumentList {
                        arguments: vec![
                            Argument {
                                name: "ObjectID".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_ObjectID.into(),
                            },
                            Argument {
                                name: "BrowseFlag".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_BrowseFlag.into(),
                            },
                            Argument {
                                name: "Filter".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_Filter.into(),
                            },
                            Argument {
                                name: "StartingIndex".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_Index.into(),
                            },
                            Argument {
                                name: "RequestedCount".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_Count.into(),
                            },
                            Argument {
                                name: "SortCriteria".into(),
                                direction: ArgumentDirection::In,
                                related_state_variable: A_ARG_TYPE_SortCriteria.into(),
                            },
                            Argument {
                                name: "Result".into(),
                                direction: ArgumentDirection::Out,
                                related_state_variable: A_ARG_TYPE_Result.into(),
                            },
                            Argument {
                                name: "NumberReturned".into(),
                                direction: ArgumentDirection::Out,
                                related_state_variable: A_ARG_TYPE_Count.into(),
                            },
                            Argument {
                                name: "TotalMatches".into(),
                                direction: ArgumentDirection::Out,
                                related_state_variable: A_ARG_TYPE_Count.into(),
                            },
                            Argument {
                                name: "UpdateID".into(),
                                direction: ArgumentDirection::Out,
                                related_state_variable: A_ARG_TYPE_UpdateID.into(),
                            },
                        ],
                    },
                },
            ],
        },
        service_state_table: ServiceStateTable {
            state_variables: vec![
                StateVariable {
                    name: A_ARG_TYPE_ObjectID.into(),
                    data_type: DataType::String,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_Result.into(),
                    data_type: DataType::String,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_BrowseFlag.into(),
                    data_type: DataType::String,
                    allowed_value_list: Some(AllowedValueList {
                        allowed_values: vec![
                            "BrowseMetadata".into(),
                            "BrowseDirectChildren".into(),
                        ],
                    }),
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_SortCriteria.into(),
                    data_type: DataType::String,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_Index.into(),
                    data_type: DataType::Ui4,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_Count.into(),
                    data_type: DataType::Ui4,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_UpdateID.into(),
                    data_type: DataType::Ui4,
                    ..StateVariable::default()
                },
                StateVariable {
                    name: SearchCapabilities.into(),
                    data_type: DataType::String,
                    // Not support search
                    default_value: Some(DataValue::String("".into())),
                    ..StateVariable::default()
                },
                StateVariable {
                    name: SortCapabilities.into(),
                    data_type: DataType::String,
                    // Not support sort
                    default_value: Some(DataValue::String("".into())),
                    ..StateVariable::default()
                },
                StateVariable {
                    name: SystemUpdateID.into(),
                    data_type: DataType::Ui4,
                    // TODO: Return real value. Maybe cab be returned unix time.
                    ..StateVariable::default()
                },
                StateVariable {
                    name: A_ARG_TYPE_Filter.into(),
                    data_type: DataType::String,
                    ..StateVariable::default()
                },
            ],
        },
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn check_description_action_related_state_variable() {
        let service_description = super::description();
        for action in service_description.action_list.actions {
            for argument in action.argument_list.arguments {
                let exists = service_description
                    .service_state_table
                    .state_variables
                    .iter()
                    .any(|state_variable| state_variable.name == argument.related_state_variable);

                assert!(
                    exists,
                    "action state variable '{}' not found",
                    argument.related_state_variable,
                );
            }
        }
    }

    #[test]
    fn check_description_state_variable_usage() {
        let service_description = super::description();

        let related_state_variables = service_description
            .action_list
            .actions
            .iter()
            .flat_map(|action| {
                action
                    .argument_list
                    .arguments
                    .iter()
                    .map(|argument| argument.related_state_variable.as_str())
            })
            .collect::<Vec<&str>>();

        for state_variable in service_description.service_state_table.state_variables {
            let exists =
                related_state_variables
                    .clone()
                    .into_iter()
                    .any(|related_state_variable| {
                        related_state_variable == state_variable.name.as_str()
                    });

            assert!(exists, "State variable '{}' not used", state_variable.name);
        }
    }
}

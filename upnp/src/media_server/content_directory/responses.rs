use yaserde::YaSerialize;

use std::io::Write;

// TODO: return namespace
#[derive(YaSerialize, Debug)]
#[yaserde(
    rename = "BrowseResponse",
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct BrowseResponse {
    #[yaserde(rename = "Result")]
    pub result: String,

    #[yaserde(rename = "NumberReturned")]
    pub number_returned: u32,

    #[yaserde(rename = "TotalMatches")]
    pub total_matches: u32,

    #[yaserde(rename = "UpdateID")]
    pub update_id: u32,
}

#[derive(YaSerialize, Debug)]
#[yaserde(
    rename = "GetSearchCapabilitiesResponse",
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct GetSearchCapabilitiesResponse {
    #[yaserde(rename = "SearchCaps")]
    pub search_caps: String,
}

#[derive(YaSerialize, Debug)]
#[yaserde(
    rename = "GetSortCapabilitiesResponse",
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct GetSortCapabilitiesResponse {
    #[yaserde(rename = "SortCaps")]
    pub sort_caps: String,
}

#[derive(YaSerialize, Debug)]
#[yaserde(rename = "errorCode")]
pub struct ErrorCode {
    value: usize,
}

pub const UPNP_ERROR_CODE_ACTION_FAILED: ErrorCode = ErrorCode { value: 501 };

#[derive(YaSerialize, Debug)]
#[yaserde(namespace = "urn:schemas-upnp-org:control-1-0")]
pub struct UPnPError {
    #[yaserde(rename = "errorCode")]
    pub error_code: ErrorCode,

    #[yaserde(rename = "errorDescription")]
    pub error_description: Option<String>,
}

#[derive(YaSerialize, Debug)]
pub enum FaultDetailValue {
    UPnPError {
        #[yaserde(rename = "UPnPError")]
        upnp_error: UPnPError,
    },
}

#[derive(YaSerialize, Debug)]
pub struct FaultDetail {
    #[yaserde(flatten)]
    pub value: FaultDetailValue,
}

#[derive(YaSerialize, Debug)]
pub struct Fault {
    #[yaserde(rename = "faultCode")]
    pub fault_code: String,

    #[yaserde(rename = "faultString")]
    pub fault_string: String,

    #[yaserde(rename = "detail")]
    pub detail: FaultDetail,
}

#[derive(Debug)]
pub enum BodyResponse {
    BrowseResponse(BrowseResponse),
    GetSearchCapabilitiesResponse(GetSearchCapabilitiesResponse),
    GetSortCapabilitiesResponse(GetSortCapabilitiesResponse),
    Fault(Fault),
}

impl BodyResponse {
    pub fn kind(&self) -> String {
        match self {
            Self::BrowseResponse(_) => "BrowseResponse".to_string(),
            Self::GetSearchCapabilitiesResponse(_) => "GetSearchCapabilitiesResponse".to_string(),
            Self::GetSortCapabilitiesResponse(_) => "GetSortCapabilitiesResponse".to_string(),
            Self::Fault(fault) => format!("Fault({})", fault.fault_string),
        }
    }
}

impl YaSerialize for BodyResponse {
    fn serialize<W: Write>(
        &self,
        writer: &mut yaserde::ser::Serializer<W>,
    ) -> Result<(), std::string::String> {
        let body_element_name = writer
            .get_start_event_name()
            .unwrap_or_else(|| "s:Body".into());
        let body_element_start = xml::writer::XmlEvent::start_element(body_element_name.as_ref());
        writer
            .write(body_element_start)
            .map_err(|e| e.to_string())?;

        match self {
            Self::BrowseResponse(browse_response) => {
                writer.set_start_event_name(Some("u:BrowseResponse".to_owned()));
                browse_response.serialize(writer)?;
            }
            Self::GetSearchCapabilitiesResponse(get_search_capabilities_resposne) => {
                writer.set_start_event_name(Some("u:GetSearchCapabilitiesResponse".to_owned()));
                get_search_capabilities_resposne.serialize(writer)?;
            }
            Self::GetSortCapabilitiesResponse(get_sort_capabilities_response) => {
                writer.set_start_event_name(Some("u:GetSortCapabilitiesResponse".to_owned()));
                get_sort_capabilities_response.serialize(writer)?;
            }
            Self::Fault(fault) => {
                writer.set_start_event_name(Some("s:Fault".to_owned()));
                fault.serialize(writer)?;
            }
        }

        let body_element_end = xml::writer::XmlEvent::end_element();
        writer.write(body_element_end).map_err(|e| e.to_string())?;

        Ok(())
    }

    fn serialize_attributes(
        &self,
        attributes: Vec<xml::attribute::OwnedAttribute>,
        namespace: xml::namespace::Namespace,
    ) -> Result<
        (
            Vec<xml::attribute::OwnedAttribute>,
            xml::namespace::Namespace,
        ),
        std::string::String,
    > {
        Ok((attributes, namespace))
    }
}

#[derive(YaSerialize, Debug)]
#[yaserde(
    prefix = "s",
    namespace = "s: http://schemas.xmlsoap.org/soap/envelope/",
    rename = "Envelope"
)]
pub struct EnvelopeResponse {
    #[yaserde(prefix = "s", rename = "Body")]
    pub body: BodyResponse,

    #[yaserde(rename = "encodingStyle", prefix = "s", attribute)]
    pub encoding_style: Option<String>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use yaserde::ser::to_string_with_config;
    use yaserde::ser::Config;

    fn serialize_config() -> Config {
        Config {
            perform_indent: true,
            indent_string: Some("    ".into()),
            ..Config::default()
        }
    }

    #[test]
    fn broswer_response_serialization() {
        let broswer_response_envelop = EnvelopeResponse {
            encoding_style: Some("http://schemas.xmlsoap.org/soap/encoding/".into()),
            body: BodyResponse::BrowseResponse(BrowseResponse {
                result: "simple result".into(),
                total_matches: 0,
                update_id: 0,
                number_returned: 0,
            }),
        };

        let xml = to_string_with_config(&broswer_response_envelop, &serialize_config()).unwrap();
        let expected_xml = include_str!("examples/simple_browse_response_envelop.xml").trim();
        assert_eq!(expected_xml, xml);
    }

    #[test]
    fn broswer_failuer_response_serialization() {
        let broswer_response_envelop = EnvelopeResponse {
            encoding_style: Some("http://schemas.xmlsoap.org/soap/encoding/".into()),
            body: BodyResponse::Fault(Fault {
                fault_string: "UPnPError".into(),
                fault_code: "s:Client".into(),
                detail: FaultDetail {
                    value: FaultDetailValue::UPnPError {
                        upnp_error: UPnPError {
                            error_code: UPNP_ERROR_CODE_ACTION_FAILED,
                            error_description: Some("timeout".into()),
                        },
                    },
                },
            }),
        };

        let xml = to_string_with_config(&broswer_response_envelop, &serialize_config()).unwrap();
        let expected_xml = include_str!("examples/error_response_envelop.xml").trim();
        assert_eq!(expected_xml, xml);
    }
}

pub const BROWSE_METADATA: &str = "BrowseMetadata";
pub const BROWSE_DIRECT_CHILDREN: &str = "BrowseDirectChildren";

#[derive(YaDeserialize, YaSerialize, Debug, PartialEq, Default)]
#[yaserde(
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct Browse {
    #[yaserde(rename = "ObjectID")]
    pub object_id: String,

    #[yaserde(rename = "BrowseFlag")]
    pub browse_flag: String,

    #[yaserde(rename = "Filter")]
    pub filter: String,

    #[yaserde(rename = "StartingIndex")]
    pub starting_index: u64,

    #[yaserde(rename = "RequestedCount")]
    pub requested_count: u64,

    #[yaserde(rename = "SortCriteria")]
    pub sort_criteria: String,
}

#[derive(YaDeserialize, YaSerialize, Debug, PartialEq)]
#[yaserde(
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct GetSearchCapabilities {}

#[derive(YaDeserialize, YaSerialize, Debug, PartialEq)]
#[yaserde(
    prefix = "u",
    namespace = "u: urn:schemas-upnp-org:service:ContentDirectory:1"
)]
pub struct GetSortCapabilities {}

#[derive(YaSerialize, YaDeserialize, Debug, PartialEq)]
#[yaserde(
    prefix = "s",
    namespace = "s: http://schemas.xmlsoap.org/soap/envelope/"
)]
pub enum Body {
    #[yaserde(rename = "Browse", prefix = "u")]
    Browse(Browse),

    #[yaserde(rename = "GetSearchCapabilities", prefix = "u")]
    GetSearchCapabilities(GetSearchCapabilities),

    #[yaserde(rename = "GetSortCapabilities", prefix = "u")]
    GetSortCapabilities(GetSortCapabilities),
}

impl Default for Body {
    fn default() -> Self {
        Self::Browse(Browse::default())
    }
}

#[derive(YaDeserialize, YaSerialize, Debug, PartialEq)]
#[yaserde(
    prefix = "s",
    namespace = "s: http://schemas.xmlsoap.org/soap/envelope/"
)]
pub struct Envelope {
    #[yaserde(prefix = "s", rename = "Body")]
    pub body: Body,

    #[yaserde(rename = "encodingStyle", prefix = "s", attribute)]
    pub encoding_style: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_browse_action() {
        let request_body = r#"
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
    s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
>
    <s:Body><u:Browse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
    <ObjectID>0</ObjectID>
    <BrowseFlag>BrowseDirectChildren</BrowseFlag>
    <Filter>*</Filter>
    <StartingIndex>0</StartingIndex>
    <RequestedCount>5000</RequestedCount>
    <SortCriteria></SortCriteria>
    </u:Browse>
    </s:Body>
    </s:Envelope>
"#;
        let envelop = yaserde::de::from_str::<Envelope>(request_body);
        let expected_envelop = Envelope {
            encoding_style: "http://schemas.xmlsoap.org/soap/encoding/".into(),
            body: Body::Browse(Browse {
                object_id: "0".into(),
                browse_flag: "BrowseDirectChildren".into(),
                filter: "*".into(),
                starting_index: 0,
                requested_count: 5000,
                sort_criteria: "".into(),
            }),
        };
        eprintln!(
            "Expected envelop \n{:?}",
            yaserde::ser::to_string_with_config(
                &expected_envelop,
                &yaserde::ser::Config {
                    indent_string: Some("  \n".into()),
                    perform_indent: true,
                    write_document_declaration: false,
                },
            ),
        );
        assert_eq!(expected_envelop, envelop.unwrap());
    }

    #[test]
    fn test_search_capabilities_action() {
        let request_body = r#"
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
    s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
>
    <s:Body>
        <u:GetSearchCapabilities xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
        </u:GetSearchCapabilities>
    </s:Body>
    </s:Envelope>
"#;
        let envelop = yaserde::de::from_str::<Envelope>(request_body);
        let expected_envelop = Envelope {
            encoding_style: "http://schemas.xmlsoap.org/soap/encoding/".into(),
            body: Body::GetSearchCapabilities(GetSearchCapabilities {}),
        };
        eprintln!(
            "Expected envelop \n{:?}",
            yaserde::ser::to_string_with_config(
                &expected_envelop,
                &yaserde::ser::Config {
                    indent_string: Some("  \n".into()),
                    perform_indent: true,
                    write_document_declaration: false,
                },
            ),
        );
        assert_eq!(expected_envelop, envelop.unwrap());
    }

    #[test]
    fn test_get_sort_capabilities_action() {
        let request_body = r#"
<?xml version="1.0"?>
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
    s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
>
    <s:Body>
        <u:GetSortCapabilities xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
        </u:GetSortCapabilities>
    </s:Body>
</s:Envelope>
"#;
        let envelop = yaserde::de::from_str::<Envelope>(request_body);
        let expected_envelop = Envelope {
            encoding_style: "http://schemas.xmlsoap.org/soap/encoding/".into(),
            body: Body::GetSortCapabilities(GetSortCapabilities {}),
        };
        eprintln!(
            "Expected envelop \n{:?}",
            yaserde::ser::to_string_with_config(
                &expected_envelop,
                &yaserde::ser::Config {
                    indent_string: Some("  \n".into()),
                    perform_indent: true,
                    write_document_declaration: false,
                },
            ),
        );
        assert_eq!(expected_envelop, envelop.unwrap());
    }
}

use super::super::common::service_description::{
    ActionList, ServiceDescription, ServiceStateTable, SpecVersion,
};

pub struct ConnectionManager {}

impl ConnectionManager {
    pub const fn describe() -> ServiceDescription {
        ServiceDescription {
            spec_version: SpecVersion::new(1, 0),
            config_id: 0,
            action_list: ActionList { actions: vec![] },
            service_state_table: ServiceStateTable {
                state_variables: vec![],
            },
        }
    }
}

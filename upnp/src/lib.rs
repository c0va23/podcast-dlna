#[macro_use]
extern crate log;
#[macro_use]
extern crate yaserde_derive;

pub mod common;
pub mod didl;
pub mod media_server;

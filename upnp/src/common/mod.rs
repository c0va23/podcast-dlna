pub mod device_description;
pub mod service_description;

pub type Error = anyhow::Error;
pub type Result<T> = anyhow::Result<T, Error>;

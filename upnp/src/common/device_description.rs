use yaserde::YaSerialize;

use std::fmt::{Display, Formatter};
use std::io::Write;

#[derive(YaSerialize)]
pub struct SpecVersion {
    major: u8,
    minor: u8,
}

impl SpecVersion {
    pub const fn new(major: u8, minor: u8) -> Self {
        Self { major, minor }
    }
}

pub struct DeviceType {
    device_type: String,
    version: u8,
}

impl DeviceType {
    pub const fn new(device_type: String, version: u8) -> Self {
        Self {
            device_type,
            version,
        }
    }
}

impl Display for DeviceType {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            formatter,
            "urn:schemas-upnp-org:device:{}:{}",
            self.device_type, self.version,
        )
    }
}

impl YaSerialize for DeviceType {
    fn serialize<W: Write>(&self, writer: &mut yaserde::ser::Serializer<W>) -> Result<(), String> {
        writer
            .write(xml::writer::XmlEvent::start_element("deviceType"))
            .map_err(|err| err.to_string())?;

        writer
            .write(format!("{}", self).as_ref())
            .map_err(|err| err.to_string())?;

        writer
            .write(xml::writer::XmlEvent::end_element())
            .map_err(|err| err.to_string())?;

        Ok(())
    }

    fn serialize_attributes(
        &self,
        attributes: Vec<xml::attribute::OwnedAttribute>,
        namespace: xml::namespace::Namespace,
    ) -> Result<
        (
            Vec<xml::attribute::OwnedAttribute>,
            xml::namespace::Namespace,
        ),
        String,
    > {
        Ok((attributes, namespace))
    }
}

pub struct Udn {
    uuid: uuid::Uuid,
}

impl Udn {
    pub const fn new(uuid: uuid::Uuid) -> Self {
        Self { uuid }
    }
}

impl Display for Udn {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "uuid:{}", self.uuid.to_hyphenated())
    }
}

impl YaSerialize for Udn {
    fn serialize<W: Write>(&self, writer: &mut yaserde::ser::Serializer<W>) -> Result<(), String> {
        writer
            .write(xml::writer::XmlEvent::start_element("UDN"))
            .map_err(|err| err.to_string())?;

        writer
            .write(format!("{}", self).as_ref())
            .map_err(|err| err.to_string())?;

        writer
            .write(xml::writer::XmlEvent::end_element())
            .map_err(|err| err.to_string())?;

        Ok(())
    }

    fn serialize_attributes(
        &self,
        attributes: Vec<xml::attribute::OwnedAttribute>,
        namespace: xml::namespace::Namespace,
    ) -> Result<
        (
            Vec<xml::attribute::OwnedAttribute>,
            xml::namespace::Namespace,
        ),
        String,
    > {
        Ok((attributes, namespace))
    }
}

#[derive(YaSerialize)]
pub struct Service {
    #[yaserde(rename = "serviceType")]
    pub service_type: String,

    #[yaserde(rename = "serviceId")]
    pub service_id: String,

    #[yaserde(rename = "SCPDURL")]
    pub scpd_url: String,

    #[yaserde(rename = "controlURL")]
    pub control_url: String,

    #[yaserde(rename = "eventSubURL")]
    pub event_sub_url: String,
}

#[derive(YaSerialize)]
pub struct ServiceList {
    #[yaserde(rename = "service")]
    pub services: Vec<Service>,
}

#[derive(YaSerialize)]
#[yaserde(rename = "device")]
pub struct EmbeddedDevice {}

#[derive(YaSerialize)]
pub struct Icon {
    pub mimetype: String,
    pub width: u64,
    pub height: u64,
    pub depth: u64,
    pub url: String,
}

#[derive(YaSerialize)]
pub struct IconList {
    #[yaserde(rename = "icon")]
    pub icons: Vec<Icon>,
}

#[derive(YaSerialize)]
pub struct Device {
    #[yaserde(rename = "deviceType")]
    pub device_type: DeviceType,

    #[yaserde(rename = "friendlyName")]
    pub friendly_name: String,

    #[yaserde(rename = "manufacturer")]
    pub manufacturer: String,

    #[yaserde(rename = "manufacturerURL")]
    pub manufacturer_url: Option<String>,

    #[yaserde(rename = "modelDescription")]
    pub model_description: Option<String>,

    #[yaserde(rename = "modelName")]
    pub model_name: String,

    #[yaserde(rename = "modelNumber")]
    pub model_number: Option<String>,

    #[yaserde(rename = "modelURL")]
    pub model_url: Option<String>,

    #[yaserde(rename = "serialNumber")]
    pub serial_number: Option<String>,

    #[yaserde(rename = "UDN")]
    pub udn: Udn,

    // Universal Product Code
    #[yaserde(rename = "UPC")]
    pub upc: Option<String>,

    #[yaserde(rename = "iconList")]
    pub icon_list: IconList,

    #[yaserde(rename = "serviceList")]
    pub service_list: ServiceList,

    // URL for presentation
    #[yaserde(rename = "presentationURL")]
    pub presentation_url: Option<String>,

    #[yaserde(rename = "deviceList")]
    pub device_list: Vec<EmbeddedDevice>,
}

#[derive(YaSerialize)]
#[yaserde(rename = "root", namespace = "urn:schemas-upnp-org:device-1-0")]
pub struct DeviceDescription {
    #[yaserde(rename = "specVersion")]
    pub spec_version: SpecVersion,

    #[yaserde(rename = "configId")]
    #[yaserde(attribute)]
    pub config_id: usize,

    pub device: Device,
}

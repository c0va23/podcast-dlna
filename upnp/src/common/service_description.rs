#[derive(YaSerialize)]
pub struct SpecVersion {
    pub major: u8,
    pub minor: u8,
}

impl SpecVersion {
    pub const fn new(major: u8, minor: u8) -> Self {
        Self { major, minor }
    }
}

#[derive(YaSerialize)]
pub enum ArgumentDirection {
    #[yaserde(rename = "in")]
    In,
    #[yaserde(rename = "out")]
    Out,
}

#[derive(YaSerialize)]
pub struct Argument {
    pub name: String,

    pub direction: ArgumentDirection,

    #[yaserde(rename = "relatedStateVariable")]
    pub related_state_variable: String,
}

#[derive(YaSerialize)]
pub struct ArgumentList {
    #[yaserde(rename = "argument")]
    pub arguments: Vec<Argument>,
}

#[derive(YaSerialize)]
pub struct Action {
    pub name: String,

    #[yaserde(rename = "argumentList")]
    pub argument_list: ArgumentList,
}

#[derive(YaSerialize)]
pub struct ActionList {
    #[yaserde(rename = "action")]
    pub actions: Vec<Action>,
}

#[derive(YaSerialize)]
pub enum StateVariableFlag {
    #[allow(dead_code)]
    #[yaserde(rename = "yes")]
    Yes,

    #[yaserde(rename = "no")]
    No,
}

#[derive(YaSerialize)]
pub enum DataType {
    #[yaserde(rename = "string")]
    String,

    #[yaserde(rename = "ui4")]
    Ui4,

    #[allow(dead_code)]
    #[yaserde(rename = "number")]
    Number,
}

#[derive(YaSerialize)]
pub enum DataValue {
    String(String),

    #[allow(dead_code)]
    Ui4(u32),

    #[allow(dead_code)]
    Number(f64),
}

#[derive(YaSerialize)]
pub struct AllowedValueList {
    #[yaserde(rename = "allowedValue")]
    pub allowed_values: Vec<String>,
}

#[derive(YaSerialize)]
pub struct StateVariable {
    #[yaserde(rename = "sendEvents")]
    #[yaserde(attribute)]
    pub send_events: StateVariableFlag,

    #[yaserde(attribute)]
    pub multicast: StateVariableFlag,

    pub name: String,

    #[yaserde(rename = "dataType")]
    pub data_type: DataType,

    #[yaserde(rename = "defaultValue")]
    pub default_value: Option<DataValue>,

    #[yaserde(rename = "allowedValueList")]
    pub allowed_value_list: Option<AllowedValueList>,
}

impl Default for StateVariable {
    fn default() -> Self {
        Self {
            name: "".into(),
            multicast: StateVariableFlag::No,
            send_events: StateVariableFlag::No,
            data_type: DataType::String,
            default_value: None,
            allowed_value_list: None,
        }
    }
}

#[derive(YaSerialize)]
pub struct ServiceStateTable {
    #[yaserde(rename = "stateVariable")]
    pub state_variables: Vec<StateVariable>,
}

#[derive(YaSerialize)]
#[yaserde(rename = "scpd", namespace = "urn:schemas-upnp-org:service-1-0")]
pub struct ServiceDescription {
    #[yaserde(rename = "specVersion")]
    pub spec_version: SpecVersion,

    #[yaserde(rename = "configId")]
    #[yaserde(attribute)]
    pub config_id: usize,

    #[yaserde(rename = "actionList")]
    pub action_list: ActionList,

    #[yaserde(rename = "serviceStateTable")]
    pub service_state_table: ServiceStateTable,
}

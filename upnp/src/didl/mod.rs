use std::io::Write;

#[derive(YaSerialize, Debug, PartialEq)]
pub enum TransportType {
    HttpGet,
}

impl std::fmt::Display for TransportType {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::HttpGet => write!(fmt, "http-get"),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct ProtocolInfo {
    pub transport_type: TransportType,
    pub content_type: String,
}

impl std::fmt::Display for ProtocolInfo {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(fmt, "{}:*:{}:*", self.transport_type, self.content_type)
    }
}

impl yaserde::YaSerialize for ProtocolInfo {
    fn serialize<W: Write>(
        &self,
        writer: &mut yaserde::ser::Serializer<W>,
    ) -> Result<(), std::string::String> {
        writer
            .write(xml::writer::XmlEvent::characters(self.to_string().as_ref()))
            .map_err(|err| err.to_string())?;
        Ok(())
    }

    fn serialize_attributes(
        &self,
        attributes: Vec<xml::attribute::OwnedAttribute>,
        namespace: xml::namespace::Namespace,
    ) -> Result<
        (
            Vec<xml::attribute::OwnedAttribute>,
            xml::namespace::Namespace,
        ),
        std::string::String,
    > {
        Ok((attributes, namespace))
    }
}

/*
#[derive(YaSerialize, Debug, PartialEq)]
pub struct Duration {
    duration: std::time::Duration,
}
*/

#[derive(YaSerialize, Debug, PartialEq)]
pub struct Resource {
    #[yaserde(rename = "protocolInfo", attribute)]
    pub protocol_info: ProtocolInfo,

    #[yaserde(rename = "duration", attribute)]
    pub duration: Option<String>,

    #[yaserde(rename = "size", attribute)]
    pub size: Option<usize>,

    #[yaserde(text)]
    pub uri: String,
}

#[derive(YaSerialize, Debug, PartialEq)]
pub struct Item {
    #[yaserde(rename = "id", attribute)]
    pub id: String,

    #[yaserde(rename = "parentID", attribute)]
    pub parent_id: String,

    #[yaserde(rename = "restricted", attribute)]
    pub restricted: String,

    #[yaserde(rename = "title", prefix = "dc")]
    pub title: String,

    #[yaserde(rename = "date", prefix = "dc")]
    pub date: Option<String>,

    #[yaserde(rename = "creator", prefix = "dc")]
    pub creator: Option<String>,

    #[yaserde(rename = "artist", prefix = "upnp")]
    pub artist: Option<String>,

    // TODO: replace to enum type
    #[yaserde(rename = "class", prefix = "upnp")]
    pub class: String,

    #[yaserde(rename = "album", prefix = "upnp")]
    pub album: Option<String>,

    #[yaserde(rename = "res")]
    pub resource: Resource,

    #[yaserde(rename = "albumArtURI", prefix = "upnp")]
    pub album_art_uri: Option<String>,

    #[yaserde(rename = "description", prefix = "dc")]
    pub description: Option<String>,

    #[yaserde(rename = "longDescription", prefix = "upnp")]
    pub long_description: Option<String>,
}

#[derive(YaSerialize, Debug, PartialEq)]
pub struct Container {
    #[yaserde(rename = "id", attribute)]
    pub id: String,

    #[yaserde(rename = "parentID", attribute)]
    pub parent_id: String,

    #[yaserde(rename = "restricted", attribute)]
    pub restricted: String,

    #[yaserde(rename = "title", prefix = "dc")]
    pub title: String,

    // TODO: replace to enum type
    #[yaserde(rename = "class", prefix = "upnp")]
    pub class: String,

    #[yaserde(rename = "storageUsed", prefix = "upnp")]
    pub storage_used: i64,

    #[yaserde(rename = "childCount", attribute)]
    pub child_count: Option<i64>,

    #[yaserde(rename = "searchable", attribute)]
    pub searchable: Option<String>,

    #[yaserde(rename = "albumArtURI", prefix = "upnp")]
    pub album_art_uri: Option<String>,
}

#[derive(YaSerialize, Debug, PartialEq)]
#[yaserde(
    namespace = "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/",
    namespace = "dc: http://purl.org/dc/elements/1.1/",
    namespace = "upnp: urn:schemas-upnp-org:metadata-1-0/upnp/",
    namespace = "dlna: urn:schemas-dlna-org:metadata-1-0",
    rename = "DIDL-Lite"
)]
#[allow(clippy::module_name_repetitions)]
pub struct DidlLite {
    #[yaserde(rename = "item")]
    pub items: Vec<Item>,

    #[yaserde(rename = "container")]
    pub containers: Vec<Container>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_serialize() {
        let item = Item {
            id: "1".into(),
            parent_id: "-1".into(),
            restricted: "1".into(),
            title: "Example".into(),
            date: Some("2020-08-22".into()),
            creator: None,
            artist: None,
            album: None,
            album_art_uri: None,
            description: None,
            long_description: None,
            resource: Resource {
                protocol_info: ProtocolInfo {
                    transport_type: TransportType::HttpGet,
                    content_type: "audio/mp3".into(),
                },
                duration: None,
                size: Some(22000),
                uri: "http://localhost/example.mp3".into(),
            },
            class: "object.item.audioItem.musicTrack".into(),
        };

        let didl_lite = DidlLite {
            items: vec![item],
            containers: vec![],
        };

        let result_xml = yaserde::ser::to_string(&didl_lite).unwrap();
        let expected_xml = include_str!("examples/didl-with-one-item.xml").trim_end();
        assert_eq!(expected_xml, result_xml);
    }
}

export RUST_VERSION ?= 1.56

.PHONE: default
default: lint test

.PHONY: lint
lint: lint-rustfmt lint-editor-config lint-clippy

.PHONY: lint-rustfmt
lint-rustfmt:
	cargo fmt -- --check

.PHONY: lint-clippy
lint-clippy:
	cargo clippy --all-features --all-targets

.PHONY: lint-editor-config
lint-editor-config: bin/ec
	./bin/ec

.PHONE: lint-visibility
lint-visibility:
	./scripts/lint-visibility.sh

.PHONY: test
test:
	cargo test

.PHONY: test-integration
test-integration:
	cargo test -p audio-metadata integration_mp3_duration -- --ignored

check-rust-version:
	./scripts/check-rust-version.sh

include makefiles/cache.mk
include makefiles/e2e.mk
include makefiles/builds.mk
include makefiles/editor-config-checker.mk

.PHONY: env-check
env-check: bin/ec
	uname -a
	./bin/ec --version
	rustc --version

	cargo --version
	cargo clippy --version
	cargo fmt --version
